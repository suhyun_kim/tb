var express = require('express')
		, app = express()
		, http = require('http')
		, server = http.createServer(app)
		, AWS = require('aws-sdk')
		, s3 = new AWS.S3({ 
				region : 'ap-northeast-2'
				, params : {Bucket : 'dongne.image'} 
			})
		, fs = require('fs')
		, path = require('path')
		, multer = require('multer')
		, upload = multer({dest : './TB/tmp/uploadFiles'})
		, mime = require('mime')
		, bodyParser = require('body-parser')//요청을 파싱
		, MD5 = require('./TB/lib/js/plugin/md5')
		, mysql = require('mysql')
		, session = require('express-session')
		, MySQLStore = require('express-mysql-session')(session)
		, cookieParser = require('cookie-parser')
		, DB_select = require('./node_db/db_select')
		, DB_insert = require('./node_db/db_insert')
		, DB_update = require('./node_db/db_update')
		, DB_delete = require('./node_db/db_delete')
		, db_config = require('./node_db/db_config')
		, mysql_info = {}
		, passport = require('passport')
		, LocalStrategy = require('passport-local').Strategy
		, GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
		, _db = require('./node_db/db')
		, fall = require('./node_lib/async_fall')
		, db
		, domain = 'http://towerbridge009.iptime.org/'
		;
	

AWS.config.region = 'ap-northeast-2';

mysql_info.host = '192.168.0.36';
mysql_info.port = 3306;
mysql_info.user = 'root';
mysql_info.password = 'password';


/*
mysql_info.host = 'dongneand.ckglk8lsir8k.ap-northeast-2.rds.amazonaws.com';
mysql_info.port = 3306;
mysql_info.user = 'zalpage';
mysql_info.password = 'setdb5597';
*/

/*=================================
 *모듈 선언
 =================================*/

var signalKey = MD5('TowerBridgePJ');
/*=================================
 *기타변수 선언
 =================================*/

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({extended:false}));

//mysql에 접속하기 위한 정보, db를 먼저 만들어 놔야 한다.
var mysql_store_options = {
	host : mysql_info.host
	, port : mysql_info.port
	, user : mysql_info.user
	, password : mysql_info.password
	, database : 'TB_session'
	, checkExpirationInterval: 1800000//세션 파기 주기
	, expiration: 18000000//세션 유효 기간
	, createDatabaseTable: true //데이터 베이스에 테이블을 생성시킨다.
	, schema : {
		tableName : 'sessions'
		, columnNames : {
			session_id : 'session_id'
			, expires : 'expires'
			, data : 'data'
		}
	}
};
var sessionStore = new MySQLStore(mysql_store_options);
/*=================================
 * 세션 스토어 정의 옵션
 =================================*/

var mysql_connection_info = {
	host : mysql_info.host
	, port :mysql_info.port
	, user : mysql_info.user
	, password : mysql_info.password
	, database : 'TB'
};
/*=================================
 * mysql 접속 정보
 =================================*/

app.use(session({
	key  : 'session_cookie_name'
	, secret : 'TB_session_cookie_secret'
	, store : sessionStore
	, proxy : true //로드밸런싱등에 대비한다. 이것을 안하면 세션간 공유가 안됨.
	, resave : false//세션 값이 변경될 경우 자동 저장 여부.
	, saveUninitialized: true//세션을 실제로 사용하기 전까지는 세션아이디를 발급 안함.
	// , cookie: {
		// secure: true
		// , maxAge : 3000 
	// }
}));
/*=================================
 * 세션 옵션
 =================================*/

//정적파일 접속 하게 변경.
app.use('/TB',express.static(path.join(__dirname, 'TB/')));

app.get('/', function(req, res){
	fs.readFile('./TB/index.html', function(req, data){
		res.contentType('text/html');
		res.end(data);
	});
});
/*==============================================
	root 파일 지정
 ==============================================*/



app.get('/admin', function(req, res){
	//주소가 안맞을 경우 리다이렉트
	if(req.originalUrl == '/admin'){
		res.redirect('/admin/');
		return false;
	}
	
	fs.readFile('./TB/admin/index.html', function(req, data){
		res.contentType('text/html');
		res.end(data);
	});
	
});
/*==============================================
	root 파일 지정
 ==============================================*/

app.use(passport.initialize());//초기화
app.use(passport.session());//인증시 세션사용
/*=================================
 * passport 초기화 와 사용선언
 =================================*/

//고유값인  user의 username을 기준으로 세션을 저장하고 이것을 기준으로 세션을 사용하게 해준다.
passport.serializeUser(function(user, done){
	//console.log('//serializeUser//', user);
	return done(null, user.id);
});
/*=================================
 * passport 고유값 설정
 =================================*/

//처음들어올때 이외에는 이 함수가 실행되고 고유값인 username은 id 인자로 들어온다. 
passport.deserializeUser(function(id, done){
	//console.log('deserializeUser');
	var get_user = new _db(mysql_connection_info);
	
	get_user.select({
		table : 'TB_member'
		, field: 'id,  nickname, member_grade, member_level, gender'
		, where : " WHERE id=binary('"+id+"') "
		, callback : function(user){
			//console.log(user);
			if(user.length){
				return done(null, user[0]);
			}else{
				return done(null, null);
			}
		}
	});
});
/*=================================
 * passport 접속시 인증
 =================================*/

passport.use(new GoogleStrategy({
    clientID: '681068595180-jnlpsra6cnvgka68rild2rhd5qilcu6t.apps.googleusercontent.com',
    clientSecret: '-huMR5A0NX0Jrtwy1cMCwBhe',
    callbackURL: domain+"auth/google/callback"
    , passReqToCallback : true
  },
  function(req, accessToken, refreshToken, profile, done) {
	var ip = req.headers['x-forwarded-for'] ||
							req.connection.remoteAddress ||
							req.socket.remoteAddress ||
							req.connection.socket.remoteAddress
							;

  	//console.log(profile);
  	profile.ip = ip;
  	var mem_db = new _db(mysql_connection_info);
  	var mem = require('./node_db/db/member')(mem_db).join_google(profile, done); 
  	//return done(null, null);
  }
));

app.get('/auth/google', passport.authenticate('google', {
	scope: [ 'profile', 'email' ]
}));

app.get('/auth/google/callback', passport.authenticate('google', {
	successRedirect: domain+'/#!/index',
	failureRedirect: domain+'/#!/login?strategy=google'
}));
/*=================================
 * passport google login
 =================================*/

app.post('/admin/logout', function(req, res, next){
	req.logOut();
	req.session.destroy(function(){
		res.redirect(domain);
	});
});
/*==============================================
	관리자 로그아웃 
 ==============================================*/

app.get('/logout', function(req, res, next){
	req.logOut();
	req.session.destroy(function(){
		res.redirect(domain);
	});
});
/*==============================================
	로그아웃 
 ==============================================*/

app.post('/TB_sw', upload.array('files', 12), function(req, res){
	//req.logOut();
	var send = {
				header:{
					result: 'success'
				}
				,body:{}
			}
			, etcData = {
				login : req.login 
				, logout : req.logout
				, passport : passport
			}
			, files = req.files
			;
	
	var ip = req.headers['x-forwarded-for'] ||
				req.connection.remoteAddress ||
				req.socket.remoteAddress ||
				req.connection.socket.remoteAddress
				;
	//시그널키 검사
	if(req.body.TB_signalKey != signalKey ){
		send.body.TB_dbMessage = '잘못된 접근입니다.. ';
		res.end(JSON.stringify(send));
	}
	else{//시그널 키 일치할 경우 시그널키 삭제.
		delete req.body.TB_signalKey;		
	}
	
	console.log("/*===============================================");
	console.log('/#===== '+req.body.reqType+' ('+req.body.reqQuery+') ========#/');
	
	//insert 쿼리
	if(req.body.reqType === 'insert'){
		db = new DB_insert(mysql_connection_info);
	}else if(req.body.reqType === 'update'||req.body.reqType === 'modify'){//업데이트 쿼리
		db = new DB_update(mysql_connection_info);
	}else if(req.body.reqType === 'select'){//셀렉트 쿼리 
		db = new DB_select(mysql_connection_info);
	}else if(req.body.reqType === 'delete'){//삭제 쿼리
		db = new DB_delete(mysql_connection_info);
	}
	
	//타입 삭제 
	delete req.body.type;
	
	//쿼리 셋팅
	db.setQuery(req, function(data){
		
		//body 넘김.
		if(data.length <= 0){ 
			send.body = null; 
		}else if(data.length ==1){
			send.body = data[0];
		}else{
			send.body = data; 
		}
		
		//클라우드 프론트에서 같은 
		res.header('Cache-Control', 'max-age=0, s-maxage=0, public');
		res.json(send);
		res.end();
		
	}, passport, res);
	
	console.log("===============================================*/");
	
});
/*==============================================
	스위치 파일 지정
 ==============================================*/

http.createServer(app).listen(80);
/*==============================================
	서버 시작
 ==============================================*/

































