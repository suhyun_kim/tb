<?php
class Db {
	private $TB_host;//호스트명
	private $TB_user;//유저명
	private $TB_password;//패스워드
	private $TB_db; //db접속 정보
	protected $TB_link;//TB db접속 정보
	private $TB_query;//쿼리문을 저장할 변수.
	private $TB_result;
	private $TB_data;
	private $TB_dataArr = array();
	
	protected function __construct($host, $user, $password, $db){
		$this->setLogInfo($host, $user, $password, $db);
		$this->connect($this->TB_host,$this->TB_user,$this->TB_password,$this->TB_db);
	}
	
	//내부 변수로 전환하고 입력시 유효성 검사
	protected function setLogInfo($host,$user, $password, $db ){
		$this->TB_host = $host;
		$this->TB_user = $user;
		$this->TB_password = $password;
		$this->TB_db = $db;
	}
	
	/*-- db 접속 --*/
	protected function connect(){
		$this->TB_link = mysqli_connect($this->TB_host,$this->TB_user,$this->TB_password,$this->TB_db);
		if(mysqli_connect_error()){
			exit('Connect Error: '.mysqli_connect_errno().'<br/>'.mysqli_connect_error());
		}else{
			mysqli_autocommit($this->TB_link,false);
		}
	}
	
	/*-- 트랜잭션 처리 --*/
	protected function _commit(){
		$this->TB_result =  mysqli_query($this->TB_link, $this->TB_query);
		if(mysqli_error($this->TB_link)){
			echo "에러발생:".mysqli_errno($this->TB_link).PHP_EOL.mysqli_error($this->TB_link).PHP_EOL.$this->TB_query;
			mysqli_rollback($this->TB_link);
		}else{
			mysqli_commit($this->TB_link);
			return $this->TB_result;
		}
	}
	
	/*- insert문: 기본형 -*/
	protected function _insert($tableName, $keys='', $values=''){
		$this->TB_query = "insert into ".$tableName." (".$keys.") values (".$values.")";
		$this->_commit($this->TB_query);
	}
	
	/*-- 알림 메시지 작성 --*/
	protected function _messageInsert($memo, $receiveDept, $content, $receive_deptDuty=null){
		$now = strftime("%Y-%m-%d %H:%M:%S");
		$_keys = "sender_no, sender_dept, sender, send_time, send_memo, content";
		$_values = decrypt_md5_base64($_SESSION['no'],$GLOBALS['primaryKey']);
		$_values .= "','".decrypt_md5_base64($_SESSION['dept'],$GLOBALS['primaryKey']);
		$_values .= "','".decrypt_md5_base64($_SESSION['name'],$GLOBALS['primaryKey']);
		$_values .= "','$now ";
		$_values .= "','".$memo."','".$content;
		if($receiveDept){//부서 전체 보기 권한.
			$_keys .= ', receive_dept';
			$_values .= "','".$receiveDept;
		}elseif($receive_deptDuty){//해당 업무 담당만 보기.
			$_keys .= ', receive_deptDuty';
			$_values .= "','".$receive_deptDuty;
		}
		$this->_insert("message", $_keys, $_values);
		unset($now); unset($_keys); unset($_values);
	}
	
	/*- select문 : 기본형-*/
	protected function _select($tableName, $where=" ", $field="*"){
		$this->TB_query = "SELECT ".$field." FROM ".$tableName ." ".$where." ";
		$this->_commit($this->TB_query);
		return $this->TB_result;
	}
	
	/*- select문 : 최상위 하나만 전달-*/
	protected function _select_1($tableName, $where=" ", $field="*"){
		$this->TB_query = "SELECT ".$field." FROM ".$tableName ." ".$where." ";
		$this->_commit($this->TB_query);
		$this->TB_data = mysqli_fetch_object($this->TB_result);
		return $this->TB_data;
	}
	
	/*- select문 : 다수의 결과를 배열로 만들어서 전달.-*/
	protected function _select_list($tableName, $where=" ", $field="*"){
		$i = 0;
		$data;
		$list = array();
		$this->TB_query = "SELECT ".$field." FROM ".$tableName ." ".$where." ";
		$this->TB_result = $this->_commit($this->TB_query);
		while($data = mysqli_fetch_object($this->TB_result)){
			$list[$i] = $data;
			$i++;
		}
		unset($i); unset($data);
		return $list;
	}
	
	/*- select문 : json 최상위 하나만 전달-*/
	protected function _select_json_1($tableName, $where=" ", $field="*"){
		$this->TB_query = "SELECT ".$field." FROM ".$tableName ." ".$where." ";
		$this->_commit($this->TB_query);
		$this->TB_data = mysqli_fetch_object($this->TB_result);
		$this->TB_data =  json_encode($this->TB_data);
		return $this->TB_data;
	}

	/*- select문 : json 배열로 전달-*/
	protected function _list_json($tableName, $where=" ", $field="*"){
		$result = array();
		$i = 0;
		$this->TB_query = "SELECT ".$field." FROM ".$tableName ." ".$where." ";
		$this->_commit($this->TB_query);
		while($this->TB_data = mysqli_fetch_object($this->TB_result)){
			$result[$i] = $this->TB_data;
			$i++;
		}
		unset($i);
		return $result;
	}
	
	/*- select문 : 값을 찾아 있는대로 내보낸다. 중복되는 값은 없다.-*/
	protected function _select_json_search($tableName, $where=" ", $field="*"){
		$this->TB_query = "SELECT ".$field." FROM ".$tableName ." ".$where." ";
		$this->_commit($this->TB_query);
		$arrData = array();
		$i = 0;
		while($this->TB_data = mysqli_fetch_object($this->TB_result)){
			// $arrData[$i] =  $this->TB_data;
			// $i++;
			foreach($this->TB_data as $key => $val){
				$arrData[$val] = $val;
			}
		}
		$this->TB_data  = json_encode($arrData);
		return $this->TB_data;
	}
	
	/*- select문 : 값을 찾아 있는대로 내보낸다. -*/
	protected function _select_json_search_2($tableName, $where=" ", $field="*"){
		$this->TB_query = "SELECT ".$field." FROM ".$tableName ." ".$where." ";
		$this->_commit($this->TB_query);
		$arrData = array();
		$i = 0;
		while($this->TB_data = mysqli_fetch_object($this->TB_result)){
			$arrData[$i] =  $this->TB_data;
			$i++;
		}
		$this->TB_data  = json_encode($arrData);
		unset($i); unset($arrData);
		return $this->TB_data;
	}
	
	/*- update: 기본형 -*/
	protected function _update($tableName, $keys_values, $where){
		$this->TB_query = "UPDATE ".$tableName." SET ".$keys_values." WHERE ".$where;
		$this->_commit($this->TB_query);
	}
	
	/*- delete문: 기본형 -*/
	protected function _delete($tableName, $where){
		$this->TB_query = "DELETE FROM  ".$tableName." WHERE ".$where;
		$this->_commit($this->TB_query);
	}
	
	/*- 파일 삭제 -*/
	protected function _fileDelete($filepath){
		if(file_exists($filepath)){
			if(unlink($filepath)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	/*- 문자 입력 될 때 처리해야 하는 것들. -*/
	protected function _inputWord($string){
		$permiTag_1 = "<a><b><blockquote><br><caption><center><code><div><em><embed><font><h1><h2><h3><hr><i>";
		$permiTag_2 = "<img><li><ol><p><pre><q><span><strike><strong><sub><sup><style><table><td><tr><u><ul>";
		$permiTag_3= "<article><section><header><footer><video><iframe>";
		$string = addslashes($string);// 특수 문자 앞에 '\' 을 붙여준다. 출력시는 stripslashes를 해준다.
		$string = trim($string);//앞뒤 문자 공백 제거
		$string = strip_tags($string, $permiTag_1.$permiTag_2.$permiTag_3 );//문자제한
		return $string;
	}
	
	/*- 문자 아웃풋처리 -*/
	protected function _outputWord($string){
		$string = stripslashes ($string);//\제거
		$string= nl2br($string);//<br/>처리
		return $string;
	}
	
	/*- count -*/
	protected function _returnCnt($tableName, $where="WHERE no",$filed="no"){
		$this->TB_query = "SELECT  COUNT(".$filed.") AS ".$filed." FROM ".$tableName ." ".$where." ";
		$this->_commit($this->TB_query);
		$this->TB_data = mysqli_fetch_array($this->TB_result);
		$this->TB_data = (int)$this->TB_data[0];
		return $this->TB_data;
	}
	
	/*- count json 리턴-*/
	protected function _returnCnt_json($tableName, $where="WHERE no",$filed="*"){
		$this->TB_query = "SELECT  COUNT(".$filed.") FROM ".$tableName ." ".$where." ";
		$this->_commit($this->TB_query);
		$this->TB_data = mysqli_fetch_array($this->TB_result);
		$this->TB_data = (int)$this->TB_data[0];
		$this->TB_data =  json_encode($this->TB_data);
		return $this->TB_data;
	}
	
	//결과 전송
	protected function _result ($result, $string=null){
			switch($result){
				case true:
					echo "<div class='TB_result'>success</div>";
					break;
				case false:
					echo "<div class='TB_result'>".$string."</div>";
					break;
			}
			exit;
			unset($this->result);
			unset($this->string);
			unset($this->TB_host);//호스트명
			unset($this->TB_user);//유저명
			unset($this->TB_password);//패스워드
			unset($this->TB_db); //db접속 정보
			unset($this->TB_link);//TB db접속 정보
			unset($this->TB_query);//쿼리문을 저장할 변수.
			unset($this->TB_result);
			unset($this->TB_data);
			unset($this->TB_dataArr);
	}
	
	/*== gcm 보내기 ==*/
	protected function _gcm($scope, $target,$subject, $content){
		$regid = array();
		$query = ''; $tableName = ''; $where = ''; $i= 0;
		$arr_target = array();
		$type_target = gettype($target);
		$searchFiled = '';
		$tableName = 'member';
		$where = "WHERE status='재직중' ";
		//var_dump($scope, $target, $subject, $content);
		
		if(mb_strlen($content) > 60){
			$content = mb_substr($content, 0, 20, 'utf-8');
			$content .= '..';
		}
		
		//var_dump($target);
		switch($scope){
			case 'all'://재직자 전체 메세지
			break; 
			case 'dept'://부서별
				$searchFiled = 'department';
			break; 
			case 'deptDuty'://부서내업무
				$searchFiled = 'deptDuty';				
			break; 
			case 'dutyLevel'://직책레벨별
			break;
			case 'no'://개인별
				$searchFiled = 'no';
			break; 
		}
		
		if($scope!='all'){
			if($type_target =='array'){
				$where .= "AND (";
				foreach ($target as $key => $value) {
					if($key==0) $where .= " $searchFiled='".$value."' ";
					else $where .= "OR $searchFiled='".$value."' ";
				}
			}elseif($type_target=='string') $where .= " AND ($searchFiled='".$target."' ";
			$where .= " ) AND (NOT regid IN ('')) " ;
		}else{
			$where = "WHERE status='재직중' AND NOT regid IN ('')";
		}
		
		$this->TB_query = "SELECT regid FROM ".$tableName." ".$where;
		$this->_commit($this->TB_query);
		//var_dump($this->TB_query);
		while($this->TB_data = mysqli_fetch_object($this->TB_result)){
			if($this->TB_data->{'regid'}&&$this->TB_data->{'regid'}!=''){
				$regid[$i] = $this->TB_data->{'regid'};
				$i++;
			}
		}
		//var_dump($regid);
		if(count($regid)<=0) return false;
		//return FALSE;
		return $this->_sendGCM($regid, $subject, $content);
	}
	
	protected function _sendGCM($regid, $subject, $content){
		$arr   = array();
		$arr['data'] = array();
		$arr['data']['title'] = $subject;
		$arr['data']['message'] = $content;
		$arr['registration_ids'] = array();
		foreach ($regid as $key => $value) {
			$arr['registration_ids'][$key] = $regid[$key];	
		}
		//var_dump($arr['registration_ids']);
		//return false;
		$headers = array(
			'Content-Type:application/json',
			'Authorization:key=AIzaSyBWY3SjYWNS58j1AueLeRL4lirEL4Iu4dg'
		);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,    'https://android.googleapis.com/gcm/send');
		curl_setopt($ch, CURLOPT_HTTPHEADER,  $headers);
		curl_setopt($ch, CURLOPT_POST,    true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($arr));
		$response = curl_exec($ch);
		curl_close($ch);
		
		return json_decode($response);
	}
	/*==/GCM전송==*/
	
	protected function _adminAuth($_adminInfo){
		$type=gettype($_adminInfo);
		$where = '';
		$adminInfo = array();
		if($type === 'string' || $type === 'object'){
			if($type === 'string'){
				$_adminInfo = json_decode(stripcslashes($_adminInfo));
			}
			$adminInfo['id'] = $_adminInfo->id;
			$adminInfo['adminAuthKey'] = $_adminInfo->adminAuthKey;
			$adminInfo['loginTime'] = $_adminInfo->loginTime;
		}elseif($type === 'array'){
			$adminInfo = $_adminInfo;
		}
		
		$where = "WHERE id='".$adminInfo['id']."' ";
		$where .= "AND adminAuthKey='".$adminInfo['adminAuthKey']."' ";
		$where .= "AND loginTime='".$adminInfo['loginTime']."' ";
		$where .= "AND sessionKey='".$_COOKIE['PHPSESSID']."' ";
		//var_dump($where);
		unset($where);unset($type); unset($adminInfo);
		return ($this->_returnCnt('TB_admin', $adminInfo) >0)?true:false;;
	}
	/*===================================
	 * 관리자 계정인지 확인 후 true or false Return
	 * params
	 * 	.$adminInfo(array) = 세션을 사용 안하기 때문에 해당 키를 가져와서 데이터를 비교해 봐야한다.
	 ===================================*/
	 
	 
	/*- 그룹 카운트 -*/
	protected function _groupCnt($tableName, $where = '',$field="", $where_2=''){
		$this->TB_query = "SELECT ".$field.",  COUNT(*)  FROM ".$tableName ." ".$where." GROUP BY ".$field." ".$where_2 ;
		$this->_commit($this->TB_query);
		$returnData = array();
		$i = 0;
		while($this->TB_data = mysqli_fetch_object($this->TB_result)){
			$returnData[$i] = $this->TB_data;
			$i++;
		}
		unset($i); unset($this->TB_data);
		return $returnData;
	}
	
	/*- 그룹 카운트 : Having-*/
	protected function _groupCntHaving($tableName, $field="", $var='', $sign=''){
		$this->TB_query = "SELECT ".$field.",  COUNT(*)  AS ".$var." FROM ".$tableName ." GROUP BY ".$field." ";
		$this->TB_query .= " HAVING ".$var.$sign;
		$this->_commit($this->TB_query);
		$returnData = array();
		$i = 0;
		echo $this->TB_query;
		while($this->TB_data = mysqli_fetch_object($this->TB_result)){
			var_dump($this->TB_data);
			$returnData[$i] = $this->TB_data;
			$i++;
		}
		unset($i); unset($this->TB_data);
		return $returnData;
	}
}





?>