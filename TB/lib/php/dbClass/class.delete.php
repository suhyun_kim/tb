<?php
session_start();
include_once $GLOBALS['TB_root_path'].'lib/php/plugin/pbkdf2.compat.php';
class Delete extends Db{
	private $query;
	private $where;
	private $whereColumn;
	private $tableName;
	private $delKey;
	private $delVal;
	private $cnt;
	private $return_result;
	private $post;
	private $isAdmin;//관리자 인증.
	
	public function __construct($host, $user, $password, $db){
		parent::__construct($host, $user, $password, $db);
		include_once $GLOBALS['TB_root_path'].'lib/php/dbClass/class.fileDelete.php';
	}
	
	//테이블 설 정 및 변수 설정.
	public function setQuery($post) {
		$this->query = $post['reqQuery'];
		$this->whereColumn = "no";
		$this->where = $this->whereColumn." = ".$post['no'];
		$this->delType = "normal";
		$this->post= $post;
		//var_dump($this->query);
		
		if($this->post['TB_adminAuthKey']){//관리자 로그인일 경우 처리
			$adminInfo = array();
			$adminInfo['id'] = $this->post['TB_adminAuthKey']['id'];
			$adminInfo['adminAuthKey'] = $this->post['TB_adminAuthKey']['adminAuthKey'];
			$adminInfo['loginTime'] = $this->post['TB_adminAuthKey']['loginTime'];
			$this->isAdmin = parent::_adminAuth($adminInfo);
			unset($this->post['TB_adminAuthKey']);unset($adminInfo);
		}
		
		switch($this->query){
			case 'comment'://답글
				$this->tableName = 'TB_bbs_comment';
				$item= parent::_select_1('TB_bbs_comment', "WHERE no='".$this->post['no']."' ", 'id, password, salt');
				//관리자가 아닐경우.
				if(!$_SESSION['user'] || $_SESSION['user']->member_grade != 'supervisor'){
					
					//관리자문서, 패스워드가 없는 경우
					if($item->id === 'asidong' || !$this->post['password']){
						 return $this->return_result = false;
					}else if($item->id != 'asidong' && $this->post['password']){
						$confirm = validate_password(md5($this->post['password']), $item->salt);
						
						//암호가 틀렸을 경우.
						if($confirm === false){
							return $this->return_result = false;
						}
						
						unset($this->post['password']);
					}
				}
				/*
				//관리자 문서인데 접속자가 관리자가 아닐경우
				if($item->id === 'asidong' && (!$_SESSION['user'] || $_SESSION['user']->member_grade != 'supervisor')){
					return $this->return_result = false;
					
				//관리자 문서가 아니면서 암호입력이 없이 수정 요청이 들어 왔을 경우.
				}else if($item->id != 'asidong' && !$this->post['password']){
					return $this->return_result = false;
				
				//관리자 문서가 아니면서 암호입력이 들어왔을 경우 유효성 검사.				
				}else if($item->id != 'asidong' && $this->post['password']){
					$confirm = validate_password(md5($this->post['password']), $item->salt);
					
					//암호가 틀렸을 경우.
					if($confirm === false){
						return $this->return_result = false;
					}
					
					unset($this->post['password']);
					
				}
				*/
				$this->return_result = true;
			break;
			
			case 'backdrop_popup':
				$this->tableName = 'TB_backdrop_popup';
				$this->where = "no='".$this->post['no']."' ";
				$item = parent::_select_1($this->tableName, 'WHERE no="'.$this->post['no'].'" '); //아이템
				if($item->src){
					$this->fileDelete_2($item->src['src']);
				}
			break;
			
			case 'bbs_item':
			case 'qna':
			case 'postscript':
			case 'notice':
			case 'yacht_sale':
				$this->tableName = 'TB_bbs';
				$images;
				$src;
				$image_table_item;
				$where = "WHERE no='".$this->post['no']."' ";
				
				
				$old_file = parent::_select_1('TB_bbs', $where, 'images_JSON, nick, password, salt');
				
				//관리자는 그냥 삭제
				if(!$_SESSION['user'] || $_SESSION['user']->member_grade != 'supervisor'){
					//var_dump($old_file);
					$confirm =  validate_password(md5($this->post['password']), $old_file->salt);
					//var_dump($this->post);
					//var_dump($confirm);
					if($confirm === false){//입력한 패스워드가 틀렸을 경우.
						return $this->return_result = false;
					}
				//관리자가 아닐경우
				}
				//var_dump($old_file);
				//return false;
								
				if($old_file->images_JSON){
					$images = json_decode(stripslashes($old_file->images_JSON));
					//var_dump($images);
					foreach($images as $key=>$val){
						$image_table_item = parent::_select_1('TB_imageTable', " WHERE no='".$val->no."' ", 'src, no');
						$this->fileDelete($image_table_item->src);
						parent::_delete('TB_imageTable', "no='".$image_table_item->no."' ");
					}
				}
				//parent::_delete('TB_bbs', "WHERE no='".$this->post['no']."' ");
				//return $this->return_result->result = 'success';
			break;
			
			case 'yacht_album':
				$this->tableName = 'dream_yacht_album';
				$old_file = parent::_select_1($this->tableName, "WHERE no='".$this->post['no']."' ", 'src');
				if($old_file->src){
					$this->fileDelete($old_file->src);
				}
			break;
				
			
			case 'TB_el_img_del'://동적 엘리먼트 삭제
				$this->tableName = 'TB_el';
				$this->whereColumn = 'name';
				$this->where = $this->whereColumn."='".$post['name']."' ";
				//$this->fileDelete($this->post['src']);
				// $filedelete = new FileDelete(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
				// $filedelete -> setQuery($this->query, $this->post['src']);
				// parent::_delete('TB_imageTable', "WHERE src='".$this->post['src']."' ");
			break;
			
			case 'del_imageTable'://'TB_imageTable'의 삭제와 그림 파일의 삭제
				//var_dump($this->post);
				return $this->fileDelete($this->post['src']);
			break;
			
			case 'delete_gallery':
			case 'TB_delete_bbs_item'://TB_bbs
			case 'delete_buildingGallery'://TB_bbs내를 삭제한다.
				$this->tableName = 'TB_bbs';
				$this->where = " _group='".$this->post['_group']."' ";
				$this->where .= " AND no='".$this->post['no']."' ";
				$bbs_item = parent::_select_1($this->tableName, "WHERE ".$this->where, 'file_src, thumbnail_src');
				
				if($bbs_item->file_src){
					$this->fileDelete_2($bbs_item->file_src);
				}
				if($bbs_item->thumbnail_src){
					$this->fileDelete_2($bbs_item->thumbnail_src);
				}
				
				unset($bbs_item);
			break;
			
			case 'delete_sale_item'://요트 판매 정보
				$this->tableName = 'TB_bbs';
				$this->where = " _group='".$this->post['_group']."' ";
				$this->where .= " AND no='".$this->post['no']."' ";
				$bbs_item = parent::_select_1($this->tableName, "WHERE ".$this->where, 'images_name');
				$bbs_item = explode('||',$bbs_item->images_name);
				foreach($bbs_item as $key => $val){
					$this->deleteImageTable($val);
				}
			break;
			
			case 'slider_images'://슬라이더 이미지 삭제
				$this->tableName="TB_templates_slider_images";
			break;
			
			case "deletePopup"://팝업 삭제
				$this->tableName = "popup";
				//파일삭제
				$filedelete = new FileDelete(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
				$filedelete -> setQuery('delPopupImg', $this->delVal[1]);
			break;//팝업 삭제하기.
			
			case 'comment_delete_list_bbs_1':
				$this->tableName = 'TB_bbs_comment';
				$where; 
				$parent_no;
				$cmt_cnt;
				$this->where = " no='".$this->post['no']."'  ";
				$where = "WHERE no='".$this->post['no']."' ";
				if(!$this->isAdmin){//관리자 로그인이 아닐경우.
					$where .= " AND password='".$this->post['password']."' ";
					$this->where .= " AND password='".$this->post['password']."' ";
				}
				$parent_no = parent::_select_1($this->tableName, $where, 'parent_no');//parent_no 획득
				//var_dump($parent_no);
				if(!$parent_no){
					return $this->return_result = '';
				}
				
				$where = "WHERE parent_no='".$parent_no->parent_no."' ";
				$cmt_cnt = parent::_returnCnt($this->tableName, $where);//전체 코멘트수 획득
				$cmt_cnt--;//코멘트 수 감소
				$where = "no='".$parent_no->parent_no."' ";
				parent::_update('TB_bbs', "comment_cnt='".$cmt_cnt."' ",$where);//코멘트수 변경
				
				unset($where); unset($parent_no); unset($cmt_cnt);
			break;
			
			case 'delete_list_bbs_1'://bbs 삭제
				$where;
				$item;
				$this->tableName = 'TB_bbs';
				$this->where = " no='".$this->post['no']."' ";
				if(!$this->isAdmin){//관리자 로그인이 아닌경우 추가로 패스워드 검사.
					$this->where .= " AND password='".$this->post['password']."' ";
				}
				$item = parent::_select_1($this->tableName, "WHERE no='".$this->post['no']."' ", 'file_src, thumbnail_src');
				if($item->file_src){//파일 있을 경우 파일 삭제
					$this->fileDelete_2($item->file_src);
				}
				if($item->thumbnail_src){//썸네일 있을 경우 썸네일 삭제
					$this->fileDelete_2($item->thumbnail_src);
				}
				$where = "parent_no='".$this->post['no']."' ";
				parent::_delete('TB_bbs_comment', $where);//자식글들도 삭제.
				unset($where);
			break;
			
			case 'delete_bbs_1_list_group_delete'://다중 글 삭제
				//var_dump($this->post);
				$this->tableName = 'TB_bbs';
				$this->where = '';
				$comment_where = '';
				$i= 0;
				$numbers_arr = explode(',',$this->post['numbers']);
				$len= count($numbers_arr);
				for(;$i < $len ; $i++){
					$this->where .= ($i==0)?"no='".$numbers_arr[$i]."' ":"OR no='".$numbers_arr[$i]."' ";
					$comment_where .= ($i==0)?"parent_no='".$numbers_arr[$i]."' ":"OR parent_no='".$numbers_arr[$i]."' ";
					$item = parent::_select_1($this->tableName, "WHERE no='".$this->post['no']."' ", 'file_src, thumbnail_src');
					if($item->file_src){
						$this->fileDelete_2($item->file_src);
					}
					if($item->thumbnail_src){//썸네일 있을 경우 썸네일 삭제
						$this->fileDelete_2($item->thumbnail_src);
					}
				}
				parent::_delete('TB_bbs_comment',$comment_where);
				$this->return_result = TRUE;
				unset($comment_where);unset($i);unset($numbers_arr);unset($len);
			break;
			
			case 'del_reservation_item'://예약 삭제
				$this->tableName = 'reservation';
			break;
			
		}
		 $this->delete();
		 
		 return $this->return_result;
	}
	
	
	private function fileDelete_2($src){
		$dir = $GLOBALS['TB_root_path'].$src;
		if(file_exists($dir)){//파일 유무 검사
			if(unlink($dir)){//파일삭제
				return 'success';
			}else{
				return 'failed';
			}
		}else{
			return 'nothing';
		}
		unset($dir);
	}
	/*=============================================
	 * 파일 삭제
	 =============================================*/
	 
	 private function fileDelete($src, $type='image'){
	 	$dir = $GLOBALS['TB_root_path'].$src;
	 	//var_dump($dir);
	 	if($type=='image'){//이미지를 삭제할 경우
			
			if(file_exists($dir)){
				if(unlink($dir)){//파일 삭제
					if($type==='image'){
						parent::_delete('TB_imageTable', "src='".$src."' ");//이미지 테이블 삭제
					}
					//echo '삭제 성공';
				}else{
					//echo '삭제 실패';
					$this->return_result->dbError =  "파일을 삭제할 수 없습니다.";
				}
			}else{
				//echo '파일없음';
				parent::_delete('TB_imageTable', "src='".$src."' ");//이미지 테이블 삭제
				$this->return_result->dbError = "삭제할 파일이 존재 하지 않습니다.";
			}
	 	}
	 	
	 }
	/*=============================================
	 * 상대경로를 기준으로 파일을 삭제한다.
	 * params
	 * 	. $src(str) : 파일의 상대 경로
	 * 	.$type(str) :  어떤 타입의 
	 =============================================*/
	 
	 private function deleteImageTable($name){
	 	$item = parent::_select_1('TB_imageTable', 'WHERE name="'.$name.'" ', 'name, src, thumbnail_src');
		$this->fileDelete_2($item->src);
		if($item->thumbnail_src){
			$this->fileDelete_2($item->thumbnail_src);
		}
		parent::_delete('TB_imageTable', "name='".$name."' ");
	 }
	/*=============================================
	 * TB_imageTable에 있는 파일 삭제
	 * params
	 * 	$name(string) : 삭제할 파일명
	 =============================================*/
	 
	 
	//삭제
	protected function delete(){
		switch($this->delType){
			case "normal":
				// var_dump($this->tableName);
				// var_dump($this->where);
				 parent::_delete($this->tableName, $this->where);
			break;
			case "del_img":
				$data = parent::_select_1($this->tableName, "WHERE no='".$this->delVal[0]."'");
				if($data->img_src != ""){
					$dataArr = explode("||", $data->img_src);
					$this->cnt  = count($dataArr);
					for($i = 0 ; $this->cnt > $i ; $i++){
						parent::_delete("img_data", "img_src='".$dataArr[$i]."'");
						$fileDel = parent::_fileDelete($GLOBALS['TB_root_path'].$dataArr[$i]);
					}
				}
				parent::_delete($this->tableName, $this->where);
				parent::_result(true);//결과 보내기
			break;
		}
		
		unset($this->query);
		unset($this->where);
		unset($this->whereColumn);
		unset($this->tableName);
		unset($this->delKey);
		unset($this->delVal);
		unset($this->cnt);
	}
	
	
}
?>