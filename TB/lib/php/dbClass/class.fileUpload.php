<?php
include_once $GLOBALS['TB_lib_path'].'plugin/class.image.php';

class FileUpload extends Db {
	private $query;
	private $tableName;
	private $_file;
	private $upType;
	private $limitSize = 5;//업로드 제한 사이즈.단위는 mb
	private $_dir;//업로드할 디렉토리
	private $table;//업로드될 테이블
	private $keys;
	private $values;
	private $file_name;//파일의크기 단위는 mb
	private $file_size;//파일의크기 단위는 mb
	private $file_relative_url;//이미지 링크등에 사용될 상대 경로.
	private $file_detail;
	private $file_allowtype  = array('image/jpeg','image/gif','image/png');
	private $result = true;//프로세스의 결과를 저장할 결과값
	private $return_result = array();//리턴할 결과값
	private $uploaded_table;
	private $limit_img_wid = 1920;
	private $limit_img_hei = 1080;
	private $src;
	private $post;
	private $imageTableUpload = true;
	
	public function __construct($host, $user, $password, $db){
		parent::__construct($host, $user, $password, $db);
	}

	//테이블 설정 및 변수 설정
	public function setQuery($uploadData, $file, $fileType='image') {
		$extension = '';//파일 확장자
		$thumbnail_src = '';//썸네일 파일의 상대 경로
		$thumbnail_url = '';//썸네일 파일 절대경로
		$thumbnail_width = '';//썸네일 제한 넓이
	 	//var_dump($uploadData);
	 	
		switch ($uploadData['reqQuery']) {
			case 'backdrop_popup':
				$this->upType = 'image';
				$this->_file = $file['file'];
				$this->file_relative_url = "data/img/backdrop_popup/".$GLOBALS['year'].'/'.$GLOBALS['month'].'/'.$GLOBALS['date'].'/';
				$this->file_url = $GLOBALS['TB_root_path'].$this->file_relative_url;
				$this->file_name = mt_rand(1000, 9999)."_".mt_rand(1000,9999).time().".jdata";
				$this->src = $this->file_relative_url.$this->file_name;
				$this->limit_img_wid = 450;
				$this->imageTableUpload = FALSE;//이미지 테이블에 업로드 안함
			break;
			
			case 'about--yacht_album'://요트 앨범
				$this->upType = 'image';
				$this->_file = $file['files'];
				$this->file_relative_url = "data/img/yacht_album/".$GLOBALS['year'].'/'.$GLOBALS['month'].'/'.$GLOBALS['date'].'/';
				$this->file_url = $GLOBALS['TB_root_path'].$this->file_relative_url;
				$this->file_name = mt_rand(1000, 9999)."_".mt_rand(1000,9999).time().".jdata";
				$this->src = $this->file_relative_url.$this->file_name;
				$this->limit_img_wid = 1920;
				$this->imageTableUpload = FALSE;//이미지 테이블에 업로드 안함
			break;
			
			case 'async_image'://비동기 이미지 업로드
				$this->upType = 'image';
				$this->_file = $file['files'];
				$this->file_relative_url = "data/img/yacht_album/".$GLOBALS['year'].'/'.$GLOBALS['month'].'/'.$GLOBALS['date'].'/';
				$this->file_url = $GLOBALS['TB_root_path'].$this->file_relative_url;
				$this->file_name = mt_rand(1000, 9999)."_".mt_rand(1000,9999).time().".jdata";
				$this->src = $this->file_relative_url.$this->file_name;
				$this->limit_img_wid = 1000;
				$this->imageTableUpload = FALSE;//이미지 테이블에 업로드 안함
			break;
				
				
			case 'TB_bbs_image':
			case 'slide_add':
			case"directEl_image" :
				$this->upType = 'image';
				$this->_file = $file['file'];
				$extension = end(explode('.', $this->_file['name']));
				
				if($uploadData['reqQuery'] === 'TB_bbs_image'){//업로드 폴더 결정
					$imgFolder = 'bbs';
					$this->imageTableUpload = FALSE;//이미지 테이블에 업로드 안함
				}elseif($uploadData['reqQuery'] === 'async_image'){//
					$imgFolder = 'async';
					$this->limit_img_wid = 800;
					$this->limit_img_hei = 600;
					$this->imageTableUpload = FALSE;//이미지 테이블에 업로드 안함
				}else{
					$imgFolder = 'directEl';
				}
				$this->file_relative_url = "data/img/".$imgFolder."/".$GLOBALS['month'].'/';
				$this->file_url = $GLOBALS['TB_root_path'].$this->file_relative_url;
				$this->file_name = mt_rand(1000, 9999)."_".mt_rand(1000,9999).'_'.time().".".$extension;
				$this->src = $this->file_relative_url.$this->file_name; 
				$this->tableName = "TB_imageTable";
				$this->keys = "name, size, uploader, uploadTime, src";
				$this->values = "'".$this->file_name."','".$this->_file['size']."','".$uploadData['uploader']."','".$GLOBALS['now']."','".$this->src."'";
			break;
			
			case 'TB_bbs_file':
				$this->upType = 'file';
				$this->_file = $file['file'];
				$extension = end(explode('.', $this->_file['name']));
				$imgFolder = 'bbs';
				$this->imageTableUpload = FALSE;//이미지 테이블에 업로드 안함
				$this->file_relative_url = "data/file/bbs/".$GLOBALS['month'].'/';
				$this->file_url = $GLOBALS['TB_root_path'].$this->file_relative_url;
				$this->file_name = mt_rand(1000, 9999)."_".mt_rand(1000,9999).'_'.time().".".$extension;
				$this->src = $this->file_relative_url.$this->file_name;
				$this->imageTableUpload = FALSE;//이미지 테이블에 업로드 안함
			break;

			case "insertPopup" ://팝업 이미지 업로드
				$this->upType = 'image';
				$this->_file = $file['imgFile'];
				$this->file_relative_url = "data/img/popup/";
				$this->file_url = $TB_root_path.$this->file_relative_url;
				$this->file_name = mt_rand(1000, 9999)."_".mt_rand(1000,9999).time().".jdata";
				$this->src = $this->file_relative_url.$this->file_name;
				$this->limit_img_wid = 1920;
			break;
		}
		/*===/변수 셋팅 ==*/
		
		$this->dateFolderChk($this->file_url);
		
		if(!$this->result = $this->validationChk()){
			echo 'Error ValidationCheck';
			return false;
		}
		
		$this->result = $this->uploading();//파일 업로드
		
		if($this->result==true) {
			$this->return_result['src'] = $this->src;
			$this->return_result['name'] = $this->_file['name'];
		}
		
		if($fileType === 'thumbnail' && $this->result==true){//썸네일을 만들경우.
			$thumbnail_dir= 'data/img/thumbnail/'.$GLOBALS['month'];
			$thumbnail_src= $thumbnail_dir.'/'.$this->file_name;
			$thumbnail_url = $GLOBALS['TB_root_path'].$thumbnail_src;
			if(file_exists($this->file_url.$this->file_name)){//실제 파일이 있을 경우
				//echo '파일 존재';
				if(!is_dir($GLOBALS['TB_root_path'].$thumbnail_dir)){//폴더 확인 후 없으면 폴더 생성.
					mkdir($GLOBALS['TB_root_path'].$thumbnail_dir, 0744);
				}
				 if(!copy($this->file_url.$this->file_name, $thumbnail_url)){
					//echo '파일 복사 실패';
				}
				 if(file_exists($thumbnail_url)){
					if($this->file_detail[0] > 240){
						$image = new Image($thumbnail_url);
						$image->width(240);
						$image->save();
					}
					$this->return_result['thumbnail_src'] = $thumbnail_src;
				}
			}
		}
		
		return $this->return_result;
	}

	protected function dateFolderChk($folder){
		if(!is_dir($folder)){
			mkdir($folder, 755, true);
		}
	}
	/*==========================================
	 * 폴더 업로드시 날짜 관련 폴더가 있는지 겁사하고 없을 경우 폴더 생성
	 * params
	 * 	. $folder(str) : 폴더 경로. 내부 서버 경로를 따른다.
	 ==========================================*/

	//유효성 검사
	protected function validationChk() {
		$exifData = null;//이미지 회전을 확인하기 위해 파일 정보를 읽어 온다.
		$degree = null;//이미지의 각도를 변경하기 위한 변수.
		$imageType = $this->_file['type'];
		$imageType = explode('/',$imageType);
		if(!$this->result){
			return false;
		}
		switch($this->upType){
			case "image"://일반적인 이미지 업로드
			
				//에러 검사
				if($this->_file['error']!=0){
					return FALSE;
				}
				
				//이미지 파일이 맞는지 확인
				if(!$this->file_detail = getimagesize($this->_file['tmp_name'])){
					return FALSE;
				}
				
				
				// 이미지 크기 제한
				// if($this->file_detail[0] > $this->limit_img_wid){
					// $this->result(false,"최대 이미지 크기는 widht : ".$this->limit_img_wid."px 입니다.");
				// }
				
				//이미지 타입 검사
				if(!in_array($this->file_detail['mime'], $this->file_allowtype)){
					//$this->result(FALSE, "허가된 이미지 타입이 아닙니다.");
					return FALSE;
				}
				
			
				//이미지 용량 검사
				if($this->_file['size']/1024/1024 > $this->limitSize){
					//$this->result(FALSE, "최대 허용용량은 ".$this->limitSize."MB 입니다.");
					return FALSE;
				}
				
				
				//이미지 회전을 확인하기 위해 파일 정보를 읽어 온다, 일단 gif 와 jpeg밖에 안된다 . png 는 왠지 모르게 에러남.
				if($imageType[1] === 'jpeg' || $imageType[1] == 'gif'){
					//var_dump(exif_read_data($this->_file['tmp_name']));
					if(!$exifData = @exif_read_data($this->_file['tmp_name'])){
						echo 'exif_read_data Error';
						return false;
					}
					
					//var_dump($exifData['Orientation']);
					
					//모바일 회전된 이미지 처리
					if($exifData['Orientation'] == 6){// 시계방향으로 90도 돌려줘야 정상인데 270도 돌려야 정상적으로 출력됨 
						$degree = 270; 
					}
					elseif($exifData['Orientation'] == 8){// 반시계방향으로 90도 돌려줘야 정상 
						$degree = 90; 
					}
					elseif($exifData['Orientation'] == 3){
						$degree = 180; 
					}
					
					
					if($degree){//돌려야될 필요성이 있다면
						if($exifData['FileType'] ==1){//git
			                $source = imagecreatefromgif($this->_file['tmp_name']); 
			                $source = imagerotate ($source , $degree, 0); 
			                imagegif($source, $this->_file['tmp_name']); 
						}
						elseif($exifData['FileType'] ==2){//jpeg
			                $source = imagecreatefromjpeg($this->_file['tmp_name']); 
			                $source = imagerotate ($source , $degree, 0); 
			                imagejpeg($source, $this->_file['tmp_name']); 
						}
						elseif($exifData['FileType'] ==3){//png
			                $source = imagecreatefrompng($this->_file['tmp_name']); 
			                $source = imagerotate ($source , $degree, 0); 
			                imagepng($source, $this->_file['tmp_name']); 
						}
						imagedestroy($source);
					}
				}
				
			break;
			
			case 'file'://일반 파일들
				if($this->_file['error']!=0){
					//echo '파일에러 : '.$this->_file['error'];
					return FALSE;
				}
				if($this->_file['size']/1024/1024 > $this->limitSize){
					//echo '파일허용용량 초과';
					return FALSE;
				}
			break;
		}
		return true;
	}

	//파일 업로드
	protected function uploading(){
		if(!$this->result){
			return '파일 업로드 실패';
		} 
		//global $TB_relative_path;
		
		switch($this->upType){
			case "image":
				//파일 존재 여부 확인
				if(file_exists($this->file_url.$this->file_name)){
					return false;
					//$this->result(false, "동일한 파일이 존재합니다.");
				}
				//파일 이동
				if(!move_uploaded_file($this->_file['tmp_name'], $this->file_url.$this->file_name)){
					return FALSE;
					//echo '파일을 이동하지 못했습니다.';
					//$this->result(false, "파일을 이동 하지 못하엿습니다.");
				}
				//이미지 크기 변경.
				$this->imageResize($this->file_url.$this->file_name,'width',$this->limit_img_wid);
				$this->imageResize($this->file_url.$this->file_name, 'height', $this->limit_img_hei);
				
				if($this->imageTableUpload){//TB_imageTable 업로드
					parent::_insert($this->tableName, $this->keys, $this->values); 
				}
			break;
			
			case 'file':
				if(file_exists($this->file_url.$this->file_name)){//파일 존재 여부 확인.
					return false;
					//echo '파일 존재';
				}
				if(!move_uploaded_file($this->_file['tmp_name'], $this->file_url.$this->file_name)){//파일 이동.
					//echo '파일 이동실패';
					return FALSE;
				}
				if($this->imageTableUpload){//TB_imageTable 업로드
					parent::_insert($this->tableName, $this->keys, $this->values); 
				}
			break;
		}
		return true;//이미지 주소 저장
	}
	
	//이미지리사이징 메소드
	protected function imageResize($file, $check='width', $limit){
		$image = new Image($file);
		if($check == 'width'){//width가 더 넓을 경우
			if($this->file_detail[0] > $limit){
				$image->width($limit);
				$image->save();
			}
		}else if($check== 'height'){//height가 더 넓을 경우.
			if($this->file_detail[1] > $limit){
				$image->height($limit);
				$image->save();
			}
		}
		
	}
}
?>