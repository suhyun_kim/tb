<?php
session_start();

include_once $GLOBALS['TB_root_path'].'lib/php/plugin/pbkdf2.compat.php';

class Select extends Db{
	private $type;
	private $query;
	private $where = '';
	private $field;
	private $whereColumn;
	private $tableName;
	private $arrKey;
	private $arrVal;
	private $arr_key_val;
	private $selType;
	private $return_result;
	private $result_data;
	private $arr_outputfield;//DB에서 출력할 필드 값
	private $numFormatfield;//출력할 필드중에서 숫자로 형태로 출력할 필드.
	private $modify;
	private $delQuery;
	private $view;
	private $arrResult = array();//결과값을 배열로 저장해서 json으로 리턴시에 쓰인다.
	private $serfield = '';//검색문을 사용할 때 검색을 할 필드.
	protected $userInfo;
	private $delModiBtnSw = false;
	private $delModiDetail = array();
	private $orderfield = 'no';//defaultList에서 정렬의 기준이 될 필드를 정함.
	private $sendData;//json으로 보낼 데이터의 데이터
	private $sendArr = array();//jsonp로 보낼 array 컨테이너.$this->sendData 와 $this->sendHeader을 담는다.
	private $post;
	private $isAdmin;//관리자 로그인 유무 판단 변수.
	private $seayacht;//seayacht 전용 데이터 불러오기.
	
	public function __construct($host, $user, $password, $db){
		parent::__construct($host, $user, $password, $db);
		include_once $GLOBALS['TB_lib_path'].'dbClass/class.select/class.seayacht.php';
		$this->seayacht = new Select_seayacht($host, $user, $password, $db);
	}
	
	
	//테이블 설정 및 변수 설정.
	public function setQuery($post) {
		$this->post  = $post;
		if($this->post['TB_adminAuthKey']){//관리자 로그인일 경우 처리
			$adminInfo = array();
			$adminInfo['id'] = $this->post['TB_adminAuthKey']['id'];
			$adminInfo['adminAuthKey'] = $this->post['TB_adminAuthKey']['adminAuthKey'];
			$adminInfo['loginTime'] = $this->post['TB_adminAuthKey']['loginTime'];
			$this->isAdmin = parent::_adminAuth($adminInfo);
			unset($this->post['TB_adminAuthKey']);unset($adminInfo);
		}
		
		switch($this->post['reqQuery']){
			
			case 'bbs_password_check':
				$where = "WHERE no='".$this->post['no']."' ";
				$field = "no, subject, memo, images_JSON, writetime, hidden, password, salt, nick";
				$item = parent::_select_1('TB_bbs', $where, $field);
				$item->images_JSON = stripslashes($item->images_JSON);
				$item->memo = stripslashes($item->memo);
				//var_dump($item);
				//var_dump($_SESSION['user']);
				if(!$this->post['password']){//패스워드를 입력 안했을 경우.
					return $this->return_result = false;
				}else {//패스워드가 있을 경우.
					//var_dump(md5($this->post['password']));
					$confirm =  validate_password(md5($this->post['password']), $item->salt);
					//var_dump($confirm);
					if($confirm === false){//입력한 패스워드가 틀렸을 경우.
						return $this->return_result = false;
					}
					
				}
				
				unset($item->password);
				unset($item->salt);
				//var_dump($item);
				return $this->return_result = $item;
			break;
				
			
			case 'backdrop_popup':
				$this->tableName = 'TB_backdrop_popup';
				$where = '';
				if(!$_SESSION['user'] || $_SESSION['user']->member_grade != 'supervisor'){
					$where = "WHERE date(now()) >= date(start_date) AND date(now()) <= date(end_date) " ; 
				}
				return $this->return_result = parent::_select_list($this->tableName, $where);
			break;
			
			case 'bbs_item':
			case 'qna_item':
			case 'postscript_item':
			case 'notice_item':
				$comment;
				$where = "WHERE no='".$this->post['no']."' ";
				$field = "no, subject, memo, images_JSON, writetime, hidden, password, salt, nick, comment_cnt";
				$item = parent::_select_1('TB_bbs', $where, $field);
				$item->images_JSON = stripslashes($item->images_JSON);
				$item->memo = stripslashes($item->memo);
				$this->return_result = true;
				//var_dump($item);
				//var_dump($_SESSION['user']);
				if($item->hidden == '1' && (!$_SESSION['user'] || $_SESSION['user']->member_grade != 'supervisor') ){
					if(!$this->post['password']){//패스워드를 입력 안했을 경우.
						return $this->return_result = false;
					}else {//패스워드가 있을 경우.
						//var_dump(md5($this->post['password']));
						$confirm =  validate_password(md5($this->post['password']), $item->salt);
						//var_dump($confirm);
						if($confirm === false){//입력한 패스워드가 틀렸을 경우.
							return $this->return_result = false;
						}
						
					}
				}
				
				if($this->return_result == true ){
					$comment_where = "WHERE parent_no='".$item->no."' ORDER BY no ASC ";
					$item->comment_list = parent::_select_list('TB_bbs_comment', $comment_where, 'no, parent_no, nick, writetime, memo');
				}
				
				
				
				unset($item->password);
				unset($item->salt);
				//var_dump($item);
				return $this->return_result = $item;
			break;
			
			case 'qna_list'://요트.보트.판매 리스트
			case 'postscript_list'://요트.보트.판매 리스트
			case 'notice_list'://요트.보트.판매 리스트
				$result;
				$this->post['table'] = 'TB_bbs';
				$this->post['list_num'] = 9;
				$this->post['field'] = 'category, no, subject,  images_JSON, writetime, hidden, nick, comment_cnt';
				$this->post['categories'] = array();
				if($this->post['reqQuery'] === 'notice_list'){
					$this->post['categories']['_group'] =  'notice';
				}elseif($this->post['reqQuery'] === 'postscript_list'){
					$this->post['categories']['_group'] =  'postscript';
				}elseif($this->post['reqQuery'] === 'qna_list'){
					$this->post['categories']['_group'] =  'qna';
				}
				
				$this->return_result = $this->get_bbs($this->post, 'categories');
				foreach ($this->return_result->list as $key => $value) {
					foreach($this->return_result->list[$key] as $k => $v){
						if($k == 'images_JSON'){
							$this->return_result->list[$key]->$k = json_decode(stripslashes($v));
						}
					}
				}
				return $this->return_result;
			break;
			
			case 'yacht_sale_item':
				$where = "WHERE no='".$this->post['no']."' ";
				$where .= ($this->post['category'])?" AND category='".$this->post['category']."' ":"";
				$field = "no, subject, memo, images_JSON, category, writetime, nick, hidden";
				$item = parent::_select_1('TB_bbs', $where, $field);
				$item->images_JSON = stripslashes($item->images_JSON);
				$item->memo = stripslashes($item->memo);
				//var_dump($item);
				return $this->return_result = $item;
			break;
			
			case 'yacht_sale'://요트.보트.판매 리스트
				$result;
				$bbs_type = ($this->post['category'])?'categories':'normal';
				$this->post['table'] = 'TB_bbs';
				$this->post['list_num'] = 9;
				$this->post['field'] = 'category, no, subject,  images_JSON';
				$this->post['categories'] = array();
				$this->post['categories']['_group'] =  'yacht_sale';
				if($this->post['category']){
					$this->post['categories']['category'] =  $this->post['category'];
				}
				
				$this->return_result = $this->get_bbs($this->post, 'categories');
				foreach ($this->return_result->list as $key => $value) {
					foreach($this->return_result->list[$key] as $k => $v){
						if($k == 'images_JSON'){
							$this->return_result->list[$key]->$k = json_decode(stripslashes($v));
						}
					}
				}
				return $this->return_result;
			break;
			
			case 'yacht_album'://요트 리스트
				$result;
				$bbs_type = ($this->post['category'])?'categories':'normal';
				$this->post['table'] = 'dream_yacht_album';
				$this->post['list_num'] = 9;
				$this->post['field'] = 'category, no, subject, src, file_name';
				$this->post['categories'] = array();
				$this->post['categories']['category'] =  $this->post['category'];
				
				//var_dump($result);
				$this->return_result = $this->get_bbs($this->post, $bbs_type);
				return $this->return_result;
			break;
			
			case 'isCreate'://홈페이지가 만들어져 있는지 확인.
				$this->return_result->isCreate = parent::_returnCnt('TB_member');
				if($this->return_result->isCreate >0){//홈페이지가 있을 때
					$this->return_result->reqType = 'login';
					$this->return_result->reqQuery = 'loginAdmin';
				}else{//홈페이지가 없을 때
					$this->return_result->reqType = 'insert';
					$this->return_result->reqQuery = 'createAdmin';
				}
			break;
			
			case 'isAdmin'://관리자 인지 확인
				$adminInfo = array();
				$adminInfo['id'] = $this->post['id'];
				//$adminInfo['adminAuthKey'] = $this->post['adminAuthKey'];
				$adminInfo['login_time'] = $this->post['login_time'];
				//$this->return_result = parent::_adminAuth('TB_admin',$adminInfo);
				if($_SESSION['user'] 
				&& $_SESSION['user']->member_grade === 'supervisor' 
				&& $_SESSION['user']->id === $this->post['id'])
				{
					$this->return_result->isAdmin = true;
				}
				//$this->return_result = parent::_adminAuth('TB_admin',$adminInfo);
			break;
			
			case 'adminLoginOut';//관리자 로그아웃.
				$where = '';
				$key_val = '';
				$key_val = "adminAuthKey=''";
				$key_val .= ", sessionKey=''";
				$key_val .= ", logoutTime='".$GLOBALS['now']."'";
				$where = "id='".$this->post['adminInfo']['id']."'";
				$where .= "AND adminAuthKey='".$this->post['adminInfo']['adminAuthKey']."'";
				$result = parent::_update("TB_admin",$key_val, $where);
				session_destroy();
				return $this->return_result; 
			break;
			
			case 'getConfig':// 홈페이지 설정 가져오기
				$this->tableName = 'TB_config';
				$this->selType = 'returnJson';
				$this->return_result = parent::_select_1($this->tableName, '', 'menu');
				// if($this->return_result->menu){
					// $this->return_result->menu = json_decode($this->return_result->menu);
				// }
				if($_SESSION['user']){
					$this->return_result->user = $_SESSION['user'];
				}
				//var_dump($this->return_result->menu);
				// /return false;
				return $this->return_result;
			break;
			
			case 'get_visitor_info'://방문자 정보 
				$this->tableName = 'TB_visitor';
				$this->return_result->totalCnt = parent::_returnCnt($this->tableName);
				//var_dump($this->return_result);
				//var_dump($this->post);
				$where = '';
				$field= '';
				if($this->post['tab'] === 'period'){//기간별보기
					if($this->post['period'] === 'year'){//년별
						$field = "year";
					}elseif($this->post['period'] === 'month'){//월별
						$field = "month";
						$where = "WHERE year='".$this->post['period_detail']['year']."' ";
					}elseif($this->post['period'] === 'date'){//일별 보기
						$field = "date";
						$where = "WHERE year='".$this->post['period_detail']['year']."' ";
						$where .= "AND month='".$this->post['period_detail']['month']."' ";
					}elseif($this->post['period'] === 'hour'){
						$field = "datetime";
						$d = $this->post['period_detail']['year'].'-'.$this->post['period_detail']['month'].'-'.$this->post['period_detail']['date'];
						$where .= "WHERE  date BETWEEN '".$d."' AND '".$d."' ";
					}
					
					$this->return_result->graph_data = parent::_groupCnt($this->tableName, $where, $field);
					
				}elseif($this->post['tab'] === 'day'){//요일별 보기
					$field = 'day';
					$this->return_result->graph_data = parent::_groupCnt($this->tableName, $where, $field);
				}elseif($this->post['tab'] === 'platform'){//기기별 보기
					$field = 'platform';
					$this->return_result->graph_data = parent::_groupCnt($this->tableName, $where, $field);
				}elseif($this->post['tab'] === 'ip' ||$this->post['tab'] === 'referrer' ){
					if($this->post['tab'] === 'ip'){
						$field = "ip";
					}else{//접속경로
						$field = "before_url";
					}
					$page = (int)$this->post['page'];
					$list_num= 20;
					$start_num = ($page-1)*$list_num;
					$where_2 = " ORDER BY COUNT(*) DESC LIMIT ".$start_num.",".$list_num." ";
					$this->return_result->graph_data = parent::_groupCnt($this->tableName, $where, $field, $where_2);
					unset($page);
				}
				unset($where); unset($field); 
			break;
			
			case 'get_TB_el'://el 정보를 가져온다.
				$this->tableName = 'TB_el';
				$this->selType = 'json_table_all';
			break;
			
			case 'get_imageTable'://관리자 메뉴에서 볼수 있는 이미지 테이블 전체 정보
				if(!$this->isAdmin){
					return $this->return_result->dbError = '권한이 올바르지 않습니다.';
				}
				$this->tableName = 'TB_imageTable';
				$this->post['table'] = $this->tableName;
				$this->post['field'] = 'no, src, name';
				$this->post['list_num'] = 16;
				
				$this->return_result = $this->get_bbs($this->post);
				$this->return_result->delQuery = "del_imageTable";
				return $this->return_result;
			break;
			
			case 'bbs_hidden_password_chk'://패스워드 체크 후 통과되면 데이터 전송 아닐경우 
				$cnt;
				$this->tableName = ($this->post['isComment'] == 'true')?'TB_bbs_comment':'TB_bbs';
				$this->where = "WHERE no='".$this->post['no']."' ";
				
				if($this->post['isComment'] != 'true'){
					$this->where .= "AND _group='".$this->post['_group']."'  ";
				}
				if($this->post['category']){
					$this->where .= "AND category='".$this->post['category']."'  ";
				}
				$this->where .= " AND password='".$this->post['password']."' ";
				$cnt = parent::_returnCnt($this->tableName, $this->where);
				
				if($this->isAdmin){//관리자 로그인일 경우 그냥 패스
					$cnt = 1;
				}
				
				if($cnt <= 0){
					$this->return_result->chk = false;
					$this->return_result->TB_dbMessage = '패스워드를 다시 확인해 주십시요.';
				}else{
					$this->return_result->chk = true;
				}
				unset($cnt);
			break;
			
			case 'get_bbs_item'://BBS데이터
				$this->tableName = 'TB_bbs';
				$this->where = "WHERE no='".$this->post['no']."' ";
				//var_dump($this->isAdmin);
				if($this->post['_group']){
					$this->where .= "AND _group='".$this->post['_group']."'  ";
				}
				if($this->post['category']){
					$this->where .= "AND category='".$this->post['category']."'  ";
				}
				if($this->post['password']){//패스워드가 있을 경우.
					$this->where .= "AND password='".$this->post['password']."'  ";
				}
				if($this->post['hit_cnt'] == 'up'){//view카운트를 올리라는 요청이 있을 경우.
					$key_val = "hit_cnt = hit_cnt+1";
					$where = "_group ='".$this->post['_group']."' AND no='".$this->post['no']."'";
					parent::_update($this->tableName, $key_val, $where);	
					unset($key_val);unset($where);
				}
				
				if($this->return_result->hidden == '1' && !$this->post['password'] && !$this->isAdmin){//숨김 글일 경우
					$this->post['field'] = 'no, _group, category, hidden, nick';
				}
				
				if($this->post['field']){
					$this->field = $this->post['field'];
				}else{
					$this->field = 'no, _group, category, notice, subject, memo, file_src, file_name, hidden, writetime, memo, nick';
				}
				
				$this->return_result = parent::_select_1($this->tableName, $this->where, $this->field);
				$this->return_result->memo = stripcslashes($this->return_result->memo);
			break;
			
			case 'get_bbs_list'://BBS리스트
				$this->post['table'] = 'TB_bbs';
				$this->post['field']  = 'no, subject, file_src, file_name, _group, category, memo';
				$this->post['categories'] = array();
				$this->post['categories']['_group'] =  $this->post['_group'];
				$this->post['categories']['category'] =  $this->post['category'];
				
				$this->return_result = $this->get_bbs($this->post, 'categories');
				//var_dump($this->return_result);
				//var_dump($this->return_result);
				
				$this->return_result->delete_reqQuery = 'TB_delete_bbs_item';
				$this->return_result->wvm_queries =  new stdClass;
				$this->return_result->wvm_queries->insert_reqQuery = 'TB_bbs_insert';
				$this->return_result->wvm_queries->modify_reqQuery = 'TB_bbs_modify';
				$this->return_result->wvm_queries->view_reqQuery = 'get_bbs_item';
			break;
			
			case 'get_gallery_list'://갤러리
				$this->post['table'] = 'TB_bbs';
				$this->post['field']  = 'no, subject, file_src, file_name, _group, thumbnail_src';
				$this->post['categories'] = array();
				$this->post['categories']['_group'] =  $this->post['_group'];
				
				$this->return_result = $this->get_bbs($this->post, 'categories');
				$this->return_result->delete_reqQuery = 'delete_gallery';
				$this->return_result->wvm_queries =  new stdClass;
				$this->return_result->wvm_queries->insert_reqQuery = 'insert_gallery';
				$this->return_result->wvm_queries->modify_reqQuery = 'modify_gallery';
				return $this->return_result;
			break;
			
			case 'list_bbs_1'://BBS 보기
				$this->post['table'] = 'TB_bbs';
				$this->post['field']  = 'no, subject, _group, hit_cnt, nick, writetime, notice, hidden, comment_cnt';
				$this->post['categories'] = array();
				$notice = array();
				$other_list = array();
				$bbs_type = ($this->post['use_search'] === 'true')?'search':'categories';//검색 여부 결정
				$list_num = $this->post['list_num'];//공지사항 출력시 손상되는 list_num을 복구
				$original_page = $this->post['page']; //원래 페이지
				$this->return_result->wvm_queries =  new stdClass;
				$this->post['categories']['_group'] =  $this->post['_group'];
				
				$this->post['page'] = 1;
				$this->post['categories']['notice'] =  1;
				$notice = $this->get_bbs($this->post, 'categories');//공지사항 글
				
				$this->post['list_num'] -= $notice->list_info->total_article;
				$this->post['page'] = $original_page;
				$this->post['categories']['notice'] =  0;
				$other_list = $this->get_bbs($this->post, $bbs_type);
				
				$this->return_result->list = array_merge($notice->list, $other_list->list);
				$this->return_result->list_info = $other_list->list_info;
				$this->return_result->list_info->list_num = $list_num;
				$this->return_result->delete_reqQuery = 'delete_bbs_1_list_group_delete';//다중 리스트 삭제 쿼리
				$this->return_result->wvm_queries->insert_reqQuery = 'insert_list_bbs_1';
				$this->return_result->wvm_queries->modify_reqQuery = 'modify_list_bbs_1';
				$this->return_result->wvm_queries->delete_reqQuery = 'delete_list_bbs_1';//개별 리스트 삭제 쿼리.
				$this->return_result->wvm_queries->comment_select_reqQuery = 'comment_select_list_bbs_1';
				$this->return_result->wvm_queries->comment_insert_reqQuery = 'comment_insert_list_bbs_1';
				$this->return_result->wvm_queries->comment_modify_reqQuery = 'comment_modify_list_bbs_1';
				$this->return_result->wvm_queries->comment_delete_reqQuery = 'comment_delete_list_bbs_1';
				
				unset($notice);unset($other_list);unset($original_page);unset($list_num);unset($bbs_type);
				return $this->return_result;
			break;
			
			case 'comment_select_list_bbs_1'://bbs_1 코멘트 데이터 가져오기.
				$this->post['table'] = 'TB_bbs_comment';
				$this->post['field'] = 'no, memo, writetime, nick';
				$this->post['categories'] = array();
				$this->post['categories']['parent_no'] = $this->post['parent_no'];
				$this->return_result->comment_list = $this->get_bbs($this->post, 'categories');
			break;
			
			case 'getSlider' : //해당 슬라이더가 있는지 확인하고 있을 경우 슬라이더 정보를 리턴
				$this->tableName = 'TB_templates_slider';
				$cnt = parent::_returnCnt('TB_templates_slider', "WHERE name='".$this->post['name']."' ", 'name');
				if($cnt){//해당 슬라이더가 있을 경우
					$this->return_result->reqType = 'modify';
					$this->return_result->reqQuery = 'modifySlider';
					$slider = parent::_select_1('TB_templates_slider', "WHERE name='".$this->post['name']."' ");
					$this->return_result->slider = $slider;
					if (parent::_returnCnt('TB_templates_slider_images', "WHERE parent_no='".$slider->no."' ", 'no')){
						$this->return_result->slider->images = parent::_select_list('TB_templates_slider_images', "WHERE parent_no='".$slider->no."' ");
					}
				}else{//슬라이더가 존재 하지 않을 경우.
					$this->return_result->reqType = 'insert';
					$this->return_result->reqQuery = 'createSlider';
				}
				$this->return_result->isCreate = $cnt;
				unset($cnt);unset($slider);
			break;
			
			case 'init_reservation'://초기 예약 정보 
				//예약 리스트 정보
				$this->return_result->status_list = $this->seayacht->reservation_list($this->post['year'], $this->post['month'], $this->post['marina_no']);
				//var_dump($this->return_result->status_list);
				$this->return_result->time_selector_list = $this->seayacht->reservation_complete_time($this->post['year'], $this->post['month'], $this->post['date'],$this->post['marina_no']);
				
				$this->return_result->queries = new stdClass;
				//예약 쿼리
				$this->return_result->queries->reservation_status = new stdClass;
				$this->return_result->queries->reservation_status->list = 'get_reservation_status_list';
				$this->return_result->queries->reservation_status->change = 'get_reservation_status_change';
			break;
			
			case 'get_reservation_status_change'://예약 현황 수정
				$tabelName = 'reservation_callendar';
				$where = '';
				if($this->post['status'] == '1'){//등록
					$keys = 'year, month, date, status, marina_no, marina_name';
					$values = $this->post['year'].', '.$this->post['month'].', '.$this->post['date'].', '.$this->post['status'];
					$values .= ', '.$this->post['marina_no'].", '".$this->post['marina_name']."' ";
					parent::_insert($tabelName, $keys, $values);
				}elseif($this->post['status'] == '0'){//삭제
					$where = "year='".$this->post['year']."' AND month='".$this->post['month']."' AND date='".$this->post['date']."' ";
					$where .= "AND marina_no='".$this->post['marina_no']."' AND marina_name='".$this->post['marina_name']."' ";
					parent::_delete($tabelName, $where);
				}
				
				$this->return_result->status_list = $this->seayacht->reservation_list($this->post['year'], $this->post['month'], $this->post['marina_no']);
			break;
			
			case 'get_time_select'://시간 선택 정보
				$this->return_result->time_selector_list 
					= $this->seayacht->reservation_complete_time($this->post['year'], $this->post['month'], $this->post['date'],$this->post['marina_no']);
			break;
			
			case 'get_reservation_list'://예약 리스트
				$this->post['table'] = 'reservation';
				$this->post['list_num'] = 10;
				//$this->post['field']  = '*';
				$this->post['categories'] = array();
				$this->post['categories']['marina_no'] =  $this->post['marina_no'];
				//var_dump($this->post);
				if($this->post['status']){
					$this->post['categories']['status'] = $this->post['status'];
					unset($this->post['status']);
				}
				
				$this->return_result = $this->get_bbs($this->post, 'categories');
				$this->return_result->list_info->status = parent::_select_list('reservation', ' WHERE marina_no="'.$this->post['marina_no'].'" GROUP BY status', 'status, COUNT(status)'); 
			break;
			
			case 'get_yacht_info_list'://요트 정보
			case 'get_sale_list'://갤러리
			case 'get_metroAlbum3_list'://메트로 앨범 3 리스트
				$this->post['table'] = 'TB_bbs';
				$this->post['field']  = 'no, subject, memo,images, images_name, images_thumbnail_src, _group, writetime';
				$this->post['categories'] = array();
				$this->post['categories']['_group'] =  $this->post['_group'];
				
				if($this->post['category']){
					$this->post['categories']['category'] =  $this->post['category'];
				}
				
				$this->return_result = $this->get_bbs($this->post, 'categories');
				$this->return_result->delete_reqQuery = 'delete_sale_item';
				$this->return_result->wvm_queries =  new stdClass;
				$this->return_result->wvm_queries->insert_reqQuery = 'insert_sale_item';
				$this->return_result->wvm_queries->modify_reqQuery = 'modify_list_bbs_1';
				return $this->return_result;
			break;
			
			case 'ships_thumbnail':
				//var_dump($this->post);
				$marina = ($this->post['marinaName']=='전곡마리나')?'전곡마리나(화성)':'의왕마리나(인천)';
				$where = "WHERE category='".$marina."' ORDER BY no DESC";
				$field = 'no, subject, images_thumbnail_src ';
				$this->return_result->list = parent::_select_list('TB_bbs', $where, $field);
			break;
		}

		if($this->selType){
			return $this->select();
		}else{
			return $this->return_result;
		}
	}

	private function get_bbs($data, $bbs_type = 'normal'){
		$bbsData = new stdClass;
		$bbsData->list_info;//리스트의 페이지 정보
		$bbsData->list  = array();
		$where = '';//where 절
		$field = ($data['field'])?$data['field']:'*';
		$result = '';
		$result_data;//쿼리의 결과를 담을 변수
		$i = 0;
		$data['sort'] = ($data['sort'])?$data['sort']:'DESC';
		
		// return $bbsData;
		
		if($bbs_type === 'search' && $data['categories']){//검색시에.
			$search_field_arr = explode('&', $data['search_field']);
			$search_field_len = count($search_field_arr);
			$categories_len = count($data['categories']); //카테고리의 총 숫자
			$keys = array_keys($data['categories']);//키값
			$j = 1;
			//var_dump($search_field_arr);
			$where = "WHERE ";
			
			if($search_field_len > 1){
				$where .= "CONCAT(".$search_field_arr[0];
				for(; $j < $search_field_len; $j++){
					$where .= ', '.$search_field_arr[$j]."";
				}
				$where .= ") LIKE '%".$data['search_value']."%' ";
			}else{//검색 필드가 한개일때
				$where .= $search_field_arr[0].' LIKE '." '%".$data['search_value']."%' "; 
			}
			$j = 0;
			if($categories_len > 1){
				for (;$j <$categories_len ; $j++ ){
					$where .= "AND ".$keys[$j]."='".$data['categories'][$keys[$j]]."' ";
				}
			}
			//var_dump($where);
			
			$bbsData->list_info = $this->paging($data, $where);
			
			$where .= " ORDER BY no ".$data['sort']." LIMIT ".$bbsData->list_info->start_num." , ".$bbsData->list_info->last_num;
			
			unset($j); unset($keys); unset($categories_len);unset($search_field_arr);
		}elseif($bbs_type === 'categories'){//분류 시에
			$categories_len = count($data['categories']); //카테고리의 총 숫자
			$keys = array_keys($data['categories']);//키값
			$j = 1;
			$where = "WHERE ".$keys[0]."='".$data['categories'][$keys[0]]."' ";
			
			if($categories_len > 1){
				for (;$j <$categories_len ; $j++ ){
					$where .= "AND ".$keys[$j]."='".$data['categories'][$keys[$j]]."' ";
				}
			}
			
			$bbsData->list_info = $this->paging($data, $where);
			
			$where .= " ORDER BY no ".$data['sort']." LIMIT ".$bbsData->list_info->start_num." , ".$bbsData->list_info->last_num;
			unset($j); unset($keys); unset($categories_len);
			
		}elseif($bbs_type === 'normal'){//일반 쿼리
			$bbsData->list_info = $this->paging($data);
			$where .= "ORDER BY no ".$data['sort']." LIMIT ".$bbsData->list_info->start_num." , ".$bbsData->list_info->last_num;
		}
		
		$result = parent::_select($data['table'], $where, $field);
		
		while($result_data = mysqli_fetch_object($result)){
			$bbsData->list[$i] = $result_data;
			$i++;
		}
		
		unset($bbsData->setup);
		return $bbsData;
	}
	/*======================================================
	 * Bbs 형태로 테이블에서 데이터를 찾아 리턴한다.
	 *======================================================*/
	 
	protected function paging($data, $where ='WHERE no'){
		$paging = new stdClass;
		$paging->page = (int)($data['page'])?$data['page']:'1';
		$paging->page_num = (int)($data['page_num'])?$data['page_num']:'10';
		$paging->list_num = (int)($data['list_num'])?$data['list_num']:'15';
		$paging->total_article;//전체 글수
		$paging->total_page;//전체 페이지
		$paging->start_num;//현재 페이지의 시작번호
		$paging->last_num;//현재 페이지의 끝번호
		$paging->cur_num;//현재 페이지 번호
		$paging->total_block;//전체 페이지 블럭
		$paging->cur_block;//현재 페이지 블럭
		$paging->first_block;//첫 페이지 블럭
		$paging->last_block;//마지막 페이지 블럭
		$paging->next_block_page;//다음 블럭의 첫 페이지
		$paging->prev_block_page;//이전 블럭의 첫 페이지
		$paging->page_link = array();//페이지 링크
		$table = $data['table'];//테이블 값 필수 
		$link;//페이지 링크 출력시 사용될 변수.
		$i = 0;
		
		$paging->total_article = parent::_returnCnt($table, $where);
		$paging->total_page = ceil($paging->total_article/$paging->list_num);
		$paging->start_num = $paging->list_num*($paging->page-1);
		$paging->last_num = $paging->list_num;
		$paging->cur_cum = $paging->total_article - $paging->list_num*($paging->page-1);//현재 글번호
		
		$paging->totalBlock = ceil($paging->total_page / $paging->page_num);
		$paging->cur_block = ceil($paging->page / $paging->page_num);
		$paging->first_block = ($paging->cur_block-1)*$paging->page_num;
		$paging->last_block = $paging->cur_block*$paging->page_num;
		
		$paging->next_block_page = ($paging->cur_block < $paging->totalBlock && $paging->totalBlock >1)?$paging->last_block+1:null;
		$paging->prev_block_page = ($paging->cur_block >1)?$paging->first_block:null;
		
		for ($link = $paging->first_block+1; $link <= $paging->last_block ; $link++ ){
				if($link <= $paging->total_page){
					$paging->page_link[$i] = new stdClass;
					$paging->page_link[$i]->link = $link;
					$i++;
				}
		} //*==/페이지 링크
		
		
		unset($i);unset($link);unset($table);
		return $paging;
	}
	 
	 
	protected function select(){
		switch($this->selType){
			case 'jsonBbs_1'://일렬로 나오는 리스트(notice 필드 필요)
				$this->return_result->notice = array();
				$this->return_result->list = array();
				($this->adminAuth==1)?$auth='auth':$auth='';
				//($_SESSION['adminAuthKey']&&$this->adminAuthKey==$_SESSION['adminAuthKey'])?$auth = 'auth': $auth='';//관리자로 로그인시
				
				if($this->arr_key_val['searchfield']){//검색시에.
					$this->where = "WHERE ".$this->arr_key_val['searchfield']." LIKE '%".$this->arr_key_val['searchVal']."%' ";
					$this->where .= "AND ".$this->arr_key_val['devidefield']."='".$this->arr_key_val['devideVal']."' ";
					$this->where .= " AND notice='' ";
					$this->setPaging($this->where);
					$this->where .= " ORDER BY no DESC LIMIT ".$this->return_result->list_info->startno." , ".$this->return_result->list_info->lastno;
				}elseif($this->arr_key_val['devidefield']){//분류 시에
					$this->where = "WHERE ".$this->arr_key_val['devidefield']."='".$this->arr_key_val['devideVal']."' ";
					$this->where .= " AND notice='' ";
					$this->setPaging($this->where);
					$this->where .= " ORDER BY no DESC LIMIT ".$this->return_result->list_info->startno." , ".$this->return_result->list_info->lastno;
				}else{//모든 정보 출력
					$this->setPaging();
					$this->where = " AND notice='' ";
					$this->where .= "ORDER BY no DESC LIMIT ".$this->return_result->list_info->startno." , ".$this->return_result->list_info->lastno;
				}
				
				$i = 0;
				if($this->arr_key_val['page']=="1"){
					$noticeWhere = " WHERE notice NOT IN ('') ";
					$noticeWhere .= "AND ".$this->arr_key_val['devidefield']."='".$this->arr_key_val['devideVal']."' ";
					$noticeResult = parent::_select($this->tableName, $noticeWhere);
					while($notice = mysqli_fetch_object($noticeResult)){
						//$notice->admin = $this->arr_key_val->admin;
						$this->return_result->list[$i] = $notice;
						$this->return_result->list[$i]->auth  = $auth;
						$i++;
					}
				}
				//*==/공지사항 검색==*/
				
				$result = parent::_select($this->tableName,$this->where);
				while($list = mysqli_fetch_object($result)){
					//$list->admin = $this->sendHeader->admin;//어드민 정보 
					$this->return_result->list[$i] = $list;
					$this->return_result->list[$i]->auth  = $auth;
					$i++;
				}
			break;
			
			
			case 'jsonBbs_2'://일반적인 BBS
				$this->return_result->list = array();
				($this->adminAuth==1)?$auth='auth':$auth='';
				
				if($this->arr_key_val['searchfield']){//검색시에.
					$this->where .= "WHERE ".$this->arr_key_val['searchfield']." LIKE '%".$this->arr_key_val['searchVal']."%' ";
					$this->where .= "AND ".$this->arr_key_val['devidefield']."='".$this->arr_key_val['devideVal']."' ";
					$this->setPaging($this->where);
					$this->where .= " ORDER BY no DESC LIMIT ".$this->sendHeader->startno." , ".$this->sendHeader->lastno;
				}elseif($this->arr_key_val['devidefield']){//분류 시에
					$this->where = "WHERE ".$this->arr_key_val['devidefield']."='".$this->arr_key_val['devideVal']."' ";
					$this->setPaging($this->where);
					$this->where .= " ORDER BY no DESC LIMIT ".$this->sendHeader->startno." , ".$this->sendHeader->lastno;
				}else{//일반 쿼리
					$this->setPaging();
					$this->where .= "ORDER BY no DESC LIMIT ".$this->return_result->list_info->startno." , ".$this->return_result->list_info->lastno;
				}
				
				$result = parent::_select($this->tableName,$this->where);
				
				$i= 0;
				while($list = mysqli_fetch_object($result)){
					$this->return_result->list[$i] = $list;
					$this->return_result->list[$i]->auth  = $auth;
					$i++;
				}
			break;
			
			case 'json_table_all'://전체 데이터를  리스트화 해서 넘긴다.
				$this->return_result->el = array();
				$this->where = " WHERE no";
				$result;//db에서 가져올 결과를 담을 변수
				
				$result = parent::_select($this->tableName, $this->where);
				
				$i= 0;
				while($list = mysqli_fetch_object($result)){
					$this->return_result->el[$i] = $list;
					$i++;
				}
			break;
			
			case "select_1"://최상위 하나만 전달
				return parent::_select_1($this->tableName, $this->where, $this->field = '*');
				break;
				
			case "returnJson"://최상위 하나만 전달
				return parent::_select_json_1($this->tableName, $this->where, $this->field = '*');
				break;
				
			case "searchJson":
				if(!$this->field) $this->field = "*";
				$this->return_result =  parent::_select_json_search($this->tableName, $this->where, $this->field);
				break;
				
			case "searchJson_2":
				if(!$this->field) $this->field = "*";
				$this->return_result =  parent::_select_json_search_2($this->tableName, $this->where, $this->field);
				break;
				
			case "returnCntJson":
				$this->return_result =  parent::_returnCnt_json($this->tableName, $this->where, $this->field);
				break;
				
			case "returnTotalCnt":
				$this->return_result =  parent::_returnCnt($this->tableName, $this->where);
				break;
		}
		
		return $this->return_result;
	}

	protected function setPaging($cntWhere='WHERE no'){
		$this->return_result->list_info;
		
		$selectPage = (int)$this->arr_key_val['page'];//유저가 선택한 페이지
		//$selectPage = 1;//유저가 선택한 페이지
		$list_num = (int)$this->arr_key_val['listNum'];//출력할 리스트 수량
		
		$total_article = parent::_returnCnt($this->tableName, $cntWhere);//전체 글수
		$total_pg = ceil($total_article/$list_num);//전체 페이지
		$startno = $list_num*($selectPage-1);//현재 페이지의 시작 번호
		$lastno = $list_num;//현재페이지의 끝번호
		$cur_num = $total_article - $list_num*($selectPage-1);//현재 글번호
		
		$page_num = 5;//한화면에 보여질 페이지수
		$totalBlock = ceil($total_pg / $list_num);//전체 블럭값
		$cur_block = ceil($selectPage / $page_num);//현재블럭
		$firstBlock = ($cur_block-1)*$page_num;//블럭이 시작하는 첫 페이지
		$lastBlock = $cur_block*$page_num;//블럭의 끝번호
		
		$this->return_result->list_info->cur_num = $cur_num;
		$this->return_result->list_info->list_num = $list_num;
		$this->return_result->list_info->total_article = $total_article;
		$this->return_result->list_info->total_pg = $total_pg;
		$this->return_result->list_info->startno = $startno;
		$this->return_result->list_info->lastno = $lastno;
		$this->return_result->list_info->page_num = $page_num;
		$this->return_result->list_info->totalBlock = $totalBlock;
		$this->return_result->list_info->cur_block = $cur_block;
		$this->return_result->list_info->firstBlock = $firstBlock;
		$this->return_result->list_info->lastBlock = $lastBlock;
		$this->return_result->list_info->selectPage = $selectPage;
		
		// var_dump($total_article);
		// var_dump($list_num);
		// var_dump($selectPage);
		// var_dump($startno);
		//echo '//';
		//var_dump($startno+$list_num);
		//var_dump(($startno*2)+$list_num);
		//echo '//';
		//exit;
		$i = 0;
		for ($page_link = $firstBlock+1; $page_link<= $lastBlock ; $page_link++ ){
				if($page_link <=$total_pg){
					$this->sendHeader->page_link->$i->{'page_link'} = $page_link;
					$i++;
				}
		} //*==/페이지 링크
		unset($i); 
	}

}
//session_destroy();
?>




































