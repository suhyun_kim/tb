<?php
class FileDelete extends Db{
	private $query;
	private $where;
	private $whereColumn;
	private $tableName;
	private $delKey;
	private $delVal;
	private $delDir;
	
	public function __construct($host, $user, $password, $db){
		parent::__construct($host, $user, $password, $db);
	}
	
	//테이블 설 정 및 변수 설정.
	public function setQuery($query, $val) {
		$this->query = $query;
		$this->delVal = $val;
		$this->delType  = 'imgDel';
		switch($this->query){
			case "delPopupImg"://팝업이미지
				$this->delDir = $GLOBALS['TB_root_path'].$this->delVal;
				break;
		}
		$this->deleteFile();
	}
	
	public function file_2($url){
		if(file_exists($url)){//파일 유무 검사
			if(unlink($url)){//파일삭제
				return 'success';
			}else{
				return 'failed';
			}
		}else{
			return 'nothing';
		}
	}
	/*=============================================
	 * 파일 삭제
	 * params
	 * 	. $url : 파일의 서버상 경로
	 =============================================*/
	
	protected function deleteFile(){
		switch($this->delType){
			case "imgDel";
				//파일이 있을 경우 
				if(file_exists($this->delDir)){
					if(unlink($this->delDir)){//파일 삭제
						//$this->where = $this->delKey." = '".$this->delVal."'";//테이블 삭제
						//parent::_delete($this->tableName, $this->where);
					}else{
						parent::_result(false,"파일을 삭제할 수 없습니다.");
					}
				}else{
					parent::_result(false,"삭제할 파일이 존재 하지 않습니다.");
				}
			break;
		}
	}
}
?>