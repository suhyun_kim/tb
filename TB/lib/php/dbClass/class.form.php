<?php
session_start();
include_once $GLOBALS['TB_root_path'].'lib/php/dbClass/class.fileUpload.php';
include_once $GLOBALS['TB_root_path'].'lib/php/dbClass/class.fileDelete.php';
include_once $GLOBALS['TB_root_path'].'lib/php/plugin/pbkdf2.compat.php';

class FormDb extends Db{
	private $type;
	private $query;
	private $files;//파일데이터
	private $query_key = '';//문자화된 키를 저장
	private $query_va = '';//문자화된 값을 저장
	private $query_key_val = '';//키와 값의  값이 같이 있음.
	private $arr_key_val;
	private $query_where;//
	private $data = array();
	private $tableName;
	private $result_data;
	private $return_result;
	private $where;
	private $arrCnt;
	private $str_key;
	private $str_val;
	private $userInfo;
	private $resultArr;
	private $queryArr;//쿼리 문구의 키, 값이 저장되어 있는 배열
	private $isAdmin;
	
	private function insert($table, $data){
		$table = $table;
		$data = $data;//필드에 입력할 배열 형태의 데이터
		$key_val = '';
		
		$key_val = $this->changeKeyVal($data, 'insert');
		parent::_insert($table, $key_val['key'], $key_val['val']);
	}
	
	
	//회원가입
	private function join_user($data){
		$data = $data;
		$result= new stdClass;
		
		$data['join_time'] = $GLOBALS['now'];
		$data['join_ip'] = $_SERVER['REMOTE_ADDR'];
		$data['approach_time'] = $data['join_time'];
		$data['approach_ip'] = $data['join_ip'];
		
		if($data['member_grade'] === 'supervisor'){
			$data['nickname'] = '관리자';
		}
		
		$this->insert('TB_member', $data);
	}
	
	public function __construct($host, $user, $password, $db){
		parent::__construct($host, $user, $password, $db);
	}
	
	/*-  쿼리를 내부 변수에 입력 및 유효성 검사 -*/
	public function setQuery($post, $files = null){
		// var_dump($files);
		
		//var_dump("데이터 값 넘어온것 : ".$data_kery."\n".$data_val);
		$this->type = $post['reqType']; 
		$this->query = $post['reqQuery']; 
		unset($post['reqType']);
		unset($post['reqQuery']);
		$this->post = $post;
		$this->files = $files;
		//var_dump($this->post);
		
		if($this->post['TB_adminAuthKey']){//관리자 로그인일 경우 처
			if(parent::_adminAuth($this->post['TB_adminAuthKey'])){
				$this->isAdmin = true;
			}
			unset($this->post['TB_adminAuthKey']);
		}
		
		($this->type=='insert')?$this->insert_form(): $this->modify_form();
		
		return $this->return_result;
		
		unset($this->isAdmin);
		unset($this->type);
		unset($this->query);
		unset($this->data_key);
		unset($this->data_val);
		unset($this->query_key);
		unset($this->query_val);
		unset($this->query_key_val);
		unset($this->arr_key_val);
		unset($this->query_where);
		unset($this->data);
		unset($this->tableName);
		unset($this->result_data);
		unset($this->return_result);
		unset($this->where);
		unset($this->arrCnt);
		unset($this->str_key);
		unset($this->str_val);
		unset($this->userInfo);
	}
	
	protected function changeKeyVal($arr, $type){
		$returnArr = array();
		switch($type){
			case "insert":
				foreach($arr as $key => $val){
					$returnArr['key'] .= $key.", ";
					$returnArr['val'] .= " '".parent::_inputWord($val)."', ";
				}
				$returnArr['key'] = mb_substr($returnArr['key'],0 , -2, "UTF-8");
				$returnArr['val'] = mb_substr($returnArr['val'],0 , -2, "UTF-8");
			break;
			case "modify":
				$returnArr = '';
				foreach($arr as $key=>$val){
					$arr[$key] = parent::_inputWord($arr[$key]);
					$returnArr .= $key."='".$val."', ";
				}
				$returnArr = mb_substr($returnArr,0 , -2, "UTF-8");
			break;
		}
		return $returnArr;
	}
	/*=======================================
	 * 배열을 자동으로 query에 알맞은 형태로 만든다.
	 * params
	 * 	. $arr(array) : key나 배열 형태로 만들 배열
	 * 	. $type(string) : insert, modify 두개의 값을 가지고 
	 =======================================*/
	
	private function autoChangeKeyVal(){
		$this->queryArr = $this->changeKeyVal($this->post, $this->type);
		if($this->type=='insert'){
			$this->query_key = $this->queryArr['key'];
			$this->query_val = $this->queryArr['val'];
		}else if($this->type == 'modify'){
		}
	}
	/*================================================
	* 배열을 자동으로 query 형태로 만들기 
	*================================================*/
	
	
	/*- insert:폼 -*/
	protected function insert_form(){
		switch($this->query){
			case 'comment'://답글 작성하기
				$this->post['nick'] = $this->post['nick'];
				$this->post['id'] = ($_SESSION['user'])?$_SESSION['user']->id:'';
				$this->post['ip'] = $_SERVER['REMOTE_ADDR'];
				$this->post['writetime'] = $GLOBALS['now'];
				
				if($this->post['password']){
					$this->post['password'] = md5($this->post['password']);
					$this->post['salt'] = create_hash($this->post['password']);
				}
				//$enc_password = pbkdf2_default('sha256', $this->post['password'], $this->post['salt'], 20, 35);
				//var_dump($this->post);
				//var_dump($this->post['password']);
				//phpinfo();
				//return false;
				
				parent::_update('TB_bbs', "comment_cnt = comment_cnt+1"," no='".$this->post['parent_no']."' ");				
				$this->insert('TB_bbs_comment', $this->post);
				
				$comment_where = "WHERE parent_no='".$this->post['parent_no']."' ORDER BY no ASC ";
				$this->return_result->list = parent::_select_list('TB_bbs_comment', $comment_where, 'no, parent_no, nick, writetime, memo');
				$this->return_result->result = 'success';
				return $this->return_result;
			break;
			
			case 'backdrop_popup'://백드롭 팝업
				if(!$_SESSION['user']||$_SESSION['user']->member_grade != 'supervisor'){
					return false;
				}
				$fileUploadData = array();
				$fileSrc = '';
				$this->tableName = 'TB_backdrop_popup';
				if($this->files){
					$fileupload = new FileUpload(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					$query['uploader'] = '관리자';
					$query['reqQuery'] = $this->query;
					$result = $fileupload->setQuery($query, $this->files);
					if(!$result['src']){
						return $this->return_result->dbError = '파일 업로드 에러.';
					}
					$this->post['src'] = $result['src'];
				}else{
					return $this->return_result->dbError = '파일 업로드 에러';
				}
				$this->insert($this->tableName, $this->post);
				return $this->return_result->result = 'success';
			break;
			
			case 'qna'://문의
			case 'postscript'://후기
			case 'notice'://요트.보트 판매
				$this->tableName = 'TB_bbs';
				if($this->query === 'notice'){
					$this->post['_group'] = 'notice';
				}elseif($this->query === 'postscript'){
					$this->post['_group'] = 'postscript';
				}elseif($this->query === 'qna'){
					$this->post['_group'] = 'qna';
				}
				$this->post['nick'] = $this->post['nick'];
				$this->post['id'] = $_SESSION['user']->id;
				$this->post['ip'] = $_SERVER['REMOTE_ADDR'];
				$this->post['writetime'] = $GLOBALS['now'];
				if($this->post['hidden'] === 'on'){$this->post['hidden'] = 1;}
				
				if($this->post['password']){
					$this->post['password'] = md5($this->post['password']);
					$this->post['salt'] = create_hash($this->post['password']);
				}
				//$enc_password = pbkdf2_default('sha256', $this->post['password'], $this->post['salt'], 20, 35);
				//var_dump($this->post);
				//var_dump($this->post['password']);
				//phpinfo();
				//return false;
				
				$this->insert($this->tableName, $this->post);
				return $this->return_result->result = 'success';
			break;
			
			case 'yacht_sale'://요트.보트 판매
				$this->tableName = 'TB_bbs';
				$this->post['_group'] = 'yacht_sale';
				$this->post['nick'] = $_SESSION['user']->nickname;
				$this->post['id'] = $_SESSION['user']->id;
				$this->post['ip'] = $_SERVER['REMOTE_ADDR'];
				$this->post['writetime'] = $GLOBALS['now'];
				
				$this->insert($this->tableName, $this->post);
				return $this->return_result->result = 'success';
			break;
	
			case 'about--yacht_album'://요트 사진 입력
				$query = array();
				$fileupload;
				$result;
			
				if($this->files){
					$fileupload = new FileUpload(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					$query['uploader'] = '관리자';
					$query['reqQuery'] = $this->query;
					$result = $fileupload->setQuery($query, $this->files);
					if(!$result['src']){
						return $this->return_result->dbError = '파일 업로드 에러.';
					}
				}else{
					return $this->return_result->dbError = '파일을 찾을 수 없습니다.';
				}
				
				$this->post['src'] = $result['src'];
				$this->post['file_name'] = $result['name'];
				$this->post['writer'] = '관리자';
				$this->post['writer_ip'] = $_SERVER['REMOTE_ADDR'];
				$this->post['write_time'] = $GLOBALS['now'];
				$this->insert('dream_yacht_album', $this->post);
				return $this->return_result->success = true;
			break;
			
			case 'createAdmin'://어드민 정보 입력
				$this->post['member_grade'] = 'supervisor';
				$this->post['password'] = md5($this->post['password']);
				$this->join_user($this->post);
				return $this->return_result['result']='createAdmin';
			break;
			
			case 'directEl_image'://다이렉트 이미지
				$this->tableName = 'TB_el';
				$fileSrc;//리턴받을 파일 경로
				$fileUploadData = array();//파일 업로드에 사용될 데이터
				// if(!$this->isAdmin){//관리자 로그인이 아닐경우
					// return $this->return_result->dbError = '관리자로 로그인하여 주십시요.';
				// }
				if($this->post['style']){
					$this->post['style'] = stripslashes($this->post['style']);
				}
				if($this->post['attr']){
					$this->post['attr'] = stripslashes($this->post['attr']);
				}
				if($this->post['memo']){
					$this->post['memo'] = stripslashes($this->post['memo']);
				}
				$this->post['writer'] = '관리자';
				$this->post['writetime'] = $GLOBALS['now'];
				if($this->files){//파일이 있을 때
					$fileUploadData['uploader'] = '관리자';
					$fileUploadData['reqQuery'] = $this->query;
					$fileupload = new FileUpload(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					$fileSrc = $fileupload -> setQuery($fileUploadData, $this->files);
					if($fileSrc['src']){
						$this->post['src'] = $fileSrc['src'];
						$this->autoChangeKeyVal();
						$this->form_insert();
						$this->post['src'] = $fileSrc['src'];
					}else{
						return $this->return_result->dbError = '파일 업로드 에러';
					}
				}else if(!$this->files && $this->post['src']){//파일은 없고 주소만 있을 때.
					unset($this->post['file']);
					$this->autoChangeKeyVal();
					$this->form_insert();
					
				}else{//파일과 DB에 둘다 없을 때.
					return $this->return_result->dbError = '이미지 혹은 이미지주소를 선택해 주십시요. ';
				}
				return $this->return_result = $this->post;//데이터 반송
			break;
			
			case 'directEl_text'://다이렉트 텍스트
			case 'directEl_element'://다이렉트 엘리먼트
				$this->tableName = 'TB_el';
				if($this->post['style']){
					$this->post['style'] = stripslashes($this->post['style']);
				}
				if($this->post['attr']){
					$this->post['attr'] = stripslashes($this->post['attr']);
				}
				if($this->post['memo']){
					$this->post['memo'] = stripslashes($this->post['memo']);
				}
				$this->post['writer'] = '관리자';
				$this->post['writetime'] = $GLOBALS['now'];
				$this->autoChangeKeyVal();
				$this->form_insert();
				return $this->return_result = $this->post;
			break;
			
			case 'createSlider'://슬라이더 생성
				$this->tableName = 'TB_templates_slider';
				$this->post['writer'] = '관리자';
				$this->post['writetime'] = $GLOBALS['now'];
				$this->return_result->TB_dbMessage='슬라이더 생성';
			break;
			
			case 'slide_add'://슬라이드 이미지 업로드
				$fileUploadData = array();
				$fileSrc = '';
				$this->tableName = 'TB_templates_slider_images';
				if($this->files){
					$fileUploadData['uploader'] = '관리자';
					$fileUploadData['reqQuery'] = $this->query;
					$fileupload = new FileUpload(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					$fileSrc = $fileupload -> setQuery($fileUploadData, $this->files);
					$this->post['src'] = $fileSrc['src'];
				}else{
					return $this->return_result->dbError = '파일 업로드 에러';
				}
				$this->post['writer'] = '관리자';
				$this->post['writetime'] = $GLOBALS['now'];
			break;
			
			case 'insert_sale_item'://요트 판매 
			case 'insert_gallery'://일반 갤러리
			case 'insert_gallery_baby'://신생아 갤러리
			case 'TB_bbs_insert'://TB_bbs : 빌딩 갤러리 사진
				$this->tableName = 'TB_bbs';
				$this->post['nick'] = '관리자';
				if(!$this->post['_group']){
					if($this->query === 'insert_buildingGallery'){
						$this->post['_group'] = 'building_gallery';
					}elseif($this->query === 'TB_bbs_insert'){
						$this->post['_group'] = 'treatment_clinic';
					}
				}
				$this->post['writetime'] = $GLOBALS['now'];
				$this->post['ip'] = $_SERVER['REMOTE_ADDR'];
				
				if($this->files){//파일이 있을 경우.
					$fileUploadData['uploader'] = '관리자';
					$fileUploadData['reqQuery'] = 'TB_bbs_image';
					$fileupload = new FileUpload(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					$fileSrc = $fileupload->setQuery($fileUploadData, $this->files, 'thumbnail');
					$this->post['file_src'] = $fileSrc['src'];
					$this->post['file_name'] = $fileSrc['name'];
					$this->post['thumbnail_src'] = $fileSrc['thumbnail_src'];
				}
				
				if($this->post['use_memo_in_images'] == 'true'){//비동기 이미지 업로드일 경우. TB_imageTable 업로드 완료 표시.
					$images = explode('||', $this->post['images_name']);
					for($i = 0, $len = count($images); $i < $len; $i++){
						parent::_update('TB_imageTable', "async_upload='uploaded'", "name='".$images[$i]."' AND category='async_image' ");
					}
					unset($this->post['use_memo_in_images']);
				}
				
				
				if($this->post['memo']){
					$this->post['memo'] = stripslashes($this->post['memo']);
				}
			break;
			
			case 'insert_list_bbs_1'://bbs_1 글작성
				$this->tableName = 'TB_bbs';
				$this->post['nick'] = ($this->isAdmin)?'관리자' : $this->post['nick'];
				$this->post['writetime'] = $GLOBALS['now'];
				$this->post['ip'] = $_SERVER['REMOTE_ADDR'];
				$images = array();
				
				if($this->files){//파일이 있을 경우.
					$fileUploadData['uploader'] = $this->post['nick'];
					$fileUploadData['reqQuery'] = 'TB_bbs_file';
					$fileupload = new FileUpload(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					$fileSrc = $fileupload -> setQuery($fileUploadData, $this->files);
					$this->post['file_src'] = $fileSrc['src'];
					$this->post['file_name'] = $fileSrc['name'];
					unset($fileSrc);unset($fileUploadData);
				}
				
				if($this->post['memo']){
					$this->post['memo'] = stripslashes($this->post['memo']);
				}
				
				unset($this->post['file']);
			break;
			
			case 'comment_insert_list_bbs_1'://bbs_1 코멘트 작성
				$this->tableName = 'TB_bbs_comment';
				$this->post['writetime'] = $GLOBALS['now'];
				$this->post['ip'] = $_SERVER['REMOTE_ADDR'];
				//var_dump($this->post);
				$where = "WHERE parent_no='".$this->post['parent_no']."' ";
				$cmt_cnt = parent::_returnCnt($this->tableName, $where);
				$cmt_cnt++;
				$where = "no='".$this->post['parent_no']."' ";
				parent::_update('TB_bbs', "comment_cnt='".$cmt_cnt."' ",$where);
				
				$this->return_result->insert_success = true;
				unset($where); unset($cmt_cnt); 
			break;
			
			case 'insert_async_image'://비동기 이미지 업로드 후 파일 받기.
				$this->tableName = 'TB_imageTable';
			//var_dump($this->files);
				if(!$this->files){
					return $this->return_result->$dbError = '파일이 존재 하지 않습니다.';
				}
				$fileUploadData['uploader'] = ($TB_isAdmin)?'관리자':'unknown';
				$fileUploadData['reqQuery'] = 'async_image';
				$fileupload = new FileUpload(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
				//$fileSrc = $fileupload->setQuery($fileUploadData, $this->files, 'thumbnail');
				$fileSrc = $fileupload->setQuery($fileUploadData, $this->files);
				$this->return_result->async_image = true;
				$this->return_result->src =$this->post['src'] = $fileSrc['src'];
				$this->post['name'] = $fileSrc['name'];
				//$this->return_result->thumbnail_src =$this->post['thumbnail_src'] = $fileSrc['thumbnail_src'];
				//$this->post['category'] = 'async_image';
				$this->post['uploader'] = $fileUploadData['uploader'];
				$this->post['uploadTime'] = $GLOBALS['now'];
				$this->insert($this->tableName, $this->post);
				$item = parent::_select_1($this->tableName, "WHERE src='".$this->post['src']."' ", 'no');
				$this->return_result->originalname = $this->post['name'];
				$this->return_result->no = $item->no;
				return $this->return_result;
				unset($fileSrc);unset($fileUploadData);
			break;
			
			case 'visitor'://방문자 정보
				if($_SESSION['user']->member_grade === 'supervisor '){return false;}
				$this->tableName  = 'TB_visitor';
				$this->post['datetime'] = $GLOBALS['now'];
				$this->post['date'] = explode(' ', $GLOBALS['now']);
				$this->post['year'] = explode('-', $this->post['date'][0]);
				$this->post['year'] = $this->post['year'][0];
				$this->post['date'] = $this->post['date'][0];
				$this->post['day'] = $GLOBALS['day'];
				$this->post['month'] = $GLOBALS['month'];
				$this->post['ip'] = $_SERVER['REMOTE_ADDR'];
				$this->post['browser'] = $_SERVER['HTTP_USER_AGENT'];
				//$this->post['before_url'] = $_SERVER["HTTP_REFERER"];
				
				$where = "WHERE ip='".$this->post['ip']."' AND  date='".$this->post['date']."' ";
				$cnt = parent::_returnCnt($this->tableName, $where);
				
				if($cnt > 0){
					return false; 
					exit;
				}
			break;
			
			case 'insert_reservation'://예약 등록
				$this->tableName = 'reservation';
				$this->post['writetime'] = $GLOBALS['now'];
				$this->post['status'] = '입금대기';
			break;
			
			case 'toggle_time_selector'://예약완료 시간 선택
				$this->tableName = 'reservation_time_complete';
				if($this->post['status'] === '0'){//추가
					unset($this->post['status']);
				}elseif($this->post['status'] === '1'){//삭제
					$where = "year ='".$this->post['year']."' AND month='".$this->post['month']."' AND date='".$this->post['date']."' ";
					$where .= "AND start_time='".$this->post['start_time']."' ";
					parent::_delete('reservation_time_complete', $where);
					return false;
				}
			break;
			
		}

		if(!$this->query_key || !$this->query_val){//query에서 autoChangeKeyVal()를 실행 안시켰을 경우.
			$this->autoChangeKeyVal();
		}
		//var_dump($this->query_val);
		//return false;
		$this->form_insert();
	}
	
	/*- modify : 데이터 수정 -*/
	protected function modify_form(){
		$no =$this->post['no']; 
		$this->where = "no=".$no;
		switch($this->query){
			case 'comment':
				$result = '';
				$confirm = false;
				$item;
				
				$item= parent::_select_1('TB_bbs_comment', "WHERE no='".$no."' ", 'id, password, salt');
				/*
				var_dump($this->post);
				var_dump($item);
				var_dump($_SESSION['user']);
				*/
				//관리자가 아닐경우.
				if(!$_SESSION['user'] || $_SESSION['user']->member_grade != 'supervisor'){
					
					//관리자문서, 패스워드가 없는 경우
					if($item->id === 'asidong' || !$this->post['password']){
						 return $this->return_result = false;
					}else if($item->id != 'asidong' && $this->post['password']){
						$confirm = validate_password(md5($this->post['password']), $item->salt);
						
						//암호가 틀렸을 경우.
						if($confirm === false){
							return $this->return_result = false;
						}
						
						unset($this->post['password']);
					}
				}
				
				
				/*
				//관리자 문서인데 접속자가 관리자가 아닐경우
				if($item->id === 'asidong' && (!$_SESSION['user'] || $_SESSION['user']->member_grade != 'supervisor')){
					return $this->return_result = false;
					
				//관리자 문서가 아니면서 암호입력이 없이 수정 요청이 들어 왔을 경우.
				}else if($item->id != 'asidong' && !$this->post['password']){
					return $this->return_result = false;
				
				//관리자 문서가 아니면서 암호입력이 들어왔을 경우 유효성 검사.				
				}else if($item->id != 'asidong' && $this->post['password']){
					$confirm = validate_password(md5($this->post['password']), $item->salt);
					
					//암호가 틀렸을 경우.
					if($confirm === false){
						return $this->return_result = false;
					}
					
					unset($this->post['password']);
				}
				 */
				
				//$this->post['hidden'] = ($this->post['hidden'] == 'on')?'1':'';
					
				$this->tableName = 'TB_bbs_comment';
				$where = "WHERE no='".$no."' ";
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
				$this->query_key_val .= ", modifytime ='".$GLOBALS['now']."' ";
				//var_dump($this->query_key_val);
				//return false;
				$this->form_modify();
				$this->return_result->item = parent::_select_1('TB_bbs_comment', "WHERE no='".$no."' ", 'no, parent_no, nick, writetime, memo ');
				$this->return_result->result = 'success';
				return $this->return_result;
				//비밀 번호 유효성 검사
				
				
			break;
			
			
			case 'backdrop_popup'://백드롭 팝업
				$fileUploadData = array();
				$fileSrc = '';
				$this->tableName = 'TB_backdrop_popup';
				if($this->files){
					//기존 파일 삭제
					$old_file = parent::_select_1($this->tableName, "WHERE no='".$no."' ", 'src' );
					$delete = new FileDelete(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					$delete->file_2($GLOBALS['TB_root_path'].$old_file->src);
					//새로운 파일 업로드
					$fileUploadData['uploader'] = '관리자';
					$fileUploadData['reqQuery'] = $this->query;
					$fileupload = new FileUpload(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					$fileSrc = $fileupload -> setQuery($fileUploadData, $this->files);
					$this->post['src'] = $fileSrc['src'];
				}
				unset($this->post['file']);
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
				parent::_update($this->tableName, $this->query_key_val, $this->where);
				return $this->return_result = array(parent::_select_1($this->tableName, 'WHERE no="'.$no.'" '));
			break;

			
			case 'qna'://
			case 'postscript'://
			case 'notice'://
				$result = '';
				$confirm = false;
				$item= parent::_select_1('TB_bbs', "WHERE no='".$no."' ", 'id, password, salt');


				//관리자가 아닐경우.
				if(!$_SESSION['user'] || $_SESSION['user']->member_grade != 'supervisor'){
					
					//관리자문서, 패스워드가 없는 경우
					if($item->id === 'asidong' || !$this->post['password']){
						 return $this->return_result->TB_dbMessage = '관리자가 아닙니다.';
					}else if($item->id != 'asidong' && $this->post['password']){
						$confirm = validate_password(md5($this->post['password']), $item->salt);
						
						//암호가 틀렸을 경우.
						if($confirm === false){
							return $this->return_result->TB_dbMessage = '암호가 틀렸습니다.';
						}
						
						unset($this->post['password']);
					}
				}

				/*
				//관리자 문서인데 접속자가 관리자가 아닐경우
				if($item->id === 'asidong' && (!$_SESSION['user'] || $_SESSION['user']->member_grade != 'supervisor')){
					return $this->return_result->TB_dbMessage = '관리자가 아닙니다.';
					
				//관리자 문서가 아니면서 암호입력이 없이 수정 요청이 들어 왔을 경우.
				}else if($item->id != 'asidong' && !$this->post['password']){
					return $this->return_result->TB_dbMessage = '암호를 입력하여주십시요';
				
				//관리자 문서가 아니면서 암호입력이 들어왔을 경우 유효성 검사.				
				}else if($item->id != 'asidong' && $this->post['password']){
					$confirm = validate_password(md5($this->post['password']), $item->salt);
					
					//암호가 틀렸을 경우.
					if($confirm === false){
						return $this->return_result->TB_dbMessage = '암호가 틀렸습니다.';
					}
					
					unset($this->post['password']);
					
				}
				 * */
				
				$this->post['hidden'] = ($this->post['hidden'] == 'on')?'1':'';
					
				$this->tableName = 'TB_bbs';
				$where = "WHERE no='".$no."' ";
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
				$this->query_key_val .= ", modifytime ='".$GLOBALS['now']."' ";
				$this->form_modify();
				
				return $this->return_result->result = 'success';
			break;
			
			case 'yacht_sale'://기본 파일 삭제
				$result = '';
				$this->tableName = 'TB_bbs';
				$where = "WHERE no='".$no."' ";
				
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
				$this->query_key_val .= ", modifytime ='".$GLOBALS['now']."' ";
				$this->form_modify();
				return $this->return_result->result = 'success';
			break;
			
			case 'about--yacht_album'://앨범 수정
				$result = '';
				$this->tableName = 'dream_yacht_album';
				$where = "WHERE no='".$no."' ";
				
				if(count($this->files) >0){//파일이 있을 경우 기존 파일 삭제
					$old_file = parent::_select_1($this->tableName, "WHERE no='".$no."' ", 'src');
					$delete = new FileDelete(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					if($old_file->src){
						$delete_result = $delete->file_2($GLOBALS['TB_root_path'].$old_file->src);
					}
					
					if($delete_result == 'failed' ){//기존 파일삭제 실패시
						return $this->return_result->dbError = '파일 삭제 실패';
					}

					$fileupload = new FileUpload(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					$query['uploader'] = '관리자';
					$query['reqQuery'] = $this->query;
					$result = $fileupload->setQuery($query, $this->files);
					
					//var_dump($result);
					if($result['src']){
						$this->post['src'] = $result['src'];
					}else{
						return $this->return_result->dbError = '파일 업로드 에러.';
					}
					
					unset($old_file); unset($delete);
				}
				unset($this->post['files']);
				
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
				$this->query_key_val .= ", modifier ='관리자' ";		
				$this->query_key_val .= ", modifier_ip ='".$_SERVER['REMOTE_ADDR']."' ";		
				$this->query_key_val .= ", modify_time ='".$GLOBALS['now']."' ";		
				unset($this->post['files']);
			break;
			
			case "saveConfig":
				// var_dump($this->post);
				$this->tableName = 'TB_config';
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
				$configCnt = parent::_returnCnt($this->tableName);
				
				if($configCnt<=0){//작성된 설정이 없을 경우 설정생성.
					parent::_insert($this->tableName); 
				}
				$config = parent::_select_1($this->tableName);//최상위 글을 불러온다.
				$this->where = "no='".$config->no."' ";
				$this->return_result->TB_dbMessage = "저장에 성공하였습니다.";
				parent::_update($this->tableName, $this->query_key_val, $this->where);
				return $this->return_result->menu = parent::_select_1($this->tableName);
			break;
			
			case 'directEl_image'://동적 엘리먼트 수정
				$this->tableName = 'TB_el';
				unset($this->post['file']);
				if($this->post['style']){
					$this->post['style'] = stripslashes($this->post['style']);
				}
				if($this->post['attr']){
					$this->post['attr'] = stripslashes($this->post['attr']);
				}
				if($this->post['memo']){
					$this->post['memo'] = stripslashes($this->post['memo']);
				}
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
				$this->where = "name='".$this->post['name']."' AND src='".$this->post['src']."' ";
				parent::_update($this->tableName, $this->query_key_val, $this->where);
				return $this->return_result = $this->post;
			break;
			
			case 'directEl_text':
			case 'directEl_element'://동적 엘리먼트 수정
				$this->tableName = 'TB_el';
				$this->post['style'] = stripslashes($this->post['style']);
				if($this->post['attr']){
					$this->post['attr'] = stripslashes($this->post['attr']);
				}
				if($this->post['memo']){
					$this->post['memo'] = stripslashes($this->post['memo']);
				}
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
				$this->where = "name='".$this->post['name']."' ";
				parent::_update($this->tableName, $this->query_key_val, $this->where);
				return $this->return_result = $this->post;
			break;
			
			case 'modifySlider'://슬라이더 수정
				$this->tableName = 'TB_templates_slider';
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
				$this->query_key_val .= ", modifier='관리자', modifytime='".$GLOBALS['now']."' ";
				$this->where = "name='".$this->post['name']."' ";
			break;
			
			case 'modify_list_bbs_1':
			case 'modify_gallery'://갤러리 수정.
			case 'TB_bbs_modify'://빌딩 갤러리 수정
			case 'modify_buildingGallery'://빌딩 갤러리 수정
				$this->tableName = 'TB_bbs';
				$fileType = 'image';
				if($this->query == 'modify_gallery'){
					$fileType = 'thumbnail';
				}
				//var_dump($this->post);
			
				if($this->query == 'modify_list_bbs_1'){
					$where; 
					$cmt_cnt;
					$cnt;
					if(!$this->isAdmin){//관리자 로그인이 아닐경우는 패스워드 체크.
						if(!$this->post['password']){
							return $this->return_result->dbError = '패스워드를 입력해 주십시요';
						}
						
						$where = "WHERE no='".$no."' AND password='".$this->post['password']."' ";
						$cnt = parent::_returnCnt($this->tableName, $where);
						
						if($cnt <= 0){//패스워드가 맞지 않을 경우.
							return $this->return_result->dbError = '패스워드를 확인해 주십시요.';
						}
					}
					
					$where = "WHERE parent_no='".$no."' ";
					$cmt_cnt = parent::_returnCnt('TB_bbs_comment', $where);//전체 코멘트수 획득
					$this->post['comment_cnt'] = $cmt_cnt;
					unset($where); unset($cnt);unset($cmt_cnt);
				}

				//메모내에 이미지 삽입이 되어 있는 경우. 빠진 이미지와 새로들어온 이미지를 처리.
				if($this->post['use_memo_in_images']){
					$og; $new; $inOut = new stdClass;
					unset($this->post['use_memo_in_images']);
					$item = parent::_select_1($this->tableName, "WHERE no='".$no."' ", 'images, images_name, images_thumbnail_src' );
					$og = explode('||',$item->images_name);
					$new = explode('||',$this->post['images_name']);
					function arrInOut($ogArr, $arr){
						$result  = new stdClass;
						$result->in = array_diff_assoc ($arr, $ogArr);
						$result->inCnt = count($result->in);
						$result->out = array_diff_assoc ($ogArr, $arr);
						$result->outCnt = count($result->out);
						return $result;
					}
					$inOut =  arrInOut($og, $new);
					if($inOut->outCnt >0){//삭제할 것이 있을 경우 삭제.
						foreach ($inOut->out as $key => $value) {
							$this->deleteImageTable($value);
						}
					}
					if($inOut->inCnt > 0){
						foreach ($inOut->out as $key => $value) {
							parent::_update('TB_imageTable', "async_upload='uploaded'", "name='".$value."' AND category='async_image' ");
						}
						
					}
					unset($og); unset($new); unset($inOut); unset($this->post['use_memo_in_images']);
				}
				
				if(count($this->files) >0){//파일이 있을 경우 기존 파일 삭제
					$old_file = parent::_select_1($this->tableName, "WHERE no='".$no."' ", 'file_src' );
					$delete = new FileDelete(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					$delete_result = $delete->file_2($GLOBALS['TB_root_path'].$old_file->file_src);
					if($delete_result == 'failed' ){//기존 파일삭제 실패시
						return $this->return_result->dbError = '파일 삭제 실패';
					}
					$fileUploadData['uploader'] = '관리자';
					$fileUploadData['reqQuery'] = 'TB_bbs_image';
					$fileupload = new FileUpload(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					$fileSrc = $fileupload->setQuery($fileUploadData, $this->files, $fileType);
					$this->post['file_src'] = $fileSrc['src'];
					$this->post['file_name'] = $fileSrc['name'];
					$this->post['thumbnail_src'] = $fileSrc['thumbnail_src'];
					//var_dump($this->post);
					unset($old_file); unset($delete);
				}
				unset($this->post['file']);
				
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
				$this->query_key_val .= ", modifytime ='".$GLOBALS['now']."' ";
			break;
			
			case 'modify_list_bbs_1'://bbs_1 에 해당하는
				 $this->tableName = 'TB_bbs';
				$where; 
				$cmt_cnt;
				$cnt;
				if(!$this->isAdmin){//관리자 로그인이 아닐경우는 패스워드 체크.
					if(!$this->post['password']){
						return $this->return_result->dbError = '패스워드를 입력해 주십시요';
					}
					
					$where = "WHERE no='".$no."' AND password='".$this->post['password']."' ";
					$cnt = parent::_returnCnt($this->tableName, $where);
					
					if($cnt <= 0){//패스워드가 맞지 않을 경우.
						return $this->return_result->dbError = '패스워드를 확인해 주십시요.';
					}
				}
				
				$where = "WHERE parent_no='".$no."' ";
				$cmt_cnt = parent::_returnCnt('TB_bbs_comment', $where);//전체 코멘트수 획득
				$this->post['comment_cnt'] = $cmt_cnt;
				
				if(count($this->files) >0){//파일이 있을 경우 기존 파일 삭제
					$old_file = parent::_select_1($this->tableName, "WHERE no='".$no."' ", 'file_src' );
					$delete = new FileDelete(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					if($old_file->file_src){
						$delete_result = $delete->file_2($GLOBALS['TB_root_path'].$old_file->file_src);
					}
					if($old_file->thumbnail_src){
						$delete_result = $delete->file_2($GLOBALS['TB_root_path'].$old_file->thumbnail_src);
					}
					if($delete_result == 'failed' ){//기존 파일삭제 실패시
						return $this->return_result->dbError = '파일 삭제 실패';
					}

					$fileUploadData['uploader'] = '관리자';
					$fileUploadData['reqQuery'] = 'TB_bbs_file';
					$fileupload = new FileUpload(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					$fileSrc = $fileupload -> setQuery($fileUploadData, $this->files);
					$this->post['file_src'] = $fileSrc['src'];
					$this->post['file_name'] = $fileSrc['name'];
					
					unset($old_file); unset($delete);
				}
				unset($this->post['file']);
				
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
				$this->query_key_val .= ", modifytime ='".$GLOBALS['now']."' ";
				
				unset($where); unset($cnt);unset($cmt_cnt);
			break;
			
			/*
			case 'modify_gallery':
				$where; 
				$bbs_item;
				$this->tableName = 'TB_bbs';
				//var_dump($this->post);
				//return false;
				
				if(count($this->files) > 0){//파일이 있을 경우 삭제
					$where = "WHERE _group='".$this->post['_group']."' ";
					$where .= " AND no='".$this->post['no']."' ";
					$bbs_item = parent::_select_1($this->tableName, $where, 'file_src, thumbnail_src');
					var_dump($where);
					return false;
					if($bbs_item->file_src){
						echo '파일 존재';
						$this->fileDelete_2($bbs_item->file_src);
					}
					if($bbs_item->thumbnail_src){
						echo '썸네일 존재';
						$this->fileDelete_2($bbs_item->thumbnail_src);
					}
					$fileUploadData['uploader'] = '관리자';
					$fileUploadData['reqQuery'] = 'TB_bbs_image';
					$fileupload = new FileUpload(TB_HOST, TB_USER, TB_PASSWORD, TB_DB);
					$fileSrc = $fileupload -> setQuery($fileUploadData, $this->files);
					$this->post['file_src'] = $fileSrc['src'];
					$this->post['file_name'] = $fileSrc['name'];
					$this->post['thumbnail_src'] = $fileSrc['thumbnail_src'];
				}
				unset($this->post['file']);
				
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
				$this->query_key_val .= ", modifytime ='".$GLOBALS['now']."' ";
				//var_dump($this->post);
			break;
			 */
			
			case 'comment_modify_list_bbs_1'://코멘트 리스트 수정
				$cmt_cnt; $where;
				$this->tableName = 'TB_bbs_comment';
				if(!$this->isAdmin){//관리자 로그인일 경우 패스워드 체크 안함.
					$this->where .= " AND password='".$this->post['password']."' ";
				}
				unset($this->post['password']);
				
				$where = "WHERE parent_no='".$this->post['parent_no']."' ";
				$cmt_cnt = parent::_returnCnt($this->tableName, $where);
				$where = "no='".$this->post['parent_no']."' ";
				parent::_update('TB_bbs', "comment_cnt='".$cmt_cnt."' ",$where);
				
				$this->post['modifytime'] = $GLOBALS['now'];
				$this->post['ip'] = $_SERVER['REMOTE_ADDR'];
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
				unset($cmt_cnt); unset($where);
			break;
			
			case 'modify_status_change'://예약 상태 수정
				$this->tableName = 'reservation';
				$this->post['modifytime'] = $GLOBALS['now'];
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
			break;
			
			case 'modity_reservation':
				$this->tableName = 'reservation';
				$this->post['modifytime'] = $GLOBALS['now'];
				$this->query_key_val = $this->changeKeyVal($this->post, $this->type);
			break;
			
		}
		$this->form_modify();
	}

	private function fileDelete_2($src){
		$dir = $GLOBALS['TB_root_path'].$src;
		if(file_exists($dir)){//파일 유무 검사
			if(unlink($dir)){//파일삭제
				return 'success';
			}else{
				return 'failed';
			}
		}else{
			return 'nothing';
		}
		unset($dir);
	}
	/*=============================================
	 * 파일 삭제
	 =============================================*/
	 
	 private function deleteImageTable($name){
	 	$item = parent::_select_1('TB_imageTable', 'WHERE name="'.$name.'" ', 'name, src, thumbnail_src');
		$this->fileDelete_2($item->src);
		if($item->thumbnail_src){
			$this->fileDelete_2($item->thumbnail_src);
		}
		parent::_delete('TB_imageTable', "name='".$name."' ");
	 }
	/*=============================================
	 * TB_imageTable에 있는 파일 삭제
	 * params
	 * 	$name(string) : 삭제할 파일명
	 =============================================*/
	
	private function form_insert(){
		parent::_insert($this->tableName, $this->query_key, $this->query_val);
	}/*==/insert 실행 함수 ==*/
	
	private function form_modify(){
		parent::_update($this->tableName, $this->query_key_val, $this->where);
	}/*==/modify 실행 함수 ==*/

	/*== 유효성 검사==*/
	protected function validityChk(){
		if($this->type=="insert")parent::_insert($this->tableName, $this->query_key, $this->query_val);
		else if($this->type =="modify") parent::_update($this->tableName, $this->query_key_val, $this->where);
	}
	
}
?>


















