<?php
session_start();
class Select_seayacht extends Select {

	public function __construct($host, $user, $password, $db) {
		Db::__construct($host, $user, $password, $db);
	}
	
	protected function reservation_list($year, $month, $marina_no){
		//echo $year.', '.$month.', '.$marina_no;
		return parent::_list_json('reservation_callendar', " WHERE year='".$year."' AND month='".$month."' AND marina_no='".$marina_no."' ");
	}
	/*============================
	 * 예약이 꽉 찼는지 가져온다.
	 * params
	 * 	$year (int) : 년도
	 * 	$month (int) : 월
	 * 	$marina_no
	 * return
	 * 	. json 배열 형태
	 ============================*/
	 
	protected function reservation_complete_time($year, $month, $date, $marina_no){
		return parent::_list_json('reservation_time_complete', " WHERE year='".$year."' AND month='".$month."' AND date='".$date."'  AND marina_no='".$marina_no."' ");
	}
	/*============================
	 * 예약한 날짜의 시간중 비활성화 시간을 가져온다.
	 ============================*/
	
	protected function deptDuty($dept = ''){
		$dept_no = parent::_select_1('department', "WHERE department_name='".$dept."' ", 'no');
		return parent::_list_json('department_1', " WHERE parentNo=".$dept_no->no, 'deptDuty');
	}
	/*============================
	 * 부서값에 따라 담당업무를 가져온다.
	 * params
	 * 	$dept(str) : 부서값 ex)원무 행정과
	 * return
	 * 	. json 배열 형태
	 ============================*/
	
	protected function department($field = 'department_name'){
		return parent::_list_json('department', '', $field);
	}
	/*============================
	 * 부서 리스트
	 * params
	 * 	$field(str) : 어느 필드를 리턴할지 결정 '*' 값으로 전체 정보 리턴
	 * return
	 * 	. json 배열 형태
	 ============================*/
	 
	protected function working_list($employee_no, $status = 0, $order='DESC', $field = '*'){
		return parent::_list_json('workLog', "WHERE writerNo='".$employee_no."' AND  status=".$status." ORDER BY no ".$order, $field);
	}
	/*============================
	 * 현재 진행중인 업무 리스트 가져오기
	 * params
	 * 	$employee_no(int) : 작성자 정보
	 * 	$status : 0(작업중)
	 * return
	 * 	. json 배열 형태
	 ============================*/
	 
	protected function working_detail($employee_no, $article_no){
		return parent::_list_json('workLog', "WHERE no='".$article_no."' ");
	}
	/*============================
	 * 현재 진행중인 업무 세부 정보 가져오기
	 ============================*/
	 
	protected function repeat_work_list($where = '', $field = '*'){
		$where_query = 'WHERE ';
		$cnt = count($where);
		$i = 0;
		if($cnt >0){
			foreach($where as $key => $val){
				$where_query .=  ($i < $cnt-1) ? $key."='".$val."' AND " : $key."='".$val."' ";
				$i++;
			}
		}
		return parent::_list_json('schedule', $where_query, $field);
	}
	/*============================
	 * 반복업무 리스트 와 기본 정고 가여오기. 가져오기.
	 * params
	 ============================*/
	
}

//session_destroy();

?>

