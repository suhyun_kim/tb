<?php
class TB_func{
	public function __construct(){
		$this->create_json_encode();
	}
   /*====================================
    * 생성자
    ====================================*/
	
	/*-- var_dump를 정렬해서 출렭 --*/
	public function prnt_alignVar($var) {
		echo "<pre style='font-size:11pt;'>";
		var_dump($var);
		echo "</pre>";
	}
	
	public function is_email($address){
		if( eregi("([a-z0-9\_\-\.]+)@([a-z0-9\_\-\.]+)", $address) ) return true;
		else return false; 
	}
	/*=====================================
	 * 메일이 맞는지 검사
	 * params
	 * 	.$address(str) : 검사할 메일 주소
	 * return 
	 * 	.true or false
	 =====================================*/
	
	/*-- php 메모리 사용량 출력하기 얻기 --*/
	public function prnt_memory() {
		echo "메모리 사용량 : " . memory_get_usage() . 'Byte(' . (memory_get_usage() / 1000) . 'KB)';
	}
	
	/*=======================
	 * 두개의 문자를 arr[str_key] = arr_val로 배열화 해서 반환한다. 
	 * params
	 * 	. $str_key(string) : key가 될 문자열
	 * 	. $str_val(string) : value가 될 문자열
	 * 	. $delimiter(string) : 문자를 자를 구분자. 기본값은 '||'
	  =======================*/
	public function TB_str_arrRe($str_key, $str_val, $delimiter = "||")
	{
		$arr_key = array();
		$arr_val = array();
		$arr = array();
		$arr_key = explode($delimiter, $str_key);
		$arr_val = explode($delimiter, $str_val);
		
		if (count($arr_key) != count($arr_val)) {
			$arr = "배열의 길이가 같지 않습니다.";
		} else {
			$cnt = count($arr_key);
			for ($i = 0; $cnt > $i; $i++) {
				$arr[$arr_key[$i]] = $arr_val[$i];
			}
		}
		
		$arr_key = null;
		$arr_val = null;
		$arr_key = null;
		$arr_val = null;
		return $arr;
	}
	
	/*=======================
	 * 서버의 최상단 위 경로를 반환. 
	  =======================*/
	public function TB_upperRoot()
	{
		$path = explode("/", $_SERVER[DOCUMENT_ROOT]);
		array_pop($path);
		$path = implode('/', $path);
		return $path."/";
	}
	
	/*=======================
	 * 복호화 
	  =======================*/
	 public function decrypt_md5($hex_buf, $key="password"){
	    $len = strlen($hex_buf);
	    for ($i=0; $i<$len; $i+=2)
	        $buf .= chr(hexdec(substr($hex_buf, $i, 2)));
	    
	    $key1 = pack("H*", md5($key));
	    while($buf)
	    {
	       $m = substr($buf, 0, 16);
	       $buf = substr($buf, 16);
	       
	       $c = "";
	       for($i=0;$i<16;$i++)
	       {
	           $c .= $m{$i}^$key1{$i};
	       }
	       
	       $ret_buf .= $m = $c;
	       $key1 = pack("H*",md5($key.$key1.$m));
	    }
	    
	    return($ret_buf);
	}
	    
	/*=======================
	 * 암호화 
	  =======================*/
	public function encrypt_md5($buf, $key="password")
	{
	    $key1 = pack("H*",md5($key));
	    while($buf)
	    {
	        $m = substr($buf, 0, 16);
	        $buf = substr($buf, 16);
	        
	        $c = "";
	        for($i=0;$i<16;$i++)
	        {
	            $c .= $m{$i}^$key1{$i};
	        }
	        $ret_buf .= $c;
	        $key1 = pack("H*",md5($key.$key1.$m));
	    }
	    
	    $len = strlen($ret_buf);
	    for($i=0; $i<$len; $i++)
	        $hex_data .= sprintf("%02x", ord(substr($ret_buf, $i, 1)));
	    return($hex_data);
	}
	
	public function encrypt_md5_base64($plain_text, $password="password", $iv_len = 16){
	    $plain_text .= "\x13";
	    $n = strlen($plain_text);
	    if ($n % 16) $plain_text .= str_repeat("\0", 16 - ($n % 16));
	    $i = 0;
	    while ($iv_len-- >0)
	    {
	        $enc_text .= chr(mt_rand() & 0xff);
	    }
	    
	    $iv = substr($password ^ $enc_text, 0, 512);
	    while($i <$n)
	    {
	        $block = substr($plain_text, $i, 16) ^ pack('H*', md5($iv));
	        $enc_text .= $block;
	        $iv = substr($block . $iv, 0, 512) ^ $password;
	        $i += 16;
	    }
	    return base64_encode($enc_text);
	}
	
	public function decrypt_md5_base64($enc_text, $password="password", $iv_len = 16){
	    $enc_text = base64_decode($enc_text);
	    $n = strlen($enc_text);
	    $i = $iv_len;
	    $plain_text = '';
	    $iv = substr($password ^ substr($enc_text, 0, $iv_len), 0, 512);
	    while($i <$n)
	    {
	        $block = substr($enc_text, $i, 16);
	        $plain_text .= $block ^ pack('H*', md5($iv));
	        $iv = substr($block . $iv, 0, 512) ^ $password;
	        $i += 16;
	    }
	    return preg_replace('/\x13\x00*$/', '', $plain_text);
	}
	
	public function create_json_encode(){
		if(!function_exists("json_encode")){ 
		  function json_encode($a=false){
				if(is_null($a)) return 'null'; 
				if($a === false) return 'false'; 
				if($a === true) return 'true'; 
				if(is_scalar($a)){ 
					if(is_float($a)) return floatval(str_replace(",", ".", strval($a))); 
					if(is_string($a)){ 
						static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"')); 
						return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"'; 
					} else return $a; 
				} 
				$isList = true; 
				for($i=0, reset($a); $i<count($a); $i++, next($a)){ 
					if(key($a) !== $i){ 
						$isList = false; 
						break; 
					} 
				} 
				$result = array(); 
				if($isList){ 
					foreach($a as $v) $result[] = json_encode($v); 
					return '[' . join(',', $result) . ']'; 
				} else{ 
					foreach($a as $k => $v) $result[] = json_encode($k).':'.json_encode($v); 
					return '{' . join(',', $result) . '}'; 
				} 
			} 
		}	
	}
	/*================================================
	 * JSON이 없는 구버전 php일 경우 json 함수를 생성한다.
	 ================================================*/
	
}

?>