var TB_contents = (function($, TB_shared, TB){
	var cache = {}, sendData = []
			, isAdmin;

	var impt = function($el){
		if($el instanceof jQuery){//jquery 오브젝트 일 경우.
			var src = $el.attr('data-TB_contents_src').split('--');
			var $target = $($el.attr('data-TB_contents_target'));
			var sendData = $el.attr('data-TB_contents_sendData')||null;
		}else if($.type($el)==='object'){//일반 오브젝트일 경우
			var src = $el.src.split('--');
			var $target =$($el.target);
			var sendData = ($.type($el.sendData)==='string')?$el.sendData : JSON.stringify($el.sendData);
		}
		var category='', type='', file='';
		// console.log($el);
		// console.log($target);

		if($target.length <= 0) return false;

		if(src.length === 3){//카테고리, 타입 , 파일
			category = src[0], type=src[1], file=src[2];
		}else if(src.length===2){//두개만 지정할 경우 자동으로 컨텐츠를 지정.
			 category='contents', type=src[0], file=src[1];
		}

		$target
		.attr('data-TB_contents_src', src[0]+'--'+src[1]+'--'+src[2])
		.attr('data-TB_contents_sendData', sendData);
		load($target);
	};
	/*=============================================================
	 *컨텐츠 임포트
		 타겟이 되는 엘리먼트의 속성을 변화 시킨뒤 해당 엘리먼트를 로드 시킨다.
	  * params
	  *  - $el (jQuery Element): 클릭이나 액션이 발생한 엘리먼트
	  =============================================================*/
	 

	 var getToParams = function(data){
	 	var _var = '', arr = [], obj = {}, _var_2 = [], i = 0, len = 0;
		arr = data.substring(1).split('&');
		len = arr.length;
		for(;i < len ; i++){
			_var_2 = arr[i].split('=');
			obj[_var_2[0]]  = _var_2[1];
		}
	 	return obj;
	 };/*GET형식의 데이터를 오브젝트로 변환
	 	*params
	 	* 	- data : GET형식의 데이터 ex) ?a=45
	    */

	var render = function(_data, _callback){
		var obj = {};
		obj = _data['obj']||_data['param'];
		//console.log(_data);
		if(_data['obj']){//obj일 경우 기본 정보 삽입
			_data['obj'].TB_userInfo = TB_config.get('user');//유저 정보
			_data['obj'].TB_root_path = TB_config.get('TB_root_path');//최상위 경로.
		}
		// console.log(_data['html']);
		TB_handlebars.render({
			html : _data['html']
			, obj : obj
			, outputEl : _data['parent']
		}, function($el){
			var $render = _data['parent'].children('div');
			if(typeof _callback ==='function') _callback($render);
			// console.log(_data['parent'].attr('class'));
			TB.binding($render);
			TB.eBinding($render);
		});
	};/*템플릿 형태등을 랜더링 한다.
	  * params
	  * 	. _data(obj) : 렌더링 자료에 사용될 HTML, obj 파일
	  * 		- _data['html'](string) : '랜더링 소스 HTML
	  * 		- _data['obj'](obj): 랜더링 소스 obj(이것이 우선시 되어 랜더링된다.)
	   													수동으로 추가 시켜 줘야함.
	  * 		- _data['param'](obj) : 넘겨받은 파라메터
	  * 		- _data['parent'](jQueryObj) : 부모 jquery 엘리먼트
	  * 	. _callback(func) : 콜백함수
	  */

	var sendDataConver = function(data){
		var firstText
			, type = $.type(data)
			;
		
		if(type === 'string'){
			firstText = data.substring(0,1);
			if(firstText==='{'){
				return $.parseJSON(data.replace(/\s/gi, ''));//JSON
			}else if(firstText === '?'){
				return  getToParams(data.replace(/\s/gi, ''));//GET ->obj
			}else{ //일반 단어일 경우는 공유 오브젝트를 넘긴다.
				return  TB_shared.get(data);
			}
		}
		
		return data;
		
		
	};
	/*===============================
	 * string 형태의 sendData 를 오브젝트로 컨버트 한다.
	 ===============================*/

	var setContents = function(options, callback){
		var op = {
			el : ''
			, category : ''
			, type : ''
			, file : ''
			, sendData : ''
		};

		$.extend(op, options);
		//console.log(op.sendData);
		//console.log(op);
		var url = TB_config.get('TB_root_path')+op.category+'/'+op.type+'/'+op.file+'/'+op.file;
		var cacheName = op.category+'--'+op.type+'--'+op.file;
		var scriptName = cacheName+'--js';
		var data= getCache(cacheName)||undefined;
		var script= TB_shared.get(scriptName)||undefined;

		var runScript = function(_obj){
			var sc_type = $.type(script);

			//해당 이름으로 된  스크립트 가 캐쉬 되어 있을 경우
			if(sc_type === 'function'){
				script(_obj);
				if(typeof callback =='function')  callback(_obj);
			}
			//스크립트가 저장되어 있으면서 on속성을 가질경우.
			else if(sc_type === 'object' && $.type(script.on)==='function'){
				script.on(_obj.param);
				if(typeof callback =='function')  callback(_obj);
			}
			//캐쉬된 스크립트가 없을 경우.
			else if(sc_type === 'undefined'){
				//console.log(url);
				$.ajax({
					url : url+'.js'
					, dataType : 'script'
					, success : function(_res){
						script= TB_shared.get(scriptName)||undefined;
						var sc_type = $.type(script);
						if(sc_type === 'function'){
							script(_obj);
						}else if(sc_type === 'object'&&$.type(script.init) === 'function'){
							script.init(_obj);
						}
						if(typeof callback =='function')  callback(_obj);
					}
					, error : function(e){
						throw  url+'.js'+' file error \n'+e.status;
					}
				});
			}
		};
		/*===============================
		 * 스크립트를 실행 시킨다.
		 ===============================*/


		var output = function(){
			var obj = {};
				obj['html'] = data,//내부의 HTML 코드
				obj['outerHTML'] = op.el[0].outerHTML,//자기 사진을 포함한 HTML 코드
				obj['parent'] = op.el//부모 객체
				, obj['obj']//정의 되어 있지 않다가 나중에 임의로 정의 한다.
				;
			//console.log(op);
			obj.parts = create_parts(obj);//파츠 관련 셋팅
			//console.log(obj);
			//obj.parts.init(obj)

			if(op.sendData){//넘길 데이터가 있을 경우.
				var _var = '',
						fistText = '',
						sendDataType = $.type(op.sendData);
				if(sendDataType === 'string'){
					obj['param'] = sendDataConver(op.sendData);
				}else if(sendDataType === 'object' || sendDataType === 'array'){
					obj['param'] = op.sendData;
				}
				//console.log(obj);
				// runScript(obj);
			}
			op.el
			.html(data).promise().done(function(){
				runScript(obj);
			});
		};/*==/output함수==*/
		
		

		if(data !== undefined){//데이터가 이미 있을 경우
			output();
		}else{//데이터가 없을 경우는 데이터를 받아와서 생성.
			//console.log(url);
			$.ajax({
				url : url+'.html'
				, type : 'GET'
				, async : true
				, dataType : 'html'
				, success : function(text, status){
					var cssTxt = ''
						, $text = $(text)
						;
					//script 로 감싸져 있을 경우 스크립트 열기
					// if($text[0].tagName==='SCRIPT'){
						// $text = $($text.html());
					// }
					
					//console.log(TB_config.get('css_load'));
					if(!TB_config.get('css_load')){//css를 먼저 로드 시켰을 경우 로드안함..
						cssTxt = "<link rel='stylesheet' href='"+url+".css'  />";
						///cssTxt += "<noscript><link rel='stylesheet' href='"+url+".css' /></noscript>";
					}
					data = $text.prepend(cssTxt)[0].outerHTML;
					/*==/CSS 로드==*/
					//console.log(cacheName, text);
					//console.log(data);


					setCache(cacheName , data);

					output();
					/*==/output==*/
				}, error: function(request,status,error){
					throw "code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error;
				}
			});
		}

	};
	/*=====================================================
	   	레이아웃, 템플릿등의 컨텐츠를 불러온다.
	   =====================================================*/

	var setCache = function($name, $data){
		if(arguments.length) cache[$name] = $data;
	};/*컨텐츠등을 캐쉬
		*동일 이름이 있으면 캐쉬하지 않는다.
	   * params
	   * 	- $name(string) : 저장될 이름
	   *  - $data(string) : 저장될 데이터
	   */

	var getCache = function($name){
		return (arguments.length&&cache[$name])? cache[$name].concat() : null;
	};/*$name에 해당하는 데이터가 있을 경우 컨텐츠 데이터 리턴
		*	params
		*		- $name(string) : 캐쉬되어 있는 데이터의 이름 형식은 "type &file"
		*	return
		*		- string
	   */


	  var load = function($el){
	  	//console.log($el instanceof $);
	  	//console.log($el instanceof jQuery);
	  	//console.log($el.html());

  		if($el instanceof $){//jquery  객체일 경우
  			if(!$el.length){return false;}
  			var src = $el.attr('data-TB_contents_src').split('--');
  			var category = src[0];
  			var type = src[1];
  			var file = src[2];
  			var sendData = $el.attr('data-TB_contents_sendData');
  		}else if($.type($el)==='object'){//일반 오브 젝트일 경우
  			var category = $el.category;
  			var type = $el.type;
  			var file = $el.file;
  			var sendData = $el.sendData;
  			var $el = $($el.target);
  		}
	  	 //console.log('asdfasdf');
			 //console.log($el[0].outerHTML);

  		setContents(
			{
				el : $el,
				category : category,
				type : type
				, file : file
				, sendData : sendData
	  		},function(){//콜백
				if($el.find('.TB_contents_load').length ){
		  			init($el);
				}
	  		}
			);
	  };
	  	/*컨텐츠 로드(init와 다른점은 단 한번만 로드한다.)
	     * params
	     * 	. $el(jQuery Eliment) :  컨텐츠를 로드.
	     * elAttr
	     * 	. data-TB_contents_category : 로드할 데이터의  카테고리
	     * 		- value : layout, contents, template
	     * 	. data-TB_contents_type : 로드할 데이터의 타입
	     * 		- value : category 하위의 폴더명
	     * 	. data-TB_contents_file : 로드할 데이터의 파일명.
	     * 		- value :  html,css,js 확장자를 밴 파일명을 적어 놓는다.
	     * 	. data-TB_contents_sendData: 템플릿 로드 등에 사용될 변수. 템플릿을 로드하기 전에 정의 되어야 한다.
	     * 		- value :  expression
	     */

	  var init = function($el){
	  	var $scope = $el||$(document);
	  	//console.log($scope.find('.TB_contents_load').length);
	  	$scope.find('.TB_contents_load')
		  	.each(function(){
		  		var $this = $(this);
	  			var src = $this.attr('data-TB_contents_src').split('--');
	  			var category = src[0];
	  			var type = src[1];
	  			var file = src[2];
		  		var sendData = $this.attr('data-TB_contents_sendData');
		  		//console.log($('#contents_wrap').html());
		  		setContents({
		  			el : $this
		  			, category : category
		  			, type : type
		  			, file : file
		  			, sendData : sendData
		  		},function(_conext){//콜백
		  			TB.binding($this);
		  			TB.eBinding($this);
		  			if($this.find('.TB_contents_load').length > 0 ){//로드할것이  남았을 때
			  			init($this);
		  			}
		  		});
		  	});
	  };/*컨텐츠 초기화
	     * params
	     * 	. $el(jQuery Eliment) :  컨텐츠를 로드할 엘리먼트 없을 경우는 document를 기준으로 한다.
	     * elAttr
	     * 	. data-TB_contents_category : 로드할 데이터의  카테고리
	     * 		- value : layout, contents, template
	     * 	. data-TB_contents_type : 로드할 데이터의 타입
	     * 		- value : category 하위의 폴더명
	     * 	. data-TB_contents_file : 로드할 데이터의 파일명.
	     * 		- value :  html,css,js 확장자를 밴 파일명을 적어 놓는다.
	     * 	. data-TB_contents_sendData: 템플릿 로드 등에 사용될 변수. 템플릿을 로드하기 전에 정의 되어야 한다.
	     * 		- value :  expression
	     */

	var binding = function($el){
		var $scope = $el||$(document);
		//init($scope);
	};/*==================================
		 * 컨텐츠 관련 바인딩
		 ==================================*/

	var eBinding  = function($el){
		var $scope = $el||$(document);
		$scope
			.off('click', '.TB_contents_import')
			.on('click', '.TB_contents_import', function(e){
				e.cancelBubble = true;
    			if(e.stopPropagation) e.stopPropagation();
				impt($(this));
			});
		/*==/컨텐츠 임포트==*/
	};
	/*==================================================
	 * 이벤트를 바인딩 한다.
	  ==================================================*/

	var create_parts = function(context){
		function Parts(context){
			var func_storage = {};//파츠 랜더링시 연동되어 같이 작동할 함수 등록

			this.storage = {};//html 소스를 저장할 객체
			this.topEl;//html을 jQuery로 변환한 객체
			this.parts;//TB_contents_parts를 배열로 보관하고 있는 객체
			this.context = {};//외부 context를 참조하는 객체
			this.context.obj = {};//랜더링에 사용될 객체
			this.context.parent = {};//
			this.context.html = '';
			/*==/변수 셋팅==*/

			this.init = function(context){
				this.context = context;
				//var context;
				//$.extend(true, context,context);
				if(context['html']){
					this.topEl = $(context.html);
					if(this.topEl[0].tagName === 'SCRIPT'){
						this.topEl = $(this.topEl.html());
					}
				}else if(context.parent instanceof jQuery && !context.html){
					this.topEl = context.parent;
				}else if(!context.parent && !context.html){
					return false;
				}
				//this.topEl = $(context['html']);
				this.parts = this.topEl.find('.TB_contents_parts');
				//console.log(this.topEl.attr('id'));
				//console.log(this.parts.length);

				this.storage = this.saveHTML();

				//console.log(this.storage);
			};
			/*==/초기화 ==*/

			this.renewal = function($topEl){
				if($topEl instanceof jQuery){
					this.topEl = $topEl;
					this.context['parent'] = $topEl;
					this.parts = $topEl.find('.TB_contents_parts');
				}
			};
			/*=================================
			 * jquery 객체 다시 셋팅
			 * 	- 다른 페이지 동일 template을 사용할 경우 객체가 지워지므로 half_bake같은 경우
			 *			최상위 객체를 다시 셋팅 해줘야 한다.
			 *			red-marine.co.kr의 커뮤니티와  정비 및 관리와의 코드를 참조.
			 =================================*/

			this.saveHTML = function(_context){
				//console.log(this.topEl);
				var html = []
					, render = this.render
					;
				//console.log(this.parts);
				if(!this.parts.length){ return false; }
				

				this.parts.each(function(){
					var $this = $(this)
						, parts_name = $this.attr('data-TB_contents_parts')
						, $script = $this.children('script')
						;
					
					html[parts_name]= ($script.length)?$script.html().trim():$this.html().trim();
					//console.log(parts_name);
					//console.log(html[parts_name]);
					//console.log(html)
					$this = null, parts_name = null;
				});
				$parts = null;
				return html;
			};
			/*==/context 내에 있는 파츠들 전부를 HTML 저장하기. 이때 초기 랜더링도 같이 실행==*/

			this.getHTML  = function(parts_name){
				if(this.storage[parts_name]){
					return TB_func.objCopy(this.storage[parts_name]);
				}
			};
			/*==/html 소스 리턴 받기==*/
			
			this.getRenderHTML = function(parts_name, obj){
				var html = this.getHTML(parts_name);
				if(html && $.type(obj) === 'object'){
					return TB_handlebars.returnRender(html, obj);
				}
			};
			/*==/html 소스 리턴 받기==*/

			this.render = function(_obj, _callback){
				var obj = {
							parts : undefined
							, obj : undefined
							, outputEl : undefined
							, callback : undefined
							, run_func : true //func 가 작동되는것을 결정한다. false일경우는 작동 안함.
						}
						, arr = []//여러개를 랜더링 해야 할 경우.
						, i = 0, j = ''
						, _partsType//파라메터가 오브젝트일 경우 parts타입 검사
						, type = $.type(_obj)
						;

				if(this.context){
					if(this.context['obj']){obj.obj = this.context['obj'];}
				}else{
					return false;
				}
				//console.log(this.context);
				if($.type(_callback) === 'function'){
					obj.callback = _callback;
				}else if(type === 'function'){//함수일 경우 전체 랜더링 후 함수 실행
					obj.callback = _obj;
					type = 'undefined';
				}

				//한개만됨, context['obj']를 랜더링
				 if(type === 'string'){
				 	obj.parts = _obj;

				 }
				 //객체일 경우 처리하기. obj 객체 지정 가능 없을 경우는 context['obj']
				 else if(type === 'object'){
				 	_partsType = $.type(_obj.parts);
				 	if(_partsType === 'string'){
					 	$.extend(obj, _obj);
				 	}
				 	else if(_partsType === 'array'){
				 		if(_obj.obj){ obj.obj = _obj.obj; }
					 	arr = _obj.parts;
				 	}
				 }
				 else if(type === 'array'){//배열일 경우 처리
				 	arr = _obj;
				 }else if(type === 'undefined'){//전체 랜더링
				 	for( j in this.storage){
				 		arr[i] = j;
				 		i++;
				 	}
				 }
				
				 if(arr.length){//여러개 랜더링 해야할 때
				 	for(var i =0, len = arr.length; i < len ; i++){
				 		obj.parts = arr[i];
					 	obj.outputEl = this.context['parent'].find('.TB_contents_parts[data-TB_contents_parts='+obj.parts+']');
					 	if(i == len-1){//마지막 랜더링을 할때 콜백 함수를 실행시킨다.
					 		//console.log($.type(_obj.callback));
							if(_obj &&$.type(_obj.callback) === 'function'){ obj.callback = _obj.callback;}
					 	}
						this.handlebars_render(obj);
					}
				 }
				 else{//하나 랜더링
					 	obj.outputEl = this.context['parent'].find('.TB_contents_parts[data-TB_contents_parts='+obj.parts+']');
					 	if(_obj &&$.type(_obj.callback) === 'function'){ obj.callback = _obj.callback;}
						this.handlebars_render(obj);
						
				 }
				//console.log(obj.callback.toString());
			};
			/*==/랜더링==*/

			this.handlebars_render = function(obj){
				var func = this.func;
				if(!this.storage[obj.parts]){//스토리지에 저장되어 있지 않으면 작동 안함.
					return false;
				}
				TB_handlebars.render({
					html : this.storage[obj.parts]
					, obj : obj.obj
					, outputEl : this.context['parent'].find('.TB_contents_parts[data-TB_contents_parts='+obj.parts+']')
				}, function($el){
					// console.log(_data['parent'].attr('class'));
					//console.log(obj);
					TB.binding(obj.outputEl);
					TB.eBinding(obj.outputEl);
					if(func_storage[obj.parts] && obj.run_func){
						func(obj.parts, obj.outputEl);
					}
					if(typeof obj.callback ==='function'){ obj.callback($el); }
				});
			};
			/*==/핸들바를 이용한 직접 랜더링==*/

			this.func = function(name, _func){
				var name_type = $.type(name)
						, _func_type = $.type(_func)
						, handlebars_render
						;
				 if(arguments.length ===2 ){
					if(name_type === 'string' && _func_type === 'function' ){//함수 등록
						func_storage[name] = _func;
					}
					else if(name_type === 'string' && func_storage[name]){//실행
						func_storage[name](_func);
					}
				}
				else if(arguments.length === 1){//실행
					if(name_type === 'string' && func_storage[name]){
						func_storage[name]();
					}
				}
				else if(!arguments.length){//전체 한번씩 실행하기.
					var func = this.func;
					this.context['parent'].find('.TB_contents_parts').each(function(){
						var $this = $(this)
								, parts_name = $this.attr('data-TB_contents_parts');
						if(func_storage[parts_name]){
							func(parts_name, $this);
						}
						$this = null, parts_name = null;
					});
					func = null;
				}
			};
			/*==/랜더링시 등록된 함수가 있으면 랜더링 후 실행.==*/

			this.init(context);//생성자 함수 실행
		}
		return new Parts(context);
	};
	/*===============================================
	 * dec
	 * 	. 엘리먼트 랜더링시에 context 객체를 이용해 part 명에 따라 랜더링
	 * 		할 수 있는 객체를 생성해서 반환한다.
	 * params
	 * 	. context(obj or string or array) : 랜더링의 위해 parts의 이름이 기록되어 있는 객체
	 ===============================================*/

	return {
		setContents : setContents
		, getCache : getCache
		, binding : binding
		, eBinding : eBinding
		, init : init
		, load : load
		, render : render
		, impt : impt
		, sendDataConver : sendDataConver
		, create_parts : create_parts
	};

})(jQuery, TB_shared, TB);
