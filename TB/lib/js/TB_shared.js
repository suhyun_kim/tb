var TB_shared = (function(TB_config){
	var cache = {};
	var func = function($func){
		$func();
	};
	
	var set = function(name, data){
		if(arguments.length) cache[name] = data;
	};
	
	var extend = function(name, data){
		// console.log(cache[name]);
		// console.log($.extend(cache[name], data));
		if(arguments.length&&cache[name]) cache[name] = $.extend(cache[name], data);
	};
	
	var get = function(name){
		return (arguments.length&&cache[name])? cache[name] : null;
	};
	
	var eBinding = function($el){
		var $scope = $el || $(document);
		$scope
			.off(TB_config.get('click'), '.TB_shared_func')
			.on(TB_config.get('click'), '.TB_shared_func', function(){
				e.cancelBubble = true;
    			if(e.stopPropagation) e.stopPropagation();
				var name = $(this).attr('TB_shared_func');
				TB_shared.get(name)();
			});
	}/*=================
	  * 이벤트 바인딩
	  * detail
	  * 	. 해당 엘리먼트를 클릭했을 때 TB_shared 에 해당 함수가 정의 되어 있다면 
	  * 		해당 함수를 실행한다.
	  ====================*/
	
	return {
		func : func
		, set : set
		, extend : extend
		, get : get
		, eBinding : eBinding
	}
})(TB_config);