var TB_handlebars = (function(Handlebars, $, TB_func, TB){
	var helper = function(){
		
		Handlebars.registerHelper('calc', function(lvalue, operator, rvalue){
			var lvalue = parseFloat(lvalue)
				, rvalue = parseFloat(rvalue)
				;
			
			if(operator === '+'){return lvalue + rvalue;}
			else if(operator === '-'){return lvalue - rvalue;}
			else if(operator === '*'){return lvalue * rvalue;}
			else if(operator === '/'){return lvalue / rvalue;}
			else if(operator === '%'){return lvalue % rvalue;}
			
		});
		/*=====================================================
		 * 계산을 한다.
		 * {{calc 5 '*' 80}}
		 =====================================================*/

		
		Handlebars.registerHelper('substr', function(text, start_num, end_num, add_text){
			var output = '';
			 output = text.substr(start_num, end_num);
			 if(add_text &&text.length > end_num){
			 	output += add_text;
			 } 
			return output;
		});/*==================================================
			문자 자르고 뒤에 문자 붙이기
			params
				. text(string) : 자를 문자열
				. start_nun(int) :  시작 인덱스
				. end_num(int) :  길이
				. add_text(string) : 문자열에 추가할 길이
		==================================================*/
		
		Handlebars.registerHelper('removeBackSlash', function(str) {
			if($.type(str) === 'string'){return str.replace(/\\/g, "");}
			else{return str;}
		});
		/*===================================================
		 * 백슬래쉬 삭제
		 * ===================================================*/
		
		Handlebars.registerHelper('inc_idx', function(_idx){
			if($.type(_idx) === 'number' && arguments.length ===2){
				this['inc_index'] = _idx+1;
			}
		});
		/*==/인덱스값에서  1씩 증가된 'inc_index'라는 변수를 추가: 리스트 출력을 위해 만든 변수.
		 * 헬퍼 명과 변수명이 겹치면 작동이 안되므로 따로 처리 했다. 
		 * ==*/
		
		Handlebars.registerHelper('setIndex', function(value){
			this.index = parseInt(value, 10);
		});/*==/부모 인덱스를 찾기 위한 인덱스 ==*/
		
		Handlebars.registerHelper('set', function(key, value){
			if($.type(key) === 'string' && arguments.length ===3){
				this[key] = value;
				arguments[arguments.length-1].data = value;
			}
			//console.log(arguments);
		});/*==/현재 객체에 key value로 해서 추가 ==*/
		
		Handlebars.registerHelper('set', function(key, value){
			if($.type(key) === 'string' && arguments.length ===3){
				this[key] = value;
				arguments[arguments.length-1].data = value;
			}
			//console.log(arguments);
		});/*==/현재 객체에 key value로 해서 추가 ==*/
		
		Handlebars.registerHelper('ifArray', function(value, array, options){
			return ($.inArray(value, array) >= 0)?options.fn(this):options.inverse(this);
		});
		/*================================
		 *	배열내에서 해당하는 요소가 있을경우 리턴 
		 * params
		 * 	. value(string) : 배열에서 찾을 값.
		 * 	. array(array) : 요소를 찾을 배열
		 * {{#ifArray 'text'  array}}테스트{{else}}당근{{/ifArray}}
		 ================================*/
		
		Handlebars.registerHelper('hasReturnVal', function(obj, hasVal, dontHaveVal, options){
			if(arguments.length === 3 && obj){
				return hasVal;
			}
			else if(arguments.length === 4){
				if(obj){ return hasVal;}
				else {return dontHaveVal;}
			}
		});
		/*================================
		 *	해당하는 오브젝트가 true 나 존재할 경우 val을 리턴한다. 
		 * params
		 * 	obj(object) : 값이있는지 확인할 오브젝트
		 * 	hasVal(string or int) : obj가 존재할 경우 리턴할 값.
		 * 	dontHaveVal(string or int) : obj 가 존재하지 않을 경우 리턴할 값
		 *	<input type="text"  {{hasReturnVal test 'has' 'none'}}/> 
		 ================================*/
		
		Handlebars.registerHelper('ifReturnVal', function(val_1, val_2, _if, _eles, options){
			if(arguments.length === 4 && val_1 == val_2){
				return _if;
			}
			else if(arguments.length === 5){
				if(val_1 == val_2){ return _if;}
				else {return _eles;}
			}
		});
		/*================================
		 *	val_1과 val_2를 비교해 같을 경우 _if를 리턴 아닐경우 _else를 리넡
		 ================================*/
		
		
		Handlebars.registerHelper('each-reverse', function(context, op){
			if(!context){return '';}
			var output = []
					, len = context.length-1
					, i = 0
					, lastIdx
					index =1;
			for(; i <= len ; len-- ){
				context[len]['_index'] = index;
				output += op.fn(context[len]);
				index++;
			}
			//console.log(output);
			
			return output;
		});/*==/리스트 역순 출력==*/
		
		Handlebars.registerHelper('entityDecode', function(context, options){
			var text = Handlebars.escapeExpression(context);
			return $('<div>').html(text).text();
		});/*==/엔티디 디코드==*/
		
		Handlebars.registerHelper('entityHtmlRemove', function(context, options){
			var text = Handlebars.escapeExpression(context);
			return text.replace(/\&lt\;.*?\&gt\;|&amp;nbsp;|&amp;lt;/gi, '');
		});/*==/엔티문자 중 &lt; &gt; 사이문자를 삭제한다., &nbsp;도제거==*/
		
		Handlebars.registerHelper('_if', function(v1,  devide, v2, options){
			//console.log(v1, devide, v2);
			if(devide == '=='){
				if(v1 ==  v2) return options.fn(this);
			}else if(devide == '!='){
				if(v1 !=  v2) return options.fn(this);
			}else if(devide === '%'){//나머지 연산, 배수가 될경우 출력
				if((v1 % v2) === 0) return options.fn(this);
			}else if(devide === '&gt;'){// > 
				if(v1> v2) return options.fn(this);
			}else if(devide === '&gt;='){// >=
				if(v1 >= v2) return options.fn(this);
			}else if(devide === '&lt;'){// <
				if(v1 < v2) return options.fn(this);
			}else if(devide === '&lt;='){ // <=
				if(v1 <= v2) return options.fn(this);
			}
			return options.inverse(this);
		});/*==/if===*/
		
	};/*헬퍼 들을 모아놓은 곳.
	   */
	  
	  
	Handlebars.registerHelper('list', function(context){
		var divider = arguments[0]//구분자
				, len = arguments.length//arguments의 길이
				, option = arguments[len-1]//옵션
				, output = ''//출력할 결과문
				, i  = 0//반복문의 시작 번호
				, length//반복문의 끝 번호
				, ctx//내부 item 값
				;
		if( divider =='&gt;'){// ' > '  다음 인자들을 순서대로 출력
			for (var i = 1, length = len-1; i < length ; i++){
				ctx = {};
				ctx['item'] = arguments[i];
				output += option.fn(ctx);
			}
		}else if(divider == '~' && len === 4){// '~' 숫자를 차례대로 출력
			i = arguments[1], length  =  arguments[2]
			for(; i <= length ; i++){
				ctx = {};
				ctx['item'] = i;
				output += option.fn(ctx);
			}
		}else if(divider == '*' ){// '* '  지정된 특정 형식 출력
			if(arguments[2] === 'today'){
				
			}
		}
		divider =null, len = null, option = null, i = null, length = null, ctx = null;
		return output;
	});
	/*==/인자로 받은 정보를 기준으로 리스트 출력==*/
	
	Handlebars.registerHelper('commSet', function(_number){
		var number = String(_number);
		return number.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
	});
	/*==/commSet==*/
	
	
	Handlebars.registerHelper('tagPrint', function(tag, attr, each){
		var print = ''
			, attr_print = '', each_print
			, i , len
			;
		
		for(i in attr){
			attr_print += ' '+i+'="'+attr[i]+'" ';
		}
		
		if(tag === 'input'){
			print = "<input "+attr_print+" />";
		}else if(tag === 'select'){
			for(i = 0 , len = each.length; i < len ; i++){
				each_print += "<option value='"+each[i].value+"' ";
				each_print += (each[i].selected)?" selected='selected'":'';
				each_print += (each[i].disabled)?" disabled ='disabled '":'';
				each_print += " >"+each[i].text+" </option>";
			}
			print = '<select '+attr_print+' >'+each_print+" </select>";
		}
		
		return new Handlebars.SafeString(print);
	});
	/*==/html 속성을 출력 할 수 있게 객체를 키, 값의 형태로 출력한다..
	 *	tag : (string) 태그
	 * obj : (obj) 속성 객체
 	* 	{
	 * 		tag : 'input'
	 * 		attr : {
	 * 			type : 'text'
	 *         , class : ''text_class
	 *         , name : 'input'
	 *     }
	 *		, each : [
				{value : '1호정', text : '1호정', selected : true}
				, {value : '2호정', text : '2호정'}
				, {value : '3호정', text : '3호정'}
			]
	 * 	}
	 * /==*/


	
	var returnRender = function(src, obj){
		var temp = Handlebars.compile(src);
		return temp(obj);
	};
	/*=============================
	 * 소스데이터를 랜더링 해서 리턴한다.
	 =============================*/
		
	var render = function(context, callback){
		var op = {
			html : context.html
			, obj : context.obj
			, outputEl : context.outputEl || null 
		};
		var temp = Handlebars.compile(op.html);
		var renderHTML = temp(op.obj)
			, $html = $(renderHTML);  
			;
		
		//console.log($html);
		//script 로 감싸져 있을 경우 스크립트 열기
		if($html.length && $html[0].tagName === 'SCRIPT'){
			renderHTML = 	$html.html();
		}
		
		if(op.outputEl){//지정된 jquery 엘리먼트가 있을 경우는 아웃풋을 한다.
			op.outputEl
			.html(renderHTML)
			.promise().done(function(){
				if($.type(callback)==='function')callback(context.outputEl);
			});
		}else{//지정된 엘리먼트가 없을 경우는 랜더링된 HTML을 리턴
			return temp(op.obj);
		}
	};/*HTML을 랜더링 한다.
	   * params
	   *  . context(obj) 
	   *   - html (string) : 랜더링 될 HTML 
	   *   - obj(obj) : 랜더링에 필요한 데이터 파일
	   *   - outputEl (jqueryObj): 랜더링된 HTML이 로드될곳의 jQuery 객체
	   * . callback(fucntion) : 콜백 함수 
	   */
		
	return {
		helper : helper
		, render : render
		, returnRender : returnRender
	};
})(Handlebars, jQuery, TB_func, TB);
