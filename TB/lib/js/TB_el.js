var TB_el = (function($,  TB_func, TB_config){
	var storage = {}//내부 객체 스토리지
			;
			
	var loadingBar = function($el, _op){
		var op = {
			'icon' : 'fa fa-2x fa-cog fa-spin'
			, 'text' : ''
		};
		$.extend(op, _op);		
		var txt = "<div id='loadingBarWrap' style='position:absolute;left:50%;margin-left:-1.5em;top:50%;margin-top:-1.5em;' >";
				txt += "<div style='color:#ff7a19;display:inline-block;' class='"+op.icon+"'></div>";
				txt += "<div class='text'>"+op.text+"</div>";
				txt += "</div>";
		$el.html(txt);
		txt = null, op= null;
	};
	/*=========================
	 * 폰트 어섬을 이용한 로딩바 생성
	 * params
	 *  . $el (jQuery Obj) : 로딩바를 생성한다.
	 *  . _op.icon : icon 클래스를 지정한다.
	 *  . _op.text :  로딩바 아래 텍스트를 집어 넣는다.
	 *  .  
	 =========================*/
	
	var sgStorage = function(_name, _obj){
		if(arguments.length===1&& typeof _name ==='string'){
			return storage[_name]?TB_func.objCopy(storage[_name]):null;
		}else if(arguments.length === 2){
			storage[_name] = _obj;
			//console.log(storage);
		}
	};
	/*=========================
	 * 스토리지에 저장과 가져오기
	 * params
	 * 	. _name(str) : 스토리지에 저장할 혹은 저장되어 있는 객체명
	 * 	. _obj(obj) : 저장할 객체명
	 =========================*/
	
	var createAttr = function(_obj){
		var obj = {},
				html = '',
				i = 0,
				j = 0, 
				len, 
				_len;
		obj = $.extend(obj, _obj);
		
		if(obj.attr){//속성 생성
			if(typeof obj.attr === 'string'){//속성이 string일경우 객체로 변경
				obj.attr = $.parseJSON(obj.attr);
			}
			
			for (i in obj.attr){
				if(obj.attr.hasOwnProperty(i)){
					if(i === 'src'&&obj.attr[i]){ 
						html += "src='"+TB_config.get('TB_root_path')+obj.attr[i]+"' ";
						//console.log(html);
					}else{
						if(obj.attr[i]){ html += i+"='"+obj.attr[i]+"' "; }
					}
				}
			}
		}
		if(obj.style){//스타일 생성
			if(typeof obj.style === 'string'){
				obj.style = $.parseJSON(obj.style);
			}
			html += "style='";
			for (i in obj.style){
				html += i+":"+obj.style[i]+"; ";
			}
			html += "' ";
		}
		
		i = null, len = null, j = null, _len = null, obj = null; 
		return html;
	};
	/*=========================
	 * 세부 속성과 스타일을 생성한다.
	 =========================*/
	
	var image = function(_obj, $el){
		var obj = {
					type : 'image',
					imageType : "src"
				},
				html = '';
		obj = $.extend(obj, _obj);
		if(obj.imageType==='src'){//일반 이미지 삽입
			html = "<img ";
			html += createAttr(obj);
			html += " />";
			$el.html(html);
		}else if(obj.type === 'background'){//백그라운드 일경우
			
		}
		//console.log(html);
	};
	/*=====================================
	 * 이미지 적용
	 =====================================*/
	
	var element = function(_obj, $el){
		if(_obj.style){
			$el.css($.parseJSON(_obj.style));
		}
	};
	/*=====================================
	 * 엘리먼트 적용
	 =====================================*/
	
	var text = function(_obj, $el){
		//console.log($el.parent().hasClass('wysiwyg-container'));
		if($el.parent().hasClass('wysiwyg-container')){//컨테이너가 적용되어 있을 경우.
			//$el.wysiwyg('shell').readOnly();
			// $el.unwrap().prev().remove().end()
			// .removeAttr('style').removeAttr('contenteditable');
		}
		//if($el.parent().)
		// $el.html(_obj.memo).addClass('wysiwyg-editor');
		$el.html(_obj.memo);
		TB_form.wysiwygSet($el,'view');
		//$el.html(_obj.memo).addClass('wysiwyg-editor');
	};
	/*=====================================
	 * 엘리먼트 적용
	 =====================================*/
	
	var dec = function($el){
		var name = $el.attr('data-TB_el_name'),
				attr = $el.attr('data-TB_el_attr'),
				obj = sgStorage(name);
				
		if(!obj&&attr){//스토리지에서 셋팅 하지 않고 엘리먼트내에서 접 셋팅 할때
			obj = $.parseJSON(attr);
			sgStorage(name, obj);
		}
		
		if(obj){
			$el.hide();
			
			if(obj.type ==='image'){
				image(obj, $el);
			}else if(obj.type === 'element'){
				element(obj,$el);
			}else if(obj.type === 'text'){
				text(obj,$el);
			}
			
			if(obj.showAni){
				TB_ani.flow(obj.showAni, $el);
			}else{
				$el.show();
			}
			/*==/오브젝트 쇼 애니==*/
			
		}
	};
	/*=========================
	 * 해석
	 =========================*/
	
	var binding = function($el){
		var $scope = $el||$(document);
		var $TB_el = $scope.find('.TB_el');
		if($TB_el.length){
			$TB_el.each(function(){
				dec($(this));
			});
		}
	};
	/*=========================
	 * 바인딩
	 =========================*/
	
	return {
		loadingBar : loadingBar,
		binding : binding, 
		sgStorage : sgStorage
	}
})(jQuery, TB_func, TB_config);
/*동적으로 생성하는 엘리먼트들
* 
*/
