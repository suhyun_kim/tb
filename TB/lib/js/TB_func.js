
	
var TB_func = {};

TB_func.scroll_to = function(target, $scrope, $scrolled_el){
	var 
		$scrolled_el = $scrolled_el || $(window)
		, $scope= $scrope||$scrolled_el
		, offset
		, $target = $scope.find(target) 
		;
	
	if(!$target.length){return false;}
	
	offset = $target.offset();
	$scrolled_el.scrollTop(offset.top);
}
/*============================================================
 * 해당하는 엘리먼트를 찾아 스크롤 한다.
 * params
 *  . target(string) : 대상이될 엘리먼트의 선택자 ex) '#id', '.class'
 *  . $scoper(jQuery object) : 대상을 찾을 범위 오브젝트
 *  . $scrolled_el(jQuery object) : 스크롤의 대상이 되는 엘리먼트 기본은 window 객체
 ============================================================*/

TB_func.json_arr_update = function(ori, res, field){
	var result = ori
			, i = 0, len = ori.length,j= 0
			, res_type = $.type(res)
			;
	
	if($.type(ori) != 'array' ||!res[field]){ return false; }
	
	for(;i < len ; i++ ){
		if(result[i][field] === res[field]){
			result[i]=  res;
		}
	}
	return result;
};
/*============================================================
 * field값이 일치하는 original을 업데이트 시킨배열을 리턴한다.
 * params
 * 	. ori(arr) : 업데이트의 배ㄹ
 * 	. res(object) : 업데이트 시킬 파일
 * 	. field (string) : 업데이트 기준 필드
 ============================================================*/

	
TB_func.json_arr_delete = function(ori, key, val){
	var result = ori
			, i = 0, len = result.length
			;
	if($.type(ori) != 'array' || !key || !val){return false;}
	
	for(;i < len ; i++){
		if(result[i][key] === val){
			result.splice(i, 1);
			break;
		}
	}
	//console.log(result);
	return result;
};
/*============================================================
 * field값이 일치하는 original을 업데이트 시킨배열을 리턴한다.
 * params
 * 	. ori(arr) : 업데이트의 대상파일
 * 	. res(object) : 업데이트 시킬 파일
 * 	. field (string) : 업데이트 기준 필드
 ============================================================*/




	/*===========================================================
	 인스턴스 생성시 직접적으로 생성 아이디 값이 노출되지 않도록 하기 위한 함수
	 * param
	 *  - $id : 대상 아이디값
	 * return expression
	 */
	TB_func.makeGetId = function($id) {
		return function() {
			return $id;
		}
	};
	
	
	TB_func.merge = function(){
	    var obj = {},
	        i = 0,
	        il = arguments.length,
	        key;
	    for (; i < il; i++) {
	        for (key in arguments[i]) {
	            if (arguments[i].hasOwnProperty(key)) {
	                obj[key] = arguments[i][key];
	            }
	        }
	    }
	    i = null, il = null, key = null;
	    return obj;
	};
	/*===========================================================
	 오브젝트 병합
	 * paranms
	 * 	- 갯수의 제한 없이 오브 젝트를 병합해서 리턴한다.
	 * return : obj
	 ===========================================================*/
	
	
	/*===========================================================
	 오브젝트 카피
	 * paranms
	 *  - $obj : 카피의 대상이될 오브젝트
	 * return : obj
	 */
	TB_func.objCopy = function($obj) {
		try {
			var obj = JSON.stringify($obj);
			obj = JSON.parse(obj);
			return obj;
		} finally {
			$obj = null, obj;
		}
	};
	
	/*===========================================================
	 * 플랫폼이 모바일 인지 확인.
	 * 모바일일 경우 true 아닐 경우 false
	 * return : boolean
	 */
	TB_func.isMobile = function() {
		var mobile = new Array('Android', 'iPhone', 'iPod', 'iPad',  'BlackBerry', 'Windows CE', 'SAMSUNG', 'LG', 'MOT', 'SonyEricsson');
		var i = 0, len = mobile.length ,result = false;
		for(; i < len ; i++) {
			if (navigator.userAgent.match(mobile[i])) {
				result =  true;
			}
		}
		return result;
	};
	
	/*===========================================================
	 * 스크롤에 관련된 이벤트를 lock 하거나 푼다.
	 * method
	 *  . disable : 마우스 스크롤 및 핸드폰에서의 touchmove이벤트도 disable 시킨다.
	 *  . enable : 스크롤 관련 이벤트를 활성화 시킨다.
	 */
	TB_func.mouseScroll = (function(){
		var disable = function(){
		    $(window).on("touchmove.disableScroll mousewheel.disableScroll DOMMouseScroll.disableScroll touchmove.disableScroll"
		    , function(e) {
		        e.preventDefault();
		        return;
		    });
		};/*==/스크롤 이벤트 잠그기==*/
		var enable = function(){
			$(window).off('.disableScroll');
		};/*==/스크롤 이벤트 활성화==*/
		return {
			enable : enable
			, disable : disable
		}
	})();
	
	/*===========================================================
	 * 브라우져 체크
	 * 브라우져의 버전을 체크 한다.
	 * return : string
	 */
	TB_func.ieChkVer = function() {
		var agent = navigator.userAgent.toLocaleLowerCase();
		
		//swing 브라우져 일경우.
		if(agent.indexOf('swing') > -1){
			alert('스윙브라우져는 옵션에서 \'스피드모드로 항상사용\'을 체크한뒤 사용해 주십시요');
			return false;	
		}
		
		//IE가 아니다.
		if (agent.indexOf('msie') == -1 && agent.indexOf('trident') == -1) return false;
		
		//호환성 보기일때 
		if (agent.indexOf('msie 7') > -1 && agent.indexOf('trident') > -1){
			var bStyle = document.body.style;
			var canvas = document.createElement('canvas');
			if (!('getContext' in canvas)) return 8;
			if (!('msTransition' in bStyle) && !('transition' in bStyle )) return 9;
			if (!canvas.getContext('webgl')) return 10;
			return 11;
		} else {// 호환성 보기가 아닐때
			if (agent.indexOf('msie') == -1) return 11;//ie11
			return parseInt(agent.substr(agent.indexOf('msie'), 6).substr(5), 10);//ie버전
		}
	};
	/*===========================================================
	 * 3자리 마다 콤마
	 * return : expression
	 */
	TB_func.commset = function(num){
		str = String(num);
		return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
	}
	
	/*===========================================================
	 * 3자리 마다 콤마 삭제
	 * return : expression
	 */
	TB_func.commUnset = function(num){
		str = String(num);
		return str.replace(/[^\d]+/g, '');
	}
	
	/*=========================
	 *trim함수
	 * 함뒤 공백을 제거
	 */
	TB_func.trim = function(value) {
		return value.replace(/^\s+|\s+$/g,"");
	};
	
	TB_func.removeBackslash = function(value) {
		return value.replace(/\\/gi, "" );
	};
	/*=========================
	 *백슬래시 제거.
	 =========================*/
	
	TB_func.unix_timestamp = function(){
		return Math.floor(new Date().getTime()/1000);
	};
	/*=========================
	 * 현재 시간을 유닉스 타입 스탬프로 리턴
	 =========================*/
	
	
	TB_func.convert_string_date = function(_date){
		if($.type(_date) != 'string'){return false;}
		var date = _date.split(' ')
				;
		date[0] = date[0].replace(/-|:/g,'/');
		if(date.length>0){ date = date.join(' '); }
		date = Date.parse(date);
		return date;
	};
	/*=========================
	 * 'yyyy-MM-dd hh:mm:ss'형태의 포맷을 date 형으로 변환
	 =========================*/
	
	TB_func.reverseKey = function(context){
		var i = '', len = '', keys = [], reverseKeys = [];
		for( i in context){
			keys.push(i);
		}
		len = keys.length-1;
		for(; 0 <= len; len--) {
			reverseKeys.push(String(keys[len]));
		}
		return reverseKeys;
	};
	/*=========================
	 *배열의 키를 역순으로 반환
	 * params 
	 * 	. context (arr) : 키를 역순으로 반환할 배열 
	 =========================*/
	
	TB_func.getDate = function(format){
		var date_format = format||'Y-M-D'
				, date = new Date()
				;
		
		if(date_format=='Y-M-D') 
			return date.getFullYear()+'-'+TB_func.addZero(date.getMonth()+1,2)+'-'+TB_func.addZero(date.getDate(), 2);
	};
	/*==================================================
	 * 지정된 포맷의  형태로 오늘 날짜를 출력 
	 * params
	 *  .dateForm.
	 *		- y-m-d: 2014-01-01
	 ==================================================*/
	
	TB_func.addZero = function(n, digits){
		var zero = '';
		n = n.toString();
		if(n.length < digits){
			for( var i = 0; i < digits-n.length ; i ++){
				zero += '0';
			}
		}
		return zero+n;
	}
	/*==================================================
	 * 숫자를 해당 자릿수에 미치지 못하면 앞에 0을 채워 넣는다.
	 * params
	 *  .n.
	 *		- 숫자
	 * . digits
	 * 	 - 자릿수
	 ==================================================*/
	
	 TB_func.getToParams = function(data){
	 	var _var = ''
	 			, arr = []
	 			, obj = {}
	 			, _var_2 = []
	 			, i = 0
	 			, len = 0
	 			;
	 	if(!arguments.length){
	 		arr= window.location.href.split('?');
	 		if(arr.length <=1){return false;}
	 		arr = window.location.href.split('?')[1].split('&');
	 	}else{
			arr = data.substring(1).split('&');
	 	}
		len = arr.length;
		for(;i < len ; i++){
			_var_2 = arr[i].split('='); 
			obj[_var_2[0]]  = _var_2[1]
		}
	 	return obj;
	 };
	/*=======================================================
	 * GET형식을 데이터로 변환
	 * 파라메터가 없을경우는 현재 URL의 GET변수를 객체로 만든다.
	 * params
	 *  .data(string) : ?data=test&b=ekdrms 형식의 데이터
	 =======================================================*/
	
	TB_func.nl2br = function(str){  
	    return str.replace(/\n/g, "<br/>");  
	};
	/*=========================
	 *nl2br
	 * 띄어쓰기.
	 =========================*/
	
	TB_func.createReady = function($el){
		$el.css({
			"display" : "block",
			"visibility" : "hidden"
		}).css({
			"display" : "none",
			"visibility" : "visible"
		});
	};
	/*=========================
	 * 동적으로 엘리먼트 생성시 뻑뻑한것을 완하하기 위해
	 * 미리 한번 보여준다. 
	 * params
	 *  .$el(jQueryObj): 동적으로 생성될 엘리먼트
	 =========================*/

	TB_func.popupDialog = function(_str, _delay){
		var $dialog =  $('#TB_popupDialog')
				, text = ''
				, delay = _delay||1000;
		if(!$dialog.length){
			text = "<div id='TB_popupDialog' "; 
			text += " style='line-height:1.5em;'>";
			text += "</div>";
			$('#TB_globalTemplates_wrap').append(text);
			$dialog =  $('#TB_popupDialog');
			
			TB_func.createReady($dialog);
		}
		$dialog.html(_str).stop().fadeIn().delay(delay).fadeOut();
	};
	/*=========================
	 * 팝업 다이얼로그
	 * params
	 *  .data(string) : ?data=test&b=ekdrms 형식의 데이터
	 =========================*/
	
	TB_func.copyClipboard = function(_str){
		if(window.clipboardData){//IE
			window.clipboardData.setData('Text', _str);
			TB_func.popupDialog('복사 되었습니다.');
		}else{
			temp = prompt('Ctrl+c를 눌러 복사하세요', _str);
		}
	};
	/*=========================
	 * 텍스트 클립보드 카피
	 * params
	 *  ._str(string) : 카피할 텍스트
	 =========================*/
	
	TB_func.imageReady_cnt = 0;
	TB_func.imageReady = function($scope, _callback){
		if(!$scope instanceof $){//jquery 오브젝트 검사
			return false;
		}
		setTimeout(function(){
			if($scope.find('img').length || TB_func.imageReady_cnt > 20){
				TB_func.imageReady_cnt = 0;
				if($.type(_callback) === 'function'){
					_callback($scope);
				}
			}else{//이미지가 있을 때.
				TB_func.imageReady_cnt++;
				TB_func.imageReady($scope, _callback);
			}
		}, 200);
	};
	/*=========================
	 * 이미지가 있을 때까지 반복
	 * 최대 20회반복
	 * params
	 *  .$scope(jqueryObj) : jquery 오브젝트
	 *  ._callback(function) : 콜백함수
	 =========================*/
	
	TB_func.enc = function(data, key){
		if(!data){return false;}
		return Aes.Ctr.encrypt(data, key,256 );
	};
	/*==========================================
	 * 암호화 후 리턴
	 ==========================================*/

	TB_func.dec = function(data, key){
		if(!data){return false;}
		return Aes.Ctr.decrypt(data, key, 256);
	};
	/*==========================================
	 * 복호화 후 리턴
	 ==========================================*/

	TB_func.getByteLength = function(s,b,i,c){
		 for(b=i=0;c=s.charCodeAt(i++);b+=c>>11?3:c>>7?2:1);
	    return b;
	};
	/*==========================================
	 * 문자열의 바이트를 계산 후 리턴
	 * params
	 * 	- s(string) : 계산할 문자열 
	 *  ==========================================*/
	
	


	TB_func.json_sort = function(_arr, field, _left, _right){
		if(_arr.length < 2){return _arr;}
		
		var arr = _arr
				, len= arr.length
				, left = _left||0, right = _right||len-1
				, idx
				;
				
		var swap = function(items, firstIndex, secondIndex){
			var temp = items[firstIndex];
			items[firstIndex] = items[secondIndex];
			items[secondIndex] = temp;
		};
		
		var partition = function(items, left, right){
			var pivot = items[Math.floor((right+left)/2)][field]
					, i = left
					, j = right
					;
					
			while( i <= j){
				while(items[i][field] < pivot){
					i++;
				}
				while(items[j][field] > pivot){
					j--;
				}
				if(i <= j){
					swap(items, i, j);
					i++;
					j--;
				}
			}
			return i;
		};
		
		
		if(len > 1){
			idx = partition(arr, left, right);
			if(left < idx-1){
				TB_func.json_sort(arr,field, left, idx-1);
			}
			if(idx < right){
				TB_func.json_sort(arr,field, idx, right);
			}
		}
		return arr;
	};
	/*==========================================
	 * JSON  배열 객체를 퀵으로 정렬
	 * 3,5번째 인자는 재귀함수를 사용하기 때문에 붙은 것이고 일반적으로 사용하지 않는다.
	 *  ==========================================*/

	if ( !Array.prototype.indexOf ) {
		Array.prototype.indexOf = function(target) {
			var arr = this, len = this.length;
			if ( 0 == len ) {return -1;}
			for ( var i=0; i < len ; i++){
				if ( target == arr[i]) {return i;}
			}	
			return -1;
		};
	}
	/*=============================================
	 * indexOf 메소드가 없는 ie8등에서 indexOf 정의
 	=============================================*/

	if(typeof String.prototype.trim !== 'function') {
	    String.prototype.trim = function() {
	        return this.replace(/^\s+|\s+$/g, ''); 
	    };
	}
	/*=============================================
	 * trim 메소드 없을 경우(IE8) 정의
	 =============================================*/

	TB_func.sendDataConvert = function(data){
		var firstText = data.substring(0,1);
		if(firstText==='{'){
			return $.parseJSON(data.replace(/\s/gi, ''));//JSON
		}else if(firstText === '?'){
			return  TB_func.getToParams(data.replace(/\s/gi, ''));//GET ->obj
		}else{ //일반 단어일 경우는 공유 오브젝트를 넘긴다.
			return  TB_shared.get(data);
		}
	} 
	/*=============================================
	 * string데이터의 첫 글자에 따라 포맷을 달리 해서 넘긴다.
	 * params
	 * 	. data(string) : { , ? , 나머지에 의해서 포맷을 정한다. 
	 =============================================*/
	
	TB_func.json_inArray = function(array, key, value){
		if(arguments.length != 3){return false;}
		if($.type(array) === 'array'){
			var i = 0, len = array.length;
			for(;i < len ; i++){
				if(array[i][key] === value){
					return i;
					break;
				}
			}
			return false;
		}
	};
	/*=============================================
	 * JSON 배열 내에서 key와 value가 일치하는 값이 있을 경우 해당 배열 index 리턴
	 =============================================*/
	
	TB_func.json_search = function(arr, key, val, child){
		var result = [], j = 0 
			//,path = [], k = 0
			//, root = arr
			;
		
		var search = function(arr, key, val, child){
			var i = 0, len = arr.length
				//, path_len =path.length
				;
			//console.log(arr)
			for(;i<len;i++){
				if(arr[i][key] === val){
					result[j] = arr[i];
					j++; 
				}
				if(child && arr[i][child]){
					//path[k] = i; k++;
					//console.log(i, arr[i] );
					search(arr[i][child], key, val, child);
				}
			}
			
		};
		
		search(arr, key, val, child);
		
		return result;
		
	};
	/*=============================================
	 * JSON 배열 내에서 key와 value가 일치하는 값이 있을 해당 값을 전부 찾아 배열로 리턴
	 * child는 해당하는 이름이 배열이 있을 경우 하위 속성을 전부 뒤진다.
	 =============================================*/
	
	//var dd = TB_func.json_search(test, 'target', true, 'child');
	//console.log(dd);
	
	TB_func.fitAlign = function($cover, $target){
		var target = {width : $target.width(), height: $target.height()}
				, cover = {width : $cover.width(), height: $cover.height()}
				, ratio = {	target : target.height/target.width*100
									, cover : cover.height/cover.width*100	}
				, css = {width:'',height:''}
				, margin = {top:'',left:''}
				;
		
		//console.log(cover, target);
		if(ratio.target > ratio.cover){
			css.width = cover.width;
		}else{
			css.height = cover.height;
		}
		
		$target.each(function(){$(this).css(css);});
		
		target = {width : $target.width(), height: $target.height()};
		
		if(css.width){
			margin['margin-top'] = -(target.height-cover.height)/2;
			margin['margin-left'] = '';
		}else{
			margin['margin-top'] = '';
			margin['margin-left'] = -(target.width-cover.width)/2;
		}
		$target.each(function(){$(this).css(margin);});
		
		target = null, cover = null, ratio = null, margin =null;
	};
	/*=============================================
	 * $cover틀에 꽉차게 $target를 맞춘다.
	 * params
	 * 	. $cover(jQueryObj) : 틀이 되는 오브젝트
	 =============================================*/
	
	TB_func.divideKeyVal = function(_obj){
		var obj = {key:[],val:[]}
		for(var i in _obj){
			obj.key.push(i);
			obj.val.push(_obj[i]);
		}
		return obj;
	};
	/*=============================================
	 * 객체를 업데이트에 유용한 형태로 반환한다. 
	 =============================================*/
	
	TB_func.fall = function(_arr){
		var arrFunc
				, pointer = 0
				, len = 0
				, last= 0
				, param = undefined
				;
		
		var reset = function(){
			pointer = 0;
			len = 0;
			param = undefined;
		};
		
		var resolve = function(param){
			
			var last= len -1;
			if(last === pointer){//전체 순회 했을 경우
				reset();
				return false;
			}
			param = param||undefined;
			pointer++;
			if(typeof arrFunc[pointer] === 'function'){
				arrFunc[pointer](resolve, param);
			}
		};
		/*==/다음 함수를 실행하기 위한 함수.==*/
		
		var init = function(_arr){
			arrFunc = _arr;
			reset();
			len = arrFunc.length;
			if(typeof arrFunc[pointer] === 'function'){
				arrFunc[pointer](resolve, param);
			}
		}
		
		init(_arr);
	};
	/*=============================================
	 * 비동기 함수를 폭포수 형태로 변화
	 =============================================*/
	
	TB_func.loading = (function(){
		var tl
			, $icon
			, $bg
			, direction
			;
		
		var init = function(){
			tl = (Modernizr.csstransforms3d)?new TimelineMax():undefined;
			$icon = $('#TB_loading_box').find('.icon');
			$bg = $('#TB_loading_bg');
		};
		/*==/로딩 엘리먼트 생성==*/
		
		var on = function(_direction){
			if(!tl){return false;}
			direction = _direction||'full';
			var icon_set = {display:'block'}
				, move = {ease : Back.easeOut.config(2)} 
				;
			
			
			if(direction === 'full'){
				$icon.removeClass('ST_theme_color').addClass('ST_col_ffffff');
				tl
				.set($bg,{
					autoAlpha : 0
					, css : {display:'block'}
				})
				.set($icon,{
					css : {
						display:'block'
						, left : '50%', right:'initial', bottom:'initial', top:'-4em'					
					}
				})
				.to($bg, 0.3,{
					alpha : 0.5
				})
				.to($icon, 0.3, {
					top: '45%'
					, ease : Back.easeOut.config(2)
				})
				;
				return false;
			}else if(direction === 'top'){
				icon_set.top = '-4em';
				icon_set.bottom = 'initial';
				icon_set.left = '50%';
				icon_set.right = 'initial';
				move.top = 20;
			}else if(direction === 'right'){
				icon_set.top = '50%';
				icon_set.bottom = 'initial';
				icon_set.left = 'initial';
				icon_set.right = '-4em';
				move.right = 20;
			}else if(direction === 'bottom'){
				icon_set.top = 'initial';
				icon_set.bottom = '-4em';
				icon_set.left = '50%';
				icon_set.right = 'initial';
				move.bottom = 20;
			}else if(direction === 'left'){
				icon_set.top = '50%';
				icon_set.bottom = 'initial';
				icon_set.left = '-4em';
				icon_set.right = 'initial';
				move.left = 20;
			}
			
			tl.set($icon, {
				css : icon_set
			})
			.to($icon,0.2 ,move);

		};
		
		var off = function(){
			var css = {}
			if(!tl){return false;}
			
			if(direction === 'top' || direction === 'full'){
				css.top = '-4em';
			}else if(direction === 'right'){
				css.right = '-4em';
			}else if(direction === 'left'){
				css.left = '-4em';
			}else if(direction === 'bottom'){
				css.bottom = '-4em';
			}
			
			tl.to($icon, 0.3,{
				css : css
				, ease : Back.easeIn.config(2)
				, onComplete : function(){
					if($icon.hasClass('ST_col_ffffff')){
						$icon.removeClass('ST_col_ffffff').addClass('ST_theme_color').css('display','none');
					}
					
				}
			})
			.to($bg, 0.3,{
				alpha : 0
				, css : {display:'none'}
			})
			;
			
		};
		
		return {
			on : on
			, off : off
			, init : init
		}
	})();
	/*=============================================
	 * 로딩 애니메이션
	 =============================================*/
	
	
	TB_func.sort = {};
	 
	TB_func.sort.insertion = function(arr){
		var i =1, len = arr.length, j, temp;
		for(; i< len ; i++){
			temp = arr[i];//새로운 숫자 선택
			//선택한 숫자를 이미 정렬된 숫자들과 비교하며 넣을 위치를 고른다.
			for(j = i-1; j >=0, temp < arr[j];j-- ){
				arr[j+1] = arr[j];
			}
			arr[j+1] = temp;
		}
		return arr;
	};
	//console.log(TB_func.sort.insertion([5, 6, 2, 4, 3, 100]));
	/*=============================================
	 * 선택 정렬
	 =============================================*/
	
	
	TB_func.sort.merge = function(_arr){
		
		function merge (left, right){
			var result = [];
			//console.log(left.length , right.length )
			while(left.length && right.length){
				if(left[0] <= right[0]){//두 배열의 첫번째 원소 비교
					result.push(left.shift());
				}else{
					result.push(right.shift());
				}
			}
			while(left.length){result.push(left.shift());}
			while(right.length){result.push(right.shift());}
			return result;
		}
		
		var merge_sort = function(arr){
			var len = arr.length;
			if(len < 2){ return arr;}
			
			var pivot = Math.floor(len/2)//대략 배열을 반으로 나눔
				, left  = arr.slice(0, pivot)//쪼갠 왼쪽
				, right = arr.slice(pivot, len)//쪼갠 오른쪽
				;
			return merge(merge_sort(left), merge_sort(right));
		};
		
		return merge_sort(_arr);
		
	};
	//console.log(TB_func.sort.merge([5,2,4,7,6,1,3,8]));
	/*=============================================
	 * 합병 정렬
	 =============================================*/
	
	
	if (!Array.prototype.filter) {
	  Array.prototype.filter = function(fun/*, thisArg*/) {
	    'use strict';
	
	    if (this === void 0 || this === null) {
	      throw new TypeError();
	    }
	
	    var t = Object(this);
	    var len = t.length >>> 0;
	    if (typeof fun !== 'function') {
	      throw new TypeError();
	    }
	
	    var res = [];
	    var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
	    for (var i = 0; i < len; i++) {
	      if (i in t) {
	        var val = t[i];
	
	        if (fun.call(thisArg, val, i, t)) {
	          res.push(val);
	        }
	      }
	    }
	
	    return res;
	  };
	}
	 /*=========================================================
	  * 'Array.filter' 폴리필
	  =========================================================*/

	
	Date.prototype.format = function(f) {
	    if (!this.valueOf()) return " ";
	 
	    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
	    var d = this;
	     
	    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
	        switch ($1) {
	            case "yyyy": return d.getFullYear();
	            case "yy": return (d.getFullYear() % 1000).zf(2);
	            case "MM": return (d.getMonth() + 1).zf(2);
	            case "dd": return d.getDate().zf(2);
	            case "E": return weekName[d.getDay()];
	            case "HH": return d.getHours().zf(2);
	            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
	            case "mm": return d.getMinutes().zf(2);
	            case "ss": return d.getSeconds().zf(2);
	            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
	            default: return $1;
	        }
	    });
	};
	 
	String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
	String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
	Number.prototype.zf = function(len){return this.toString().zf(len);};
	 /*=========================================================
	  * dateform
	  =========================================================*/
	
	String.prototype.escapeHtml = function(){
	  return this.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/\"/g, "&quot;");
	};
	 /*=========================================================
	  * escapeHTML
	  =========================================================*/
	 
	String.prototype.unescapeHtml = function(){
	  return this.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, "\"");
	};
	
	String.prototype.getIP = function() {
	    var pattern = /((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})/g;
	    return this.match(pattern);
	};
	
	
	
	// 숫자 타입에서 쓸 수 있도록 format() 함수 추가
	Number.prototype.comma = function(){
	    if(this==0) return 0;
	    var reg = /(^[+-]?\d+)(\d{3})/;
	    var n = (this + '');
	    while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');
	    return n;
	};
	 
	// 문자열 타입에서 쓸 수 있도록 format() 함수 추가
	String.prototype.comma = function(){
	    var num = parseFloat(this, 10);
	    if( isNaN(num) ) return "0";
	    return num.comma();
	};


try{
	var module = module||undefined;
	if(module){module.exports = TB_func;}
}catch(e){
	throw e;
}

//구버전 IE console.log 오류 해결
console = console || {log:function(){}};




































































