/**
 * @author user
 */
var TB_style = (function($){
	var setCss = function($el,_style){
		var style = _style.split('_')
				, val=0;
				
		//console.log($el[0].outerHTML);
		$el.ready(function(){
			var winWid = TB_shared.get('winWid')
					, winHei = TB_shared.get('winHei')
					, mode = (winWid < 992)?'mobile':'pc';
			switch(style[1]){
				case 'minWinHei'://윈도우 높이
					if(style[3]==='p'){//높이 윈도우 기준으로 조정
						val = TB_shared.get('winHei')*style[2]/100;
						$el.css('min-height', val+'px');
					}
				break;
				case 'winHei'://윈도우 높이
					if(style[3]==='p'){//높이 윈도우 기준으로 조정
						val = TB_shared.get('winHei')*style[2]/100;
						$el.css('height', val+'px');
					}
				break;
				case 'winTop':
					if(style[3] === 'p'){
						val = TB_shared.get('winHei')*parseInt(style[2], 10)/100
					}
					$el.css('top',val);
				break;				
				case 'winWid'://윈도우 넓이
					if(style[3]==='p'){//높이 윈도우 기준으로 조정
						val = TB_shared.get('winWid')*style[2]/100;
						$el.css('width', val+'px');
					}
				break;
				case 'winWidMinus'://윈도우 넓이에서 일부를 뺀넓이
					if(style[3] === 'px'){
						val = (TB_shared.get('winWid')-style[2]);
						$el.css('width', val+'px');
					}
				break;
				case 'documentHei'://문서 높이
					if(style[3]==='p'){//높이 윈도우 기준으로 조정
						val = TB_shared.get('documentHei')*style[2]/100;
						$el.css('height', val+'px');
					}
				break;
				case 'parentHei'://부모를 기준으로 높이를 조정한다.
					if(style[3]==='p'){
						val = $el.parent().outerHeight()*style[2]/100;
						$el.css('height', val+'px');
					}
				break;
				case 'parentWid'://부모를 기준으로 높이를 조정한다.
					if(style[3]==='p'){
						val = $el.parent().outerWidth()*style[2]/100;
						$el.css('width', val+'px');
					}
				break;
				case 'closestHei'://부모를 기준으로 높이를 조정한다.
					if(style[3]==='p'){
						var $target = $el.closest($el.attr('data-TB_style_closestTarget'));
						$el.css({
							'height': $target.height()+'px'
						});
					}
				break;
				case 'findHei':
					var $target = $el.find($el.attr('data-TB_style_findTarget')).first();
					$el.css('height', $target.height()+'px');
				break;
				
				case 'findWid':
					var $target = $el.find($el.attr('data-TB_style_findTarget'));
					$el.css('width', $target.width()+'px');
				break;
				case 'findFit':
					var $target = $el.find($el.attr('data-TB_style_findTarget'));
					$el.css({
						'width': $target.width()+'px',
						'height': $target.height()+'px'
					});
				break;
				
				case 'closestFit':
					var $target = $el.closest($el.attr('data-TB_style_closestTarget'));
					$el.css({
						'width': $target.width()+'px',
						'height': $target.height()+'px'
					});
				break;
				
				case 'parentFit':
					var $target = $el.parent();
					$el.css({
						'width': $target.width()+'px',
						'height': $target.height()+'px'
					});
				break;
				
				case 'hidden'://반응형, 숨기기, ST_hidden_mobile
					 var display = '';
					if(style[2] === mode){ display = 'none'; }
					$el.css('display', display);
					display = null;
				break;
				
				case 'visible'://반응형, 보이기, ST_visible_mobile_block
					var display = 'none';
					if(style[2] === mode){ display = style[3]; }
					$el.css('display', display);
				break;
				
				case 'response'://반응형 넓이 조정, ST_response_mobile_100%
					if(style[3] === 'winHei'){
						style[3] = 'height', style[4] = winHei+'px';
					}else if(style[3] === 'winWid'){
						style[3] = 'width', style[4] = winWid+'px';
					}
					$el.css(style[3], (style[2] === mode)?style[4]:'');
				break;
				
				case 'align'://정렬
					if(style[2]==='center'){
						var imgTag = $el[0].tagName	;
						var align_center = function(){
							$el.css({
								'position' : 'absolute'
								, 'left' : '50%'
								, 'margin-left' :  -$el.outerWidth()/2
							});
						};
						if(imgTag === 'IMG'){
							$el.load(function(){
								align_center();
							});
						}
						align_center();
						
					}else if(style[2]==='middle'){
						$el.css({
							'position': 'absolute' , 'top' : '50%', 'margin-top' : -$el.outerHeight()/2
						});
					}
				break;
				
				case 'fitAlign'://대상의 이미지를 현재 객체에 꽉차게 가운데 정렬한다.
					var $target = $el.find($el.attr('data-TB_style_findTarget'))
							, loaded = $target.attr('data-TB_style_fitAlign_loaded')
							, repeat_cnt =0 
							;
					//console.log('당근');
					//console.log($target.length);
					if(!$target.length){return false;
						throw "data-TB_style_findTarget을 설정해 주십시요."; return false;}
					$el.css({
						'position':'relative', 'overflow':'hidden'
					});
					
					$target.each(function(){
						var _$target = $(this);
						TB_func.fitAlign($el, _$target);
						
						if(_$target[0].tagName === 'IMG' && !loaded){//처음 이미지를 로딩할 때.
							//console.log('처음');
							_$target.load(function(){
								TB_func.fitAlign($el, _$target);
								$target.attr('data-TB_style_fitAlign_loaded', 'true');
							});
							TB_func.fitAlign($el, _$target);
						}else{
							//console.log('셋');
							TB_func.fitAlign($el, _$target);
						}
					});
				break;
				
			}
		});
	};
	/*=========================================
	  *엘리먼트를 셋팅 한다.
	  * params
	  * 	. $el : CSS를 적용시킬 엘리먼트
	  *  	. _style : Css 'ST_winHei_100_p'
	  =========================================*/
	
	var binding = function($el){
		var $scope = $el||$(document);
		if($scope.find('.TB_style').length){
			$scope.find('.TB_style').each(function(){
				var $this = $(this)
						, arr = $this.attr('data-TB_style') 
						, len  
						, i = 0;
						
				if(arr){
					arr = arr.split('||');
					len= arr.length;
				}else{
					throw 'not defined "data-TB_style" \n'+$this[0].outerHTML;
					return false;
				}
				
				if(len === 0){
					setCss($this, arr[0]);
				}else{
					for(;i < len ; i++){
						setCss($this,arr[i]);
					}
				} 
			});
		}
	};/*엘리먼트 스타일을 적용 시킨다.
	  * parmas
	  * 	. $el (jQueryElement) : 해당하는 엘리먼트에 적용한다.
	  */
	
	return {
		binding : binding
	};
})(jQuery);

