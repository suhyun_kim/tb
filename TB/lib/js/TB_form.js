/**
 * @author user
 */
var TB_form = (function($, TB_config, TB_shared){
	var TB_signalKey ;//시그널키
	var swUrl;//데이터를 전송할 곳의 주소
	var sslUrl;//안호화 전송할곳의 주소.
	var addArr = new Array()//폼 전송시 데이터를 추가할 배열
			, debug = false; 
			;
	
	var set = function(_name, _val){
		if(_name==='TB_signalKey'){
			TB_signalKey = _val;
		}else if(_name==='swUrl'){
			swUrl = _val;
		}else if(_name === 'sslUrl'){
			sslUrl = _val;
		}
		if(debug){
			if(console){console.log('설정 셋팅', _name, _val, swUrl);}
		}
	};
	/*============================
		*private 변수 셋팅
		============================*/
		
	var autoInsert = function(formEl, infoObj){
		var chkBoxIdx = new Array();
	  	//console.log(infoObj);
		formEl.find('[name]').each(function() {
			var $this = $(this);
		  	var key = $this.attr('name');
		  	var val;
		  	var inputLimitType = $(this).attr('data-inputLimitType');
		  	//con(infoObj[key]);
			if($this.attr('type')=="file"){//그냥 넘어감
			}else if($this.attr('type')=="radio"){//라디오 버튼의 경우.
				if(infoObj[key]){
					if(infoObj[key] == $this.val()){
						$this.attr('checked', true);
					}else{
						$this.attr('checked', false);
					}
				}
			}else if($this.attr('type')=='checkbox'){//체크 박스
				if(infoObj[key]){
					if(infoObj[key].indexOf($this.val())!=-1)$this.attr('checked', true);
				}
			}
			else{
			  if(infoObj[key]){
			  	if($(this).attr('data-formViewFormat')){//포맷 변환
			  		switch($(this).attr('data-formViewFormat')){
			  			case'num'://3자리 마다 콤마 숫자 변환.
			  				val = commSet(Number(infoObj[key]));
			  			break;
			  		}
			  	}else if(inputLimitType){
			  		switch(inputLimitType){
						case 'commSet': val = commSet(Number(infoObj[key])); break; //3자리마다 콤마	
			  		}
			  	}else if($this.hasClass('TB_form_set')){
			  		$this.parent().find('.wysiwyg-editor').html(infoObj[key]);
			  		val = infoObj[key];
			  	}else{
			  		val = infoObj[key];
			  	}
			  	 $this.val(val);
			  	 //console.log(key, val);
			  }
			}
		});
		formEl.find('');
		formEl = null;
		infoObj = null;
		key = null;
	};
	/*====================================================
	 * 객체를 폼에 자동으로 입력한다.
	 * param
	 *   - formEl : 폼 엘리먼트
	 *   - infoObj : 값이 되어줄 엘리먼트 단 키가 name속성의 값과 같아야 한다.
	 ====================================================*/
	
	var imageUpload = function($formEl, $fileEl){
		if(!$fileEl.val())return false;
		if(window.confirm('이미지를 업로드 하시겠습니까??')){
			var $target = $($fileEl.attr('data-TB_form_imageUpload_target'));
			var txt = "";
			
			TB_shared.set('TB_editorImageUpload', function(res){
				if($target.length){
					txt ="<img src='"+TB_config.get('TB_data_path')+res.src+"' title='"+res.name+"' />";
					$target.append(txt).focus();//타겟에 그림추가
				}
				//console.log(res);
			});/*==/전송 후 함수(aftAc) 셋팅 ==*/
			
			addArr = [	
							{'name' : 'reqType', 'value' : 'fileupload'}
							, {'name' : 'reqQuery', 'value' : 'editorImage'}
							];
			$formEl.find('[type=submit]').trigger('click');
		}
	};
	/*============================
		* 위지위 에디터에서 비동기 이미지 업로드
		============================*/
		
	var wysiwyg_in_html = function($form, html){
		$form.find('[name=memo]').val(html).end()
		.find('.wysiwyg-editor').html(html);
		$main_form = null;
	};
	/*============================
		* 위지윅 내에 HTML 삽입
		============================*/
		
	var result = function(_data){
		return (_data.header.result=='success')?true : false;
	};
	/*============================
	*성공했을 경우 true를 반환 실패했을 경우 false를 반환
	============================*/
		
	var wysiwygSet = function($el , mode, _option) {
		var option = {
			remove : false
		},
		toolbar = 'selection'
		;
		
		if(!mode||mode==='view'){
			toolbar = 'selection';
		}else if(mode){
			toolbar = mode;
		}
		
		if(_option){
			$.extend(option, _option);
		}
			$el.wysiwyg({
				classes : 'some-more-classes',
				// 'selection'|'top'|'top-selection'|'bottom'|'bottom-selection'
				toolbar : toolbar,
				buttons : {
					insertimage : {//base64 이미지 삽입 버튼
						title : 'Insert image',
						image : '\uf030', // <img src="path/to/image.png" width="16" height="16" alt="" />
						showstatic: false,    // wanted on the toolbar
						showselection : false// wanted on selection
					},
					insertvideo : {
						title : 'Insert video',
						image : '\uf03d', // <img src="path/to/image.png" width="16" height="16" alt="" />
						placeholder_embed : '유튜브복사',
						showstatic: false,    // wanted on the toolbar
						showselection : true// wanted on selection
					},
					insertlink : {//링크추가
						title : 'Insert link',
						image : '\uf08e' // <img src="path/to/image.png" width="16" height="16" alt="" />
						, showselection : true // wanted on selection
					},
					fontname : {//폰트 관련
						title : 'Font',
						image : '\uf031', // <img src="path/to/image.png" width="16" height="16" alt="" />
						popup : function($popup, $button) {
							var list_fontnames = {
								// Name : Font
								'맑은고딕' : 'Malgun Gothic',
								'굴림' : 'Gulim',
								'나눔고딕' : 'Nanum Gothic',
								'Arial, Helvetica' : 'Arial,Helvetica',
								'Verdana' : 'Verdana,Geneva',
								'Georgia' : 'Georgia',
								'Courier New' : 'Courier New,Courier',
								'Times New Roman' : 'Times New Roman,Times'
							};
							var $list = $('<select/>').addClass('wysiwyg-plugin-list').attr('unselectable', 'on');
							$.each(list_fontnames, function(name, font) {
								var $link = $('<option/>').attr('href', '#').css('font-family', font).html(name).click(function(event) {
									$el.wysiwyg('shell').fontName(font).closePopup();
									// prevent link-href-#
									event.stopPropagation();
									event.preventDefault();
									return false;
								});
								$list.append($link);
							});
							$popup.append($list);
						},
						showstatic: true,    // wanted on the toolbar
						showselection : false // wanted on selection
					},
					fontsize : {// 폰트 사이즈 플러그인
						title : 'Size',
						image : '\uf034', // <img src="path/to/image.png" width="16" height="16" alt="" />
						popup : function($popup, $button) {
							// Hack: http://stackoverflow.com/questions/5868295/document-execcommand-fontsize-in-pixels/5870603#5870603
							var list_fontsizes = [];
							list_fontsizes.push('0.7em');
							list_fontsizes.push('0.8em');
							list_fontsizes.push('0.9em');
							list_fontsizes.push('1em');
							list_fontsizes.push('1.3em');
							list_fontsizes.push('1.5em');
							list_fontsizes.push('2em');
							list_fontsizes.push('2.3em');
							list_fontsizes.push('2.5em');
							/*
							for (var i = 8; i <= 11; ++i)
								list_fontsizes.push(i + 'px');
							for (var i = 12; i <= 28; i += 2)
								list_fontsizes.push(i + 'px');
							list_fontsizes.push('36px');
							list_fontsizes.push('48px');
							list_fontsizes.push('72px');
							*/
							var $list = $('<div/>').addClass('wysiwyg-plugin-list').attr('unselectable', 'on');
							$.each(list_fontsizes, function(index, size) {
								var $link = $('<a/>').attr('href', '#').html(size).click(function(event) {
									$el.wysiwyg('shell').fontSize(7).closePopup();
									$el.wysiwyg('container').find('font[size=7]').removeAttr("size").css("font-size", size);
									// prevent link-href-#
									event.stopPropagation();
									event.preventDefault();
									return false;
								});
								$list.append($link);
							});
							$popup.append($list);
						}
						 , showstatic: true,    // wanted on the toolbar
						showselection: true    // wanted on selection
					},
					// Header plugin
					header : {
						title : 'Header',
						image : '\uf1dc', // <img src="path/to/image.png" width="16" height="16" alt="" />
						popup : function($popup, $button) {
							var list_headers = {
								// Name : Font
								'Header 1' : '<h1>',
								'Header 2' : '<h2>',
								'Header 3' : '<h3>',
								'Header 4' : '<h4>',
								'Header 5' : '<h5>',
								'Header 6' : '<h6>',
								'Code' : '<pre>'
							};
							var $list = $('<div/>').addClass('wysiwyg-plugin-list').attr('unselectable', 'on');
							$.each(list_headers, function(name, format) {
								var $link = $('<a/>').attr('href', '#').css('font-family', format).html(name).click(function(event) {
									$el.wysiwyg('shell').format(format).closePopup();
									// prevent link-href-#
									event.stopPropagation();
									event.preventDefault();
									return false;
								});
								$list.append($link);
							});
							$popup.append($list);
						}
						, showstatic: false    // wanted on the toolbar
						, showselection: false   // wanted on selection
					},
					bold : {
						title : 'Bold (Ctrl+B)',
						image : '\uf032', // <img src="path/to/image.png" width="16" height="16" alt="" />
						hotkey : 'b'
						, showselection : true // wanted on selection
					},
					italic : {
						title : 'Italic (Ctrl+I)',
						image : '\uf033', // <img src="path/to/image.png" width="16" height="16" alt="" />
						hotkey : 'i'
						, showselection : true // wanted on selection
					},
					underline : {
						title : 'Underline (Ctrl+U)',
						image : '\uf0cd', // <img src="path/to/image.png" width="16" height="16" alt="" />
						hotkey : 'u'
						, showselection : true // wanted on selection
					},
					strikethrough : {
						title : 'Strikethrough (Ctrl+S)',
						image : '\uf0cc', // <img src="path/to/image.png" width="16" height="16" alt="" />
						hotkey : 's'
						, showselection : false // wanted on selection
					},
					forecolor : {
						title : 'Text color',
						image : '\uf1fc' // <img src="path/to/image.png" width="16" height="16" alt="" />
						, showselection : true // wanted on selection
					},
					highlight : {
						title : 'Background color',
						image : '\uf043' // <img src="path/to/image.png" width="16" height="16" alt="" />
						, showselection : false // wanted on selection
					},
					alignleft : {
						title : 'Left',
						image : '\uf036', // <img src="path/to/image.png" width="16" height="16" alt="" />
						//showstatic: true,    // wanted on the toolbar
						showselection : true // wanted on selection
					},
					aligncenter : {
						title : 'Center',
						image : '\uf037', // <img src="path/to/image.png" width="16" height="16" alt="" />
						//showstatic: true,    // wanted on the toolbar
						showselection : true // wanted on selection
					},
					alignright : {
						title : 'Right',
						image : '\uf038', // <img src="path/to/image.png" width="16" height="16" alt="" />
						//showstatic: true,    // wanted on the toolbar
						showselection : true // wanted on selection
					},
					alignjustify : {
						title : 'Justify',
						image : '\uf039', // <img src="path/to/image.png" width="16" height="16" alt="" />
						//showstatic: true,    // wanted on the toolbar
						showselection : true // wanted on selection
					},
					subscript : {
						title : 'Subscript',
						image : '\uf12c', // <img src="path/to/image.png" width="16" height="16" alt="" />
						//showstatic: true,    // wanted on the toolbar
						showselection : false // wanted on selection
					},
					superscript : {
						title : 'Superscript',
						image : '\uf12b', // <img src="path/to/image.png" width="16" height="16" alt="" />
						//showstatic: true,    // wanted on the toolbar
						showselection : false // wanted on selection
					},
					indent : {
						title : 'Indent',
						image : '\uf03c', // <img src="path/to/image.png" width="16" height="16" alt="" />
						//showstatic: true,    // wanted on the toolbar
						showselection : false // wanted on selection
					},
					outdent : {
						title : 'Outdent',
						image : '\uf03b', // <img src="path/to/image.png" width="16" height="16" alt="" />
						//showstatic: true,    // wanted on the toolbar
						showselection : false // wanted on selection
					},
					orderedList : {
						title : 'Ordered list',
						image : '\uf0cb', // <img src="path/to/image.png" width="16" height="16" alt="" />
						//showstatic: true,    // wanted on the toolbar
						showselection : false // wanted on selection
					},
					unorderedList : {
						title : 'Unordered list',
						image : '\uf0ca', // <img src="path/to/image.png" width="16" height="16" alt="" />
						//showstatic: true,    // wanted on the toolbar
						showselection : false // wanted on selection
					},
					removeformat : {
						title : 'Remove format',
						image : '\uf12d' // <img src="path/to/image.png" width="16" height="16" alt="" />
						, showselection : true // wanted on selection
					}
				},
				// Submit-Button
				submit : {
					title : 'Submit',
					image : '\uf00c' // <img src="path/to/image.png" width="16" height="16" alt="" />
					, showstatic : true
				},
				// Other properties
				selectImage : '더블클릭하세요',
				placeholderUrl : 'www.example.com/test.jpg',
				placeholderEmbed : '유튜브 영상에서 마우스 우클릭 후 소스 코드 복사',
				maxImageSize : [500, 400],
				onKeyDown : function(key, character, shiftKey, altKey, ctrlKey, metaKey) {
					// E.g.: submit form on enter-key:
					//if( (key == 10 || key == 13) && !shiftKey && !altKey && !ctrlKey && !metaKey ) {
					//    submit_form();
					//    return false; // swallow enter
					//}
				},
				onKeyPress : function(key, character, shiftKey, altKey, ctrlKey, metaKey) {
				},
				onKeyUp : function(key, character, shiftKey, altKey, ctrlKey, metaKey) {
				},
				onAutocomplete : function(typed, key, character, shiftKey, altKey, ctrlKey, metaKey) {
					if (typed.indexOf('@') == 0)// startswith '@'
					{
						var usernames = ['Evelyn', 'Harry', 'Amelia', 'Oliver', 'Isabelle', 'Eddie', 'Editha', 'Jacob', 'Emily', 'George', 'Edison'];
						var $list = $('<div/>').addClass('wysiwyg-plugin-list').attr('unselectable', 'on');
						$.each(usernames, function(index, username) {
							if (username.toLowerCase().indexOf(typed.substring(1).toLowerCase()) !== 0)// don't count first character '@'
								return;
							var $link = $('<a/>').attr('href', '#').text(username).click(function(event) {
								var url = 'http://example.com/user/' + username, html = '<a href="' + url + '">@' + username + '</a> ';
								var editor = $(element).wysiwyg('shell');
								// Expand selection and set inject HTML
								editor.expandSelection(typed.length, 0).insertHTML(html);
								editor.closePopup().getElement().focus();
								// prevent link-href-#
								event.stopPropagation();
								event.preventDefault();
								return false;
							});
							$list.append($link);
						});
						if ($list.children().length) {
							if (key == 13) {
								$list.children(':first').click();
								return false;
								// swallow enter
							}
							// Show popup
							else if (character || key == 8)
								return $list;
						}
					}
				},
				onImageUpload : function(insert_image) {
					//console.log(insert_image);
					// A bit tricky, because we can't easily upload a file via
					// '$.ajax()' on a legacy browser without XMLHttpRequest2.
					// You have to submit the form into an '<iframe/>' element.
					// Call 'insert_image(url)' as soon as the file is online
					// and the URL is available.
					// Example server script (written in PHP):
					/*
					<?php
					$upload = $_FILES['upload-filename'];
					// Crucial: Forbid code files
					$file_extension = pathinfo( $upload['name'], PATHINFO_EXTENSION );
					if( $file_extension != 'jpeg' && $file_extension != 'jpg' && $file_extension != 'png' && $file_extension != 'gif' )
					die("Wrong file extension.");
					$filename = 'image-'.md5(microtime(true)).'.'.$file_extension;
					$filepath = '/path/to/'.$filename;
					$serverpath = 'http://domain.com/path/to/'.$filename;
					move_uploaded_file( $upload['tmp_name'], $filepath );
					echo $serverpath;
					*/
					// Example client script (without upload-progressbar):
					var iframe_name = 'legacy-uploader-' + Math.random().toString(36).substring(2);
					$('<iframe>').attr('name', iframe_name).load(function() {
						// <iframe> is ready - we will find the URL in the iframe-body
						var iframe = this;
						var iframedoc = iframe.contentDocument ? iframe.contentDocument : (iframe.contentWindow ? iframe.contentWindow.document : iframe.document);
						var iframebody = iframedoc.getElementsByTagName('body')[0];
						var image_url = iframebody.innerHTML;
						insert_image(image_url);
						$(iframe).remove();
					}).appendTo(document.body);
					var $input = $(this);
					$input.attr('name', 'upload-filename').parents('form').attr('action', '/script.php')// accessing cross domain <iframes> could be difficult
					.attr('method', 'POST').attr('enctype', 'multipart/form-data').attr('target', iframe_name).submit();
				},
				forceImageUpload : false, // upload images even if File-API is present
				videoFromUrl : function(url) {
					// Contributions are welcome :-)
	
					// youtube - http://stackoverflow.com/questions/3392993/php-regex-to-get-youtube-video-id
					var youtube_match = url.match(/^(?:http(?:s)?:\/\/)?(?:[a-z0-9.]+\.)?(?:youtu\.be|youtube\.com)\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/)([^\?&\"'>]+)/);
					if (youtube_match && youtube_match[1].length == 11)
						return '<iframe src="//www.youtube.com/embed/' + youtube_match[1] + '" width="640" height="360" frameborder="0" allowfullscreen></iframe>';
	
					// vimeo - http://embedresponsively.com/
					var vimeo_match = url.match(/^(?:http(?:s)?:\/\/)?(?:[a-z0-9.]+\.)?vimeo\.com\/([0-9]+)$/);
					if (vimeo_match)
						return '<iframe src="//player.vimeo.com/video/' + vimeo_match[1] + '" width="640" height="360" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
	
					// dailymotion - http://embedresponsively.com/
					var dailymotion_match = url.match(/^(?:http(?:s)?:\/\/)?(?:[a-z0-9.]+\.)?dailymotion\.com\/video\/([0-9a-z]+)$/);
					if (dailymotion_match)
						return '<iframe src="//www.dailymotion.com/embed/video/' + dailymotion_match[1] + '" width="640" height="360" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
	
					// undefined -> create '<video/>' tag
				}
			})
			.change(function(e) {//에디터 내에 문자가 바뀔경우
				if(typeof option.change ==='function'){ option.change(e); }
				
			})
			.focus(function(e) {//포커스가 맞춰질 경우
				if(typeof option.focus ==='function'){ option.focus(e); }
				
			})
			.blur(function(e) {//포커스가 벗어날 경우
				if(typeof option.blur ==='function'){ option.blur(e); }
				
			})
			;
				
			if(mode === 'view'){
				$el.wysiwyg('shell').readOnly(true);
			}else{
				$el.wysiwyg('shell').readOnly(false);
			}
		};
	/*============================
		*해당 엘리먼트에 위지윅을 셋팅한다.
		* params
		* 	- $el(jqueryObj) : 위지윅을 셋팅할 엘리먼트 객체
		*  - mode(str) : 에디터의 모드
		* 		. normal : 기본 텍스트 작성 모드
		* 		. selection : 셀렉션 모드
		* 		. view : 텍스트를 수정 불가능한 뷰 모드 
		============================*/
	
	var add = function(_arr){
		if($.type(_arr)!=='array') return false;
		addArr = _arr;
	};/*============================
		*폼에 추가할 데이터를 정한다.
		* param 
		*  . arr(Array) : 폼전송시 추가할 배열을 정한다. 
		* 		해당 배열의 요소는 객체의 형태를 가지며 아래의 키를 가진다.
		*     . 'name' : 'test'
		*    . required :  true
		*    . type : 'input'
		*   . value : '오이
		============================*/
		
	var vadilation = function($form){
		var $validationEl = $form.find('.TB_form_validate');
		var errText="", result = true, $errEl;
		if($validationEl.length===0) return true;//검사할 항목이 없을 경우 넘어감
		
		$validationEl.each(function(a, b){
			var $this = $(this)
					, value = $this.val()
					, obj;
			if(!value.trim()){//값이 없을 경우 
				result = false;
			}
			
			if($this.attr('data-TB_form_validate')){//하위 속성이 있을 경우
				try{
					obj = $.parseJSON($this.attr('data-TB_form_validate'));
				}catch(e){
					throw e+'\n JSON 배열형태로 입력해 주십시요.ex ) [{"inputType":"number","errText":"숫자입력"}]';
					result = false;
				}
				
				for( var i = 0, len = obj.length ; i < len ; i++){
					
					if(obj[i].exact){//지정된 엘리먼트가 같은 값을 갖는지 확인.
						$form.find(obj[i].exact).each(function(){
							if($this.val() != $(this).val()) result = false;
						});
					}else if(obj[i].minLen){//최소글자 제한
					 	if($this.val().length < obj[i].minLen) result = false;
					}else if(obj[i].isEmail){//이메일형태인지 확인
						var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
						result = regex.test(value);
					}else if(obj[i].isNumber){////숫자형인지 확인
						result = !isNaN(parseInt(value, 10));
					}
					if(obj[i].errText && !result) errText =obj[i].errText;
					
					if(!result) break;  
				}
			}
			
			if(result===false){//에러가 났을 경우 처리
				if(errText==='')errText = $this.attr('title');
				//if(errText) alert(errText);
				$this.focus();
				return false;
			}
		});
		
		if(errText&&result===false){//에러메세지가 있을 경우 해당 툴팁 보여주기
			TB_func.popupDialog(errText);
		}
		
		
		return result;
	};/*============================
		*폼의 유효성 검사
		* detail 
		*   . 검사에 실패시 false를 반환
		* Trigger Class
		*   .TB_form_validate
		* Attr : 속성을 아무것도 지정 안했을 경우 해당값이 존재 하는지만 검사한다.
		*  . title(string) : 실패했을 경우 툴팁으로 보여줄문구를 정한다. 
		* 								HTML의 기본 요소인 title과 혼란을 방지하기 위해 동일하게 처리
		============================*/
	
	var beforeAc = function(_obj, data, $form){
		var obj , result = true, func;
		if($.type(_obj) === 'string'){
			obj = $.parseJSON(_obj);
		}else{
			obj = _obj;
		}
		if(obj.isSend===false){//전송 처리 가 아직 끝나지 않았을 경우.
			return false;
		}
		if(obj.confirm) result = confirm(obj.confirm);
		if(obj.sharedFunc&&result) {
			func = TB_shared.get(obj.sharedFunc);
			if(typeof func==='function') result = func(data, $form);
		}
		return result;
	};
	/*==============================
	 * 폼전송전 처리. 통과를 못했을시 false를 리턴
	 * prams
	 * 	._obj.confirm : confirm()을 호출
	 * return 
	 * 	. true or  false : true를 리턴할 경우 통과. false일 경우 통과 못함.
	 ==============================*/
		
	var aftAc = function(_context){
		var result = _context.result;
		var process = _context.aftac.split('||');
		var obj = [];
		//console.log(result);
		//console.log(_context);
		for( var i = 0 , len = process.length ; i < len ; i++){
			var delimiter = process[i].split(':');
			if(delimiter[0] ==='sharedFunc'){//지정된 함수 실행
				TB_shared.get(delimiter[1])(result, _context.$form);
			}
			if(delimiter[0]==='contentsLoad'){//컨텐츠 로드
				TB_contents.load($(delimiter[1]));
			}
			if(delimiter[0]==='history'){//컨텐츠 로드
				window.history.go(delimiter[1]);
			}
		}
		result = null, process = null, obj = null;
	};/*============================
		*폼을 전송하고 난뒤의 행동.
		* 구분자는 '||'를 기준으로 한다.
		============================*/
	
	var setForm = function($el){
		$el
		.each(function(){
			var $setEl = $(this).find('.TB_form_set');
			var attr = '',items, itemDetail;
			//var click = TB_config.get('click');
			
			if(!$setEl.length) return false;
			
			$setEl.each(function(){
				var $this = $(this);
				attr = $this.attr('data-TB_form_set');
				
				items = attr.split('||');
				for( var i = 0, len = items.length ; i <len ; i++){
					itemDetail =  items[i].split(':');
				}//설정등을 추가.
				
				//console.log(itemDetail);
				if(itemDetail[0]=='wysiwyg'){
					wysiwygSet($this, itemDetail[1]);
					//console.log(itemDetail[1]);
				}else if(itemDetail[0]=='spinner') $this.spinner();
				else if(itemDetail[0]=='datepicker') $this.datepicker();
				//위지윅 추가
				
			});
			
		});
	};/*============================
		*폼에 추가할것이 있거나 하면 셋팅한다.
		* params
		* 	.$el(jQueryObj) : 폼엘리먼트
		* trigger
		* 	.TB_form_set
		* attr
		* 	. data-TB_form_set
		* 		- wysiwyg : 해당위치에 위지윅을 셋팅한다.
		* 				wysiwyg의 옵션 :  normal, simple, view 
		============================*/
	
	var reqForm = function($el){
		var aftac=undefined
				, beforeac = $el.attr('data-TB_form_beforeAc')
				, resetForm = true
				, sendUrl = $el.attr('post')||swUrl
				, $reqForm
				, sw = true //연속으로 submit 하지 못하게 
				, before_func = (typeof $el.data('before_func') ==='function')?$el.data('before_func'):undefined
				, after_func =(typeof $el.data('after_func') ==='function')?$el.data('after_func'):undefined
				;
		//console.log($el.attr('post'), swUrl);
		if(beforeac){//beforeAc
			beforeac = $.parseJSON(beforeac);
			if(beforeac.ssl === true){sendUrl = sslUrl;}
		}
		
		
		$el
		.ajaxForm({
			url : sendUrl
			, type : 'POST'
			, dataType : 'json'
			, crossDoamin:true
			, beforeSubmit : function(data, $form, options){
				if(!sw){return false;}
				sw = false;

				var adminInfo = TB_auth.getAdminInfo();
				
				if(vadilation($form)!==true){
					sw = true;
					return false;//유효성 검사
				}
				
				if(beforeac){
					if(!beforeAc(beforeac, data, $form)){
						sw= true;
						return false;
					}
				}
				
				if(before_func){
					if(!before_func(data, $form)){return false;}
				}
				/*==/폼전송전 처리==*/
				
				
				//return false;
				
				if($form.attr('data-TB_form_resetForm')){
					var attrResetForm = $form.attr('data-TB_form_resetForm');
					if(attrResetForm==='true'){
						resetForm = true;
					}else if(attrResetForm === 'false'){
						resetForm = false;
					}
					attrResetForm = null;
				}/*==/리셋 폼 ==*/
				
				addArr.push({
					'name' : 'TB_signalKey'
					, 'value' : TB_signalKey
				});//시그널키 입력
				
				if(adminInfo){
					addArr.push({
						'name' : 'TB_adminAuthKey',
						'value' : JSON.stringify(adminInfo)
					});
				}/*==/관리자 정보==*/
				
				if(addArr.length >=0){
					var i = 0, len = addArr.length;
					for(;i<len;i++){
						data.push(addArr[i]);
						//$form.append("<input type='text' name='"+addArr[i].name+"' value='"+addArr[i].value+"' />");
					} 
					addArr = new Array();
				}
				/*==/ 추가할 데이터가 있을 경우 추가 ==*/
					
				aftac = $form.attr('data-TB_form_aftAc'); //폼뒤의 애프터 액션 입력
				if(debug){if(console)console.log('폼 전송', data);}
				$reqForm = $form;
			}
			, success : function(res, status){
					//console.log(TB_shared.get(aftac));
					//console.log(result);
				
				if(!$.isEmptyObject(res.body)){
					if(res.body.TB_dbMessage){
						TB_func.popupDialog(res.body.TB_dbMessage);
					}
					if(res.body.TB_redirect){
						location.replace(res.body.TB_redirect);
						return false;
					}
				}
				
				if(res.header.result==='success'&&after_func){
					after_func(res.body, $reqForm);
				}
				
				if(res.header.result==='success'&&aftac){
					//console.log(res);
					aftAc({
						result : res.body,
						aftac : aftac,
						$form : $el
					});
				}else if(res.header.result==='failed'&&res.body.dbError){
					//console.log(res);
					TB_func.popupDialog(res.body.dbError);
				}else{
					//console.log('폼 에러');
				}
				
				if(resetForm === true){
					$reqForm[0].reset();	
					if($reqForm.find('.wysiwyg-editor').length){
						$reqForm.find('.wysiwyg-editor').html('');
					}
				}
				
				$reqForm = null;
				sw = true;
			}
			, error : function(e){
				sw = true;
				throw e.status+'\n'+e.responseText;
			}
		});
	};/*====================================
	 * 폼 전송시에 처리
	 * params
	 * 	. $el(jQueryObj) : 
	 ====================================*/
	
	var binding = function($el){
		var $form = $el.find('.TB_form')||$(document).find('.TB_form');
		if(!$form.length) return false;
		$form.each(function(){
			var $this = $(this);
			setForm($this);
			reqForm($this);
			$this = null;
		});
	};
	/*====================================
	 * 바인딩
	 ====================================*/
	
	var daum_address = function($thisForm){
		var geocoder = new daum.maps.services.Geocoder();
		
		$thisForm.find('.TB_form_postCodeField').each(function(){
			var $this = $(this)
					;
			daum.postcode.load(function(){
				new daum.Postcode({
					oncomplete : function(postCodeData){
						var address = (postCodeData.userSelectedType == 'J')?postCodeData.jibunAddress:postCodeData.roadAddress;
						
						//주소를 기준으로 좌표값 획득
						geocoder.addr2coord({
							addr : address
							, callback : function(status, result){
								if(status === 'OK'){
									
									//console.log(result);
									postCodeData.lat = result.addr[0].lat;
									postCodeData.lng = result.addr[0].lng;
									
									//postCodeData에 따라 데이터 자동 입력
									$thisForm.find('[data-TB_form_postCodeVal]')
									.each(function(){
										var $postCodeInput = $(this)
												, val = $postCodeInput.attr('data-TB_form_postCodeVal')
												;
										$postCodeInput.val(postCodeData[val]);
										$postCodeInput = null, val = null;
									}).end()
									.find('.TB_form_postCodeAddress').val(address).end()
									.find('.TB_form_postCodeField').css('display','none').end()
									.find('[name=address_2]').focus()
									;
									
									address = null;
								}else{
									TB_func.popupDialog('주소 좌표값을 불러오는데 실패했습니다.');
									return false;
								}
							}
						});
						
						
					}
					, onresize : function(size){
						$this.css({ height:size.height,display:'block','overflow':'auto', 'max-height':'275px'});
					}
					, width : '100%'
					, height : '100%'
				}).embed($this[0]);
			});
		});
	};
	/*====================================
	 * 다음 주소 API 
	 ====================================*/
	
	var eBinding = function($el){
		var $form = $el.find('.TB_form')||$(document).find('.TB_form'),
				$changeSubmit = $form.find('.TB_form_changeSubmit');
		
		if($form.length > 0){
			$form.each(function(){
				
				var $this = $(this)
						, $thisForm = $this 
						;
				var $imageUpload = $this.find('.TB_form_imageUpload')
						, $syncVal = $this.find('.TB_form_syncVal')
						, $postCode = $thisForm.find('.TB_form_postCode')
						;
				
				if($imageUpload.length){
					$imageUpload.off('change')
					.on('change', function(){
						imageUpload($this, $imageUpload);	
					});
				}
				/*==/파일 이미지 업로드 ==*/
				
				if($this.find('.TB_form_chainClick').length){
					var $chainClick =  $this.find('.TB_form_chainClick');
					var target = $chainClick.attr('data-TB_form_chainClick_target');
					var click = "click";
					$chainClick
					.off(click)
					.on(click, function(){
						$(this).closest('form').find(target)
						.trigger(click);
						//click = null, $chainClick = null, target = null;
					});
				}
				/*====/체인 클릭==*/
				
				if($syncVal.length){
					$syncVal.each(function(){
						$(this).off('change.TB_form_syncVal')
						.on('change.TB_form_syncVal', function(){
							var $this = $(this)
									, val = $this.val()
									, dataSyncVal = $this.attr('data-TB_form_syncVal')
									;
							$thisForm.find('[data-TB_form_syncVal='+dataSyncVal+']').each(function(){
								$(this).val(val);
							});
							$this= null, val = null, dataSyncVal = null;
						});
					});
				}
				/*====/해당 폼의 값이 변했을 경우동일 값을 갖는다.==*/
				
				if($this.find('.TB_form_join').length){
					(function(){
						var $join = $this.find('.TB_form_join')
								, $target = $this.find($join.attr('data-TB_form_join_target'))
								, delimiter = $join.attr('data-TB_form_join_delimiter')||''
								,  val = []
								;
						
						$target.off('focusout')
						.on('focusout', function(){
							val = []; 
							$target.each(function(){
								val.push($(this).val());
							});
							$join.val('').val(val.join(delimiter));
						});
					})();
				}
				/*==/구분자를 기준으로 타켓의 값이 입력되거나 했을때 합쳐준다.(전화번호등)==*/
				
				if($changeSubmit.length){
					$changeSubmit.off('change')
					.on('change', function(){
						var $this = $(this);
						var val = $this.val();
						if(!val){
							return false;
						}
						$this.closest('form').find('[type=submit]').trigger('click');
						$this = null, val = null;
					});
				}
				/*==/해당 하는 엘리먼트의 값이 바뀔경우 submit을 작동 시킨다.==*/
				
				if($postCode.length){
					
					//다음 주소 스크립트 생성
					if(!$thisForm.find('.TB_form_postCodeScript').length){
						$thisForm.prepend('<script class="TB_form_postCodeScript" src="http://dmaps.daum.net/map_js_init/postcode.v2.js?autoload=false"></script>');
					}
					
					$postCode.off('click.TB_form_postCode')
					.on('click.TB_form_postCode', function(){
						daum_address($thisForm);
					});
					
				}
				/*==/다음 지도 api==*/
				
			});
		}
	};
	/*====================================
	 * 이벤트 바인딩
	 ====================================*/
	
	return {
		set : set,
		binding : binding,
		eBinding : eBinding,
		autoInsert : autoInsert,
		wysiwygSet : wysiwygSet
		, wysiwyg_in_html : wysiwyg_in_html
	};
})(jQuery, TB_config, TB_shared);

/*표시 안되었지만 사용된 것.
 * 
 *TB_auth,
 */

