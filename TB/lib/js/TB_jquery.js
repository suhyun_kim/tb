


(function($){
	$.fn.each_swap_class = function(evt, swap_class, p_callback){
		var callback = ($.type(p_callback) === 'function')?callback:undefined
				, $this = this
				;
		
		return $this.off(evt+'.each_swap_class')
		.on(evt+'.each_swap_class', function(){
			var $selected = $(this);
			
			//현재 동일한 객체에서 이벤트가 발생할 경우 클래스 추가 안함.
			if(evt === 'click' &&$selected.hasClass(swap_class)){return false;}
			
			$this.each(function(idx, el){
					var $el = $(el);
					if($el.hasClass(swap_class)){$el.removeClass(swap_class);}
			});
			
			$selected.addClass(swap_class);
			if(callback){callback($selected);}
		});
	};
	/*=========================================================
	 * 여러개의 배열 jquery 객체중에서 현재 이벤트가 일어난 객체에 클래스를 추가하고 
	 * 그렇지 않은 객체에서는 클래스를 삭제한다.
	 =========================================================*/
})(jQuery);
