/**
 * @author user
 */
var TB_ui = (function($){
	var create_no = 0
		, ui_storage = {//자동 생성된 객체를 담는다.
			parallaxScroll : {}
		}
		, modern_browser = Modernizr.csstransforms3d 
		;
		
	var Dropdown = function($el){
		
		function dropdown($el){
			this.$top = $el.find('.ST_dropdown');
			this.$header = $el.find('.ST_dropdown_header');//hoverble시 사용될 것
			this.$body = $el.find('.ST_dropdown_body');//
			this.$scope = this.$header.parent()
			this.drop = (modern_browser)?new TimelineMax({paused: true}): {};
			this.options = {
				hoverble : true //hover 시 드롭다운이 될것인지	
				, speed : 0.7
			};
			this.bind();
		}
		/*==/초기화 함수/==*/
		
		dropdown.prototype.bind = function($scope){
			var drop_open= this.drop_open
				, fade_out_close = this.fade_out_close
				, open = this.open
				, close = this.close
				, $body = this.$body
				, $scope = this.$scope
				, drop = this.drop
				, options = this.options
				;
			
			//참고
			//http://codepen.io/rhernando/pen/Apvag
			
			if(this.options.hoverble === true){
				console.log($body.height())
				drop.from($body, options.speed, {height: 0});
				
				this.$header.off('mouseenter.ST_dropdown')
				.on('mouseenter.ST_dropdown', function(){
					//console.log(Modernizr.csstransforms3d);
					console.log('open');
					(modern_browser)?drop_open(drop) : open($body);
				});
				
				$scope.off('mouseleave.ST_dropdown')
				.on('mouseleave.ST_dropdown', function(){
					console.log('leave');
					(modern_browser)?fade_out_close($body, tl, options) : close($body);
				});
			}
		}
		/*==/이벤트 바인딩/==*/
		
		
		dropdown.prototype.drop_open = function(drop){
				
			drop.play();
			return false;
			tl.set($body, {
				css:{display:'block', height:0, autoAlpha:1}
			})
			.to($body, options.speed , 
				{ height: height
					, onComplete : function(){
						$body.css({
							height: ''
							, visibility: '', opacity:''
						});
					}
				}
			)
		}
		
		dropdown.prototype.fade_out_close = function($body, tl, options){
			console.log('sdf');
			tl.clear()
			.to($body, options.speed ,{
				//alpha : 0
				 css : {
					autoAlpha : 0
				}
				, onComplete : function(){
					console.log('ddfsd');
					$body.css({
						'display': ''
						, 'opacity':''
					});
				}
			})
		}
		
		dropdown.prototype.open = function($body){
			
		}
		
		dropdown.prototype.close = function($body){
			
		}
		
		return new dropdown($el);
	};
	 /*=========================================
	  * 드롭다운
	  =========================================*/
	
	
		
	var get_storage = function(ui_type, no){
		//console.log(ui_storage)
		if(ui_storage[ui_type][no]){
			return ui_storage[ui_type][no];
		}
	};
	 /*=========================================
	  * ui_storage에서 생성된 객체를 확인한다 
	  =========================================*/
	 
	 var parallax_reset = function($scope){
	 	var $scope = $scope
	 		, $parallax = $scope.find('.TB_ui_parallaxScroll')
	 		;
		$parallax.each(function(){
			var no = this.getAttribute('tb_ui_parallaxscroll_no')
				;
			if(no){
				//console.log(no);
				ui_storage.parallaxScroll[no].set_status();
			}
		});
	 };
	 /*=========================================
	  * 지정된 jquery 객체내의 status 를 재설정한다.
	  =========================================*/

	 var ParallaxScroll = function($el, create_no){
	 	var $win = $(window) 
	 			;
	 	/*=======/스클롤 이벤트 =========*/
	
	 	function parallaxScroll($el, create_no){
	 		this.create_no = create_no;
	 		this.tl = new TimelineMax;
			this.$el = $el;
			this.options = {
				animation: ''//left, leftReverse, top, topReverse (아무것도 없을 경우 반응형)
				, power : 1//애니메이션이 움직이는 정도.
				, move_distance : 0//스크롤시 움직일 거리
				, container : ''//부모 컨테이너 객체 지정되어 있지 않으면 parent()
			}, 
			this.status = {
				$container : {}//부모 컨테이너 jquery 객체
				, container_width :0
				, container_height : 0 
				, container_left:0//엘리먼트의 초기 offset()값
				, container_top:0
				, width : 0
				, height : 0
				, start : 0//애니메이션 시작값
				, end : 0//컨테이너 끝값
				, margin_top: 0//마진이 있을 경우 처리할 값
				, margin_left :0//
			}
			;
			
			//고유 번호 생성
			this.$el.attr('TB_ui_parallaxScroll_no', this.create_no);
	 	}
	 	/*==/ 생성자 함수 /==*/
	 	
	 	//var parallaxScroll = new _parallaxScroll($el, create_no);
	 	
	 	parallaxScroll.prototype.init= function(){
	 		var p = this
 				, json_opts = p.$el.attr('data-TB_ui_parallaxScroll')
 				, offset
 				, winWid = TB_shared.get('winWid')
 				, winHei = TB_shared.get('winHei')
 				;
 			
 			
	 		//옵션 merge
	 		if(json_opts){
		 		p.options = $.extend( p.options, JSON.parse(p.$el.attr('data-TB_ui_parallaxScroll')));
	 		}
	 		
	 		p.older_sc = {};
	 		
	 		//animation 처리
	 		if(!p.options.animation){
	 			p.options.response = true;
	 		}
	 		if(p.options.response === true){
	 			p.options.animation = (winWid > 991)?'left':'top';
	 		}
	 		
	 		//스테이터스 설정
			 if(p.$el[0].tagName === 'IMG'){
			 	p.$el.load(function(){
			 		p.set_status();
			 	});
			 }
			 
			 p.set_status();
	 	};
	 	/*==/ 패럴렉스 엘리먼트 개체 생성 /==*/
	 	
	 	parallaxScroll.prototype.set_status = function(){
	 		var p = this
	 			, offset = {}//컨테이너 위치
 				, winWid = TB_shared.get('winWid')
 				, winHei = TB_shared.get('winHei')
	 			;
	 		//console.log('set_status', p.create_no);
	 		//container 값
	 		p.status.$container = (!p.options.container)?p.$el.parent():p.$el.closest(p.options.container);
	 		
	 		p.status.container_width = p.status.$container.outerWidth(); 
	 		p.status.container_height = p.status.$container.outerHeight();
			p.status.width = p.$el.outerWidth();
			p.status.height = p.$el.outerHeight();
			
			//컨테이너 위치
	 		offset = p.status.$container.offset();
	 		p.status.container_left = offset.left;
	 		p.status.container_top = offset.top;
	 		
	 		//제한  및 마진 있을 경우 처리
	 		if(p.options.animation === 'left'|| p.options.animation === 'leftReverse'){
				p.status.start = (winWid > p.status.container_left)?0 : p.status.container_left - winWid;
		 		p.status.end = p.status.container_left+p.status.container_width;
	 		}else if(p.options.animation === 'top'|| p.options.animation === 'topReverse'){
				p.status.start = (winHei > p.status.container_top)?0 : p.status.container_top - winHei;
		 		p.status.end = p.status.container_top+p.status.container_height;
	 		}
	
	 		//move_distance  
		 	if(p.options.animation === 'left'|| p.options.animation === 'leftReverse'){
			 	p.options.move_distance = p.status.width - p.status.container_width;
				p.status.margin_left = p.$el.css('margin-left');
				p.status.margin_left = (p.status.margin_left)?parseInt(p.status.margin_left.slice(0,-2),10):0;
		 	}else if(p.options.animation === 'top'|| p.options.animation === 'topReverse'){
			 	p.options.move_distance = p.status.height - p.status.container_height;
				p.status.margin_top = p.$el.css('margin-top');
				p.status.margin_top = (p.status.margin_top)?parseInt(p.status.margin_top.slice(0,-2),10):0;
		 	}
		 	
		 	//move_distance 가  마이너스 일경우 0으롤 바꿔서 움직임을 주지 않는다.
		 	if(p.options.move_distance < 0){
		 		p.options.move_distance = 0;
	 		}
	 		
	 	};
	 	/*==/스테이터스 설정/==*/
	 	
	 	
	 	parallaxScroll.prototype.evt_scroll = function(){
	 		var p = this;
	 		
		 	$win.off('scroll.TB_ui_parallaxScroll_'+this.create_no)
		 	.on('scroll.TB_ui_parallaxScroll_'+this.create_no, function(e){
				var sc = {
							left : $win.scrollLeft()
							, top : $win.scrollTop()
						}
	 					, move
	 					, move_obj = {ease:Power0.easeNone}
	 					//, p = parallaxScroll
	 					, ani = p.options.animation
	 					, winWid = TB_shared.get('winWid')
	 					;
	 					
	 			//if(!sc.left){alert(sc.left);}
	 			//console.log(sc);
		 		//console.log('evt_scroll', this.create_no);
	 			
	 			//법위를 벗어났을 경우 처리 안함.
	 			if(ani === 'left' || ani === 'leftReverse'){
	 				if(p.status.start > sc.left || p.status.end < sc.left){return false;}
		 			move = sc.left * (p.options.move_distance+p.status.margin_left) / (p.status.container_left+p.status.container_width) ;
		 			move = Math.floor(move*p.options.power);
		 			if(ani=== 'left'){move = -move;}
	 				move_obj.y = null;
		 			move_obj.x = move;
	 			}
	 			else if(ani === 'top' || ani === 'topReverse'){
	 				
	 				if(p.status.start > sc.top || p.status.end < sc.top){return false;}
		 			move = sc.top * (p.options.move_distance+p.status.margin_top) / (p.status.container_top+p.status.container_height) ;
		 			move = Math.floor(move);
		 			if(ani === 'top'){move = -move;}
	 				if(move_obj.x){delete move_obj.x;}
		 			move_obj.y = move;
		 		}
 				//console.log(ani,move_obj);
 				//console.log('ani',$.event);
 				//console.log('ani',move_obj, sc);
 				
	 			TweenLite.to(p.$el, 0.2,move_obj);
		 	});

	 	};
 		/*==/ 스크롤시 사용할 함수 등록 /==*/
 		
 		parallaxScroll.prototype.response = function(){
 			var setFunc = undefined
 				, p = this
 				;
 			//console.log(parallaxScroll.create_no);
			//모바일에서 이것때문에 버그가 있는듯 하다.
 			$win.off('resize.TB_ui_parallaxScroll_resize_'+this.create_no)
 			.on('resize.TB_ui_parallaxScroll_resize_'+this.create_no, function(){
 				if(TB_config.get('isMobile')){return false;}
 				clearTimeout(setFunc);
 				setFunc = setTimeout(function(){
 					TweenLite.to(p.$el, 0, {x:0,y:0});
 					p.init();
 					//console.log(parallaxScroll.options.animation);
 				}, 500);
 			});
 		};
 		/*==/ 반응형 처리 /==*/
 		
 		parallaxScroll.prototype.create = function(){
		 	this.init();
	 		this.evt_scroll();
	 		this.response();
	 		// console.log(this.status)
	 		// console.log(this.options)
 		};
 		/*==/ 생성자 함수 /==*/
	 	
	 	return  new parallaxScroll($el, create_no);;
	 };
	 /*=========================================
	  * 병렬 스크롤 
	  =========================================*/

	
	var setUi = function($el,_ui){
		var arr = _ui.split('_'), val='';
		$el.ready(function(){
			switch(arr[0]){
				case 'draggable'://드래그블
					$el.draggable();
				break;
				case 'resizable'://리사이즈
					$el.resizable();
				break;
			}
		});
	}/*=========================================
	  *옵션별로 UI를 적용
	  * params
	  * 	. $el : 적용대상 엘리먼트 
	  *  	. _ui :ui 옵션 
	  =========================================*/
	 
	 var acFunc = function($el, _obj){
	 	var obj = {
	 				'ac':'click'
	 				, 'sharedFunc':''
	 				, 'sendData':''
	 			}
	 			, evt =''//이벤트 명 
	 			;
	 	
	 	if(!arguments || $.type(obj) != 'object'){ return false; }
	 	
	 	obj = $.extend(obj, _obj);
	 	evt = obj.ac+'.'+obj.sharedFunc;
	 	
	 	$el.on(evt, function(){
	 		if($.type(TB_shared.get(obj.sharedFunc)) === 'function' ){
	 			TB_shared.get(obj.sharedFunc)(obj.sendData);
	 		}
	 	});
	 	
	 };
	 /*=========================================
	  *클릭등의 이벤트에 sharedFunc.를 바인딩
	  * params
	  * 	. $el(jquery obj) : 이벤트를 바인딩할 오브젝트
	  * 	. obj(obj) : 내부 객체 
	  =========================================*/
	 
	 
	 var Slider = function($el){
	 	
	 	function _slider($el){
	 		this.$el = $el;
	 		this.$ul = $el.find('ul');//슬라이드 모아놓은 컨테이너
	 		this.$li = this.$ul.find('li');//슬라이드
	 		this.$next_btn = this.$el.find('.next_btn');
	 		this.$prev_btn = this.$el.find('.prev_btn');
	 		this.setFunc;//타임 아웃 함수를 저장 할 변수
	 		this.tl = (Modernizr.csstransforms3d)?new TimelineMax:{};
	 	
	 		this.options = {
	 			delay : 10000 //슬라이드간의 딜레이
	 			, moveSpeed : 0.7//슬라이드가 움직이는속도
	 			, autoPlay : false //자동 움직입
	 			, animationType : 'left' //left , fadeInOut
	 			, ctrl_btn : true //움직임  버튼
	 			, navigator : true//네비게이터
	 			, navigator_icon: 'fa-circle-o'
	 			, navigator_icon_active : 'fa-circle'
	 			, ease : Expo.easeOut
	 			, callback : undefined//슬라이드 시에  호출될 콜백함수 
	 			, auto_size : true // 이미지로드후 이미지의 사이즈를 잰 후 .TB_ui_slider 에 적용한다.
	 		};
	 		
 			this.status = {
 				pointer : 0 //현재 슬라이더 번호
 				, prev_pointer : this.$li.length-1
 				, next_pointer : 1
 				, width : undefined //기준 width
 				, height : undefined //기준 height
 				, sw : true//다음 액션을 취하지 못하도록 처리.
 				, length : 0//슬라이더 갯수
 			};
	 		
	 	};
	 	/*============/slide 개체 ==============*/
	 	
	 	var slider = new _slider($el); 
	 	
	 	slider.setSlider = function(){
	 		var height = this.status.height = this.$el.outerHeight();
	 		var width = this.status.width = this.$el.outerWidth();
	 		var length
	 				, options = slider.$el.attr('data-TB_ui')
	 				, $first_img = slider.$ul.find('img').first()
	 				;
	 		
	 		//console.log(slider.$ul.find('img').first());
	 		
	 		if(options){
	 			slider.options = $.extend(slider.options, JSON.parse(options));
	 		}
	 		
	 		//이미지로드후 이미지의 사이즈를 잰 후 .TB_ui_slider 에 적용한다.
	 		if(slider.options.auto_size === true){
	 			
		 		$first_img.load(function(){
					var f_wid = $first_img.width()
							, f_hei = $first_img.height()
							, old_wid
							;
					if(f_wid > TB_shared.get('winWid')){
						old_wid = f_wid;
						f_wid = TB_shared.get('winWid')+(-20);
						f_hei = f_hei*f_wid/old_wid;
		 				slider.$ul.find('img').css('width', '100%');
					}
		 			slider.$el.css({
		 				'width': f_wid,
		 				'height': f_hei, 
		 				'left' : '50%', 'top' : '50%',
		 				'margin-left' : -Math.floor(f_wid/2),
		 				'margin-top' : -Math.floor(f_hei/2)
		 			});
		 			slider.options.auto_size = false;
		 			slider.setSlider();
		 		});
		 		
	 		}
	 		
	 		//슬라이더 갯수가 2개일 때
	 		if(slider.$li.length === 2){
	 			this.$ul.append(this.$li[0].outerHTML); 
	 			this.$ul.prepend(this.$li[slider.$li.length-1].outerHTML); 
	 		}else if(slider.$li.length === 1 || !slider.$li.length){
	 			return false;
	 		}
	 		
 			this.$li = this.$ul.find('li');
 			length = this.status.length  = this.$li.length;
 			
 			//슬라이드 위치 셋팅
	 		this.$li.each(function(idx){
	 			var css = {
	 						'position' : 'absolute'
	 						, 'display' : 'block'
	 					}, 
	 					x = 0
	 					;
	 			
	 			if(idx ===0){//첫번째
	 				x = idx*width;
	 				css.display = 'block';
 				}else if(idx === 1){//두번째
	 				x = width; 
	 				css.display = 'block';
	 			}else if(idx === length-1){//마지막
	 				x = -width; 
	 				css.display = 'block';
 				}else{//그외
 					x = idx*width;
 					css.display = 'none';
 				}
 				
	 			$(this).css(css);
	 			slider.tl.to(this, 0, { x : x });
	 		});
	 		
			if(slider.options.callback){
				TB_ui.sliderCallback(slider.options.callback)(slider.$el, slider.$li, slider.options, slider.status);
			}
	 		
	 		//slider.drag_evt();
	 	};
	 	/*============/초반 슬라이드 셋팅 ==============*/
	 	
	 	_slider.prototype.setElements = function($el, _options){
	 		var options = {
			 			ctrl_btn : _options.ctrl_btn||true
			 			, navigator : _options.navigator||true
			 		}
			 		, $slider = $el
			 		, length = slider.status.length
			 		, createHTML = ''
			 		;
			 
			//컨트롤 버튼
	 		if(options.ctrl_btn === true){
	 			//next_btn 버튼
	 			if(!$slider.find('.next_btn').length){
		 			createHTML += '<div class="slider_next_btn slider_ctrl" >';
		 			createHTML += '<span class="fa fa-angle-left"></span>';
		 			createHTML += '</div>';
	 			}
	 			
	 			//next_btn 버튼
	 			if(!$slider.find('.prev_btn').length){
		 			createHTML += '<div class="slider_prev_btn slider_ctrl" >';
		 			createHTML += '<span class="fa fa-angle-right"></span>';
		 			createHTML += '</div>';
	 			}
	 		}
	 		
	 		//네비게이터
	 		if(options.navigator === true && !$slider.find('.slider_navigator').length){
	 			createHTML += '<div class="slider_navigator">';
	 			createHTML += '<div class="slider_navigator_container ST_float_left_wrap ST_float_left_margin_02_em">';
	 			for(var i = 0 ; i < length ; i++){
	 				if(i === 0){ 
		 				createHTML += '<div class="slider_navigator_cover navigator_active" data-idx='+i+'><span class="fa '+slider.options.navigator_icon_active+'"></span></div>';
 					}else{
		 				createHTML += '<div class="slider_navigator_cover" data-idx='+i+'><span class="fa '+slider.options.navigator_icon+'"></span></div>';
 					}
	 			}
	 			createHTML += '</div>';
	 			createHTML += '</div>';
	 		}
	 		
 			$slider.append(createHTML);
	 	};
	 	/*============/초반 개체 셋팅 ==============*/
	 	
	 	slider.reposition = function(direction){
	 		
	 		//console.log(slider.status.prev_pointer, slider.status.pointer, slider.status.next_pointer);
	 		if(direction === 'next'){
		 		if(slider.status.pointer === 0){//처음
		 			//console.log(direction, '처음');
		 			slider.status.pointer++;
		 			slider.status.prev_pointer = 0;
		 			slider.status.next_pointer = slider.status.pointer+1;
		 		}else if(slider.status.pointer === slider.status.length-1){//마지막
		 			//console.log(direction, '마지막');
		 			slider.status.pointer = 0;
		 			slider.status.prev_pointer = slider.status.length-1;
		 			slider.status.next_pointer = slider.status.pointer+1;
		 		}else{//보통
		 			//console.log(direction, '보통');
		 			slider.status.pointer++;
		 			slider.status.prev_pointer = slider.status.pointer-1;
		 			slider.status.next_pointer =(slider.status.pointer === slider.status.length-1)?0:slider.status.pointer+1;
		 		}
	 		}else if(direction === 'prev') {
		 		if(slider.status.pointer === 0){//처음
		 			//console.log(direction, '처음');
		 			slider.status.pointer = slider.status.length-1;
		 			slider.status.prev_pointer = slider.status.pointer-1;
		 			slider.status.next_pointer = 0;
		 		}else if(slider.status.pointer === 1){//2번째
		 			//console.log(direction, '2번째');
		 			slider.status.pointer--;
		 			slider.status.prev_pointer = slider.status.length-1;
		 			slider.status.next_pointer = slider.status.pointer+1;
		 		}else{//보통
		 			//console.log(direction, '일반');
		 			slider.status.pointer--;
		 			slider.status.prev_pointer = slider.status.pointer-1;
		 			slider.status.next_pointer = slider.status.pointer+1;
		 			//slider.status.next_pointer =(slider.status.pointer === 0)?0:slider.status.pointer+2;
		 		}
	 		}
	 		
	 		//display block 인지 알아보기.
 			slider.$li.each(function(idx){
 				var display = '';
 				if(idx == slider.status.pointer || idx == slider.status.prev_pointer || idx == slider.status.next_pointer){
 					display = 'block';
 				}else{
 					display = 'none';
 				}
				$(this).css('display', display);
 			});
 			
 			//슬라이더 위치 재조정
 			//console.log(slider.status.width);
	 		slider.tl.clear()
	 		.to(slider.$li[slider.status.prev_pointer], 0, {
	 			x : -slider.status.width
	 		})
	 		.to(slider.$li[slider.status.pointer], 0, {
	 			x : 0
	 		})
	 		.to(slider.$li[slider.status.next_pointer], 0, {
	 			x : slider.status.width
	 		})
	 		;
	 		
	 		if(slider.options.navigator == true){
	 			slider.navigatorView();
	 		}
	 		
	 		//이벤트 활성화
	 		slider.navigator_evt();
	 		slider.ctrl_evt();
	 	};
	 	/*======================================
	 	 * /이동전 위치 및 포인터 셋팅 
	 	 * . 한번 play 된 뒤에 작동
	 	 * ======================================*/
	 	
	 	slider.navigatorView = function(_idx){
	 		var pointer =  ($.type(_idx) === 'number')?_idx : slider.status.pointer;
	 		
	 		//네비게이터 위치 표시
	 		slider.$el.find('.slider_navigator_cover').each(function(idx){
	 			if(idx === pointer){
	 				$(this).addClass('navigator_active')
	 				.find('.fa').removeClass(slider.options.navigator_icon).addClass(slider.options.navigator_icon_active);	
	 			}else{
	 				$(this).removeClass('navigator_active')
	 				.find('.fa').removeClass(slider.options.navigator_icon_active).addClass(slider.options.navigator_icon);	
	 			}
	 		});
	 		
	 	};
	 	/*======================================
	 	 * 네이게이터 표시. 
	 	 * ======================================*/
	 	
	 	slider.play = function(_direction, callback){
	 		var options = this.options
	 				, status = this.status
	 				, direction  = _direction||'next'
	 				, obj = {
	 					moveSpeed : options.moveSpeed
	 					, distance  : {
	 						pointer : 0
	 						, prev : undefined
	 						, next : undefined
	 					}
	 					, onComplete : function(){//움직임 완료후 함수.
	 						var $curEl = $(slider.$li[slider.status.pointer])
	 								;
		 					if($.type(callback) === 'function'){callback();}
			 				slider.reposition(direction);
			 				//console.log($ui.length);
			 				
			 				//콜백함수 등록이 되어 있으면 실행.
							if(slider.options.callback){
								TB_ui.sliderCallback(slider.options.callback)(slider.$el, slider.options, slider.status);
							}

			 				//현재 슬라이더에 UI 재적용
			 				slider.setFunc = setTimeout(function(){
			 					status.sw = true;
			 					if(options.autoPlay){
				 					slider.play('next');
			 					}
			 				}, slider.options.delay);
	 					}
	 				}
	 				;
	 		
	 		if(!this.status.sw){
	 			return false;
	 		}
	 		status.sw = false;//다른 동작을 못하게 스위치는 끈다.
	 		
	 		slider.stop();
	 		
	 		if(direction === 'next'){
	 			obj.distance.pointer = -status.width;
	 			obj.distance.prev_pointer = -(status.width*2);
	 			obj.distance.next_pointer = 0;
	 		}else if(direction === 'prev'){
	 			obj.distance.pointer = status.width;
	 			obj.distance.prev_pointer = 0;
	 			obj.distance.next_pointer = +(status.width*2);
	 		}
	 		
	 		this.tl.clear()
	 		.to(slider.$li[slider.status.pointer], obj.moveSpeed, {
	 			x : obj.distance.pointer
	 			, onComplete : obj.onComplete
	 			, ease : options.ease
	 		})
	 		.to(slider.$li[slider.status.prev_pointer], obj.moveSpeed, {
	 			x : obj.distance.prev_pointer
	 			, ease : options.ease
	 		}, 0)
	 		.to(slider.$li[slider.status.next_pointer], obj.moveSpeed, {
	 			x : obj.distance.next_pointer
	 			, ease : options.ease
	 		}, 0)
	 		;
	 		
	 	};
	 	/*============/움직임 ==============*/
	 	
	 	slider.stop = function(){
	 		slider.status.sw = false;
	 		//slider.tl.clear();
	 		clearTimeout(slider.setFunc);
	 	};
	 	/*============/멈추기 ==============*/
	 	
	 	slider.navigator_move = function(_idx){
	 		var idx= parseInt(_idx,10);
	 		
	 		if(idx == NaN){throw 'navigator의 type이 number가 아닙니다.'; return false;}
	 		
	 		//같을 경우
	 		if(idx===slider.status.pointer){
	 			return false;
	 		}else if(idx === 0 && slider.status.pointer === slider.status.length-1){//마지막에서 첫번째 것을 누를때
	 			slider.status.next_pointer = slider.status.length-2;
	 			$(slider.$li[slider.status.next_pointer]).css('display','none');
	 			slider.status.prev_pointer = 0;
	 			$(slider.$li[0]).css('display','block');
	 			slider.tl.clear().to(slider.$li[0],0,{x:-slider.status.width});
	 			slider.play('prev', function(){
		 			slider.status.pointer = idx+1;
	 			});
 			}else if(idx > slider.status.pointer){//클때
	 			//다음 포인터의  변경과 슬라이더 위치  조정
	 			$(slider.$li[slider.status.next_pointer]).css('display','none');
	 			slider.status.next_pointer = idx;
	 			$(slider.$li[idx]).css('display','block');
	 			slider.tl.clear().to(slider.$li[idx],0,{x:slider.status.width});
	 			slider.play('next', function(){
		 			slider.status.pointer = idx-1;
	 			});
	 		}else if(idx < slider.status.pointer){//작을 때
	 			$(slider.$li[slider.status.prev_pointer]).css('display','none');
	 			slider.status.prev_pointer = idx;
	 			$(slider.$li[idx]).css('display','block');
	 			slider.tl.clear().to(slider.$li[idx],0,{x:-slider.status.width});
	 			slider.play('prev', function(){
		 			slider.status.pointer = idx+1;
	 			});
	 		}
	 	};
	 	/*============ / 네비게이터 움직임 / ==============*/
	 	
	 	slider.mouseenter = function(){
	 		slider.$el.off('mouseenter.slider_enter')
	 		.on('mouseenter.slider_enter', function(){
	 			if(!slider.options.autoPlay){ return false;}
	 			slider.stop();
	 		});
	 		
	 		slider.$el.off('mouseleave.slider_leave')
	 		.on('mouseleave.slider_leave', function(){
	 			if(!slider.options.autoPlay){return false;}
	 			slider.status.sw = true;
	 			sliderFunc = setTimeout(function(){
	 				 slider.play('next');
	 			}, slider.options.delay);
	 		});
	 	};
	 	/*============/마우스 오버 이벤트 ==============*/
	 	
	 	slider.ctrl_evt = function(){
	 		if(!slider.options.ctrl_btn){return false;}
	 		slider.$el.find('.slider_next_btn').off('click.slider_next_btn')
	 		.on('click.slider_next_btn', function(){
		 		slider.$el.find('.slider_prev_btn').off('click.slider_prev_btn');
	 			slider.status.sw = true;
	 			//console.log('prev_btn');
	 			slider.navigatorView(slider.status.prev_pointer);
	 			slider.play('prev');
	 		});
	 		
	 		slider.$el.find('.slider_prev_btn').off('click.slider_prev_btn')
	 		.on('click.slider_prev_btn', function(){
		 		slider.$el.find('.slider_next_btn').off('click.slider_next_btn');
	 			slider.status.sw = true;
	 			//console.log('next_btn');
	 			slider.navigatorView(slider.status.next_pointer);
	 			slider.play('next');
	 		});
	 		
	 	};
	 	/*============/컨트롤 버튼 클릭 이벤트 ==============*/

	 	slider.navigator_evt = function(){
	 		if(!slider.options.navigator){return false;}
	 		slider.$el.find('.slider_navigator_cover').off('click.navigator_evt')
	 		.on('click.navigator_evt', function(){
	 			var idx = parseInt($(this).attr('data-idx'), 10);
	 			//console.log('navigator_click', $(this).attr('data-idx'));
		 		slider.$el.find('.slider_navigator_cover').off('click.navigator_evt');
	 			slider.status.sw = true;
	 			slider.navigatorView(idx);
	 			
	 			slider.navigator_move($(this).attr('data-idx'));
	 		});
	 	};
	 	/*============/네비게이터 버튼 클릭 이벤트 ==============*/
	 	
	 	slider.drag_evt = function(){
	 		//console.log('drag_evt');
	 		Draggable.create(
	 			slider.$li,
	 			{
	 				type:'x'
	 				, edgeResistance:0.5
	 				, throwProps: true
	 				, dragResistance : 0.5
	 				, onDrag : function(e){
	 					var x = $(slider.$li[slider.status.pointer]).position().left;
	 					//console.log(arguments);
	 					
	 					slider.tl.clear()
	 					.to(slider.$li[slider.status.prev_pointer],0,{
	 						x : x+(-slider.status.width)
	 					})
	 					.to(slider.$li[slider.status.next_pointer],0,{
	 						x : x+(slider.status.width)
	 					});
	 				}
	 				, onDragEnd : function(){
	 					var x = $(slider.$li[slider.status.pointer]).position().left
	 							, moveSpeed = 0.1;
	 					
	 					slider.status.sw = true;
	 					
	 					if(x  < -15){
		 					//console.log('dragEnd_next', x);
		 					slider.navigatorView(slider.status.next_pointer);
	 						slider.play('next');
	 					}else if(x > 15){
		 					//console.log('dragEnd_prev', x);
		 					slider.navigatorView(slider.status.prev_pointer);
	 						slider.play('prev');
	 					}else{
		 					//console.log('dragEnd', slider.options.moveSpeed);
		 					//console.log('dragEnd', x);
	 						slider.tl.clear()
	 						.to(slider.$li[slider.status.prev_pointer], moveSpeed,{
		 						x : -slider.status.width
		 						, esse : Expo.easeOut
		 					})
	 						.to(slider.$li[slider.status.pointer], moveSpeed,{
		 						x : 0
		 						, esse : Expo.easeOut
		 					}, 0)
	 						.to(slider.$li[slider.status.next_pointer], moveSpeed,{
		 						x : slider.status.width
		 						, esse : Expo.easeOut
		 					}, 0)
		 					;
	 					}
	 				}
 				}
 			);
	 	};
	 	/*============/드래그 이벤트 ==============*/
	 	
	 	slider.resize = function(){
	 		var setFunc; 
	 		$(window).off('resize.TB_ui_slider_resize')
	 		.on('resize.TB_ui_slider_resize', function(){
	 			clearTimeout(setFunc);
	 			setFunc = setTimeout(function(){
			 		slider.status.height = slider.$el.outerHeight();
			 		slider.status.width = slider.$el.outerWidth();
					//console.log(slider.$el.width(), slider.$el.height(), slider.status.width, slider.status.height);
					slider.reposition();
		 			//console.log(slider.$el);
	 			}, 1000);
	 		});
	 	};
	 	/*============/리사이즈 이벤트 ==============*/
	 	
	 	slider.init = function(){
	 		this.setSlider(this.pointer);
	 		if(slider.status.length < 2){return false;}
	 		slider.setElements(slider.$el, slider.options);
	 		if(slider.options.autoPlay){
		 		setTimeout(function(){
			 		slider.play();
		 		}, slider.delay);
	 		}
	 		this.mouseenter();
	 		this.ctrl_evt();
	 		this.navigator_evt();
	 		this.drag_evt();
	 		slider.resize();
	 	};
	 	/*============/초기화 ==============*/
	 	
	 	return slider;
	 };
	 /*=========================================
	  * 슬라이더 
	  =========================================*/
	 
	 var Tab = function($container){
	 	function _tab($container){
	 		var $container = $container
	 				, $tab = $container.find('.tab')
	 				, $location = $container.find('.tab_location')
	 				;
	 		//초기화
	 		$($tab[0]).addClass('active');
	 		$location.each(function(idx){
	 			$(this).css('display',(idx==0)?'block':'none');
	 		});
	 		
	 		$tab.off('click.TB_ui_tab')
	 		.on('click.TB_ui_tab', function(){
				var $this  = $(this)
						, tab =$this.attr('data-tab')
						;
					
				//탭 표시
				$tab.each(function(){
					var $this = $(this);
					($this.attr('data-tab') == tab)?$this.addClass('active'):$this.removeClass('active');
					$this = null;
				});
				
				//지정된 로케이션 보이기.
				$location.each(function(){
					var $this = $(this)
							,css = {};
					css = ($this.attr('data-tab_location')==tab)?{display:'block'}:{display:'none'};
					
					$this.css(css);
					$this = null, css = null;
				});
				$this = null, tab = null;
	 		});
	 		;
	 	}
	 	
	 	var tab = new _tab($container);
	 	
	 	return tab;
	 };
	 /*=========================================
	  * 탭 
	  =========================================*/
	
	var binding = function($el){
		var $scope = $el||$(document)
			, $acFunc = $scope.find('.TB_ui_acFunc')
			, $slider = $scope.find('.TB_ui_slider')
			, $tab= $scope.find('.TB_ui_tab')
			, $parallaxScroll = $scope.find('.TB_ui_parallaxScroll')
			;
		
		if($scope.find('.TB_ui').length){
			$scope.find('.TB_ui').each(function(){
				var $this = $(this);
				var arr = $this.attr('data-TB_ui').split('||'), len = arr.length, i = 0;
				for(;i < len ; i++) setUi($this,arr[i]);
			});
		}
		
		if($acFunc.length){
			$acFunc.each(function(){
				var $this = $(this)
						, obj = $.parseJSON($this.attr('data-TB_ui_acFunc'))
						;
				acFunc($this, obj);
			});
		}
		
		if($slider.length){
			var slider_list = {};
			$slider.each(function(idx){
				slider_list[idx] = Slider($(this));
				slider_list[idx].init();
			});
		}/*==/슬라이더==*/
		
		if($tab.length){
			$tab.each(function(){
				Tab($(this));
			});
		}
		/*==/슬라이더==*/
		
		if($parallaxScroll.length){
			$parallaxScroll.each(function(){
				if(!Modernizr.csstransforms3d){return false;}
				
				ui_storage.parallaxScroll[create_no] = ParallaxScroll($(this), create_no);
				ui_storage.parallaxScroll[create_no].create();
				
				create_no++;
			});
		}
		/*==/병렬스크롤==*/

		
	};
	/*========================================================
	 *UI를 적용 시킨다.
	  * parmas
	  * 	. $el (jQueryElement) : 해당하는 엘리먼트에 적용한다.
	  *========================================================*/
	 
	return {
		binding : binding
		, get_storage : get_storage
		, parallax_reset : parallax_reset
		, dropdown : Dropdown
	}
})(jQuery);


;