var TB_config =(function (){
	var instance = [] ,id = 0, config = {};
	/*==/변수 설정==*/
	
	var makeGetId = function($id){
		return function(){
			return $id;
		}
	}
	/*==/키생성 ==*/
	
	var setConfig = function(key, val){
		if(arguments.length === 2&&!getConfig(key)){ return  config[key] = val;}
	};
	/*/=============================
	 * 설정셋팅 
	 * detail
	 *   .설정은 한번만 할 수 있고 기존 설정이 있을 경우는 새로 설정할 수 없다.
	 * =============================*/
	
	var getConfig = function ($var){
		return arguments.length ? config[$var] : TB_func.objCopy(config);
	}
	/*==/설정 리턴 ==*/
	
	return {
		set : setConfig
		, get : getConfig
	};
}());/*==/TB==*/




