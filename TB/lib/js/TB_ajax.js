var TB_ajax =(function ($, TB_config, TB_func){
	var TB_signalKey;//시그널키
	var swUrl;//데이터를 전송할 곳의 주소
	
	var set = function(_name, _val){
		if(_name==='TB_signalKey') TB_signalKey = _val;
		else if(_name==='swUrl') swUrl = _val;
		//console.log(TB_signalKey, swUrl);
	};
	/*============================
		*private 변수 셋팅
		============================*/
		
	var result = function(_data){
		return (_data.header.result=='success')?true : false;
	};
	/*============================
		*성공했을 경우 true를 반환 실패했을 경우 false를 반환
		============================*/
		
	var popupDialog = function(_str, _delay){
		var $dialog =  $('#TB_popupDialog')
				, text = ''
				, delay = _delay||1000;
		//console.log($dialog);
		if(!$dialog.length){
			text = "<div id='TB_popupDialog' "; 
			text += " style='line-height:1.5em;'>";
			text += "</div>";
			$('#TB_globalTemplates_wrap').append(text);
			$dialog =  $('#TB_popupDialog');
			TB_func.createReady($dialog);
		}
		$dialog.html(_str).stop().fadeIn().delay(delay).fadeOut();
		$dialog = null, text =null, delay = null;
	};
	/*============================
		*메시지 띄우기
		============================*/
	
	var get_json = function(_obj){
		var op = {
					'post' : null//form의 post 데이터
					, 'sendData' : {}//전송할 데이터 
					, 'callback' :  null
					, 'dataType' : 'json'//json으로 받을 것인지 
				}, 
				adminAuthKey = TB_auth.getAdminInfo()
				;
		
		$.extend(op, _obj);
		
		if(op.post){
			for(var i in op.post){
				if(op.post.hasOwnProperty(i)) op.sendData[i] =op.post[i]; 
			}
		}
		
		op.sendData.reqType = op.sendData.reqType||'select';
		op.sendData.TB_signalKey = TB_signalKey;
		
		if(adminAuthKey){
			op.sendData.TB_adminAuthKey = adminAuthKey;
		}//관리자 정보가 있을 경우.
		
		//if(console){console.log('send: '+op.sendData.reqQuery, op);}
		//console.log($.type(op.callback));
		//console.log(op.sendData.reqType,':',op.sendData.reqQuery );
		$.ajax({
			url : swUrl
			, type : 'post'
			, dataType : op.dataType
			, data : op.sendData
			, success: function(res){
				
				console.log(op.sendData.reqType,':',op.sendData.reqQuery, ":",res.body);
				if(op.dataType==='text'){//데이터 타입이 텍스트 일 경우
					res = res.trim();
					//console.log(res);
				}else if (op.dataType==='json'){//json 타입일 경우
					//alert(res.body);
					
					if($.type(res.body) ==='object'&&res.body.TB_dbMessage){
						popupDialog(res.body.TB_dbMessage);
					}
					
					if(!result(res)){//데이터받기에 실패 했을 경우
						// console.log('TB_ajax.get_json 에러');
						// console.log(res);
					}	
				}
				
				if($.type(op.callback)==='function') {
					op.callback(res);
				}
				
			}, error: function(request,status,error){
				
				throw "code:"+request.status+"\n"+"message:"+$.trim(request.responseText)+"\n"+"error:"+error;
			}
		});
	};
	/*====================================
	 * 데이터 받아 오기.
	 ====================================*/
	
	return {
		get_json : get_json
		, set : set
	};
})(jQuery, TB_config, TB_func);
/*표시 안되었지만 사용된 것.
 *TB_auth,
 */

