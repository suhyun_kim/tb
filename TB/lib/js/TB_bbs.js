/**
 * @author user
 */
var TB_bbs = (function($, TB_shared, TB_func, TB_page, TB_ajax, TB_contents, TB_handlebars, TB, TB_ani){
//html 소스
	var src_total= {};
	var src_list = {};
	var src_ctrl = {};
	var create_no = 0;
	var $window;

	function TB_bbs(options){
		if(!$window){$window = TB_shared.get('$window');}

		this.no = create_no++;//생성된 객체의 번호 등록

		this.options = {
			page : 1,
			page_num : 5, 
			list_num : 8,
			list_type : 'change',//change, append, prepend
			query_get_list: '', //기본 데이터에 사용될 쿼리
			query_update_item : '', //아이템 하나 업데이트한다. modify와 다른점은 해당하는 객체가 이미 수정되어 있어야 하고 그객체를 list에서 업데이트시키는 것이다.
			query_modify_item : '',
			query_delete_item : '', //삭제 쿼리
			infinity_scroll : false,  //무한 스크롤을 사용할지 결정.
			$parent : {}, //리스트의 상위 jquery object
			scope : '',//private,normal, global 
			category : '', //bbs 데이터를 가져올 때 분류시 사용
			search_field : '',//검색 필드
			search_value : ''//검색 값
			, insert_form : undefined//dynamic_form 사용시 사용할 폼
			, insert_before : undefined //insert_form 전송전 사용될 함수.
			, insert_after : undefined //insert_form 전송후 사용될 함수.
			, modify_names : undefined //수정시 dynamicform에 값이 입력된 필드들
			, modify_form : undefined //dynamic_form 사용시 사용할 폼.
			, modify_before : undefined//modify_form 전송전 함수
			, modify_after : undefined//modify_form 전송 후 함수
		};

		this.list_convert = undefined;//받은 list를 랜더링에 적함하게 가공 해서 넘긴다.

		this.sw = false;//true 일 경우는 중복 작동을 방지한다.

		this.src_total = '';//전체 html 소스
		this.src_list = '';//list 부분의 html 소스

		this.name_total = '';//  전체 html 소스명
		this.name_list = ''; //list부분의 html 소스명

		this.render_obj = {};//랜더링에 사용될  데이터
		this.render_obj.list = []; //랜더링에 사용될 list 데이터
		this.render_obj.list_info = {};//리스트 정보

		this.evt_on_storage = [];//이벤트등의 리스트 랜더링 후 사용될 함수
		this.evt_off_storage = []; //이벤트 off시 사용될 함수.

		this.options = $.extend(this.options, options);

		if(!this.options.query_get_list){
			throw '쿼리를 지정하십시요.!';
		}

		if(!this.options.$parent instanceof jQuery){
			throw '$parent가 지정되어 있지 않습니다.';
		}
		
		this.options.page = parseInt(this.options.page, 10);
		this.first_page = this.options.page;//로드한 페이지중 가장 처음 페이지
		this.last_page = this.options.page;//로드한 페이지중 가장 마지막 페이지

		//리스트 형태 변환 함수
		if(this.options.list_convert){
			this.list_convert = this.options.list_convert;
			delete this.options.list_convert;
		}

		//엘리먼트 셋팅
		this.$el= {};
		this.$el.parent = this.options.$parent;
		this.$el.add_button = this.$el.parent.find('.TB_bbs_add_button');
		
		
		//dynamic_form 글로벌 변수 셋팅
		TB_shared.set('TB_bbs_insert_form_'+this.no, options.insert_form);
		TB_shared.set('TB_bbs_modify_form_'+this.no, options.modify_form);
		
		
		delete this.options.$parent;
		
		//console.log(this.render_obj, this.$el.parent);
		//console.log(this.render_obj);
		//console.log(this.$el.parent.html());
		//파츠 셋팅
		this.parts = TB_contents.create_parts({ obj : this.render_obj, parent : this.$el.parent });
		//console.log(this.parts);
	}
	/*================================
	 * 생성자 함수
	 ================================*/
	
	TB_bbs.prototype.set_el = function($parent){
		var parts = this.parts.parts 
			, i = 0
			, len = parts.length
			, $set_el , parts_name
			;
		
		this.options.$parent = $parent;
		this.$el.parent = this.options.$parent;
		this.$el.add_button = this.$el.parent.find('.TB_bbs_add_button');
		this.parts.context.parent = $parent;
		
		// /console.log(i,len);
		//console.log(this.parts.parts);
		//TB_bbs_list_wrap
		/*
		for(; i< len ; i++){
			//console.log(parts[i].getAttribute('data-TB_contents_parts') );
			parts_name = parts[i].getAttribute('data-TB_contents_parts') 
			$set_el = $parent.find('.TB_contents_parts[data-TB_contents_parts='+parts_name+']')[0];
			//console.log($set_el);
		}
		this.parts.parts = $parent.find('.TB_contents_parts');
		*/
		//TB_bbs_pagination
	};
	/*================================
	 * $parent나 나머지 파츠의 엘리먼트가 TB_contents.render 등으로 삭제 되었을 경우 
	 * 다시 셋팅해준다.
	 ================================*/
	
	TB_bbs.prototype.modify = function(bbs, find_item){
		var bbs = bbs
			, form = bbs.options.modify_form
			, form_len = form.form.length
			, item_no = find_item.no+''
			, create_no = bbs.no
			, list = bbs.render_obj.list 
			, i = 0, len = list.length, k = 0
			, item , names = bbs.options.modify_names
			, names_len = names.length, j = 0
			, modify_before = bbs.options.modify_before
			, modify_after = bbs.options.modify_after
			, e = 0, each_len
			;
		
		//console.log(no);
		//no값을 가지고 list의 아이템 찾기
		item = TB_func.json_search(list, 'no', item_no)[0];
		
		//console.log(list, item_no);
		if(!item){throw 'item을 찾을 수 없습니다. ';return false;}
		
		//names를 순회하면서 form에 값입력 하기
		for(j=0,names_len; j< names_len; j++){//값을 입력할 필드인 names 순회
			//console.log(names[j], item[names[j]]);
			for(i = 0 ; i < form_len ; i++){//form 순회
				//console.log(form.form[i]);
				if(item[names[j]] && form.form[i].attr.name === names[j]){
					if(form.form[i].each){//select 태그와 같이 each일 경우
						for(e = 0, each_len = form.form[i].each.length; e<each_len; e++ ){
							if(item[names[j]] ==  form.form[i].each[e].value){
								form.form[i].each[e].selected = true;
							}else{
								if(form.form[i].each[e].selected){
									delete form.form[i].each[e].selected;
								}
							}
						}
					}else{
						form.form[i].attr.value = item[names[j]];
					}
				}
			}
		}
		//console.log(form);
		
		var before =  function(data, $form){
			var re;
			//console.log('asdfaf');
			if($.type(modify_before) === 'function'){
				re = modify_before(data, $form);
				if(re === false){return false;}
			}
			//$form.find('.submit_wrap').remove();
			//TB_func.loading.on();
			return true;
		};
		
		//입력 후 함수
		//데이터를 수정 한뒤 수정한데이터를 받아 오고 나서 해당하는 아이템을 수정한다.
		var after = function(res){
			var re;
			if($.type(modify_after) === 'function'){
				re = modify_after(res);
				if(re === false){return false;}
			}
			TB_func.loading.off();
			window.history.back();
		};
		
		form.before= before;
		form.after= after;
		
		//console.log(form);
		
		//return false;	
		location.href = '#!/dynamic_form?sharedObj=TB_bbs_modify_form_'+create_no;
	};
	/*==============================
	 * 수정
	 ==============================*/
	
	
	TB_bbs.prototype.insert = function(bbs){
		var bbs = bbs
			, form = bbs.options.insert_form
			, create_no = bbs.no
			, insert_before = bbs.options.insert_before
			, insert_after = bbs.options.insert_after
			, before, after
			;
		
		//console.log(form);
		if($.type(form) != 'object'){
			throw 'TB_bbs.prototype.insert Error : insert_form이 지정되어 있지 않습니다.';
		}
		
		before =  function(data, $form){
			var re;
			//console.log(insert_before);
			if($.type(insert_before) === 'function'){
				re = insert_before(data, $form);
				if(re === false){return false;}
			}
			$form.find('.submit_wrap').remove();
			TB_func.loading.on();
			return true;
		};
		
		after = function(res){
			var re;
			if($.type(insert_after) === 'function'){
				re = insert_after(res);
				if(re === false){return false;}
			}
			TB_func.loading.off();
			window.history.back();
		};
		
		bbs.options.insert_form.before= before;
		bbs.options.insert_form.after= after;
		
		location.href = '#!/dynamic_form?sharedObj=TB_bbs_insert_form_'+create_no;
		
	};
	/*================================
	 * 글 작성 
	 * . dynamic_form 반드시 필요.
	 ================================*/
	
	
	TB_bbs.prototype.get_list = function(_options,  _callback){
		var options = {
					page : undefined
					, list_num : undefined
					, list_type : 'change'
					, query_get_list : undefined, //데이터 검색 쿼리
					scope : '', //private,normal, global 
					category : '', 
					search_field : '', 
					search_value : ''
				}
				, query//일반적인 get_list인지 search인지 최종 쿼리 결정.
				;

		options = TB_func.merge(options, _options);
		
		query = options.query_get_list;
		
		//console.log(options);
		
		TB_ajax.get_json({
			sendData : {
				reqQuery : query,
				page: options.page, 
				page_num : options.page_num, 
				list_num : options.list_num,
				scope : options.scope, 
				category : options.category, 
				search_field : options.search_field, 
				search_value : options.search_value
			},
			callback: function(data){
				if($.type(_callback) === 'function'){
					_callback(data.body);
				}
			}
		});

	};
	/*================================
	 * 데이터 가져오기
	 ================================*/

	TB_bbs.prototype.render = function(_bbs, _callback){
		var options = {
					page : undefined
					, list_num : undefined
					, list_type : 'change'//append, prepend, change
					, query_get_list : undefined //데이터 검색 쿼리
					, $parent : undefined
					, scope : ''//private,normal, global 
					, category : ''//
					, search_field : ''//검색 필드
					, search_value : ''//검색 값
				}
				, get_list = this.get_list//
				, bbs = _bbs||this
				, add_html = ''//prepend나 apend시 추가될 html소스
				, $add_button
				;
		
		options = TB_func.merge(options, bbs.options);
		//console.log(bbs.options);
		
		TB_func.fall([
			//기본 검색 리스트 데이터 변환
			function(next, param){
				
				get_list(options, function(data){
					data.list_info.page = parseInt(data.list_info.page, 10);//페이지 형변환
					bbs.render_obj.list_info = data.list_info;
					
					if($.type(bbs.list_convert) ===  'function'){ data.list = bbs.list_convert(data.list); }
					
					//console.log(data.list_info.page, data.list_info.total_page, bbs.$el.add_button);
					//마지막 페이지 일 때 add_button 삭제.
					if(data.list_info.page >= data.list_info.total_page && bbs.$el.add_button.length > 0){
						bbs.$el.parent.find('.TB_bbs_add_button').remove();
					}

					//console.log(bbs.first_page, bbs.last_page, data.list_info.page);
					if(bbs.first_page > data.list_info.page){
						bbs.first_page = data.list_info.page;
					}else if(bbs.last_page < data.list_info.page){
						bbs.last_page = data.list_info.page;
					}
					//console.log(bbs.first_page, bbs.last_page, data.list_info.page);

					next(data);
				});
			}
			//redner_obj.list 셋팅 -> 랜더링 혹은 엘리먼트 추가.
			, function(next, data){
				if(!data.list.length){
					bbs.render_obj.list = data.list;
					next();
					return false;
				}

				if(bbs.options.list_type === 'append'){
					add_html = bbs.parts.getRenderHTML('TB_bbs_list_wrap', data);
					bbs.render_obj.list = bbs.render_obj.list.concat(data.list);
				}else if(bbs.options.list_type === 'prepend'){
					add_html = bbs.parts.getRenderHTML('TB_bbs_list_wrap', data);
					bbs.render_obj.list = data.list.concat(bbs.render_obj.list);
				}else if(bbs.options.list_type === 'change'){
					bbs.render_obj.list = data.list;
				}
				
				next(data);
			},
			
			//랜더링 혹은 엘리먼트 추가.
			function(next, data){
				var render_parts = []
					, i = 0;
				//console.log(bbs.parts);
				
				if(bbs.options.list_type === 'change'){
					bbs.parts.render('TB_bbs_list_wrap', function(){
						next(data);
					});
				}else if(bbs.options.list_type === 'append' ){
					bbs.$el.parent.find('.TB_bbs_list_wrap').append(add_html);
					next(data);
				}else if(bbs.options.list_type === 'prepend'){
					bbs.$el.parent.find('.TB_bbs_list_wrap').prepend(add_html);
					next(data);
				}
				
				for ( var parts in bbs.parts.storage){
					//console.log(parts);
					if(parts === 'TB_bbs_pagination'){
						render_parts[i] = 'TB_bbs_pagination'; 
						i++;
					}
					if(parts === 'TB_bbs_list_info'){
						render_parts[i] = 'TB_bbs_list_info'; 
						i++;
					}
					bbs.parts.render(render_parts);
				}
				
				/*
				if('TB_bbs_list_info' in bbs.parts.storage === true){
					bbs.parts.render('TB_bbs_list_info');
				}
				*/
				//if(bbs.parts.storage)
				//console.log(bbs.render_obj);
				
			}
			//이벤트 등의 적용
			, function(next, data){
				bbs.render_aft(bbs, function(){
					if($.type(_callback) === 'function'){
						//console.log(data);
						_callback(data);
					}
				});
			}
		]);
	};
	/*================================
	 * 랜더링 및 리스트 추가.
	 ================================*/


	TB_bbs.prototype.render_aft = function(bbs, _callback ){
		if(TB_shared.get('metroResponsive')){
			TB_shared.get('metroResponsive')();
		}

		TB_form.binding(bbs.$el.parent);
		TB.globalStyleSet();

		bbs.evt_on(bbs);

		if($.type(_callback) === 'function'){
			_callback();
		}
		
		//올바르게 위치를 잡기 위해.
		setTimeout(function(){
			$window.trigger('resize');
		}, 500);

	};
	/*================================
	 * 랜더링 후 이벤트 처리
	 ================================*/

	TB_bbs.prototype.search = function(bbs, _options, callback){
		var bbs = bbs
			, options = {
				scope : ''
				, category : ''
				, page : ''
				, search_field : ''
				, search_value : ''
			}
			, list_type = bbs.options.list_type
			, callback = callback
			;
			
		options = TB_func.merge(options, _options);
		options.page = (_options.page)?parseInt(_options.page, 10):1;
		
		if(list_type === 'append'){ bbs.options.list_type = 'change'; }
		
		bbs.first_page = options.page;
		bbs.last_page = options.page;
		bbs.options.page = options.page;
		bbs.options.scope = options.scope;
		bbs.options.category = options.category;
		bbs.options.search_field = options.search_field;
		bbs.options.search_value = options.search_value;
		
		//console.log(bbs.options);
		
		bbs.render(bbs, function(data){
			if(list_type === 'append'){ bbs.options.list_type = 'append'; }
			if($.type(callback) === 'function'){callback(data);}
		});
		
		
	};
	/*================================
	 * 검색
	 ================================*/



	TB_bbs.prototype.list_add = function(_ac, bbs, callback){
		var icon_direction = (TB_shared.get('winWid') > 991)?'right':'bottom'//로딩 아이콘이 나올 방향
				, $add_button
				, ac = _ac || 'append'//append, prepend
				;
		//console.log(bbs);
		//추가할 것이 없을 경우 add_button 사라짐
		if(bbs.last_page >= bbs.render_obj.list_info.total_page){
			$add_button= bbs.$el.parent.find('.TB_bbs_add_button');
			if($add_button.length){$add_button.remove();}
			if($.type(callback) === 'function'){callback();}
			return false;
		}

		//아이콘 로딩
		TB_func.loading.on(icon_direction);

		bbs.options.list_type = _ac;
		bbs.options.page = (bbs.options.list_type === 'append')?bbs.last_page+1:bbs.first_page-1;
		bbs.render(bbs, function(data){
			if($.type(callback) === 'function'){callback(data);}
			TB_func.loading.off();
		});

	};
	/*================================
	 * 리스트 추가
	 ================================*/


	TB_bbs.prototype.delete_item = function(bbs, item, callback){
		//console.log(bbs);
		//스위치를 이용해 연속 방지
		if(bbs.sw === true){return false;}
		bbs.sw = true;
		
		TB_func.fall([
			//삭제 확인
			function(next, param){
				if($.type(item) !== 'object'){
					throw '아이템을 object 형식으로 지정해 주십시요';
					return false;
				}
				
				TB_ani.show_backdrop({
					src : 'templates--backdrop--confirm_1'
					, sendData : '{"title":"정말삭제하시겠습니까?", "shared_func":"TB_bbs_delete_item"}'
					, width: 500
					, height : 300
					, history_back : false
					, callback : function(){
					}
					, off : function(){
						bbs.sw = false;
					}
				});
				
				TB_shared.set('TB_bbs_delete_item', function(result){
					bbs.sw = false;
					TB_ani.hide_backdrop({
						history_back : false
					});
					if(result === 'yes'){next();}
				});
			}
			//리스트 엘리먼트 삭제 후 재 랜더링
			, function(next, param){
				var list = bbs.render_obj.list
						, i = 0, len = list.length
						;

				//console.log(list)
				for(; i < len ; i++){
					if(item.no == list[i].no){
						list.splice(i,1);
						break;
					}
				}
				//console.log(list);

				//bbs.list_convert가 있을 경우 알맞게 변형
				if($.type(bbs.list_convert) ===  'function'){ bbs.render_obj.list = bbs.list_convert(list); }
				else{
					bbs.render_obj.list = list;
				}

				//없어진 리스트를 랜더링
				bbs.parts.render('TB_bbs_list_wrap', function(){
					bbs.render_aft(bbs, function(){
						if ($.type(callback) === 'function') { callback(list); }
					});
				});

				next();
			}
			//DB에서 삭제
			, function(next, param){
				if(!bbs.options.query_delete_item){return false;}
				var sendData = {};
				sendData.reqType = 'delete';
				sendData.reqQuery = bbs.options.query_delete_item;
				sendData = $.extend(sendData, item);
				
				//console.log(sendData);
				TB_ajax.get_json({
					sendData : sendData
					, callback: function(data){
						bbs.sw = false;
						//console.log('end', data);
						
					}
				});
			}
		]);
	};
	/*================================
	 * 삭제
	 ================================*/


	TB_bbs.prototype.update_item = function(bbs, item, callback, _options){
		var options = {
				primary_key : 'no'//업데이트시 기준이 될 필드
				, ajax : true//데이터를 가져오지 않는다. 
			}
			;
		
		options = $.extend(options, _options);

		TB_func.fall([
			function(next, param){//수정된 아이템을 DB로 보낸다.
				if(options.ajax === false){
					next(item);
					return false;
				}
				
				if(!bbs.options.query_update_item){return false;}
				
				var sendData = {};
				sendData.reqQuery = bbs.options.query_update_item;
				sendData = $.extend(sendData, item);
				
				//console.log(sendData);
				TB_ajax.get_json({
					sendData : sendData
					, callback : function(res){
						next(res.body);
					}
				});

			}
			, function(next, data){
				var data = data
						, list = bbs.render_obj.list
						, article, html , render
						;

				for(var i = 0, len = list.length ; i < len ; i++){
					if(list[i][options.primary_key] === data[options.primary_key]){
						list[i] = data;
						break;
					}
				}

				//bbs.list_convert가 있을 경우 알맞게 변형
				if($.type(bbs.list_convert) ===  'function'){ bbs.render_obj.list = bbs.list_convert(list); }
				else{
					bbs.render_obj.list = list;
				}

				//없어진 리스트를 랜더링
				bbs.parts.render('TB_bbs_list_wrap', function(){
					console.log('랜더완료.');
					bbs.render_aft(bbs, function(){
						if ($.type(callback) === 'function') { callback(list); }
					});
				});

			}
		]);
	};
	/*================================
	 * 글 하나 업데이트
	 * 	. 리스트에 있는 아이템을 찾아서 수정한뒤 인자로 보내면 해당 아이템을  업데이트 시킨뒤에 파일을 가져오고 랜더링한다.
	 * 		(item를 직접 랜더링 하지 않는 이유는 이미지 같은것이나 DB쪽에서 처리가 바뀔수도 있기 때문이다.)
	 * . 가지고 있는 전체 리스트 배열 중 하나를 업데이트 한뒤 처리
	 ================================*/


	TB_bbs.prototype.page_move = function(bbs, page, callback){
		var bbs = bbs
			, page = parseInt(page, 10)
			, callback = callback
			, list_type = bbs.options.list_type
			;
		
		if($.type(page) !== 'number'){return false;}
		
		if(list_type === 'append'){ bbs.options.list_type = 'change'; }
		bbs.first_page = page;
		bbs.last_page = page;
		bbs.options.page = page;
		bbs.render(bbs, function(data){
			if(list_type === 'append'){ bbs.options.list_type = 'append'; }
			if($.type(callback) === 'function'){callback(data);}
		});
		
	};
	/*================================
	 * 페이지 이동
	 ================================*/


	TB_bbs.prototype.infinity_ac = function(bbs){
		var sc_end, sc_cur, end
				, direction//append, prepend
				, mode = ''
				;

		if(TB_shared.get('winWid') >991 ){//pc
			sc_end = TB_shared.get('documentWid') - TB_shared.get('winWid');
			sc_cur = $window.scrollLeft();
			mode = 'pc';
		}else{//mobile
			sc_end = TB_shared.get('documentHei') - TB_shared.get('winHei');
			sc_cur = $window.scrollTop();
			mode = 'mobile';
		}

		end = sc_end-200;

		//console.log(end, sc_end, sc_cur, bbs.last_page);
		if(sc_cur >= end){//스크롤이 끝에 닿았을 때.
			//console.log('추가시작', bbs.sw);
			if(bbs.last_page >= bbs.render_obj.list_info.total_page){return false;}
			if(bbs.sw === true){ return false; }
			bbs.sw  = true;
			bbs.list_add('append',bbs, function(){
				bbs.sw = false;
			});
		}else if(sc_cur <= 0){//리스트의 처음으로 갈 때.
			if(bbs.first_page <= 1){return false;}
			if(bbs.sw === true){ return false; }
			bbs.sw  = true;
			bbs.list_add('prepend',bbs, function(){
				bbs.sw = false;
			});
		}

	};
	/*================================
	 * 무한 스크롤 evt
	 ================================*/


	TB_bbs.prototype.evt_on = function(bbs){
		var bbs = bbs
			, $parent = bbs.$el.parent
			, $add_button = bbs.$el.add_button||$parent.find('.TB_bbs_add_button')
			, $insert_button= $parent.find('.TB_bbs_insert_button')
			, $modify_button =$parent.find('.TB_bbs_modify_button')
			, $delete_button= $parent.find('.TB_bbs_delete_button')
			;

		if($add_button.length){
			$add_button.off('click.TB_bbs_add_button')
			.on('click.TB_bbs_add_button', function(){
				if(bbs.sw===true){return false;}
				bbs.sw = true;
				bbs.list_add('append',bbs, function(){
					bbs.sw = false;
				});
			});
		}
		/*==/리스트 추가 버튼/==*/
		
		if($insert_button.length){
			$insert_button.off('click.TB_bbs_insert_button')
			.on('click.TB_bbs_insert_button', function(){
				bbs.insert(bbs);
			});
		}
		/*==/글 작성 버튼/==*/
		
		
		if($modify_button.length){
			$modify_button.off('click.TB_bbs_modify_button')
			.on('click.TB_bbs_modify_button', function(){
				bbs.modify(bbs, JSON.parse(this.getAttribute('data-modify_item')) );
			});
		}
		/*==/글 수정 버튼/==*/


		if($delete_button.length){
			$delete_button.off('click.TB_bbs_delete_button')
			.on('click.TB_bbs_delete_button', function(){
				var item= JSON.parse(this.getAttribute('data-delete_item'));
				bbs.delete_item(bbs, item, function(){
				});
			});
		}
		/*==/삭제 버튼/==*/

		if(bbs.options.infinity_scroll === true){
			$window.off('scroll.TB_bbs_infinity_scroll_'+bbs.no)
			.on('scroll.TB_bbs_infinity_scroll_'+bbs.no, function(e){
				bbs.infinity_ac(bbs);
			});
		}
		/*==/무한 스클롤/==*/

	};
	/*================================
	 * 이벤트 on
	 ================================*/


	TB_bbs.prototype.evt_off = function(bbs){
		var $parent = bbs.$el.parent
				, $add_button = $parent.find('.TB_bbs_add_button')
				, $insert_button= $parent.find('.TB_bbs_insert_button')
				, $modify_button =$parent.find('.TB_bbs_modify_button')
				, $delete_button= $parent.find('.TB_bbs_delete_button')
				;

		if($add_button.length){
			$add_button.off('click.TB_bbs_add_button');
		}
		/*==/리스트 추가 버튼/==*/
		
		if($insert_button.length){
			$add_button.off('click.TB_bbs_insert_button');
		}
		/*==/글작성 버튼/==*/
		
		if($modify_button.length){
			$modify_button.off('click.TB_bbs_modify_button');
		}
		/*==/글작성 버튼/==*/

		if($delete_button.length){
			$delete_button.off('click.TB_bbs_delete_button');
		}
		/*==/삭제 버튼/==*/

		if(bbs.options.infinity_scroll === true){
			$window.off('scroll.TB_bbs_infinity_scroll_'+bbs.no);
		}
		/*==/무한 스클롤/==*/
	};
	/*================================
	 * 이벤트 off
	 ================================*/


	var create = function(_options){
		var tb_bbs =new TB_bbs(_options);
		return tb_bbs;
	};


	return create;
})(jQuery, TB_shared, TB_func, TB_page, TB_ajax, TB_contents, TB_handlebars, TB, TB_ani);

