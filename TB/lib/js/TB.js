
var TB = (function($){
	//alert(JSON.stringify(TB_func));
	var bindingStorage = {}, bindingName = [];
	var eBindingStorage = {}, eBindingName = []
			, browser = TB_func.ieChkVer()
			, text = ''
			;
	
	//글로벌 스카일 셋팅
	var globalStyleSet = function(){
		TB_shared.set('winWid', TB_shared.get('$window').width());
		TB_shared.set('winHei', TB_shared.get('$window').height());
		TB_shared.set('documentWid', TB_shared.get('$document').width());
		TB_shared.set('documentHei', TB_shared.get('$document').height());
	};
	
	//초기화 함수
	var init = function(){
		var sw_uri
			, domain
			, server 
			;
		server = TB_config.set('serverType', 'nodejs');//서버 타입 설정.php or nodejs
		TB_config.set('TB_signalKey', MD5('TowerBridgePJ'));//DB와의 기본 통신을 위한 키
		TB_config.set('defautLayout', 'layout--vertical--default');//기본레이아웃
		TB_config.set('default_contents_src', 'contents--dreamyacht--');//기본컨텐츠 주소
		
		TB_config.set('TB_relative_path', '/TB/');//상대 경로 설정
		domain = TB_config.set('TB_domain', location.protocol+'//'+location.hostname);//도메인 정보
		TB_config.set('TB_root_path', TB_config.get('TB_domain')+TB_config.get('TB_relative_path'));//도메인 정보
		//TB_config.set('TB_ssl_path', 'https://'+location.hostname+':46271'+TB_config.get('TB_relative_path')+'/');//https도메인 정보
		TB_config.set('index_path', TB_config.get('TB_relative_path'));//첫페이지 가기
		TB_config.set('isMobile', TB_func.isMobile());//모바일인지 확인
		TB_config.set('device', TB_config.get('isMobile')?'mobile':'pc');//디바이스 종류
		TB_config.set('click', (TB_config.get('isMobile'))? 'touchstart':'click' );//모바일일 경우와 피씨일  경우 이벤트 바인딩 분류
		TB_config.set('page_animation', 'fadeIn');//페이지 전화시 애니메이션
		TB_config.set('browser', browser);//브라우져 이름
		
		if(server === 'nodejs'){
			sw_uri = TB_config.set('SW_URI', TB_config.get('TB_domain')+'/TB_sw');
		}else if(server === 'php'){
			sw_uri = TB_config.set('SW_URI', TB_config.get('TB_root_path')+'lib/php/TB_sw.php');//php 일 경우 URI
		}
		//console.log(sw_uri);
		//TB_config.set('SW_URI', TB_config.get('TB_domain')+'/TB_sw');//nodejs일 경우 URI
		//console.log(TB_config.get('SW_URI'));

		if($.type(browser) == 'number'){//css 먼저 로드 시키기, true는 먼저 로드, false는 로드안함. TB_contents에도 영향
			if(browser <= 9){//구버전 IE
				TB_config.set('css_load', false);
				text = '현재 사용하시는 브라우져는 오래된 브라우져 입니다.';
				text += '<br/> 간혹 제대로 표시가 안될수 있고';
				text += '<br/> 보안상에 심각한 위험이 있으므로';
				text += '<br/> 반드시 모바일이나 크롬브라우져등을 사용 하시기 바랍니다';
				TB_func.popupDialog(text, 10000);
				$('#TB_opening_wrap').remove();
			}else{
				TB_config.set('css_load', true);
			}
		}else{
			TB_config.set('css_load',true);
		}
		/*==/기본값 설정==*/

		binding('style', TB_style.binding);
		binding('contents', TB_contents.binding);
		binding('ui', TB_ui.binding);
		binding('ani', TB_ani.binding);
		binding('form', TB_form.binding);
		binding('el', TB_el.binding);
		/*==/바인딩==*/

		eBinding('form', TB_contents.eBinding);
		eBinding('contents', TB_contents.eBinding);
		eBinding('sharedFunc', TB_shared.eBinding);//해당 버튼을 눌렀을 TB_shared에 정의되어 있는 함수를 실행
		/*==/이벤트 바인딩==*/

		TB_ajax.set('TB_signalKey', TB_config.get('TB_signalKey'));
		TB_ajax.set('swUrl', TB_config.get('SW_URI'));
		TB_form.set('TB_signalKey', TB_config.get('TB_signalKey'));
		TB_form.set('swUrl', TB_config.get('SW_URI'));
		//TB_form.set('sslUrl', TB_config.get('TB_ssl_path')+'lib/php/TB_sw.php');
		TB_shared.set('$window', $(window));
		TB_shared.set('$document', $(document));
		/*==/ajax 주요 변수 셋팅===*/

		globalStyleSet();
		/*==/글로벌 스타일 셋팅 ==*/

		resizeFunc = '';
		$(window)
		.on('resize.TB', function(){
			clearTimeout(resizeFunc);
			resizeFunc = setTimeout(function(){
				globalStyleSet();//글로벌 변수 셋
				TB_style.binding();//리사이징 시에도 스타일 변경되어 적용
			}, 300);
		});
		/*==/리사이즈시 작동할 것들==*/

		//TB_el_set();
		//return false;
		TB_handlebars.helper();//핸들바 헬퍼 활성화

		TB_func.loading.init();//로딩
		menuSet(function(){
			templateSet();
			visitor();
			//console.log(TB_shared.get('admin--auth--login--js'));

			if(TB_config.get('css_load')){//CSS 먼저 로드 시키기
				cssLoad(function(){

					if($('#TB_admin_page').length){//관리자 페이지일 경우.
						TB_contents.init();
						TB_style.binding();//스타일 초기화
						return false;
					}

					$('#TB_opening_wrap').remove();

					contents_load();
					TB_style.binding();//스타일 초기화


					/*
					if(!show_opening()){//동영상 재생을 안하면 그냥 처리 하기.
						setTimeout(function(){
							contents_load();
							TB_style.binding();//스타일 초기화
						}, 500);
					}
					*/

				});
			}else{//구버전 브라우져
				$('#TB_opening_wrap').remove();//오프닝 엘리먼트 삭제
				if($('#TB_admin_page').length){//관리자 페이지일 경우.
					TB_contents.init();
				}else{
					contents_load();
				}
				TB_style.binding();//스타일 초기화
			}
				
			//console.log(TB_page.sgmap().templates)
			
		});//메뉴셋팅
		/*==/기타 초기화 ==*/
	};
	/*============================================
	*초기화
	==============================================*/
	
	var contents_load = function(){
		//console.log('sadfa');
		TB_contents.impt({
			target : $('#TB_index_wrap')
			, src : TB_config.get('defautLayout')
			, sendData : {'result' : 'success'}
		});
	};
	/*============================================
	*기초 레이아웃 로드
	==============================================*/

	/*
	var show_opening = function(){
		if(!Modernizr.csstransforms3d || $.cookie('opening_viewed')){
			$('#TB_opening_wrap').remove();
			return false;
		}
		$.cookie('opening_viewed', 'true', {expires:365, path:'/'});
		TB_shared.set('templates--opening--opening_1--data', {
			text : 'Sea Yacht'
			, logo : TB_config.get('TB_root_path')+'img/logo.png'
		});

		TB_contents.impt({
			src : 'templates--opening--opening_1'
			, target : '#TB_opening_wrap'
			, sendData : 'templates--opening--opening_1--data'
		});//컨텐츠 초기화
		return true;
	};
	/*============================================
	*오프닝 재생
	==============================================*/

	var eBinding = function($name, $func){
		var len = arguments.length, nameLen = eBindingName.length, i = 0;
		if( len ===2){//이벤트 셋팅
			if(eBindingStorage[$name]){
				throw('TB.binding Error : 해당하는 바인딩 값이 있습니다.');
			}else{
				eBindingName.push($name);
				eBindingStorage[$name] = $func;
			}
		}else{//바인딩
			var $el = $name||null;
			for (;i < nameLen ; i++){
				eBindingStorage[eBindingName[i]]($el);
			}
		}
	};
	/*=======================================================
	 * 이벤트 관련 바인딩 내용은 아래 와 같다.
	   =======================================================*/

	var binding = function($name, $func){
		var len = arguments.length, nameLen = bindingName.length, i = 0;
		if( len ===2){//이벤트 셋팅
			if(bindingStorage[$name]){
				throw('TB.binding Error : 해당하는 바인딩 값이 있습니다.');

			}else{
				bindingName.push($name);
				bindingStorage[$name] = $func;
			}
		}else if(len < 2){//바인딩 하기
			var $el = $name||null;
			for (;i < nameLen ; i++){
				bindingStorage[bindingName[i]]($el);
			}
		}
	};/*바인딩 하는것을 모아 놓는다.
		*파라메터가 하나일 경우는 해당 엘리먼트를 순회 하면서 클래스나 이벤트를 바인딩 시킨다.
	   * params
	   * 	- $name : 바인딩할 함수의 이름, 또는 엘리먼트
	   *  - $func : 바인딩할 함수
	   */

	  /*
	var adminAuth = function(_callback){
		var id = $.cookie('id'), adminAuthKey = $.cookie('adminAuthKey');
		if(!id || !adminAuthKey) return false;
		// console.log(adminAuth);

		TB_ajax.get_json({
			'sendData' : {
				'adminAuthKey' : adminAuthKey,
				'id' : id,
				'reqQuery' : 'adminAuth'
			},
			'callback' : function(res){
				//if(res.body!==1) return false;
				if($.type(_callback)==='function') _callback(res.body);
			}
		});
	};/*===============================
	   * 관리자 인증
	   * detail
	   * 	. 관리자로 로그인 했을 경우 콜백 함수에 true를 아닐경우 false를 리턴한다.
	   ===============================*/

	var TB_el_set = function(_callback){
		TB_ajax.get_json({
			'sendData' : {
				'reqQuery' : 'get_TB_el'
			},
			'callback' : function(_res){
				var el = _res.body.el
					, type = $.type(el),
						i = 0, len
						;
				//생성된 EL엘리먼트가 없을 경우.
				if(_res.body === 'null'){
					TB_el.binding();
					return false;
				}
				//console.log(type, el);
				if(type === 'array'){
				//console.log('1');
					len = el.length;
				//console.log('2');
					for(; i < len ; i++){
				//console.log('loop', i);
						el[i].attr = el[i].attr||{};
						el[i].style = el[i].style||{};
						el[i].attr.src = el[i].src;
						delete el[i].src;
						delete el[i].writer;
						delete el[i].writetime;
						delete el[i].modifytime;
						delete el[i].modifier;
						//console.log(el[i]);
						TB_el.sgStorage(el[i].name, el[i]);
					}
				}else if(type === 'object'){
					TB_el.sgStorage(el.name, el);
				}
				TB_el.binding();
				if($.type(_callback)==='function'){ _callback(el); el = null;}
				i = null, len=  null, type = null;
			}
		});
	};
	/*====================================
	 * DB에서 데이터를 가져와 동적 엘리먼트를 셋팅 한다.
	 * params
	 * 	. _callback(function) : ajax 후 콜백 함수
	 ====================================*/
	

	var menuSet = function(_callback){
		TB_ajax.get_json({
			'sendData' : {
				'reqType' : 'select',
				'reqQuery' : 'getConfig'
			},
			'callback' : function(_res){
				var menu ,type
					;
				
				//메뉴값이 없는 초기상태
				if(_res.body === 'null' || !_res.body|| !_res.body.menu){
					if($.type(_callback) === 'function')_callback();
					return false;
				}
				
				//유저 정보 셋팅
				TB_config.set('user', _res.body.user);
				//console.log(TB_config.get('user'));
				
				type = $.type(_res.body.menu);
				//console.log(type)
				if(type === 'string'){
					//menu = (_res.body.menu)?$.parseJSON(_res.body.menu):$.parseJSON($.parseJSON(_res.body).menu);
					//menu = (_res.body.menu)?$.parseJSON(_res.body.menu):$.parseJSON($.parseJSON(_res.body).menu);
					menu = JSON.parse(_res.body.menu);
					var dd = '[{"page":"about","title":"드림요트","bake":"half_bake","index":"0","depth":"0","parent":"","src":"contents--dreamyacht--about","target":"#contents_wrap","sendData":""}]';
				}else if(type === 'array'){
					menu  = _res.body.menu;
				}
				
				//console.log(_res.body.menu);
				//맴 셋팅
				TB_page.sgmap({
					'menu' : menu ,
					'pageType' : 'animation',
					'animation' : TB_config.get('page_animation')
				});
				
				

				TB_page.convertMap();

				//dongneand 컨피그 셋팅
				TB_config.set('dongneand_sector', _res.body.category_list);

				if(typeof _callback==='function')_callback();
			}
		});
	};/*==/메뉴 셋팅 ==*/


	var show_modal_template = function(dec){
		TB_shared.get('metro_scroll')('y');
		TB_ani.show_modal({
			dec: dec
			, $fixedEl : $('#contents--nansolo--header')
			, onComplete : function(){
				$('#TB_modal').css('transform','');
			}
		});
	};

	var hide_modal_template = function(){
		TB_ani.hide_modal({
			$fixedEl :$('#contents--nansolo--header')
		});
		TB_shared.get('metro_scroll')('x');
	};
	/*==/modal 템플릿/==*/


	var templateSet = function(){
		//var tl = new TimelineMax();
		//console.log(TB_page.sgmap());
		//return false;
		TB_page.sgmap({
			'templates' : {
				"opening": {	'src' : "templates--opening--opening_1"	}//오프닝
				,'layout' : {'src' : TB_config.get('defautLayout')}//레이아웃
				, "album_1": { 'src' : "templates--albums--album_1" }//앨범_1
				, "metro_album_1": {'src' : "templates--albums--metro_album_1"}//메트로 앨범
				, "metro_album_2": { 'src' : "templates--albums--metro_album_2" }//앨범_2
				, "metro_album_3": { 'src' : "templates--albums--metro_album_3" }//앨범_3
				, "slider_1": { 'src' : "templates--slider--slider_1" }//슬라이더 1
				, "bbs_1": { 'src' : "templates--bbs--bbs_1" }//일반 bbs
				, "metro_bbs_1": { 'src' : "templates--bbs--metro_bbs_1" }//metro bbs
				, "cofirm_1": { 'src' : "templates--backdrop--confirm_1" }//confirm
				,"wvm_1" : {
					"page" : "wvm_1",
					"src" : "templates--wvm--wvm_1",
					on : function(dec){
						var $TB_modal = $('#TB_modal');
						TB_shared.get('metro_scroll')('y');
						//console.log('on');
						TB_ani.flow(TB_page.sgmap().animation, $TB_modal, function(){
							//console.log(dec);
							var arr= dec.templates.src.split('--');
							TB_contents.setContents({
								category : arr[0],
								type : arr[1],
								file : arr[2],
								sendData : dec.urlParam,
								el : $TB_modal
							});
							// $(document).css('overflow-y', 'auto');
							TB_ani.flow('fadeOut', $('#contents_wrap').children('.TB_page_container_active'));
							//$('#contents_wrap').children('.TB_page_container_active').hide();
						});
					},
					off : function(){
						var $active_contents = $('#contents_wrap').children('.TB_page_container_active');
						//console.log('off');
						TB_ani.ready($active_contents, function(){
							TB_ani.flow('fadeIn',$active_contents);
						});
						TB_shared.get('metro_scroll')('x');
						$('#TB_modal').hide().empty();
					}
				}//*===/모달 뷰, modify ===/
				, 'auth_login' : {'src':'admin--auth--login'}//관리자 정보
				, 'auth_tool' : {'src':'admin--controller--index'}//관리자 전용 툴
				, 'auth_tool_element' : {'src':'admin--controller--element'}//동적생성 엘리먼트
				, 'auth_tool_map' : {'src':'admin--controller--map'}//홈페이지 맵 생성
				, 'auth_tool_visit_chart' : {'src':'admin--controller--visit_chart'}//홈페이지 맵 생성

				,'dreamyacht_header' : {'src' : 'contents--dreamyacht--header'}//pc 헤더
				,'nansolo_nav' : {'src' : 'templates--nav--nansolo_nav'}//pc 네비게이션
				,'nansolo_m_nav' : {'src' : 'templates--nav--m_nav_2'}//모바일 네비게이션
				, 'login' : {
					page : 'login'
					, src : 'templates--login--strategy'
					, on : function(dec){
						//console.log(dec);
						TB_ani.show_backdrop({
							src : dec.templates.src
							, sendData : '{"strategy":"google"}'
							, width: 500
							, height: 300
						});
					}
					, off : function(){
						TB_ani.hide_backdrop({
							history_back : false
						});
					}
				}
				, 'gender_select' : {
					page : 'gender_select'
					, src : 'templates--login--gender_select'
				}
				,'search' : {//검색 팝업
					page : 'search'
					, src : 'templates--search_form--backdrop_search_1'
					, bake : 'half_bake'
					, on : function(dec){
						//console.log(dec);
						TB_ani.show_backdrop({
							src : dec.templates.src
							, sendData : dec.urlParam
							, width: 500
							, height: 350
						});
					}
					, off : function(){
						TB_ani.hide_backdrop({
							history_back : false
						});
					}
				}
				,'dynamic_form' : {//검색 팝업
					page : 'dynamic_form'
					, src : 'templates--backdrop--dynamic_form'
					, bake : 'half_bake'
					, on : function(dec){						
						TB_ani.show_backdrop({
							src : dec.templates.src
							, sendData : dec.urlParam
							, width: 500
						});
					}
					, off : function(){
						TB_ani.hide_backdrop({
							history_back : false
						});
					}
				}
				,'write_form' : {//작성폼
					page :'write_form'
					, src : 'templates--write_form--write_form_1'
					, on : function(dec){
						TB_shared.get('metro_scroll')('y');
						TB_ani.show_modal({
							dec: dec
							, $fixedEl : $('#contents--nansolo--header')
						});
					}
					, off : function(){
						TB_ani.hide_modal({
							$fixedEl :$('#contents--nansolo--header')
						});
						TB_shared.get('metro_scroll')('x');
					}
				}
				,'view_item_dreamyacht' : {//작성폼
					page :'view_item_dreamyacht'
					, src : 'templates--view--dreamyacht'
					, on : function(dec){
						TB_ani.show_modal({
							dec: dec
						});
					}
					, off : function(){
						TB_ani.hide_modal({
						});
					}
				}
				,'detail_view' : {//뷰
					page :'detail_view'
					, src : 'templates--view--detail_view_1'
					, on : function(dec){
						show_modal_template(dec);
					}
					, off : function(){
						hide_modal_template();
					}
				}
				,'piece_modify' : {//뷰
					page :'piece_modify'
					, src : 'templates--modify_form--modify_form_1'
					, on : function(dec){
						show_modal_template(dec);
					}
					, off : function(){
						hide_modal_template();
					}
				}
				,'write_form_dreamyacht' : {//작성폼
					page :'write_form_dreamyacht'
					, src : 'templates--write_form--dreamyacht'
					, on : function(dec){
						TB_ani.show_modal({
							dec: dec
							//, $fixedEl : $('#contents--nansolo--header')
						});
					}
					, off : function(){
						TB_ani.hide_modal({
							//$fixedEl :$('#contents--nansolo--header')
						});
					}
				}
				,'modify_form_dreamyacht' : {//작성폼
					page :'modify_form_dreamyacht'
					, src : 'templates--modify_form--dreamyacht'
					, on : function(dec){
						TB_ani.show_modal({
							dec: dec
						});
					}
					, off : function(){
						TB_ani.hide_modal();
					}
				}
				, 'backdrop_popup' : {
					page : 'backdrop_popup', 
					src : 'templates--backdrop--popup'
					, on : function(dec){
						var sendData ={}
							, location = TB_page.locationDec()
							, user = TB_config.get('user')
							;
						
						//console.log(user)
						
						if(!Modernizr.csstransforms3d){
							//alert('사용하시는 브라우져는 구형 브라우저이므로 \n 제대로 표시 되지 않을 수 있습니다. \n 스마트폰 혹은 크롬으로의 접속을 권장합니다.');
							return false;
						}else if(!location){//페이지가 제대로 로드 되지 않았을 때.
							return false;
						}
						
						
						
								
						var limit_date = parseInt($.cookie('backdrop_view_period'), 10)
							, now = new Date()
							, over_time =  (limit_date)?(limit_date-now.getTime())/60/60/1000:0
							;
						
						//console.log('시간 ', limit_date, over_time);
						
						if(over_time > 0){
							context = null;
							close();
							return false;
						}
						
						$('#TB_backdrop_bg').removeClass('ST_dis_none');
						$('#backdrop_popup_wrap').addClass('ST_hei_inherit');
						$('#TB_index_wrap').css({
							'overflow':'hidden'
							, 'height' : Math.floor(TB_shared.get('winHei')*98/100)
							, 'position' : 'relative' 
						});
						
						sendData.query = {};
						sendData.query.select = 'backdrop_popup';
						sendData.query.insert = 'backdrop_popup';
						sendData.query.modify = 'backdrop_popup';
						sendData.query.delete = 'backdrop_popup';
						
						sendData.before = function(data, $form){
							return true;
						};
						sendData.after = function(res){
						};
						
						TB_contents.setContents({
							category : 'templates',
							type : 'backdrop',
							file : 'popup',
							sendData : sendData ,
							el : $('#backdrop_popup_wrap')
						});
						
					}, 
					off : function(){
						$('#TB_backdrop_bg').addClass('ST_dis_none');
						$('#backdrop_popup_wrap').removeClass('ST_hei_inherit').remove();
						$('#TB_index_wrap').css({
							'overflow':''
							, 'height' : ''
							, 'position' : '' 
						});
					}
				}

			}
		});
	};
	/*=================================
	 * 맵에 추가 템플릿 셋팅
	 =================================*/



	var cssLoad = function( _callback){
		var $css_loaded_wrap = $('#css_loaded_wrap')
				, text = ""
				, storage = []//이미 저장한 css를 확인하기 위한 저장소
				, sgmap = TB_page.sgmap()
				, maps = [sgmap.templates, sgmap.map]
				;
		for(var i = 0, len = maps.length ; i < len ; i++){
			 $.each(maps[i] , function(idx, val){
			 	var src = maps[i][idx].src.split('--');
			 	if($.inArray(maps[i][idx].src, storage) < 0){
			 		storage.push(maps[i][idx].src);
			 		text += "<link rel='stylesheet' type='text/css' href='";
					text += TB_config.get('TB_root_path')+src[0]+'/'+src[1]+'/'+src[2]+'/'+src[2]+'.css';
					text += "' media='screen' /> ";
			 	}
			 });
		}
		//console.log(text);
		$css_loaded_wrap.html(text).promise().done(function(){
			$css_loaded_wrap.find('link').last().ready(function(){
				if($.type(_callback) === 'function'){_callback();}
			});
			 text = null, storage = null, $css_loaded_wrap = null, i = null, len= null, sgmap = null, maps = null;
		});
	};
	/*=================================
	 * CSS 로드
	 * params
	 * 	. callback(function) : src 값을 가지고 있는 오브젝트나 배열
	 =================================*/

	//console.log(document.referrer);

	var visitor = function(){
		TB_ajax.get_json({
			'sendData' : {
				'reqQuery' : 'visitor'
				, 'reqType' : 'insert'
				, 'url' : location.href
				, 'before_url':document.referrer||'직접주소'
				, 'platform' : (TB_config.get('isMobile'))?'mobile':'pc'
			}
		});
	};
	/*=================================
	 * 방문자 정보 등록
	 =================================*/

	return {
		init : init,
		binding : binding,
		eBinding : eBinding,
		menuSet: menuSet ,
		contents_load : contents_load
		, globalStyleSet : globalStyleSet
	};

})(jQuery);

$(document).ready(function(){
	TB.init();//초기화
});
