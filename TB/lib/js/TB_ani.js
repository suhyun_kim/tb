/**
 * @author user
 */
var TB_ani = (function($, Modernizr){
	var aniStorage = {};
	
	
	var show = (function(jQuery){
		var $ = jQuery
			, create_no = 0
			;
		
		
		function Show($top, _options){
			this.options = {
					ani_time : 0.5
					, delay : 0.5
					, callback : undefined
					, ease : Power0.easeNone
				}
				, this.$staggers = $top.find('.TB_ani_show')//애니메이션할 전체 객체
				, this.staggers_len = this.$staggers.length//애니메이션할 전체 객체의 수. 
				, this.tl
				, this.$finish =  $top.find('.TB_ani_show_finish')//화면에 보일 경우 전체 애니메이션을 보여줄객체 
				, this.finish_offset//finish 트리거 위치
				, this.$obj = []//애니메이션 요건에 충족이 되서 애니메이션될 것들
				, this.i = 0 , this.len= this.$staggers.length
				, this.winHei = TB_shared.get('winHei')
				, this.setFunc = {}
				, this.$window =TB_shared.get('$window')
				, this.$document = TB_shared.get('$document')
				, this.scrollTop = 0
				, this.scrollEnd = 0
				, this.animated_idx = 0 //가장 마지막에 애니메이션한 인덱스
				, this.sw = false 
				, this.create_no = create_no++
				;
			
			$.extend(this.options, _options);
			//console.log(this.options)
			
			this.tl = (Modernizr.csstransforms3d)?new TimelineMax({delay:this.options.delay}):undefined;
			//console.log(this.tl)
			if(!this.staggers_len || !this.tl  ){return false;}
			
			//finish엘리먼트 위치
			this.finish_offset = (this.$finish.length)?this.$finish.offset():undefined;
			
		}
		/*===/ 생성자/===*/
		
		Show.prototype.evt = function(){
			var setFunc
				, $staggers = this.$staggers
				, $window = this.$window
				, $document = this.$document
				, show = this
				, finish_offset = this.finish_offset
				, run = this.run
				;
			//console.log('evt', this.create_no);
			
			$window.off('scroll.TB_ani_show_scroll_'+this.creat_no)
			.on('scroll.TB_ani_show_scroll_'+this.creat_no, function(){
				// console.log('scrolled');
				clearTimeout(setFunc);
				setFunc = setTimeout(function(){
					var i = 0, $obj = [];
					winHei = TB_shared.get('winHei');
					scrollTop = $window.scrollTop();
					scrollEnd = $document.height()-winHei-25;
					if(scrollTop >= scrollEnd || (finish_offset && scrollTop >= finish_offset.top-winHei)){
						run($staggers, show);
					}
					
					$staggers.each(function(idx){
						if(scrollTop >= this.offset.top-winHei-100 && this.ani_show === false){
							//console.log(this, scrollTop, this.offset.top-winHei-100, this.ani_show);
							$obj[i] = $staggers[idx];
							i++;
						}
					});
					//console.log($obj);
					run($obj, show);
					//if(scrollTop >= stay_el.offset.top-winHei-100){
						//run($staggers, animated_idx+1);
					//}
				}, 200);
			});

		};
		/*===/바인딩할 이벤트/===*/
		
		Show.prototype.off = function(){
			//console.log('off', this.create_no);
			this.$window.off('scroll.TB_ani_show_scroll_'+this.create_no);
		};
		/*===/ 종료/===*/
		
		Show.prototype.run = function($el, show ){
			var ani = {}
				, i=0
				, len= $el.length
				, tl = show.tl
				, animated_idx = show.animated_idx
				, staggers_len = show.staggers_len
				, options = show.options
				;
				
			//console.log(show);
			if(!len  ){return false;}
			tl.clear().add('run');
			//sw = true;
			for(;i < len ; i++){
				if($el[i].ani_show === false){
					ani.opacity = 1;
					ani.ease = options.ease;
					ani.onComplete = function(){
						this.target.ani_show = true;
						this.target.style.removeProperty('transform');
						animated_idx++;
						//console.log(animated_idx, staggers_len);
						if(animated_idx >= staggers_len){//애니메이션을 전체 랜더링 했을 경우.
							//console.log(animated_idx, staggers_len);
							show.off();
						}
						
						if(typeof options.callback === 'function'){options.callback(this.target);}
					};
					if($el[i].ani_type === 'fadeInUp'){
						ani.y = 0;
						ani.opacity = 1;
					}else if($el[i].ani_type === 'scaleUp'){
						ani.scale  = 1;
					}
					tl.add(TweenMax.to($el[i], options.ani_time, ani), 'run'+i);
				}
			}

		};
		/*===/ 애니메이션/===*/
		
		
		Show.prototype.init = function(){
			var $staggers = this.$staggers
				, winHei = this.winHei
				, $obj = this.$obj
				, tl = this.tl
				, finish_offset = this.finish_offset
				, $window = this.$window
				, setFunc= this.setFunc
				, scrollTop = this.scrollTop
				, scrollEnd = this.scrollEnd
				, run = this.run
				, $document = this.$document
				, show = this
				, create_no = this.create_no
				;
			
			//각 엘리먼트 위치저장 및 위치셋팅
			$staggers.each(function(idx){
				var $this = $(this)
					, type = $this.attr('data-TB_ani_show')
					;
					
				$staggers[idx].offset = $this.offset();
				$staggers[idx].ani_show = false;
				$staggers[idx].ani_type = type;
				
				//console.log($staggers[idx].offset, TB_shared.get('winHei'))
				
				if(type === 'fadeInUp'){
					tl.set(this, {y : 70});
				}else if(type === 'scaleUp'){
					tl.set(this,{scale:0});
				}
				
				//현재 윈도우내에 있는 것들은 전부 담는다.
				if(winHei >= $staggers[idx].offset.top){
					$obj[idx] = $staggers[idx]; 
				}
			});
			
			//현재 화면에 보이는 것들 애니메이션
			if($obj.length){
				this.run($obj, show); 
				$obj = undefined;
			}
			
			//finish가 있을 경우 애니메이션
			if(finish_offset && winHei >= finish_offset.top){
				this.run($staggers);
			}
			
			//이벤트 바인딩
			this.evt();
			
			
			$window.off('scroll.TB_ani_show_scroll_'+this.creat_no)
			.on('scroll.TB_ani_show_scroll_'+this.creat_no, function(){
				clearTimeout(setFunc);
				setFunc = setTimeout(function(){
					var i = 0, $obj = [];
					winHei = TB_shared.get('winHei');
					scrollTop = $window.scrollTop();
					scrollEnd = $document.height()-winHei-25;
					if(scrollTop >= scrollEnd || (finish_offset && scrollTop >= finish_offset.top-winHei)){
						run($staggers, show);
					}
					
					$staggers.each(function(idx){
						if(scrollTop >= this.offset.top-winHei-100 && this.ani_show === false){
							//console.log(this, scrollTop, this.offset.top-winHei-100, this.ani_show);
							$obj[i] = $staggers[idx];
							i++;
						}
					});
					//console.log($obj);
					run($obj, show);
					//if(scrollTop >= stay_el.offset.top-winHei-100){
						//run($staggers, animated_idx+1);
					//}
				}, 200);
			});


		};
		/*===/ 초기화 /===*/
		
		return Show;

	})(jQuery);
	/*=================================================================
	 * 처음으로 엘리먼트가 보여지게될 애니메이션을 정하게된다.
	 *  . opacity : 0 이 적용되어 있어야 제대로 보인다.
	 *  .  랜더링 되고 나서 scrollTop 이 해당 엘리먼트 위치내에 있다면 자동으로 보여지게 된다. 
	 *     아니라면 스크롤시에 해당 스크롤이 window에서 중간쯤에 위치하게 되면 보여진다.
	 *      스크롤이 끝에 서 50px정도 남았을 경우도 보여지게 된다.
	 *      TB_ani_show_finish 엘리먼트 위치가 보여지자 마자 나머지 애니메이션이 전부 발동된다
	 =================================================================*/		

	
	var ready = function($el, _callback){
		 var type = $.type($el)
		 		;
		 var setCss = function($_el){
			if(Modernizr.csstransforms3d){
				$_el.addClass('ST_opacity_0').css({
					'visibility':'visible',
					'display' : 'block'
				});
			}else{
				$_el.hide();
			}
		 };
		
		if(type === 'object' && $el instanceof $){//jquery 오브젝트
			setCss($el);
		}else if(type === 'array'){//배열일 경우
			for(var i =0, len = $el.length ; i < len ; i++){
				setCss($el[i]);
			}
		}
		if(typeof _callback === 'function'){
			_callback($el);
		}
	};
	/*===========================================
	 * 애니메이션을 주기전 보여지지는 않는 기본 상태
	 * params
	 ===========================================*/
	
	var show_backdrop = function(_obj){
		var obj = {
					src : undefined //페이지 정보 데이터 
					, sendData : undefined //파라메터 
					, callback : undefined
					, ani_time : 0.3
					, width : ''
					, height : ''
					, background: '#ffffff'
					, history_back : true//(bool)hide_backdrop시 이동 할 것인지 정한다. 
					, hide_lock : false //백그라운드를 클릭했을 경우 hide_backdrop이 작동할것인지 정한다.
					, off : undefined//백드라운드를 눌렀을 경우 함수
				}
				, tl = (Modernizr.csstransforms3d)?new TimelineMax():undefined
				, $backdrop = $('#TB_backdrop_bg')
				, $backdrop_box = $('#TB_backdrop_box')
				, $backdrop_wrap = $('#TB_backdrop')
				;
		
		//console.log($backdrop.length, $backdrop_box.length, $backdrop_wrap.length)
		
		if(_obj){ obj = $.extend(obj, _obj); }
		
		//console.log(_obj)
		//src를 지정 안했을 경우 주소를 기준으로 데이터를 가져온다. 
		if(!obj.src){
			 var dec = TB_page.locationDec();
			 obj.src = obj.dec.map.src;
			 obj.sendData = obj.dec.map.sendData;  
		 }
		
		$backdrop_wrap.css({
			'width' : '100%'
			, height : TB_shared.get('winHei')+50
		});
		
		$('body').css('overflow', 'hidden');
		
		ready([$backdrop, $backdrop_box], function(){
			tl
			.set($backdrop,{
				autoAlpha : 0
			})
			.set($backdrop_box, {
				 css : {
					width: obj.width
					, height : obj.height
					, background: obj.background
					, 'margin-bottom' : 300
					, top: 50
				}
			})
			.set($backdrop_box , {
				 rotationX : 90
				 , transformStyle : 'preserve-3d'
				 , transformPerspective : 1000
				 
			})
			.to($backdrop, obj.ani_time, {
				autoAlpha : 0.8
			})
			.to($backdrop_box, obj.ani_time, {
				autoAlpha : 1
				, onComplete : function(){
					var arr = obj.src.split('--');
					//console.log(arr);
					TB_contents.setContents({
						category : arr[0],
						type : arr[1],
						file : arr[2],
						sendData : obj.sendData,
						el : $backdrop_box
					});
				}
			}, '-='+obj.ani_time)
			.to($backdrop_box, obj.ani_time, {
				 rotationX : 0
				 , transformOrigin:"center bottom"
				 , onComplete : function(){
				 	if(obj.hide_lock === false){
					 	$backdrop.off('click.backdrop_hide')
					 	.on('click.backdrop_hide', function(e){
						 	hide_backdrop({
						 		history_back : obj.history_back
						 	});
						 	if($.type(obj.off) === 'function'){
						 		obj.off();
						 	}
					 	});
				 	}
				 	if($.type(obj.callback) === 'function'){
				 		obj.callback();
				 	}
				 }
			} )
			;
		});
	};
	/*===========================================
	 * show_backdrop
	 ===========================================*/
	
	
	var hide_backdrop = function(_obj){
		var obj = {
					callback : undefined
					, ani_time : 0.3
					, width : undefined
					, height : undefined
					, background: '#ffffff'
					, history_back : true//뒤로가기
				}
				, tl = (Modernizr.csstransforms3d)?new TimelineMax():undefined
				, $backdrop = $('#TB_backdrop_bg')
				, $backdrop_box = $('#TB_backdrop_box')
				, $backdrop_wrap = $('#TB_backdrop')
				;
		if(_obj){ obj = $.extend(obj, _obj); }
		
		tl
		.to($backdrop_box, obj.ani_time, {
			 rotationX : 90
			 , transformOrigin:"center bottom"
		})
		.to($backdrop, obj.ani_time, {
			autoAlpha : 0
			 , onComplete : function(){
			 	$('body').css('overflow', '');
			 	//$backdrop_box.css('display', 'none').empty();
			 	$backdrop_box.removeAttr('style').empty();
				$backdrop_wrap.css({
					'width' : ''
					, height : ''
				});
				$backdrop.css('display', 'none');
				if(obj.history_back){
					window.history.go(-1);
				}
			 }
		}, '-='+obj.ani_time);
	};
	/*===========================================
	 * hide_backdrop
	 ===========================================*/
	
	var show_modal = function(_obj){
		var obj = {
					dec : undefined//임포트할 페이지 정보
					, $fixedEl : undefined //헤더같이 고정되어 있는엘리먼트. 애니메이션을 부드럽게 하기 위해
					, callback : undefined
					, ani_time : 0.5//애니메이션 시간
				}
				, tl = (Modernizr.csstransforms3d)?new TimelineMax():undefined
				, $modal = $('#TB_modal')
				, $backdrop = $('#TB_backdrop_bg')
				, $contents = $('#TB_index_wrap')
				;
				
		if(_obj){ obj = $.extend(obj, _obj); }
		
		if(!obj.dec){ obj.dec = TB_page.locationDec(); }
		$('body').css('overflow', 'hidden');
		
		ready([$modal, $backdrop], function(){
			tl
			.set($backdrop, {
				autoAlpha : 0
			})
			.set($modal, {
				y : '70%'
				, autoAlpha : 0.7
			})
			// .to($contents, obj.ani_time,{
				// scale : 0.9
			// })
			.to($backdrop, obj.ani_time, {
					autoAlpha : 0.9
			//}, '-='+obj.ani_time)
			}, 0)
			.to($modal, obj.ani_time, {
				autoAlpha : 1
				, y : '0%'
				, onComplete : function(){
					var arr = obj.dec.templates.src.split('--');
					TB_contents.setContents({
						category : arr[0],
						type : arr[1],
						file : arr[2],
						sendData : obj.dec.urlParam,
						el : $modal
					});
					 
					 if($.type(obj.$fixedEl) === 'object'){//고정되어 있는 엘리먼트 숨기기.
						 obj.$fixedEl.hide();
					 }
					TB_ani.flow('fadeOut', $contents, function(){
						if($.type(obj.callback) === 'function'){
							obj.callback();
						}
					});
				}
			//}, '-='+obj.ani_time)
			}, 0)
			;
		});
	};
	/*===========================================
	 * show modal
	 ===========================================*/
	
	var hide_modal = function(_obj){
		var $modal = $('#TB_modal')
				, $backdrop = $('#TB_backdrop_bg')
				, $contents = $('#TB_index_wrap')
				, obj = {
					$fixedEl : undefined//fixed되어 있는 엘리먼트
					, ani_time : 0.5
					, callback : undefined
				}
				, tl = (Modernizr.csstransforms3d)?new TimelineMax():undefined
				;
				
		if(_obj){
			obj = $.extend(obj, _obj); 
		}
		
		tl
		.to($contents, obj.ani_time, {
			scale : 1
			, autoAlpha : 1
			, onComplete :function(){
				$contents.css('transform','');
				if($.type(obj.$fixedEl)==='object'){
					flow('fadeIn', obj.$fixedEl);
				}
			}
		})
		.to($backdrop, 0.5,{
			autoAlpha : 0
			, onComplete : function(){
				setTimeout(function(){
					$backdrop.css('display','none');
				}, (obj.ani_time*1000));
			}
		}, '-='+obj.ani_time)
		.to($modal, obj.ani_time, {
			y : '100%'
			, onComplete : function(){
				$('body').css('overflow', '');
				$modal.empty().css('display','none');
				if($.type(obj.callback) === 'function'){
					obj.callback();
				}
			}
		}, '-='+obj.ani_time)
		;
	};
	/*===========================================
	 * hide modal  
	 ===========================================*/
	
	var changeEl = function(_obj){
		var obj ={
			'ani' : 'fadeIn',
			'$oldEl' : {},
			'$newEl' : {},
			'callback' : ''
		};
		
		obj = $.extend(obj, _obj);
		
		if(obj.$oldEl.attr('id') ===  obj.$newEl.attr('id')){//같은 객체일 경우 작동 체인지 안함.
			return false;
		}
		
		if(!Modernizr.cssanimations||!Modernizr.csstransforms3d){
			obj.ani = 'fadeIn';
		}
		
		TB_ani.ready(obj.$newEl, function(){
			if(obj.ani ==='fadeIn'){
				if(Modernizr.csstransforms3d){
					var tl_1 = new TimelineMax()
						, old_z = obj.$oldEl.css('z-index')
						, new_z = obj.$newEl.css('z-index') 
						;
					
					if(old_z){
						new_z = parseInt(old_z, 10)+1;
					}else{
						new_z = 1;
					}
					
					obj.$newEl.css({
						'z-index' : new_z
						, 'display' : 'block'
						, 'opacity' : 0
						, 'absolute' : 'absolute'
					});
					
					tl_1
					.set(obj.$newEl, {
						autoAlpha : 0
					})
					.to(obj.$newEl, 0.5, {
						autoAlpha : 1
						, onComplete : function(){
							obj.$newEl.css('z-index', old_z);
							obj.$oldEl.css({
								'display' : 'none'
								, 'z-index' : ''
							});
							if($.type(obj.callback) === 'callback'){
								callback(obj.$newEl, obj.$oldEl);
							}
						}
					})
					;
				}else{
					flow('fadeIn', obj.$newEl, function(){
						obj.$oldEl.fadeOut();
					});
				}
			}else if(obj.ani === 'scaleFull'){
				var tl_1 = new TimelineMax();
				ready(obj.$newEl, function(){
					tl_1.set(obj.$newEl, {
						scale : 0,
						alpha : 0
					}).to(obj.$newEl, 0.5, {
						autoAlpha : 1,
						scale : 1
						, onComplete : function(){
							obj.$newEl.css('transform','');
						}
					})
					.to(obj.$oldEl, 0.5,{
						autoAlpha : 0
					}, '-=0.3');
				});
			}
		});
	};
	/*===========================================
	 * 엘리먼트의 위치를 변경한다.
	 ===========================================*/
	
	var flow = function(_flow, _$el, _callback){
		var arr = _flow.split('--');
		var i = 0,
				len = arr.length,
				tl, 
				confirmAni = Modernizr.csstransforms3d,
				oldCss = '',
				tl = new TimelineMax();
		
		for(;i < len; i++){
			if(arr[i]==='fadeIn'){
				if(confirmAni){
					ready(_$el, function(){
						tl
						.set(_$el, {
							alpha : 0
						})
						.to(_$el,0.3,{
							autoAlpha : 1
						});
					});
				}else{
					_$el.removeClass('ST_opacity_0')
					.css({
						'opacity':'1',
						'display':'none'
					}).fadeIn();
				}
			}else if(arr[i] === 'fadeOut'){
				if(confirmAni){
					TweenLite.to(_$el, 0.3 , {
						alpha : 0
					});
				}else{
					_$el.stop().fadeOut();
				}
			}else if(arr[i] ==='slideDown' ){
				if(confirmAni){//최신 버전 브라우젼
					var $img = _$el.find('img'),
							hei;
					var slideDown = function(_hei){
						TweenLite
						.to(_$el, 0.3, {
							height : _hei
						});
					};
					_$el.css({
						"overflow" : 'hidden',
						"display" : "block",
						"visibility" : "hidden"
					});
					if($img.length){
						$img.load(function(){
							hei = _$el.outerHeight();
							_$el.outerHeight(0).css('visibility','visible');
							slideDown(hei);
						});
					}else{
						hei = _$el.outerHeight();
						slideDown();
					}
				}else{//구 브라우저
					_$el.slideDown();
				}
			} 
			else if(arr[i] ==='slideUp' ) _$el.stop().slideUp();
			else if(arr[i] === 'slideToggle'){
				_$el.slideToggle();
			}
			else if(arr[i] === 'toggle'){
				_$el.toggle('hide');
			}else if(arr[i]==='scaleSmall_fadeOut'){
				if(confirmAni){
					TweenLite
					.to(_$el, 0.3,{
						scale : 0,
						opacity: 0,
						ease : Expo.easeIn
					});
				}else{
					_$el.fadeOut();
				}
			}else if(arr[i]==='widHei_0_fadeOut'){
				if(confirmAni){
					TweenMax.to(_$el,0.3, {
						width: 0, height: 0, alpha : 0, ease : Expo.easeIn
					});
				}else{ _$el.fadeOut();}
			}else if(arr[i]==='remove'){
				_$el.remove();
			}else if(arr[i] === 'staggerTo_up'){
				if(!confirmAni){
						_$el.removeClass('ST_opacity_0')
						.css({
							'opacity':'1',
							'display':'none'
						}).fadeIn();
				}else{
					ready(_$el, function(){
						tl.set(_$el,{
							top : '+=70px'
						}).staggerTo(_$el, 0.3,{
							autoAlpha: 1,
							top : '-=70px'
						}, 0.1);
					});
				}
			}else if(arr[i] === 'staggerTo_left'){
				if(!confirmAni){
					_$el.removeClass('ST_opacity_0')
					.css({
						'opacity':'1',
						'display':'none'
					}).fadeIn();
				}else{
					ready(_$el, function(){
						tl.set(_$el,{
							left : '+=70px'
						}).staggerTo(_$el, 0.3,{
							autoAlpha: 1,
							left: '-=70px'
						}, 0.1);
					});
				}
			}else if(arr[i] === 'scaleFull'){
				if(!confirmAni){
					_$el.removeClass('ST_opacity_0')
					.css({
						'opacity':1 ,
						'display':'none'
					}).fadeIn();
				}else{
					ready(_$el, function(){
						tl.set(_$el, {
							scale : 0,
							alpha : 0
						}).to(_$el, 0.5, {
							autoAlpha : 1,
							scale : 1
							, onComplete : function(){
								_$el.css('transform','');
							}
						});
					});
				}
			}
		}
		setTimeout(function(){
			if(typeof _callback === 'function'){_callback();}
		}, 500)
	};
	
	
	
	
	var dec = function(_obj, $el){
		var $target, i = 0, len  = _obj.length;
		for( ; i < len ; i++){
			//console.log(_obj[i]);
			if(_obj[i].target==='parent') $target = $el.parent() ;
			else if(_obj[i].target==='this') $target = $el;
			else if(_obj[i].target ==='find') $target = $el.find(_obj[i].findTarget);
			else if(_obj[i].target ==='closest'){
				$target = $el.closest(_obj[i].closestTarget);
			}else if(_obj[i].target ==='closest_find'){
				$target = $el.closest(_obj[i]['closestTarget']).find(_obj[i]['findTarget']);
			}
			// console.log($target);
			/*==/타겟 설정==*/
			
			if(_obj[i].ani.click){// 클릭 이벤트
				aniStorage[_obj[i].name] = function(){
					var ani = _obj[i].ani.click;
					$el.on('click', function(){
						flow(ani, $target);
					});
				};
				aniStorage[_obj[i].name]();
			}else if(_obj[i].ani.hover){//하버
				aniStorage[_obj[i].name] = function(){
					var mouseenter = _obj[i].ani.hover.mouseenter;
					var mouseleave = _obj[i].ani.hover.mouseleave;
					$el.on('mouseenter', function(){
						flow(mouseenter, $target);
					});
					$el.on('mouseleave', function(){
						flow(mouseleave, $target);
					});
				};
				aniStorage[_obj[i].name]();
			}
			/*==/이벤트 바인딩 및 처리==*/
		}
	};
	
	var binding = function(_$el){
		var $scope = _$el||$(document);
		if($scope.find('.TB_ani').length){
			$scope.find('.TB_ani').each(function(){
				var $this = $(this);
				var aniObj = $.parseJSON($this.attr('data-TB_ani'));
				dec(aniObj, $this);
			});
		}
	};
	
	return {
		binding : binding,
		flow : flow,
		changeEl : changeEl,
		ready : ready
		, show_modal : show_modal
		, hide_modal : hide_modal
		, show_backdrop : show_backdrop
		, hide_backdrop : hide_backdrop
		, show : show
	}
	
})(jQuery, Modernizr);
