


var TB_page = (function($, TB_config,TB_ajax, TB_contents, TB_func){
	var map = {
				menu : [],//맵을 array형태로 볼수 있는 객체
				map : {},//메뉴 객체를 참조 하고 있으므로 map을 수정하면 map도 바뀐다.
				pageType : 'animation',//페이지를 전환시 어떻게 처리 할 것인지에 대한 것.
															//'animation', 'import' 의 두가지 타입을 가진다.
															//여기서 설정한 값은 의미 없고 TB의 menuSet에서 다시 설정된다.
				templates : {},//템플릿 관련
				templatesHistory : [], //템플릿작성시 히스토리 객체
				history : []//history 최대 3개를 보관한다.
			},
			bakeFunc_storage = [], //bake 시에도 실행되어야 할 함수를 등록
			moveAc_storage = [], //페이지 이동시  사용될 변수. src를 저장.
			backed_src_storage = []//베이킹 되어 있는 페이지를 담을 배열
			;

	var init = function(){
		var last_str = '';
		//console.log(map);
		
		//페이지 변경시 함수 실행
		if(window.addEventListener){
			window.addEventListener('hashchange', move);//URL이 변경 되었을 경우 캐치해서 move(실행)
		}else{
			window.attachEvent('onhashchange', move);//올드 브라우져
		}
		
		if(map.pageType === 'animation'){//페이지 전환시 애니메이션
			createPageEl();//엘리먼트 생성
			move();
		}else if(map.pageType === 'import'){//그냥 임포트
			move();
		}
		
		//
		if(!locationDec()){
			//index페이지 지정
			if(location.href.substring(location.href.length-1) != '/'){
				if(window.history.replaceState){//pushState가 있을 경우
					window.history.replaceState(null, '', location.href+"/#/index");
				}else{//오래된 브라우져
					window.location.replace(location.href+"/#/index");
				}
			}else{
				window.location.hash = "#!/index";//index페이지 지정
			}
			move();
		}
	};
	/*===========================================
	 * 초기화
	 ===========================================*/


	var getBackedStorage = function(){
		return TB_func.objCopy(backed_src_storage);
	};
	/*===========================================
	 * 베이킹 되어 있는 배열 src 리턴
	 ===========================================*/

	var bakeFunc = function(_func){
		//console.log(_func);
		if(!arguments.length && bakeFunc_storage.length){
			for (var i = 0, len = bakeFunc_storage.length ; i < len ; i++){
				bakeFunc_storage[i]();
			}
		}else if($.type(_func)  === 'function'){
			bakeFunc_storage.push(_func);
		}
	};
	/*===========================================
	 * 베이크 됐을 시에만 사용되는 함수.
	 ===========================================*/

	var moveAc = function( _func){
		var data;
		 if($.type(_func)  === 'function'){//함수 등록
			moveAc_storage.push(_func);
		}else{//객체나 파라메터가 없거나 할경우 함수 실행
			if(moveAc_storage.length){
				if(_func){data = _func;}
				for (var i = 0, len = moveAc_storage.length ; i < len ; i++){
					moveAc_storage[i](data);
				}
			}
		}
	};
	/*===========================================
	 * 맵 이동시에 사용될 함수를 셋팅 하거나 저장되어 있는 함수를 실행한다.
	 * bakeFunc는 베이킹 되어 있는것을 이동시에만 사용되는데
	 * 이것은 조건없이 사용된다.
	 *  params
	 * 	. _func(function) : 함수일 경우 함수를 셋팅하고 파라메터가 없을 경우는 저장되어 있는것을 실행
	 ===========================================*/

	var sgmap = function(_obj){
		var type = $.type(_obj);
		if(type==='object'){ $.extend(map, _obj);}
		else if(!arguments.length){
			 //return TB_func.objCopy(map);
			 return map;
			 //objCopy를 할 경우 함수의 복제가 안된다.
		}
	};
	/*===========================================
	 * 맵을 셋팅 한다.
	 * 인자로 오브젝트를 받으면 셋팅하고 인자가 없을 경우는 설정된 map을 반환.
	 * params
	 * 	._obj (obj) : 맵 오브젝트
	 * return
	 * 	.map (obj) : parmas가 없을 경우 복제된 맵 객체를 리턴한다.
	 ===========================================*/

	var createPageEl = function(){
		var i,
				$container,
				$target,
				html,
				z_index = 1;

		for(i in map.map){
			if(map.map.hasOwnProperty(i)){
				$target = $(map.map[i].target);
				if($target.length){
					$container = $target.find('#TB_page_container_'+map.map[i].src);
					// console.log($container.length);
					if(!$container.length){
						html = "<div class='TB_page_container ST_posi_absolute ST_dis_none'  ";
						html += "id='TB_page_container_"+map.map[i].src+"'  ";
						html += "data-TB_page_container='"+map.map[i].page+"'  ";
						html += "data-TB_contents_src='"+map.map[i].src+"'  ";

						if(map.map[i].sendData){//sendData 데이터가 있을 경우
							if($.type(map.map[i].sendData) === 'object'){map.map[i].sendData = JSON.stringify(map.map[i].sendData);}
							html += "data-TB_contents_sendData='"+map.map[i].sendData+"'  ";
						}

						html += "style='z-index:"+z_index+";' ";
						html += "></div>";
						$target.append(html);
						z_index++;
					}
				}
			}
		}
		map.top_z_index = z_index;

		i = null; $container = null; html = null;	z_index = null;	$target= null;
	};
	/*===========================================
	 * 페이지 타입이 애니메이션일 경우 타켓에 컨테이너 엘리먼트를 미리 생성한다.
	 * TB_contets_load 를 미리 클래스를 할당하지 않고 있다가 페이지 클릭시 할당한다.
	 * 아이디는 'TB_page_conrainer-'+map.map[i].src 의 형태로 지정된다.
	 ===========================================*/

	var convertMap = function(){
		var i = 0,
				len = map.menu.length,
				page='';
		map.map = {};
		for(;i < len ; i++){
			map.map[map.menu[i].page] = map.menu[i];
			if(map.menu[i].childMenu){//자식
				for(var j = 0, _len=map.menu[i].childMenu.length; j< _len; j++){
					map.map[map.menu[i].childMenu[j].page] = map.menu[i].childMenu[j];
					if(map.menu[i].childMenu[j].childMenu){//손자
						for(var k = 0 , __len=map.menu[i].childMenu[j].childMenu.length ; k < __len ; k++){
							map.map[map.menu[i].childMenu[j].childMenu[k].page] = map.menu[i].childMenu[j].childMenu[k];
						}
					}
				}
			}
		}
		i=null, len = null, cMenu = null, page = null;
	};
	/*===========================================
	 * 메뉴를 기초로 맵으로 컨버트 한다.
	 ===========================================*/
	
	var modify_menu = function(page, modify){
		var menu = map.menu 
			, modify = modify
			, i = 0, len = menu.length
			, found = []
			;
			
		
		found = TB_func.json_search(menu, 'page', page, 'childMenu');
		
		if(!found.length){
			throw '해당하는 페이지가 없습니다.';
			return false;
		}
		
		if(modify.sendData && $.type(modify.sendData)=== 'string'){
			modify.sendData = $.parseJSON(modify.sendData);
		}
		
		$.extend(found[0], modify);
		
		convertMap();
		return TB_func.objCopy(menu);
		
	};
	/*===========================================
	 * menu 수정 후 map.map 까지 수정
	 * 최상단 엘리먼트 밖에 수정하지 못하므로 더 수정해야 한다.
	 ===========================================*/
	
	var delete_menu = function(page){
		var page = page
			, menu = map.menu
			, found = []
			, i = 0, len, idx
			;
			
		if($.type(page) != 'string'){
			throw 'type check failed';
			return false;
		}
		
		//console.log(menu);
		
		found = TB_func.json_search(menu, 'page', page, 'childMenu');
		//console.log(found);
		
		if(!found.length){
			throw '삭제할 메뉴가 없습니다.';			
			return false;
		}
		
		idx = found[0].index.split('_');
		len = idx.length;
		
		if(len ===1){
			delete menu.splice(idx[0], 1);
		}else if( len === 2){
			delete menu[idx[0]].childMenu.splice(idx[1],1);
		}else if( len === 3){
			delete menu[idx[0]].childMenu[idx[1]].childMenu.splice(idx[2],1);
		}
		
		convertMap();
		
		return TB_func.objCopy(menu);
	};
	/*===========================================
	 * menu 삭제
	 * 최상단 엘리먼트 밖에 삭제하지 못하므로 더 수정해야 한다.
	 ===========================================*/

	var formAddMap = function($form){
		var obj = {};
		var i = 0
				, len = $form[0].length
				;

		for (;i< len ; i++){
			//console.log($form[0][i].type+" : "+$form[0][i].name+' : '+$form[0][i].value);
			if($form[0][i].type ==='submit'){}
			else if($form[0][i].name =='sendData'&&$form[0][i].value){
				obj[$form[0][i].name] = $.parseJSON($form[0][i].value);
			}else if($form[0][i].type === 'radio' ){
				if($form[0][i].checked === true){
					obj[$form[0][i].name] = $form[0][i].value;
				}
			}else{
				obj[$form[0][i].name] = $form[0][i].value;
			}
		}
		
		//자식 객체인지 확인하고 자식인데 부모객체가 없으면 추가 불가
		if(parseInt(obj.depth,10) > 0){
			if(!map.map[obj.parent]){
				return false;
			}
		}
		//console.log(obj);

		if(obj.depth==='0'){ //최상단 부모
			map.menu.push(obj);
		}
		else if(obj.depth==='1'){//차일드
			for(var j = 0, _len = map.menu.length; j < _len ; j++){
				if(map.menu[j].page===obj.parent){
					if(!map.menu[j].childMenu) map.menu[j].childMenu=[];
					map.menu[j].childMenu.push(obj);
				}
			}
		}
		else if(obj.depth==='2')//마지막 차일드
		{
			for(var j = 0, _len = map.menu.length; j < _len ; j++){
				//console.log(map.menu[j]);
				if(map.menu[j].childMenu){
					for(var k = 0, __len = map.menu[j].childMenu.length ; k < __len ; k++){
						if(map.menu[j].childMenu[k].page===obj.parent){
							if(!map.menu[j].childMenu[k].childMenu) map.menu[j].childMenu[k].childMenu=[];
							map.menu[j].childMenu[k].childMenu.push(obj);
						}
					}
				}
			}
		}
		//console.log(map.menu[5]);
		convertMap();
		//console.log(map.menu[5]);
	};
	/*===========================================
	 * 폼 jquery 오브젝트를 menu으로 변형 시킨뒤 추가 시킨다.
	 * params
	 * 	. $form(jquery Obj) : 폼 오브젝트
	 ===========================================*/


	var run_half_bake = function(_param){
		var obj  = {
				script : _param.script||undefined//실행시킬 스크립트
				,data : _param.data||undefined //파라메터로 넘길 데이터
			}
			, sc_type = $.type(obj.script)//스크립트의 타입
			;
		
		
		
		if(sc_type === 'function'){
			obj.script(obj.data);
		}else if(sc_type === 'object' && obj.script.on){
			//console.log('TB_page run half bake', obj.data)
			//console.log('TB_page run half bake', obj.script.on)
			obj.script.on(obj.data);
		}
	};
	/*===========================================
	 * half bake 스크립트를 실행 시킨다.
	 * params
	 * 	. param(obj)
	 ===========================================*/

	var move = function(){
		var dec = locationDec(),//주소를 해석한 데이터
			data,//import시 사용할 수 있게 가공된 데이터
			$newPage,//선택된 페이지
			$oldPage,//이전 페이지
			newPage_id //새페이지 아이디
			, oldPage_id//이전페이지 아이디
			, isTransition =  Modernizr.cssanimations
			, isBacked//페이지의 베이킹 유무 판단.
			, sendData//전송할 데이터 string 이어야 한다.
			;
		
		
		if(!dec){return false;}	//url에 해당하는 맵이 없을 경우

		map.history.push(dec.map||dec.templates);

		if(map.history.length > 3){
			map.history.splice(0,1);
		}

		//이전페이지에 off함수가 있을 경우  off함수 실행
		if(map.history.length >=2
			&&$.type(map.history[map.history.length-2].src) != 'undefined'
			&&($.type(TB_shared.get(map.history[map.history.length-2].src+'--js')) === 'object')
			&&$.type(TB_shared.get(map.history[map.history.length-2].src+'--js').off) === 'function'
		){
			TB_shared.get(map.history[map.history.length-2].src+'--js').off(map.history);
		}
		
		//if(!dec&&!map.templatesHistory.length){return false;}	//url에 해당하는 맵이 없으면서 템플릿 저장된것도 없을 경우.
		//data.sendData = locationDec().map;
		if(!map.pageType ||  map.pageType === 'import'){//페이지 이동 없이 해당 엘리먼트에 임포트
			if(dec.urlParam){//주소 GET변수가 있을 경우
				data = dec.map;
				if(!data.sendData){
					data.sendData = {};//맵에 sendData없을 경우 생성
				}
				data.sendData.TB_urlParam = dec.urlParam;
			}else{
				data = dec.map;
			}
			//console.log(data);
			TB_contents.impt(data);

		}else if(map.pageType === 'animation'){//페이지 전환시 애니메이션 할때.
			
			if(dec.templates){//템플릿으로 작업할때 등록된 함수 실행
				map.templatesHistory.push(dec.templates.page);
				if(typeof dec.templates.on === 'function'){
					dec.templates.on(dec);
				}
				return false;
			}
			
			//console.log(dec.map.src);

			//이전에 템플릿 작업한것이 있을 경우. off 함수 실행후 삭제
			if(map.templatesHistory.length){
				if(typeof map.templates[map.templatesHistory[map.templatesHistory.length-1]].off === 'function'){
					map.templates[map.templatesHistory[map.templatesHistory.length-1]].off();
				}
				map.templatesHistory.shift();
			}
			
			$newPage = $('#TB_page_container_'+dec.map.src);//새 페이지 찾기
			$oldPage = $('.TB_page_container_active');//이전 활성화 되어 있는 페이지.
			newPage_id = $newPage.attr('id');
			oldPage_id = $oldPage.attr('id');
			
			//console.log(dec.map);
			
			//변수가 있을 경우
			if(dec.urlParam){
				$newPage.attr('data-TB_contents_sendData', JSON.stringify(dec.urlParam));
			}else if(dec.map.sendData){
				sendData = ($.type(dec.map.sendData) === 'string')?dec.map.sendData:JSON.stringify(dec.map.sendData);
				$newPage.attr('data-TB_contents_sendData', sendData);
			}
			
			//애니메이션
			TB_ani.changeEl({
				'ani' : map.animation,
				'$newEl' : $newPage,
				'$oldEl' : $oldPage
				, callback : function($new, $old){
					$newPage.css({
						'z-index':map.top_z_index+1,
						'position' : 'relative'
					});
					$oldPage.css({
						'z-index' : '',
						'position':''
					});
				}
			});
			
			isBacked = ($.inArray(dec.map.src, backed_src_storage) < 0)?false:true;
			
			if(!isBacked){//베이크 안되어 있는 것들.
				if(dec.map.bake === 'bake'||dec.map.bake === 'half_bake'){//베이크 요청이 있는 것들을 베이크 표시
					 backed_src_storage.push(dec.map.src);
				}

				TB_contents.load($newPage);

			}else if(dec.map.bake === 'half_bake'&&isBacked){//half_bake 속성을 가지면서 이미 엘리먼트가 베이킹 되어 있을 경우.
				
				var halfBakedData = {}
						;
				
				halfBakedData.param = TB_contents.sendDataConver(sendData);
				
				//console.log('sendData ',sendData,  halfBakedData.param);
				
				//파라메터가 있을 경우 변수 합치기
				if(dec.urlParam){
					halfBakedData.param = $.extend(halfBakedData.param, dec.urlParam);
				}
				
				halfBakedData.TB_half_bake = true;

				TB_style.binding($newPage);
				
				//half bake 스크립트 실행 시키기
				run_half_bake({
					script : TB_shared.get(dec.map.src+'--js')
					, data : halfBakedData
				});
				//TB_shared.get(dec.map.src+'--js')(halfBakedData);

			}else{//베이크 되어 있는 것들. 베이크시 사용되는 함수들 실행
				TB_style.binding($newPage);
				bakeFunc();
			}

			//console.log(dec);
			moveAc(dec);//페이지 이동시에 등록되어 있는 함수들 실행

			if(newPage_id === oldPage_id){//이전 페이지와 새페이지 같을 경우.(변수만 변경되거나 할때.)
				return false;
			}
			
			
			$newPage.addClass('TB_page_container_active');
			$oldPage.removeClass('TB_page_container_active');
		}

	};
	/*===========================================
	 * 맵을 기초로 해서 페이지를 셋팅한다.
	 ===========================================*/

	var locationDec = function(){
		var curHref = '',//도메인의 제외한 나머지 주소(string)
				delimiter = '',//주소를 분류할 것.
				returnData = {},//해석한뒤 리턴할 데이터
				href = decodeURIComponent(window.location.href),
				arr = [],//주소를 '/'기준으로 배열로 나눈 변수
				path,//도메인의 제외한 주소 (string)
				param;//GET변수가 있을 경우

		if(href.indexOf('/#!/')>=0){//기본 주소
			delimiter = '/#!/';
		}else if(href.indexOf('/#/') >= 0){//기본주소_2
			delimiter = '/#/';
		}else if(href.indexOf('/?_escaped_fragment_=/')>=0){//구글 클롤러가 요청한 주소
			delimiter = '/?_escaped_fragment_=/';
		}

		curHref = '/'+decodeURIComponent(window.location.hash);
		curHref = curHref.replace(delimiter,'');//딜리미터 삭제

		//console.log(curHref, map);
		if(curHref==='/'||!curHref){//주소가 없을 경우
			return false;
		}

		path = curHref.split('?');//GET변수가 있을 경우 자르기

		(path[1])? param = TB_func.getToParams('?'+path[1]):param = undefined;//GET변수

		arr = path[0].split('/');
		//console.log(arr);

		if(map.map[arr[0]]){//맵에 해당 주소가 있을 경우.
			returnData.map = TB_func.objCopy(map.map[arr[0]]);
		}else if(map.templates[arr[0]]){//템플릿
			returnData.templates ={};
			$.extend(returnData.templates, map.templates[arr[0]]);
			returnData.url = arr;
		}else{//맵과 주소가 일치 하지 않을때.
			returnData = false;
		}


		//GET변수
		returnData.urlParam = (param)?param:undefined;

		 //console.log(returnData);
		return returnData;
	};
	/*===========================================
	 * 주소를 해석한뒤 해당 맵 객체를 반환한다.
	 * return
	 * 	.returnData(obj)
	 * 		.map : 주소에 해당하는 맵 객체
	 * 		.param : GET 변수
	 ===========================================*/

	var reload = function(scope){
		if(scope === 'activePage'){
			TB_contents.load($('.TB_page_container_active'));
		}
	};
	/*============================================
	 * 페이지 리로드
	 * params
	 * 	. scope(string) :
	 * 		- 'activePage' :  현재 활성화 되어있는 페이지 리로드 . #TB_page_container_active
	 ============================================*/

	var eBinding = function($el){
		var $scope  = $el||$(document),
				$TB_page = $scope.find('.TB_page');
		if($TB_page.length){
			$TB_page.off('click')
			.on('click', function(){

			});
		}
	};
	/*===========================================
	 * 이벤트 바인딩
	 ===========================================*/

	return {
		init : init
		, sgmap : sgmap
		, locationDec : locationDec
		, move : move
		, moveAc : moveAc
		, formAddMap : formAddMap
		, convertMap : convertMap
		, bakeFunc : bakeFunc
		, reload : reload
		, getBackedStorage :getBackedStorage
		, modify_menu : modify_menu
		, delete_menu : delete_menu
	};

})(jQuery, TB_config, TB_ajax, TB_contents, TB_func);
