var TB_auth = (function($, TB_ajax, TB_func){
	var adminInfo
			, userInfo
			, encKey = 'TB_encKey'
			, admin_cookie = MD5('TB_adminCookie')
			, user_cookie = MD5('TB_userCookie')
			;
	
	
	var setAdmin = function( _val){
		if(arguments.length === 1&&!adminInfo){
			adminInfo = _val;
			userInfo = _val;
			//if(debug){ if(console)console.log('setAdmin', adminInfo); 			}
			$.cookie(admin_cookie, TB_func.enc(JSON.stringify(adminInfo), encKey),{ path : '/'});
		}
	};
	/*========================================
	 * 어드민 정보 쿠키에 셋팅.
	 ========================================*/
	
	var getAdminCookie = function(){
		var cookie = $.cookie(admin_cookie);
		if(!cookie){return false;}
		return JSON.parse(TB_func.dec(cookie,encKey));
	};
	/*========================================
	 * 쿠키에서 어드민 정보 리턴
	 ========================================*/
	
	var getAdminInfo = function(){
		if(adminInfo){
			return TB_func.objCopy(adminInfo);
		}
		return false;
	};
	/*========================================
	 * 어드민 정보 가져오기
	 ========================================*/
	
	var getUserInfo = function(){
		if(adminInfo){
			return TB_func.objCopy(adminInfo);
		}else if( userInfo ){
			return TB_func.objCopy(userInfo);
		}
	};
	/*========================================
	 * 유저 정보 가져오기.
	 ========================================*/
	
	var delAdminInfo = function(_callback){
		TB_ajax.get_json({
			'sendData':{
				'reqQuery':'adminLoginOut'
				, 'adminInfo' : adminInfo
			}
			, 'callback' : function(res){
				adminInfo = undefined;
				$.removeCookie(admin_cookie,{path:'/'});
				if($.type(_callback) === 'function'){ _callback(res.body); }
			}
		});
	};
	/*========================================
	 * 어드민 정보 지우기, 로그아웃
	 ========================================*/
	
	var isAdmin = function(_callback){
		var cookie_adminInfo = getAdminCookie();
		if(cookie_adminInfo){
			adminInfo = cookie_adminInfo;
		}
		
		if($.isEmptyObject(adminInfo)){//어드민 정보가 셋팅 안되어 있을 경우
			if($.type(_callback)==='function'){_callback(false);}
			return false;
		}
		
		TB_ajax.get_json({
			'sendData' : {
				'reqQuery' : 'isAdmin',
				'id' : adminInfo.id ,
				//'adminAuthKey' : adminInfo.adminAuthKey ,
				//user_level : adminInfo.user_level , 
				'loginTime' : adminInfo.loginTime
			}
			, 'callback':function(res){
				//console.log(res);
				if(res.body.isAdmin ==false){$.removeCookie(admin_cookie,{path:'/'});}
				if($.type(_callback) ==='function'){_callback(res.body.isAdmin);}
				cookie_adminInfo = null;
			}
		});
	};
	/*==============================================
	 * 로그인 확인 돼었을 경우 인자의 함수에 true를 리턴
	 * params
	 * 	- _callback(function) : 관리자로 로그인 됐을 경우 인자로 true를 아닐경우 없다.
	 ==============================================*/
	
	return {
		setAdmin : setAdmin,
		getAdminInfo : getAdminInfo,
		delAdminInfo : delAdminInfo,
		getUserInfo : getUserInfo,
		isAdmin : isAdmin
	};
})(jQuery, TB_ajax, TB_func);
