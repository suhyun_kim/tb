TB_shared.set('contents--seayacht--gallery--js', function(context){
	var sharedObjName='contents--seayacht--gallery--sendData'
			, _group = 'gallery'||context['param']
			;
	
	TB_shared.set(sharedObjName, {
		'sharedObj' : sharedObjName
		, 'list_options' : {//리스트 쿼리에 사용할 것
			'reqQuery' : 'get_gallery_list'
			, '_group' : _group
			, 'delete_reqQuery' : undefined
			//, 'use_metro_introduce':'gallery'//리스트 바로 전에 생성될 정보 글 작성. false는 작성 안함.
		}
		, 'wvm_options': {//wvm에서 사용할 것.
			'none_hidden':'true'
			, 'none_notice':'true'
			, 'none_memo':'true'
			, 'use_file': true
			, '_group' : _group
			, 'use_list' : true //다음 글등을 볼 수 있게 배열을 사용해 리스트를 구성해서 Wvm에 넘긴다.
			, 'list' : [] //imageView , modify, view에서 다음글등으로 넘어 갈 수 있게 해주는 글의 배열
			, 'list_index' : undefined //list 사용시에 어느 것을 보고 있는지 알 수 있게 해주는 인덱스 
			, 'queries' : {
				'insert_reqQuery' : undefined
				, 'select_reqQuery' : undefined
				, 'modfiy_reqQuery' : undefined
			}
		}
	});
	/*===================================
	 * 리스트에서 사용될 객체 정의
	 ===================================*/
	
	//console.log(TB_shared.get(sharedObjName));
	//console.log(context);
	//if(context['TB_half_bake']){return false;}
	
	TB_contents.render(context);
	
});
