TB_shared.set('contents--happyborn--map--js', function(_context){
	
	TB_contents.render(_context, function($topEl){
		var $fullContainer = $topEl.find('.metro_fullContainer');
		
		TB_shared.get('metroResponsive')({$scope : $topEl});//메트로 반응형 처리
		
		function map_init() { 
			var myLatlng = new google.maps.LatLng(37.1302537,126.9228482); 
			var mapOptions = { 
			      zoom: 17, 
			      center: myLatlng, 
			      mapTypeId: google.maps.MapTypeId.ROADMAP 
			} 
			var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
			var marker = new google.maps.Marker({ 
		            position: myLatlng, 
		            map: map, 
		            title: "전곡항마리나"
			        , animation : google.maps.Animation.DROP
			});
			
			
			var infowindow = new google.maps.InfoWindow();
			var infowindowLatlng = new google.maps.LatLng(37.1305537,126.9228482);
			infowindow.setContent('<b>해피본 산부인과</b>');
			infowindow.setPosition(infowindowLatlng);
			infowindow.open(map);
			
			var resizeFunc;
			
			google.maps.event.addDomListener(window, "resize", function() { //리사이즈에 따른 마커 위치
				clearTimeout(resizeFunc);
				
				resizeFunc = setTimeout(function(){
			        var center = map.getCenter();
			        google.maps.event.trigger(map, "resize");
			        map.setCenter(center); 
				}, 700);
				
		    });
		    /*===/리사이즈 이벤트 ==*/
		   
		   google.maps.event.addListenerOnce(map, 'idle', function(){
				$topEl.find('.gm-style-iw').find('div').css('overflow', 'hidden');
				
				var $box = $topEl.find('.box');
				var $container = $topEl.find('.metro_2, .metro_3');
				TB_ani.flow('fadeIn', $container, function(){
					TB_ani.flow('staggerTo_up', $box);
				});
		   });
		    /*===/맵 로딩 완료 후 이벤트==*/
		}
		/*=================================================
		 * 구글 맵
		 =================================================*/
		
		TB_ani.flow('fadeIn', $topEl, function(){
			map_init();
		});
		
	});
});