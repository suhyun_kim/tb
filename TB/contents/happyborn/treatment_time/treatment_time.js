TB_shared.set('contents--happyborn--treatment_time--js', function(context){
			
	TB_shared.set('contents--happyborn--treatment_time--doctors',{
		'select_reqQuery' : 'doctors_timetable'
		, 'select_doctor_list_reqQuery' : 'select_doctor_list'//의사 정보
		, 'getInfo_query' : 'select_doctor_info'//의사 한명 정보
		,'sharedObj' : 'contents--happyborn--treatment_time--doctors'
		, 'none_memo' : 'true'
		, 'none_hidden' : 'true'
		, 'none_notice':'true'
		, 'use_file':true
		, 'categories' : [
			{'name' : '1과'}
			,{'name' : '2과'}
			,{'name' : '3과'}
			,{'name' : '4과'}
			,{'name' : '5과'}
			,{'name' : '6과'}
			,{'name' : '7과'}
			,{'name' : '8과'}
			,{'name' : '9과'}
			,{'name' : '10과'}
		]
	});
	
	TB_contents.render(context,function($topEl){
		TB_shared.get('metroResponsive')({$scope : $topEl});
		TB_func.imageReady($topEl,function(){
			TB_ani.flow('fadeIn', $topEl);
			setTimeout(function(){
				TB_ani.flow('staggerTo_left', $topEl.find('.metro_container'));
			}, 500);
		});
	});
	
	
});