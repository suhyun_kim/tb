TB_shared.set('contents--happyborn--doctors--js', function(_context){
	
	var resizeDoctor;
	
	var resizeFunc = function(){
		var $topEl = $('#contents--happyborn--doctors');
		var $pageContainer = $('#contents--happyborn--doctors').closest('.TB_page_container');
		var $bg = $topEl.find('.metro_bg');
		if(!$pageContainer.hasClass('TB_page_container_active'))return false;
		TB_style.binding($topEl);
	};
	
	$(window).on('resize.resize_doctor', function(){
		clearTimeout(resizeDoctor);
		resizeDoctor = setTimeout(function(){
			resizeFunc();
		}, 1000);
	});/*====/스타일 재정의 ==*/
	
	
	TB_contents.render(_context, function($topEl){
		var $box = $topEl.find('.check_box');
		TB_shared.get('metroResponsive')({$scope : $topEl});
		
		TB_ani.ready($topEl, function(){
			var $img = $topEl.find('img');
			resizeFunc();
			
			if($img.length){
				$img.last().load(function(){
					TB_ani.flow('fadeIn', $topEl, function(){
						TB_style.binding($topEl);
						TB_ani.flow('staggerTo_up', $box);
					});
				});
			}else{
				setTimeout(function(){
					TB_ani.flow('fadeIn', $topEl, function(){
						TB_style.binding($topEl);
						TB_ani.flow('staggerTo_up', $box);
					});
				}, 300);
			}
		});
		
	});
});