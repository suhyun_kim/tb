TB_shared.set('contents--happyborn--gallery_supersonicWave--js', function(){
	var $topEl = $('#contents--happyborn--gallery_supersonicWave')
			, $login_container = $topEl.find('.login_form_wrap')
			, $login_btn = $login_container.find('.submit_btn')
			, $list_wrap = $topEl.find('.list_wrap')
			, context = {}
			, download_url_header = 'http://movie.inicode.net/babysu_happyborn/'
			, page = 1
			;
	context['list'] = [];
	context['date'] = [];
	context['nextpage'];
	
	var setList = function(data){
		//console.log(data);
		var obj = {
					list : []
					, date : []
				}
				, url = ''
				, len= data.length
				, page;
		for(var i = 3; i < len-1 ; i++){
			url = data[i].split('?');
			url = url[1].split('=')[1];
			obj.list.push(url);
			url = url.split('m')
			//console.log(url);
			//console.log(url.length);
			if(url.length > 1){
				url = url[1].substr(0, 8);
				//console.log(url);	
				obj.date.push(url);
			}else if(url.length == 1){//페이지일 경우
				obj.list.pop();
				context['nextpage'] = url[0];
			}
		}
		//console.log(url);
		//obj.list.pop();
		url = null, len = null;
		return obj;
	};
	/*=================================
	 * 가져온 데이터를 파싱해 리스트 형태로 만든다.
	 * return 
	 * 	. obj(obj) : 리스트 데이터
	 =================================*/
	
	var renderLIst= function(){
		var src = ''
				, i = 0
				, len =context['list'].length
				;
		
		for(; i < len ; i++){
			src += '<a target="_blank" href="'+download_url_header+context['list'][i]+'" >';
			src += '<li class="ST_list_item">';
			src += '<span class="date ST_list_subject">';
			src += context['date'][i];
			src += '</span>';
			src += '</li>';
			src += '</a>';
		}
		//console.log(src);
		if(page == 1){
			$list_wrap.find('ul').html(src).promise().done(function(){
				$list_wrap.fadeIn();
			});
			$login_container.hide();
		}else if(page > 1){
			$list_wrap.find('ul').append(src);
		}
		//console.log(page, context['nextpage']);
		if(page < context['nextpage']){//페이지가 남아 있을 경우.
			$list_wrap.find('.list_add_wrap').show();
		}else{
			$list_wrap.find('.list_add_wrap').hide();
		}
		return src;
	};
	
	var get_list = function(data){
		var sendData= {
			id : undefined
			, password : undefined
			, callback : undefined
			, page : page
			, reqQuery : 'superSonicMovie'
		};
		
		$.extend(sendData, data);
		//console.log(sendData);
		
		if(!sendData.id || !sendData.password){
			alert('아이디와 패스워드를 확인해 주십시요.');
			return false;
		}
		TB_ajax.get_json({
			'sendData':sendData
			, 'callback': function(res){
				var res_data;
				//console.log(res);
				if(res.body.length <= 2){//아이디가 맞았을 경우
					alert('동영상이 존재 하지 않습니다.\n 문의전화 031-8059-8866 (내선1번)');
					return false;
				}
				res_data = setList(res.body);
				context['list'] = res_data.list;
				context['date'] = res_data.date;
				renderLIst();
			}
		});
	};
	/*====================================
	 * 데이터 가져오기 -> 파싱 -> 랜더링
	 ====================================*/
	
	TB_shared.set('contents--happyborn--gallery_supersonicWave--login', function(data){
		var obj = {
			id : data[0].value
			, password :data[1].value 
		};
		context['id'] = data[0].value;
		context['password'] = data[1].value;
		get_list(obj);
		obj = null;
		return false;
	});
	/*====================================
	 * 로그인 처리
	 ====================================*/
	
	var eBinding = function(){
		/*
		$login_btn.off('click.login')
		.on('click.login', function(){
			var obj = {
				id : $login_container.find('.id').val()||undefined
				, password : $login_container.find('.password').val()||undefined
			}
			context['id'] = obj.id;
			context['password'] = obj.password;
			get_list(obj);
		});
		/*==/ 로그인 ==*/
		
		$topEl.find('.list_add_btn').off('click.add')
		.on('click.add', function(){
			page++;
			get_list({
				id : context['id']
				, password : context['password']  
			});
		});
		/*==/ 더보기 버튼 ==*/
	};
	/*====================================
	 * 이벤트 바인딩
	 ====================================*/
	
	var init = function(){
		TB.binding($topEl);
		TB.eBinding($topEl);
		TB_shared.get('metroResponsive')({scope:$topEl})
		eBinding();
	};
	/*====================================
	 * 초기화 
	 ====================================*/
	
	init();

	
});
