TB_shared.set('contents--happyborn--greeting--js', function(_context){
	_context['obj'] = {};
	_context['obj'].root_path = TB_config.get('TB_root_path');
	
	var setHei = function($el){
		$el.height($(window).height());
	};
	
	TB_contents.render(_context, function($topEl){
		var $show_items = $topEl.find('.show_item');
		
		TB_shared.get('metroResponsive')();
		TB_func.imageReady($topEl, function(){
			TB_ani.flow('fadeIn', $topEl);
		});
		
	});
});