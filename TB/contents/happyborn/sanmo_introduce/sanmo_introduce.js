TB_shared.set('contents--happyborn--sanmo_introduce--js', function(context){
	
	TB_contents.render(context, function($topEl){
		var imageReadyFunc
				, setFunc
				, $container = $topEl.find('.metro_fullContainer')
				;
		
		TB_shared.get('metroResponsive')({scope:$topEl});//반응형 메트로 함수
		
		TB_func.imageReady($topEl, function(){
			setTimeout(function(){
				TB_ani.flow('fadeIn', $topEl);
			}, 200);
			setTimeout(function(){
				TB_ani.flow('staggerTo_left', $container);
			}, 500);
		});
		/*=======================================
		 * 랜더링 후 컨테이너 애니메이션
		 =======================================*/
		
	});
	
});
