TB_shared.set('contents--happyborn--sanmo_info--js', function(context){
	// console.log('테스트');
	var wvm_type = TB_shared.get('templates--wvm--wvm_1--reqType')
			; 
	
	if(wvm_type === 'view' ||wvm_type === 'insert' ||wvm_type === 'modify'  ){//wvm에서 view일경우 페이리 다시 로딩 안함.
		TB_shared.set('templates--wvm--wvm_1--reqType', null);
		return false;
	}
	
	if(context['TB_half_bake']){//베이킹 되어 있을 경우
		$('#contents--happyborn--sanmo_info').find('.nav_container ')
		.find('[data-category='+context['param'].category+']')
		.trigger('click')
		return false;
	}
	//console.log(context['param']);
	
	TB_shared.set('contents--happyborn--sanmo_info--bbs_data', {
		'sharedObj' : 'contents--happyborn--sanmo_info--bbs_data'
		, 'category_title' : '정보안내'
		, 'categories' : [
			{"page":"sanmo_info_1"
				,'name':"출산전유익정보"}
			, {"page":"sanmo_info_2"
				,'name':"출산후유익정보"}
		]
		, 'list_options' : {//리스트 쿼리에 사용할 것
			'reqQuery' : 'get_bbs_list'
			, '_group' : 'sanmo_info'
			, 'delete_reqQuery' : undefined
			, 'category' : context['param'].category
		}
		, 'wvm_options': {//wvm에서 사용할 것.
			'none_hidden':'true'
			, 'none_notice':'true'
			, 'none_file':'true'
			, '_group' : 'sanmo_info'
			, 'queries' : {
				'insert_reqQuery' : undefined
				, 'select_reqQuery' : undefined
				, 'modfiy_reqQuery' : undefined
			}
		}
	});
	
	TB_contents.render(context);
	
});