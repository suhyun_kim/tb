TB_shared.set('contents--happyborn--index--js', function(_context){
	var $html = $(_context['html'])
			, doctor_list_src = $html.find('.doctor_list_wrap').html()
	;
	
	var render_flow = function(){
		getDoctor(function(){
			render();
		});
	};
	/*============================================
	 * 랜더링 흐름
	 ============================================*/
	
	var render = function(){
		//console.log(_context['obj']);
		TB_contents.render(_context, function($topEl){
			
			var $box = $topEl.find('.metro_container').find('.box');
			TB_shared.get('metroResponsive')();//반응형 메트로 함수
			
			TB_ani.flow('fadeIn', $topEl);
			
			
			TB_ani.ready($box, function(){
				TB_ani.flow('staggerTo_up', $box);
				setTimeout(function(){
					if(_context['obj'].TB_isAdmin){
						TB_contents.load($topEl.find('.index_slider'));//슬라이더 로드
					}
					get_short_list();
				}, 1000)
				
			});/*==/첫 로딩 애니메이션==*/
			
			box_hover_ani($topEl);//박스  애니메이션
		});
	};
	/*===================================
	 * 컨텐츠 랜더링
	 ===================================*/

	var getDoctor = function(_callback){
		TB_ajax.get_json({
			sendData : {
				reqQuery : 'select_doctor_list'
			}
			, callback : function(res){
				res.body.TB_root_path = TB_config.get('TB_root_path');
				_context['obj'] =$.extend(_context['obj'], res.body);
				if($.type(_callback) === 'function'){_callback();} 
			}
		});
	};
	/*===================================
	 * 원장 리스트 불러오기
	 ===================================*/
	
	var get_short_list = function(_callback){
		TB_ajax.get_json({
			sendData : {
				'reqQuery': 'get_notice_short_list'
				, 'list_num':4
				, 'page':1
			}
			, callback : function(res){
				//console.log(res);
				_context['parts'].render({
					'parts':['contents--happyborn--index--notice_list'
									, 'contents--happyborn--index--qna_list'
									, 'contents--happyborn--index--postscript_list']
					, 'obj': res.body
					, 'callback': function(){
						if($.type(_callback) === 'function'){_callback(res);} 
					}
				});
			}
		});
	};
	/*===================================
	 * 공지사항, 온라인상담, 이용후기 shot list 불러오기.
	 ===================================*/
	
	var box_hover_ani = function($scope){
		var $box = $scope.find('.box');
		
		if(Modernizr.csstransitions){
			$box
			.on('mouseenter', function(){
				TweenMax.to(this, 0.5, {
					css : { 'border-radius' : '1.5em' }
					, ease : Power3.easeOut
				});
			});
			$box
			.on('mouseleave', function(){
				TweenMax.to(this, 0.5, {
					css : { 'border-radius' : '0' }
				});
			});
		}
		$box = null;
	};
	/*===================================
	 * 박스 hover 애니메이션
	 ===================================*/



	
	render_flow();
	
	
	
});
