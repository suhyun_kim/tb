TB_shared.set('contents--happyborn--building--js', function(_context){
	
	TB_shared.set('contents--happyborn--building--galleryData', 
		{
			"reqQuery":"showBuilding",
			"sharedObj":"contents--happyborn--building--galleryData",
			"category_title" : "둘러보기",
			"categories" : [
				{"name" : "7F(산후조리원)"},
				{"name" : "6F(산후조리원)"},
				{"name" : "5F(산모병실)"},
				{"name" : "4F(분만실)"},
				{"name" : "3F(진료실,로비)"},
				{"name" : "2F(소아과)"},
				{"name" : "1F(약국,카페 등)"},
				{"name" : "B1(주차장)"},
				{"name" : "B2(주차장)"},
			],
			"category":"7F(산후조리원)",
			"none_memo":"true",
			"none_hidden":"true",
			"none_notice":"true"
			, 'use_file':true
		}
	);
	/*==============================
	 * building gallery 정보 객체 셋팅
	 ==============================*/
	
	TB_contents.render(_context, function($topEl){
		TB_shared.get('metroResponsive')({$scope : $topEl});
	});
});