TB_shared.set('contents--happyborn--postpartumCare_program--js', function(context){
	var $topEl = {}
			, $container
			, $pc_nav 
			, nav_toggle_sw = true
			;
	context['obj'] = {};
	
	
	
	var create_name = function(_week, _time){
		var list = [];
		for(var i = 1, len = 7; i<len ; i++){
			list.push({week: _week, time: _time, num: i});
		}
		return list;
	};
	/*=======================================
	 * 칸 생성 하기
	 * params 
	 * 	. _week(int) : 몇 주차 인지 표시 '1 or 2';
	 * 	. _ time(int) : 오전 = 1 , 오후 = 2
	 =======================================*/
	
	var render_list = function(_week, _time){
		context['obj'].list =create_name(_week,_time);  
		context['parts'].render({
			parts:'contents--happyborn--postpartumCare_program--list'
			, callback : function(){
				TB_shared.get('metroResponsive')({scope:$topEl});
				setTimeout(function(){
					TB_ani.flow('staggerTo_left', $topEl.find('.program_detail'));
				}, 500);
			} 
		});
	};
	/*=======================================
	 * 리스트 랜더링
	 * params 
	 * 	. _week(int) : 몇 주차 인지 표시 '1 or 2';
	 * 	. _ time(int) : 오전 = 1 , 오후 = 2
	 =======================================*/
	
	
	TB_contents.render(context, function(_$topEl){
		$topEl = _$topEl;
		$container = $topEl.find('.metro_fullContainer')//컨테이너 객체
		$pc_nav = $topEl.find('.pc_nav_container')
		
		;
		
		TB_shared.get('metroResponsive')({scope:$topEl});//반응형 메트로 함수
		TB_ani.flow('fadeIn', $topEl);
		/*=======================================
		 * 이미지 랜더링 후 애니메이션
		 =======================================*/
		
		$topEl.find('.dec_wrap');
		/*=======================================
		 * 이벤트
		 =======================================*/
		
		render_list(1,1);
		/*=======================================
		 * 리스트 출력
		 =======================================*/
		
		$topEl.find('.navBtn').off('click.program_nav')
		.on('click.program_nav', function(){
			var $this = $(this)
					, week = $this.attr('data-week')
					, time = $this.attr('data-time')
					;
			$this.closest('ul').find('.navBtn').each(function(){
				$(this).removeClass('nav_active');
			});
			$this.addClass('nav_active');
			
			render_list(week, time);
		});
		
		$topEl.find('.nav_foldBtn').off('click.nav_folding')
		.on('click.nav_foldBtn', function(){
			var $this = $(this)
					, $nav_container = $this.closest('.nav_container')
					, has_caret_down = $this.hasClass('fa-caret-down')
					, caret = ''
					;
			$nav_container.find('ul').slideToggle('slide');
			if(has_caret_down){
				$this.removeClass('fa-caret-down').addClass('fa-caret-up');
			}else{
				$this.removeClass('fa-caret-up').addClass('fa-caret-down');
			}
			$this = null, $nav_container = null, has_caret_down = null, caret = null;
		});/*==/네비게이션 슬라이드 ==*/
		/*=======================================
		 * 이벤트 바인딩
		 =======================================*/
		
		
	});
	
});
