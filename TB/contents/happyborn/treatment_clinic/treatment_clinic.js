TB_shared.set('contents--happyborn--treatment_clinic--js', function(context){
	TB_shared.set('contents--happyborn--treatment_clinic--bbs_data', {
		'sharedObj' : 'contents--happyborn--treatment_clinic--bbs_data'
		, 'category_title' : '진료안내'
		, 'categories' : [
			{"page":"treatment_obstetrics"
				,'name':"산과클리닉"}
			, {"page":"treatment_gynecology"
				,'name':"부인과클리닉"}
			, {"page":"treatment_special"
				,'name':"특수클리닉"}
			, {"page":"treatment_etc"
				,'name':"기타클리닉"}
			, {"page":"treatment_examine"
				,'name':"검진"}
		]
		, 'list_options' : {//리스트 쿼리에 사용할 것
			'reqQuery' : 'get_bbs_list'
			, '_group' : 'treatment_clinic'
			, 'delete_reqQuery' : undefined
			, 'category' : context['param'].category
		}
		, 'wvm_options': {//wvm에서 사용할 것.
			'none_hidden':'true'
			, 'none_notice':'true'
			, 'none_file':'true'
			, 'queries' : {
				'insert_reqQuery' : undefined
				, 'select_reqQuery' : undefined
				, 'modfiy_reqQuery' : undefined
			}
		}
	});
	
	TB_contents.render(context);
	
});