TB_shared.set('contents--happyborn--postpartumCare_centers--js', function(context){
	var imageReadyFunc,
			setFunc;
			
	context['obj'] = {};
	context['obj'].root_path = TB_config.get('TB_root_path');
			
	
	TB_contents.render(context, function($topEl){
		
		var $container = $topEl.find('.metro_fullContainer')
				;
		
		/*=======================================
		 * 애니메이션
		 =======================================*/
		
		
		TB_shared.get('metroResponsive')({scope:$topEl});//반응형 메트로 함수
		
		TB_func.imageReady($topEl, function(){
			
			setTimeout(function(){
				TB_ani.flow('fadeIn', $topEl);
			}, 200);
			
			setTimeout(function(){
				TB_ani.flow('staggerTo_left', $container);
			}, 500);
			
		});
		/*=======================================
		 * 이미지 랜더링 후 보이기
		 =======================================*/
		
		
		$topEl.find('.dec_wrap')
		/*=======================================
		 * 이벤트
		 =======================================*/
		
		
		 
		
	});
});
