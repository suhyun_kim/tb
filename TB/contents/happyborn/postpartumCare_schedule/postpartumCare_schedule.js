TB_shared.set('contents--happyborn--postpartumCare_schedule--js', function(context){
	var $topEl = {}
			, $container
			, $pc_nav 
			, parts_list_name = 'contents--happyborn--postpartumCare_schedule--list'
			, list_len = []//카테고리에 따라 리스트 생성할 갯수.
			, $parts_list//리스트를 랜더링할 파트
			, $nav_container//pc, mobile 2개의 네비 게이션
			, $nav_btn
			;
			
	list_len[0] = 8;
	list_len[1] = 6;
	
	context['obj'] = {};
	
	var create_el = function(category){
		var list  = []
				i = 0
				;
		for(;i <  list_len[category] ; i++){
			list[i] = {};
			list[i].category = category;
			list[i].num = i;
		}
		i = null;
		return list;
	};
	/*=======================================
	 * 카테고리에 따른 요소 생성하기.
	 * params 
	 * 	- category (int) : 카테고리
	 =======================================*/
	
	var render_list = function(_category){
		context['obj'].list =create_el(_category);  
		context['parts'].render({
			parts:parts_list_name
			, callback : function(){
				TB_shared.get('metroResponsive')({scope:$topEl});
				setTimeout(function(){
					TB_ani.flow('staggerTo_left', $parts_list.find('.metro_fullContainer'));
				}, 500);
			} 
		});
	};
	/*=======================================
	 * 리스트 랜더링
	 * params 
	 * 	. _week(int) : 몇 주차 인지 표시 '1 or 2';
	 * 	. _ time(int) : 오전 = 1 , 오후 = 2
	 =======================================*/
	
	
	TB_contents.render(context, function(_$topEl){
		$topEl = _$topEl
		, $container = $topEl.find('.metro_fullContainer')//컨테이너 객체
		, $pc_nav = $topEl.find('.pc_nav_container')
		, $nav_container = $topEl.find('.nav_container')
		, $nav_btn = $nav_container.find('.navBtn')
		, $parts_list = $topEl.find('.parts_list_wrap')
		;
		
		TB_shared.get('metroResponsive')({scope:$topEl});//반응형 메트로 함수
		TB_ani.flow('fadeIn', $topEl);
		/*=======================================
		 * 이미지 랜더링 후 애니메이션
		 =======================================*/
		
		$topEl.find('.dec_wrap');
		/*=======================================
		 * 이벤트
		 =======================================*/
		
		render_list(0);
		/*=======================================
		 * 리스트 출력
		 =======================================*/
		
		$nav_btn.off('click.schedule_nav')
		.on('click.schedule_nav', function(){
			var $this = $(this)
					, category = $this.attr('data-category')
					;
			$topEl.find('.navBtn').each(function(){
				var _$this = $(this);
				if(_$this.attr('data-category') === category){
					$this.addClass('nav_active');
				}else{
					_$this.removeClass('nav_active');
				}
				_$this = null;
			});
			
			$this = null;
			render_list(category);
		});
		/*==/네비 버튼 ==*/
		
		$topEl.find('.nav_foldBtn').off('click.nav_folding')
		.on('click.nav_foldBtn', function(){
			var $this = $(this)
					, $nav_container = $this.closest('.nav_container')
					, has_caret_down = $this.hasClass('fa-caret-down')
					, caret = ''
					;
			$nav_container.find('ul').slideToggle('slide');
			if(has_caret_down){
				$this.removeClass('fa-caret-down').addClass('fa-caret-up');
			}else{
				$this.removeClass('fa-caret-up').addClass('fa-caret-down');
			}
			$this = null, $nav_container = null, has_caret_down = null, caret = null;
		});/*==/네비게이션 슬라이드 ==*/
		/*=======================================
		 * 이벤트 바인딩
		 =======================================*/
		
		
	});
	
});
