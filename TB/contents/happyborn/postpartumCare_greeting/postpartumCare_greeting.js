TB_shared.set('contents--happyborn--postpartumCare_greeting--js', function(context){
	//console.log(context);
	var imageReadyFunc,
			setFunc;
	
	TB_contents.render(context, function($topEl){
		var $container = $topEl.find('.metro_fullContainer')
				;
		
		TB_shared.get('metroResponsive')({scope:$topEl});//반응형 메트로 함수
		
		TB_func.imageReady($topEl, function(){
			
			setTimeout(function(){
				TB_ani.flow('fadeIn', $topEl);
			}, 200);
			
			setTimeout(function(){
				TB_ani.flow('staggerTo_left', $container);
			}, 500);
			
		});
		/*=======================================
		 * 이미지 랜더링 후 보이기
		 =======================================*/
		
	});
});
