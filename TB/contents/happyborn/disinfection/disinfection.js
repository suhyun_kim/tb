TB_shared.set('contents--happyborn--disinfection--js', function(_context){
	_context['obj'] = {};
	_context['obj'].root_path = TB_config.get('TB_root_path');
	//console.log(_context);
	
	TB_contents.render(_context, function($topEl){
		var $fullContainer = $topEl.find('.metro_fullContainer');
		TB_shared.get('metroResponsive')({$scope : $topEl});
		
		TB_func.imageReady($topEl, function(){
			TB_ani.flow('fadeIn', $topEl, function(){
				TB_ani.flow('staggerTo_left', $fullContainer);
			});
		});
		
	});
});