TB_shared.set('contents--happyborn--gallery_baby--js', function(context){
	var sharedObjName='contents--happyborn--gallery_baby--galleryData'
			, _group = 'gallery_baby'
			;
	
	TB_shared.set(sharedObjName, {
		'sharedObj' : sharedObjName
		, 'list_options' : {//리스트 쿼리에 사용할 것
			'reqQuery' : 'list_gallery_baby'
			, '_group' : _group
			, 'delete_reqQuery' : undefined
			, 'use_metro_introduce':'gallery_baby'//리스트 바로 전에 생성될 정보 글 작성. false는 작성 안함.
		}
		, 'wvm_options': {//wvm에서 사용할 것.
			'none_hidden':'true'
			, 'none_notice':'true'
			, 'none_memo':'true'
			, 'use_file': true
			, '_group' : _group
			, 'queries' : {
				'insert_reqQuery' : undefined
				, 'select_reqQuery' : undefined
				, 'modfiy_reqQuery' : undefined
			}
		}
	});
	/*===================================
	 * 리스트에서 사용될 객체 정의
	 ===================================*/
	
	//console.log(TB_shared.get(sharedObjName));
	
	TB_contents.render(context);
	
});
