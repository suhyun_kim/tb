TB_shared.set('contents--happyborn--sanmo_schedule--js', function(context){
	var $topEl = {}
			, $container
			, $pc_nav 
			, parts_list_name = 'contents--happyborn--sanmo_schedule--list'
			, list_len = []//카테고리에 따라 리스트 생성할 갯수.
			, $parts_list//리스트를 랜더링할 파트
			, $nav_container//pc, mobile 2개의 네비 게이션
			, $nav_btn
			;
			
	list_len[0] = 3;
	
	context['obj'] = {};
	
	var create_el = function(category){
		var list  = []
				i = 0
				;
		for(;i <  list_len[category] ; i++){
			list[i] = {};
			list[i].category = category;
			list[i].num = i;
		}
		i = null;
		return list;
	};
	/*=======================================
	 * 카테고리에 따른 요소 생성하기.
	 * params 
	 * 	- category (int) : 카테고리
	 =======================================*/
	
	var render_list = function(_category){
		context['obj'].list =create_el(_category);  
		context['parts'].render({
			parts:parts_list_name
			, callback : function(){
				TB_shared.get('metroResponsive')({scope:$topEl});
				setTimeout(function(){
					TB_ani.flow('staggerTo_left', $parts_list.find('.metro_fullContainer'));
				}, 500);
			} 
		});
	};
	/*=======================================
	 * 리스트 랜더링
	 * params 
	 * 	_category(int) : 출력할 카테고리 넘버
	 =======================================*/
	
	
	TB_contents.render(context, function(_$topEl){
		$topEl = _$topEl
		, $container = $topEl.find('.metro_fullContainer')//컨테이너 객체
		, $pc_nav = $topEl.find('.pc_nav_container')
		, $nav_container = $topEl.find('.nav_container')
		, $nav_btn = $nav_container.find('.navBtn')
		, $parts_list = $topEl.find('.parts_list_wrap')
		;
		
		TB_shared.get('metroResponsive')({scope:$topEl});//반응형 메트로 함수
		TB_ani.flow('fadeIn', $topEl);
		/*=======================================
		 * 이미지 랜더링 후 애니메이션
		 =======================================*/
		
		render_list(0);
		/*=======================================
		 * 리스트 출력
		 =======================================*/
		
		/*=======================================
		 * 이벤트 바인딩
		 =======================================*/
		
		
	});
	
});
