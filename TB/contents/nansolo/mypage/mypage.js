TB_shared.set('contents--nansolo--mypage--js', (function() {
	var context = undefined
		, $window  = $(window)
		, $document = $(document)
		, $add_button

		;

	var add_HTML = function(obj){
		var html = context.parts.getHTML('TB_bbs_list_wrap');
		return TB_handlebars.returnRender(html, obj);
	};
	/*==/리스트 추가 HTML ==*/
	
	

	var list_convert = function(array){
		var result = array,
				story_type = ['c1', 'c2r1', 'c2r2', 'c3r1', 'c3r2', 'c4r1', 'c4r2', 'c4r3'],
				story_height = [100, 40, 60, 60, 40, 33, 33, 34],
				i = 0, k= 0, len = result.length,
				len2 = story_type.length-1,
				icon = '', title_tag = '',
				color = "#FFFFFF"
		    ;

		for (; i < len; i++) {
			result[i].story_type = story_type[k];
			result[i].story_height = story_height[k];

			if (result[i].category === 'etc') {
				icon = 'fa-book';
				title_tag = '기타 이야기';
			} else if (result[i].category === 'food') {
				icon = 'fa-cutlery';
				title_tag = '음식 이야기';
			} else if (result[i].category === 'travel') {
				icon = 'fa-map';
				title_tag = '여행 이야기';
			}

			result[i].is_group = (result[i].is_group === 'group') ? true : false;
			result[i].modify_mode = (result[i].is_group) ? 'story_group_modify' : 'piece_modify';
			//li 태그를 생성할지 결정
			result[i].piece_container_top = (story_type[k] === 'c1' || story_type[k] === 'c2r1' || story_type[k] === 'c3r1' || story_type[k] === 'c4r1') ? true : false;
			result[i].piece_container_bottom = (story_type[k] === 'c1' || story_type[k] === 'c2r2' || story_type[k] === 'c3r2' || story_type[k] === 'c4r3') ? true : false;
			result[i].color = (result[i].images_src) ? '#ffffff' : '#555555';
			result[i].category_icon = icon;
			result[i].title_tag = title_tag;

			if(k ==len2){ k = 0 ;}
			else {k++;}
		}
		return result;
	};
	
	
	/*
	var list_convert_render_data = function(list) {
		var result = list,
		    story_type = ['c1', 'c2r1', 'c2r2', 'c3r1', 'c3r2', 'c4r1', 'c4r2', 'c4r3'],
		    story_height = [100, 40, 60, 60, 40, 33, 33, 34]
		    , i = 0, k= 0
		    , len = result.length
		    , len2 = story_type.length-1
		    , icon = '', title_tag = ''
		    , color = "#FFFFFF"
		    ;
		for (; i < len; i++) {
			result[i].story_type = story_type[k];
			result[i].story_height = story_height[k];

			if (result[i].category === 'etc') {
				icon = 'fa-book';
				title_tag = '기타 이야기';
			} else if (result[i].category === 'food') {
				icon = 'fa-cutlery';
				title_tag = '음식 이야기';
			} else if (result[i].category === 'travel') {
				icon = 'fa-map';
				title_tag = '여행 이야기';
			}

			result[i].is_group = (result[i].is_group === 'group') ? true : false;
			result[i].modify_mode = (result[i].is_group) ? 'story_group_modify' : 'piece_modify';
			//li 태그를 생성할지 결정
			result[i].piece_container_top = (story_type[k] === 'c1' || story_type[k] === 'c2r1' || story_type[k] === 'c3r1' || story_type[k] === 'c4r1') ? true : false;
			result[i].piece_container_bottom = (story_type[k] === 'c1' || story_type[k] === 'c2r2' || story_type[k] === 'c3r2' || story_type[k] === 'c4r3') ? true : false;
			result[i].color = (result[i].images_src) ? '#ffffff' : '#555555';
			result[i].category_icon = icon;
			result[i].title_tag = title_tag;

			if(k ==len2){ k = 0 ;}
			else {k++;}
		}
		return result;
	};
	/*==/리스트 배열에 랜더링에 사용될 값 추가./==*/


	var get_detail_story = function(_obj, callback ){
		var obj = {
				no : undefined
				, category : undefined
				, re_render : false
			};

		obj = $.extend(obj, _obj);

		TB_func.fall([
			//데이터 가져오기
			function(next, param){
				TB_ajax.get_json({
					sendData : {
						reqQuery : 'get_detail_story'
						, no : obj.no
						, category : obj.category
					}
					, callback : function(res){
						if($.type(callback) === 'function'){callback(res.body);}
						if(obj.re_render === true){
							next(res.body);
						}
					}
				});

			}
			//리스트 배열의 데이터 바꾼후 해당 부분만 재랜더링
			, function(next, param){
				var data= param
					, list = context.obj.render.list
					, article
					, html , render
					, $el = context.parent.find('.story_no_'+data.no)
					;

				for(var i = 0, len = list.length ; i < len ; i++){
					if(list[i].no === data.no){
						list[i] = data;
						break;
					}
				}

				list = list_convert_render_data(list);

				for( var i = 0, len= list.length ; i < len ;i++){
					//console.log(list[i].no, data.no);
					if(list[i].no === data.no){
						article  = list[i];
						break;
					}
				}

				html = context.parts.getHTML('TB_bbs_list_wrap');
				render = TB_handlebars.returnRender(html, {TB_worked:context.obj.TB_worked,render:{list : [article]}});
				render = $(render).find('.article_card').html();

				$el.html(render).promise().done(function(){
					var $img = $el.find('img');
					TB_shared.get('metroResponsive')();
					TB_form.binding($el);
					evt(context);
					if($img.length){
						$img.load(function(){
							TB_style.binding($el.parent());
						});
					}else{
						TB_style.binding($el);
					}
				});

			}
		]);

	};
	/*==/세부 데이터 가져오기./==*/
	
	
	/*
	var get_mystory = function(_options) {
		var param = TB_page.locationDec().urlParam||{}
				, options = {
					callback : undefined
					, add : false//리스트를 추가할것인지 전체를 새롭게 랜더링할것인 정하는 옵션.
					, prepend : false//리스트 앞에 추가.
					, max_len : 3 //리스트를 늘리때 제한 갯수
					, page : param.page||context.obj.page||1
					, category : (param.category !='all')?param.category:undefined
					, search_value : param.search_value||undefined//검색 값
					, search_field : param.search_field||undefined//db field
					, search_type : param.search_type||undefined//검색 타입
				}
				;

		options = TB_func.merge(options, _options);

		TB_func.fall([
			//이미지 받아 오기
			function(next, param) {
				TB_ajax.get_json({
					sendData : {
						reqQuery : 'mystory'
						, page: options.page
						, list_num : 8
						, category : options.category
						, search_value : options.search_value
						, search_field : options.search_field
						, search_type : options.search_type
					},
					callback : function(data) {

						//console.log(data);
						data.body.list_info.page = parseInt(data.body.list_info.page, 10);
						context.obj.render.list_info = data.body.list_info;
						data.body.list = list_convert_render_data(data.body.list);

						//마지막 페이지에 도달 했을 경우 add 버튼 삭제
						if(context.obj.render.list_info.page >= context.obj.render.list_info.total_page){
							$add_button.remove();
						}

						if(context.obj.first_page > context.obj.render.list_info.page){
							context.obj.first_page = context.obj.render.list_info.page;
						}else if(context.obj.last_page < context.obj.render.list_info.page){
							context.obj.last_page = context.obj.render.list_info.page;
						}

						next(data.body);
					}
				});
			}

			//리스트 랜더링에 알맞게 수정
			,function(next, data) {
				if (!data.list.length) {
					context.obj.render.list = data.list;
					next();
					return false;
				}

				var del_stories = [];

				//배열을 무한정 늘릴순 없으므로 삭제를 해야 하기는 하는데 일단 보류
				if(options.add){//페이지 마지막 추가
					//context.obj.render.list = context.obj.render.list.concat(data.list);
					//if(context.obj.render.list.length > options.max_len){
						//del_stories = context.obj.render.list.splice(0,8);
					//}
				}else if(options.prepend){//페이지 이전 추가.
					context.obj.render.list = data.list.concat(context.obj.render.list);
					//if(context.obj.render.list.length > options.max_len){
						//del_stories = context.obj.render.list.splice(-1,8);
					//}
				}else{//변환된 배열 대입
					context.obj.render.list = data.list;
				}

				next(data.list);
			}

			//랜더링 혹은 엘리먼트 추가
			, function(next, list) {

				if(options.add || options.prepend){
					var html = context.parts.getHTML('TB_bbs_list_wrap');
					var render = TB_handlebars.returnRender(html, {TB_worked:context.obj.TB_worked, render:{list : list}});
					if(options.add){
						context.parent.find('.list_wrap').append(render);
					}else{
						context.parent.find('.list_wrap').prepend(render);
					}
				}else{
					context.parts.render('TB_bbs_list_wrap');
				}

				next();
			}

			//메트로 적용
			, function(next, param) {
				TB_shared.get('metroResponsive')();
				TB_style.binding(context.parent);
				TB_form.binding(context.parent);
				evt(context);

				if($.type(options.callback) === 'function'){
					options.callback(context.obj.render);
				}
				//TB.globalStyleSet();
				//console.log(context.obj)
			}
		]);
	};
	/*==/내 이야기 가져오기 ==*/

	/*
	var delete_story = function(_category, _no, callback) {
		var category  = _category
			, no = _no
			;
			//console.log(context.obj.render.list.length);

		TB_func.fall([
			//삭제 재 확인
			function(next, param) {
				TB_ani.show_backdrop({
					src : 'templates--backdrop--confirm_1',
					sendData : '{"title":"정말삭제하시겠습니까?", "shared_func":"story_delete"}',
					width : 500,
					height : 300,
					history_back : false
				});

				TB_shared.set('story_delete', function(result) {
					TB_ani.hide_backdrop({
						history_back : false
					});
					if (result === 'yes') {
						next();
					}
				});
			}
			//리스트 엘리먼트 삭제 후 재랜더링
			, function(next, param) {
				context.obj.list_sw = true;
				var list = context.obj.render.list,
				    i = 0,
				    len = list.length
				    ;

				for (; i < len; i++) {
					if(no === list[i].no){
						list.splice(i,1);
						break;
					}
				}

				context.obj.render.list = list_convert_render_data(list);
				context.parts.render('TB_bbs_list_wrap');
				TB_shared.get('metroResponsive')();
				TB_style.binding(context.parent);
				context.obj.list_sw = false;
				evt(context);
				next();
			}
			//db에서 리스트 삭제
			, function(next, param) {
				TB_ajax.get_json({
					sendData : {
						reqType : 'delete',
						reqQuery : 'delete_story',
						no : no
					},
					callback : function(data) {
						if ($.type(callback) === 'function') { callback(data); }
						//TB.globalStyleSet();
					}
				});
			}
		]);
	};
	/*==/스토리 삭제==*/

	var init = function(context){
		context.obj.page = 1;
		context.obj.first_page = context.obj.page;//로드한 가장 앞 페이지
		context.obj.last_page = 1;//로드한 마지막 페이지
		context.obj.list_sw = false;
		//console.log('초기화');


		get_mystory({
			page:1,
			callback : function(){
				TB_shared.get('metroResponsive')();
				evt(context);
			}
		});
	};
	/*==/초기화 ==*/
	
	/*
	var prepend_list = function(_callback){

		if(context.obj.first_page <= 1){
			return false;
		}

		TB_func.loading.on('top');
		//console.log('prepend', context.obj.first_page);

		get_mystory({
			page : --context.obj.page
			, prepend : true
			, callback : function(list){
				if($.type(_callback) === 'function'){_callback(list);}
				//console.log('prepend result', context.obj.render.list.length);
			}
		});

	};
	/*==/리스트 앞에 추가 하기==*/
	
	/*
	var add_list = function(_callback){
		var icon_direction = (TB_shared.get('winWid') > 991)?'right':'bottom';//로딩 아이콘이 나올 방향

		if(context.obj.last_page >= context.obj.render.list_info.total_page){
			var $add_button= context.parent.find('.add_button');
			if($add_button.length){
				$add_button.remove();
			}
			//console.log('stop');
			return false;
		}

		//console.log('추가함수', context.obj.list_sw);
		TB_func.loading.on(icon_direction);
		//console.log('add',context.obj.last_page, context.obj.render.list_info.total_page);

		var obj = {
					page : ++context.obj.page
					, add: true
				};

		obj.callback  = function(list){
			TB_func.loading.off();
			if($.type(_callback) === 'function'){_callback(list);}
			//console.log('add result', context.obj.render.list.length);
		};

		get_mystory(obj);
	};
	/*==/리스트 뒤에 추가 하기.==*/

	/*
	var list_append_ac = function(_ac){
		var sc_end
			, sc_cur
			, end
			, ac = _ac//add, prepend
			, mode = ''
			;

		TB.globalStyleSet();

		if(TB_shared.get('winWid') >991 ){//pc
			sc_end = TB_shared.get('documentWid') - TB_shared.get('winWid');
			sc_cur = $window.scrollLeft();
			mode = 'pc';
		}else{//mobile
			sc_end = TB_shared.get('documentHei') - TB_shared.get('winHei');
			sc_cur = $window.scrollTop();
			mode = 'mobile';
		}
		//end = Math.floor(sc_end*90/100)-40;
		end = sc_end-100;
		//console.log(sc_end, end, sc_cur, context.obj.list_sw);
		//console.log(sc, end);

		if(context.obj.list_sw  == true){return false;}
		//console.log(context.obj.list_sw);

		if(sc_cur >= end||ac === 'add'){//스크롤이 끝에 닿았을 때.
			//console.log('추가시작', context.obj.list_sw);
			context.obj.list_sw  = true;
			add_list(function(){
				context.obj.list_sw  = false;
				//console.log('추가 완료');
			});
		}else if(sc_cur <= 0 || ac === 'prepend'){//리스트의 처음으로 갈 때.
			context.obj.list_sw  = true;
			prepend_list(function(){
				context.obj.list_sw  = false;
			});
		}

	};
	/*==/리스트 추가 하는 이벤트 함수.==*/

	/*===========================================
	 * 함수
	 ===========================================*/


	/*
	var evt = function($parent){

		$parent.find('.list_wrap').find('.delete_story_button').off('click.delete_story_button')
		.on('click.delete_story_button', function() {
			var obj = JSON.parse($(this).attr('data-delete_story'));
			delete_story(obj.category, obj.no);
		});
		/*==/스토리 삭제 버튼 ==*/
		
		/*
		$window.off('scroll.append_list')
		.on('scroll.append_list', function(e){
			var dec = TB_page.locationDec();
			if(!dec.map){ return false;}
			if(!dec.map.page){ return false;}
			if(dec.map.page != 'mypage'){return false;}

			list_append_ac();
		});
		/*==/무한 스크롤링==*/

		/*
		$parent.find('.add_button').off('click.add_list_button')
		.on('click.add_list_button', function(){
			context.obj.list_sw = false;
			list_append_ac('add');
		});
		/*==/추가 버튼 직접 클릭==*/

		/*
		TB_shared.get('metro_add_each_func')(function(sc){
			//console.log(sc);
			var end = Math.floor(sc.end*90/100)-40;
			if(context.obj.list_sw  == true){return false;}

			//console.log(sc, end);
			if(sc.cur >= end){//스크롤이 끝에 닿았을 때.
				context.obj.list_sw  = true;
				add_list(function(){
					context.obj.list_sw  = false;
				});
			}else if(sc.cur === 0 &&sc.move_distance < 0){//리스트의 처음으로 갈 때.
				context.obj.list_sw  = true;
				prepend_list(function(){
					context.obj.list_sw  = false;
				});
			}
		});
		/*==/스크롤이 마지막에 닿았을 때 이벤트 추가. ==*/
	/*
	};
	/*===========================================
	* 이벤트
	===========================================*/

	return {
		init : function(_context) {
			var param = _context.param||{}

			//console.log(_context)
			context = _context;

			//console.log(TB_page.locationDec());
			context['obj'] = {};

			if(!context.param) { context.param = {};}

			context.obj.TB_worked = TB_page.locationDec().map.page;
			context.obj.page = context.param.page||1;
			context.obj.first_page = context.obj.page;//로드한 가장 앞 페이지
			context.obj.last_page = 1;//로드한 마지막 페이지
			context.obj.list_sw = false;

			context.obj.refresh = false;//true 일경우 on에서 페이지를 초기화
			context.obj.re_render = {
				re_render : false//true 일경우 TB_half_bake에서 페이지내에서글을 찾아 랜더링
				, no : undefined
				, category : undefined
			};

			context.obj.render = {};
			context.obj.render.list = [];
			context.obj.render.list_info = {};

			$add_button = context.parent.find('.add_button');



			/*===========================================
			 * 변수설정
			 ===========================================*/

			context.bbs_options = {
				page : context.obj.page
				, list_type : 'append'
				, query_get_list : 'mystory'
				, query_update_item : 'get_detail_story'
				, query_delete_item : 'delete_story'
				, $parent : context.parent
				, list_convert : list_convert
				, $parent : context.parent
				, infinity_scroll : true
				, category : param.category||''
				, search_field : param.search_field||'',
				search_value : param.search_value||''
			};

			//console.log(context.parts);
			// console.log(context.parent.html());
			// return false;
			//console.log(param)

			context.bbs = TB_bbs(context.bbs_options);

			//console.log(context.bbs.options);

			/*
			var c = TB_bbs({
				page : 1
				, list_num : 2
				, list_type : 'change'
				, query_get_list : 'mystory'
				, $parent : context.parent
				, list_convert : list_convert
				, infinity_scroll : true
			});

			c.evt_on(c);
			*/
			
			//console.log('init');
			TB_contents.render(context, function($topEl) {
				context.bbs.render(context.bbs);
				//console.log(context.bbs);
			});
			/*===========================================
			 * 랜더링
			 ===========================================*/
		}
		, on : function(p){
			var curPosi = (TB_shared.get('winWid')>991)?$window.scrollLeft():$window.scrollTop()
					, param  = p.param||{}
					, search_options = {}
					;
			
			//console.log($.isEmptyObject(param));
			
			
			
			//검색 값이 있을 경우
			if($.isEmptyObject(param) === false){
				/*
				if(param.page && param.page != context.obj.page){
					context.bbs.page_move(context.bbs, param.page);
				}
				*/
				//console.log(param);
				search_options.page = param.page||'';
				search_options.category = param.category||'';
				search_options.search_field = param.search_field||'';
				search_options.search_value = param.search_value||'';
				context.bbs.search(context.bbs, search_options);
			}else{
				search_options.page = 1;
				search_options.category = '';
				search_options.search_field = '';
				search_options.search_value = '';
				context.bbs.search(context.bbs, search_options);
			}
			
			
			//리프레쉬
			if(context.obj.refresh === true){
				context.obj.refresh = false;
				curPosi = 0;
				//init(context);
			}

			setTimeout(function(){
				(TB_shared.get('winWid')>991)?$window.scrollLeft(curPosi):$window.scrollTop(curPosi);
				TB_shared.get('metro_set_curPosi')(curPosi);
				context.obj.list_sw = false;
			}, 500);

			if(context.obj.re_render.re_render === true){
				var item = {no : context.obj.re_render.no, category : context.obj.re_render.category};
				context.bbs.update_item(context.bbs, item );
				return false;
				//get_detail_story(context.obj.re_render);
				//context.obj.re_render.re_render = false;
			}

		}
		//문서닫을 때 함수
		, off : function(history){
			//console.log(history);
			context.bbs.evt_off(context.bbs);
			//$window.off('scroll.append_list');
		}
		//처음부터 보여준다.
		, refresh : function(){
			context.obj.refresh = true;
		}
		//업데이트된 글이 어느것인지 정한다.
		, re_render : function(no, category){
			context.obj.re_render.re_render = true;
			context.obj.re_render.no = no;
			context.obj.re_render.category = category;
		}
	};
})());
