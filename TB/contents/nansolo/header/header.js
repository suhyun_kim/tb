TB_shared.set('contents--nansolo--header--js', function(context){
	var tl = (Modernizr.csstransforms3d)?new TimelineMax():undefined;
	var $header;
	var $contents;
	var $m_nav_bg;
	var sw = true;
	var view = false ; 
	
	context.obj = {};
	
	/*======================================
	 * 변수 설정
	 ======================================*/
	
	var m_nav_on = function(callback){
		$m_nav_bg.removeClass('ST_dis_none');
		context.parent.find('.write_button_link').addClass('ST_dis_none');
		$contents.css({
			height: Math.ceil(TB_shared.get('winHei')*0.98)
			, overflow: 'hidden' 
		});
		tl.to([$header, $contents], 0.3,{
			x: -200
			, onComplete : function(){
				if($.type(callback) === 'function'){callback();}
			} 
		});
	};
	/*== / 네비게이션 열기 ==*/
	
	var m_nav_off = function(callback){
		tl.to([$header, $contents], 0.3,{
			x: 0
			, onComplete : function(){
				$m_nav_bg.addClass('ST_dis_none');
				$header.css('transform','');
				context.parent.find('.write_button_link').removeClass('ST_dis_none');
				$contents.css({
					height: '' , overflow: '', transform : ''
				});
				if($.type(callback) === 'function'){callback();}
			} 
		});

	};
	/*== / 네비게이션 닫기 ==*/
	
	var hide_show_search = function(_page){
		var page = undefined
			, used_page = ['mypage', 'food', 'travel', 'etc']
			, $serch = context.parent.find('.search_button_link')
			, dec = TB_page.locationDec()
			, param = "#!/search?page="
			;
		
		if(dec.map){
			page = dec.map.page;
		}else if(dec.templates){
			page = dec.templates.page;
		}
		
		if(_page){page = _page;}
			
		
		if(used_page.indexOf(page) < 0){
			$serch.hide();
		}else{
			$serch.show().attr('href', param+page);
		}
	};
	/*== / 네비게이션 닫기 ==*/
	
	
	var init = function(){
		$header = $('#header_wrap');
		$contents =$('#contents_wrap'); 
		$m_nav_bg = context.parent.find('.m_nav_bg');
	};
	/*==/ 초기화 / ==*/ 
	/*======================================
	 * 함수
	 ======================================*/
	
	var evt = function(_$topEl){
		var $topEl = _$topEl;
		
		$topEl.find('.toggle_m_nav_btn').off('click.toggle_m_nav_btn')
		.on('click.toggle_m_nav_btn', function(){
			if(view){return false;}
			if(!sw){return false;}
			sw = false;
			
			m_nav_on(function(){
				view = true;
				sw = true;
			});
			
		});
		/*== /모바일 토글 ON/ ==*/
		
		$topEl.find('.toggle_m_nav_btn, .m_nav_bg').off('click.toggle_m_nav_Offbtn')
		.on('click.toggle_m_nav_Offbtn', function(){
			if(!view){return false;}
			if(!sw){return false;}
			sw = false;
			
			m_nav_off(function(){
				sw = true;
				view = false;
			});
		});
		/*==/ 모바일 토글 OFF==*/
		
		
		TB_page.moveAc(function(dec){
			var page  = dec.map.page
				;
			hide_show_search(page)
		});
	};
	/*======================================
	 * 이벤트
	 ======================================*/
	
	init();
	TB_contents.render(context, function($topEl){
		evt($topEl);
		hide_show_search()
	});
	/*======================================
	 * 컨트롤
	 ======================================*/
	
});



























