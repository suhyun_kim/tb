TB_shared.set('contents--nansolo--achievement--js', (function() {
	var context = undefined
		;

	
	TB_shared.set('contents--nansolo--achievement--insert_category_aftac', function(data){
		context.bbs.options.page = 1;
		context.bbs.render(context.bbs);
	});
	/*==/분류 입력하고 난뒤 /==*/
	

	var init = function(context){
		context.obj.page = 1;
		context.obj.first_page = context.obj.page;//로드한 가장 앞 페이지
		context.obj.last_page = 1;//로드한 마지막 페이지
		context.obj.list_sw = false;
		//console.log('초기화');


		get_mystory({
			page:1,
			callback : function(){
				TB_shared.get('metroResponsive')();
				evt(context);
			}
		});
	};
	/*==/초기화 ==*/
	
	
	/*===========================================
	 * 함수
	 ===========================================*/


	
	
	var evt = function(){
		var $parent = context.parent;
		//console.log('evt');
		
		$parent.find('.achievement_category_metro').find('.TB_bbs_list_wrap').find('.modify_button')
		.off('click.achievement_modify')
		.on('click.achievement_modify', function(){
			//console.log('click');
			//#!/dynamic_form?sharedObj=achievement--category_modify
		});
		/*==/수정 버튼 클릭/==*/
	};
	
	var bbs_evt = function(){
		context.bbs.parts.func('TB_bbs_list_wrap', function($el){
			
			$el.find('.modify_button').off('click.achievement_modify')
			.on('click.achievement_modify', function(){
				var item = JSON.parse($(this).attr('data-item'));
				TB_shared.get('achievement--category_modify').form[0].attr.value = item.category;
				TB_shared.get('achievement--category_modify').form[1].attr.value = item.no;
				location.href = '#!/dynamic_form?sharedObj=achievement--category_modify';
			});
			/*==/bbs 카테고리 수정/*==*/
			
		});
	};
	/*===========================================
	* 이벤트
	===========================================*/

	return {
		init : function(_context) {
			var param = _context.param||{}
				
				; 

			context = _context;
			//console.log(TB_page.locationDec());
			
			context['obj'] = {};
			context.obj.new_achievement= {};
			context.obj.new_achievement.categories= [];
			
			context.bbs = {};
			
			
			if(!context.param) { context.param = {};}

			/*===========================================
			 * 변수설정
			 ===========================================*/
			
			context.bbs_options = {
				page : param.bbs_page
				, page_num : 5
				, list_type : 'change'
				, query_get_list : 'achievement_category_list'
				, query_update_item : ''
				, query_delete_item : 'delete_achievement_category'
				, $parent : context.parent.find('.achievement_category_metro')
				, list_convert : undefined
				, $parent : context.parent
				, infinity_scroll : false
				, category : param.category||''
				, search_field : param.search_field||''
				, search_value : param.search_value||''
			};
			
			context.bbs = TB_bbs(context.bbs_options);
			
			context.bbs.render_obj.TB_page = TB_page.locationDec().map.page;//현재 주소 정보
			
			/*===========================================
			 * bbs변수
			 ===========================================*/
			
			TB_shared.set('achievement--category_modify', {
				title : 'Category'
				, req_type : 'modify' 
				, req_query : 'achievement--category_modify'
				, before : function(data, $form){
					//console.log('before');
					return true;
				}
				, after : function(result){
					//context.bbs.render(context.bbs);
					window.history.back();
				}
				, submit : '수 정'
				, form : [
					{
						tag : 'input'
						//, label : '카테고리'
						, attr :{
							type : 'text'
							, name : 'category'
							, class : 'TB_form_validate'
							, value : ''
						}
						
					}
					, {
						tag : 'input'
						//, label : 'no.'
						, attr :{
							type : 'hidden'
							, name : 'no'
							, class : 'TB_form_validate'
							, value : ''
						}
						
					}
				]
			});
			context.bbs.render_obj.category_modify = 'achievement--category_modify';
			/*===========================================
			 * 폼 변수
			 ===========================================*/

			//console.log('init');
			//console.log(context);
			TB_contents.render(context, function($topEl) {
				TB_shared.get('metroResponsive')();
				$topEl.find('.tabs').tabs();
				bbs_evt();
				context.bbs.render(context.bbs, function(){
					context.obj.new_achievement.categories= context.bbs.render_obj.list;
					//console.log(context.obj.new_achievement.categories);
					//context.obj.new_achievement.category = category.bbs
					context.parts.render([
						'new_achievement_category'
					], function(){
						context.parent.find('select').material_select();					
					});
				});
				//console.log(context.bbs);
			});
			/*===========================================
			 * 랜더링
			 ===========================================*/
		}
		, on : function(p){
			
			//대분류 페이지 변경.
			if(p.param && p.param.bbs_page){
				context.bbs.options.page= p.param.bbs_page;
				context.bbs.render(context.bbs);
			}

		}
		//문서닫을 때 함수
		, off : function(history){
			//console.log(history);
			context.bbs.evt_off(context.bbs);
			//$window.off('scroll.append_list');
		}
	};
})());
