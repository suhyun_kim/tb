TB_shared.set('contents--seayacht--local_marina--js', function(context){
	var marinas = []
			, selected_no = 0;
	context['obj'] = {};
	
	context['obj'].marina;//현재 랜더링 하고 있는 마리나
	
	marinas[0] = {
		'no' : 0
		, 'name' : '전곡마리나'
		, 'Lat' : 37.186169
		, 'Lng' : 126.6518853
	};
	
	marinas[1] = {
		'no' : 1
		, 'name' : '왕산마리나'
		, 'Lat' : 37.4575952
		, 'Lng' : 126.35887
	};
	
	if(context['param']){
		if(context['param'].selected_no){
			selected_no = context['param'].selected_no;
		}
	}
	
	context['obj'].marina = marinas[selected_no];
	
	/*===========================================
	 * 변수설정
	 ===========================================*/
	
	function map_init() {
		
		var myLatlng = new google.maps.LatLng(context['obj'].marina.Lat, context['obj'].marina.Lng);
		 
		var mapOptions = { 
		      zoom: 17
		      , center: myLatlng
		      , scrollwheel : false
		      , mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		, map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
		
		var marker = new google.maps.Marker({ 
	            position: myLatlng, 
	            map: map, 
	            title: context['obj'].marina.name
		        , animation : google.maps.Animation.DROP
		});
		
		var infowindow = new google.maps.InfoWindow();
		var infowindowLatlng = new google.maps.LatLng(context['obj'].marina.Lat, context['obj'].marina.Lng);
		infowindow.setContent('<b>'+context['obj'].marina.name+'</b>');
		infowindow.setPosition(infowindowLatlng);
		infowindow.open(map);
		
		var resizeFunc;
		
		/*
		google.maps.event.addDomListener(window, "resize", function() { //리사이즈에 따른 마커 위치
			clearTimeout(resizeFunc);
			
			resizeFunc = setTimeout(function(){
		        var center = map.getCenter();
		        google.maps.event.trigger(map, "resize");
		        map.setCenter(center); 
			}, 700);
			
	    });
	    /*===/리사이즈 이벤트 ==*/
	   
	   google.maps.event.addListenerOnce(map, 'idle', function(){
	   		//명칭 줄바꿈 안되게
			$('#contents--seayacht--local_marina').find('.map').find('.gm-style-iw').children('div').css('overflow', '')
			.children('div').css('overflow', '');
	   });
	    /*===/맵 로딩 완료 후 이벤트==*/
	}
	
	//맵을 글로벌로 넘기기 위한 변수.
	TB_shared.set('contents--seayacht--local_marina--map_init', function(){
		map_init();
	});
	/*===========================================
	 * 맵
	 ===========================================*/
	
	
	//console.log('로컬', context['TB_half_bake']);
	if(context['TB_half_bake']){//하프 베이킹
		var _context  = TB_shared.get('contents--seayacht--local_marina--context');
		$.extend(_context['obj'], context['obj']);
		
		//console.log(_context['obj']);
		_context['obj'].TB_half_bake = true;
		
		_context['parts'].render({
			'parts' : ['intro', 'greeting', 'reservation', 'address', 'traffic', 'hours', 'ships']
			, 'callback' : function(){
				TB_el.binding(_context['parent']);
				
				//console.log(_context['parent'].length);
				setTimeout(function(){
					TB_contents.load(_context['parent'].find('.reservation_loader'));
				}, 300);
				//TB_shared.get('contents--seayacht--reservation--js')(context);
			}
		});
		
		_context['parts'].func('ships');
		//TB_contents.render(_context);
		map_init();
		//context['parts'].render('bg');
		//console.log('하프 베이크');
		return false;
	}
	/*==/하프 베이킹 처리 ==*/
	
	
	TB_contents.render(context, function($topEl){
		
		TB_shared.set('contents--seayacht--local_marina--context', context);
		
		
		TB_shared.get('contents--seayacht--local_marina--context')['parts'].func('ships', function($el){
			var con = TB_shared.get('contents--seayacht--local_marina--context')
					;
			con.obj.ships = [];
			
			//return false;
			TB_ajax.get_json({
				'sendData' : {
					'reqType':'select'
					, 'reqQuery' : 'ships_thumbnail'
					, 'marinaName' : con.obj.marina.name
				}
				, 'callback' : function(res){
					for(var i = 0, len = res.body.list.length ; i < len ; i++){
						res.body.list[i].images_thumbnail_src = con.obj.TB_root_path+res.body.list[i].images_thumbnail_src.split('||')[0];
					}
					con.obj.ships = res.body.list;
					
					con['parts'].render({
						'parts' : 'ships'
						, 'run_func' : false
					});
				}
			});
		});
		/*==/요트 사진들==*/
		
		/*===========================================
		 * 파츠펑션
		 ===========================================*/
		
		TB_shared.get('contents--seayacht--local_marina--context')['parts'].func();
		
	});
	/*===========================================
	 * 랜더링
	 ===========================================*/
	
	
});