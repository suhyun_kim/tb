TB_shared.set('contents--seayacht--repair--js', function(context){
	//console.log(context);
	var sharedObjName='contents--seayacht--rapair--bbsData'
			, _group = context['_group']||context['param']['_group']
			, wvm_type = TB_shared.get('templates--wvm--wvm_1--reqType')
			, sharedObj = TB_shared.get(sharedObjName)
			, page
			, no
			, admin_only = false
			, password_validate//관리자로 로그인 했을 경우 패스워드 폼 체크 안함.
			, bbs_title  = context['param'].bbs_title||undefined
			;
			

	if(context['param']){//파라메터가 있을 경우 셋팅
		page = context['param'].page||undefined;
		no = context['param'].no||undefined
		admin_only = context['param'].admin_only||false
	}
	
	//console.log(admin_only);
	var memoryReset = function(){
		sharedObjName=null
		, _group = null
		, wvm_type = null
		, sharedObj = null
		, page = null
		, no = null
		, admin_only =null
		;
	};
	/*================================================
	 * 메모리 회수 함수.
	 ================================================*/

	if(wvm_type === 'view'+'__'+sharedObjName 
		||wvm_type === 'insert'+'__'+sharedObjName 
		||wvm_type === 'modify'+'__'+sharedObjName
		||wvm_type === 'modify'+'__'+sharedObjName
		||wvm_type === 'comment_insert'
		||wvm_type === 'comment_modify'
	){//wvm에서 view일경우 페이지 다시 로딩 안함.
		TB_shared.set('templates--wvm--wvm_1--reqType', null);
		return false;
	}
	/*==/view일 경우 로딩을 안하나 insert와 modify 는 자체적으로 로딩 되도록 되어 있다. ==*/
	
	//하프 베이킹 되어 있을 때.
	if(context['TB_half_bake']){ 
		$('#contents--seayacht--bbs').find('.bbs_title').html(context['param'].bbs_title);//타이틀
		//console.log(TB_shared.get(sharedObjName));
		var sendObj = {
			_group : context['param']['_group']
			, page : context['param']['page']
			, no : context['param']['no']
			, admin_only : admin_only
		};
		
		TB_shared.get('templates--bbs--bbs_1--group_render')(sendObj);
		memoryReset();
		return false;
	}
	
	TB_shared.set(sharedObjName, {
		'sharedObj' : sharedObjName
		, 'list_options' : {//리스트 쿼리에 사용할 것
			'reqQuery' : 'list_bbs_1'
			, '_group' : _group
			, 'page':page
			, 'no':no
			, 'use_search':false//검색 하고 난뒤 페이징시에 검색값을 기준으로 검색할 것인지 결정.
			, 'search_field':undefined//검색필드 'subject&memo' 와 같이 &을 기준으로 다중 필드 검색
			, 'search_value':undefined
			, 'delete_reqQuery' : undefined//리스트 다중 삭제 쿼리
			, 'admin_only':admin_only//글 작성, 삭제 를 관리자만 할수 있게 설정한다.TRUE or FALSE
			, 'bbs_title':bbs_title
		}
		, 'wvm_options': {//wvm에서 사용할 것.
			'_group' : _group
			, 'queries' : {
				'insert_reqQuery' : undefined
				, 'select_reqQuery' : undefined
				, 'modfiy_reqQuery' : undefined
				, 'delete_reqQuery' : undefined
				, 'comment_select_reqQuery' : undefined
				, 'comment_insert_reqQuery' : undefined
				, 'comment_moidfy_reqQuery' : undefined
				, 'comment_delete_reqQuery' : undefined
			}
			, 'add_form':[//작성자란 추가.
				{
					'tag' : 'input'
					, 'name':'작 성  자'
					, 'attr' : [
						{ 'key':'type', 'value':'text'}
						, {'key':'name', 'value':'nick'}
						, {'key':'title', 'value':'작성자명을 입력해주세요.'}
						, {'key':'class', 'value':'TB_form_validate'}
						, {
							'if' : ['view', 'modify']
							, 'if_key':'readonly', 'if_value':'readonly'
						}
						, {'key':'size', 'value':'10'}
					]
				},{
					'tag' : 'input'
					, 'name':'패스워드'
					, 'attr' : [
						{ 'key':'type', 'value':'password'}
						, {'key':'name', 'value':'password'}
						, {'key':'title', 'value':'패스워드를 입력해주세요.'}
						, {'key':'class', 'value':'TB_form_validate password'}
						, {'key':'size', 'value':'10'}
					]
				}
			]
			, 'none_notice' : 'true'
			, 'use_delete' : true//메인글 삭제 기능 활성화
			, 'use_modify_change' : true//view모드에서 modify로 변경 가능한 버튼
			, 'use_comment':true//답글 기능을 사용할지 결정 여부.
			, 'use_nick':false//작성자 사용.
			, 'use_view_count':true//view시에 카운트를 올리는 기능 사용.
			, 'use_file': true
		}
	});
	/*===================================
	 * 리스트에서 사용될 객체 정의
	 ===================================*/
	
	TB_auth.isAdmin(function(isAdmin){
		password_validate = (isAdmin)?'':'TB_form_validate '
		TB_shared.get(sharedObjName).wvm_options.add_form[1].attr[3].value = password_validate+' password';
		//console.log(isAdmin);
	});
	/*===================================
	 * sharedObj에 정의
	 ===================================*/
	
	TB_contents.render(context, function(){
		TB_shared.get('metroResponsive')();
	});
	
	
});
