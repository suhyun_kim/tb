TB_shared.set('contents--seayacht--tour_info--js', function(context){
	
	if(context['TB_half_bake']){return false;}
	/*===========================================
	 * 하프베이킹 되어 있으면 실행 안함.
	 ===========================================*/
	
	context['obj'] = {};
	
	context['obj'].tour_list = [];
	context['obj'].tour_list[0] = { 'no' : 0, 'name' : '낚시투어'};
	context['obj'].tour_list[1] = { 'no' : 1 , 'name' : '일몰투어'};
	context['obj'].tour_list[2] = { 'no' : 2 , 'name' : '일출투어'};
	context['obj'].tour_list[3] = { 'no' : 3 , 'name' : '아일랜드투어'};
	context['obj'].tour_list[4] = { 'no' : 4 , 'name' : '장거리 세일링'};
	context['obj'].tour_list[5] = { 'no' : 5 , 'name' : '기념일 이벤트'};
	context['obj'].tour_list[6] = { 'no' : 6 , 'name' : '웨딩/화보 촬영'};
	context['obj'].tour_list[7] = { 'no' : 7 , 'name' : '프로포즈'};
	
	context['obj'].selected_tab = 0;//현재 선택되어져 있는 탭
	/*===========================================
	 * 변수 설정
	 ===========================================*/
	
	
	context['parts'].func('tabs', function($el){
		
		$el.find('.tab').off('click.tab')
		.on('click.tab', function(){
			var $this = $(this)
					, no = $this.attr('data-tab')
					;
			context['obj'].selected_tab = no;
			
			context['parts'].render({
				'parts' : ['pc_tab', 'text', 'bg', 'tabs']
				, obj : context['obj']
			});				
		});
		
		//모바일 메뉴 토글
		$el.find('.toggle_icon').off('click.toggle')
		.on('click.toggle', function(){
			var $this  = $(this);
			if($this.hasClass('fa-angle-down')){//
				$this.removeClass('fa-angle-down').addClass('fa-angle-up')
				.closest('.tab_wrap').find('.tab_container').slideDown();
			}else{
				$this.removeClass('fa-angle-up').addClass('fa-angle-down')
				.closest('.tab_wrap').find('.tab_container').hide();
			}
		});
	});
	/*==tabs ==*/
	
	context['parts'].func('bg',function($el){
		var $bg = $el.find('img');
		
		$bg.load(function(){
			TB_ani.ready($bg,function(){
				TB_style.binding(context['parent']);
				TB_ani.flow('fadeIn', $bg);
			});
		});
		
		
	});
	/*==/이미지 ==*/
	
	/*==모바일 슬라이드 토글==*/
	
	/*===============================================
	 * 파츠 펑션
	 ===============================================*/
	
	
	TB_contents.render(context, function($topEl){
		TB_shared.get('metroResponsive')();
		TB_style.binding($topEl);
		context['parts'].func();
	});
	/*===========================================
	 * 랜더링
	 ===========================================*/
	
	
});