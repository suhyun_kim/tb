TB_shared.set('contents--seayacht--sale--js', function(context){
	//console.log(context);
	var sharedObjName='contents--seayacht--sale--sendData'
			, _group = 'sale'
			;
	
	TB_shared.set(sharedObjName, {
		'sharedObj' : sharedObjName
		, 'list_options' : {//리스트 쿼리에 사용할 것
			'reqQuery' : 'get_sale_list'
			, 'list_num' : 5
			, '_group' : _group
			, 'delete_reqQuery' : undefined
		}
		, 'wvm_options': {//wvm에서 사용할 것.
			'none_hidden':'true'
			, 'none_notice':'true'
			, 'use_file': false//파일 사용 유무
			, 'use_memo_in_image' : true //메모 내에 이미지 삽입이 가능하게
			, '_group' : _group
			, 'queries' : {
				'insert_reqQuery' : undefined
				, 'select_reqQuery' : undefined
				, 'modfiy_reqQuery' : undefined
			}
		}
	});
	/*===================================
	 * 리스트에서 사용될 객체 정의
	 ===================================*/
	
	//console.log(TB_shared.get(sharedObjName));
	//console.log(context);
	//if(context['TB_half_bake']){return false;}
	
	TB_contents.render(context);
	
});
