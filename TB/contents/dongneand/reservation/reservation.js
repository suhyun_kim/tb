TB_shared.set('contents--seayacht--reservation--js', function(context){
	
	if(!context['param'].selected_no||!context['param']){throw "not defined selected_no";}
	
	
	var queries = {}//쿼리를 담을 객체
			, d = new Date()
			, today = {}//오늘날짜 정보를 저장할 객체
			, callendar_day = {}//달력 출력 날짜
			;
	
	today.year = d.getFullYear(),
	today.month = d.getMonth()+1,
	today.date = d.getDate(),
	today.day = d.getDay()
	;
	/*==/ 오늘 기준 날짜 ==*/
	
	context['obj'] = {};
	
	context['obj'].render_obj = {
		'cur_year' : today.year
		, 'cur_month' : today.month
		, 'cur_date' : today.date
		, 'cur_day' : today.day
		, 'year' : today.year
		, 'month' : today.month
		, 'date' : today.date
		, 'day' : today.day
		, 'start_time' : undefined
		, 'end_time' : undefined
		, 'lastMonth' : (today.month == 1)?12:today.month-1
		, 'nextMonth' : (today.month < 12)?today.month+1:1
		, 'status_list' : []//달력의 예약완료를 표시할 배열
		, 'reservation_status' : 0 //예약 완료 스위치
		, 'time_selector_list' : []//완료 시간 선택 리스트
		, 'marina_no' : context['param'].selected_no
		, 'marina_name' : undefined
		, 'reservation_list_page':1
		, 'selected_status' : undefined
	};
	
	//마리나 리스트
	context['obj'].render_obj.marinas = [];//마리나 정보를 담을 배열
	context['obj'].render_obj.marinas = [
		{
			no : 0, marina_name : '전곡마리나항'
			, sido : '화성' , selected : 'selected'
		},{
			no : 1, marina_name : '왕산마리나'
			, sido : '인천' 
		}
	];
	
	//마리나명
	context['obj'].render_obj.marina_name = context['obj'].render_obj.marinas[context['obj'].render_obj.marina_no].marina_name;
	
	//예약 리스트
	context['obj'].render_obj.reservation_list = {};
	
	//예약 상태
	context['obj'].render_obj.reservation_status = [
		{'reservation_status' : '입금대기'}
		, {'reservation_status' : '입금완료'}
		, {'reservation_status' : '예약취소'}
		, {'reservation_status' : '투어완료'}
	];
	
	context['obj'].render_obj.time_selector_init = [
		{ start_time : 8, end_time:9, selected : false, full : false }
		, { start_time : 9, end_time:10, selected : false, full : false }
		, { start_time : 10, end_time: 11, selected : false, full : false }
		, { start_time : 11, end_time: 12, selected : false, full : false }
		, { start_time : 12, end_time: 1, selected : false, full : false }
		, { start_time : 1, end_time: 2, selected : false, full : false }
		, { start_time : 2, end_time: 3, selected : false, full : false }
		, { start_time : 3, end_time: 4, selected : false, full : false }
		, { start_time : 4, end_time: 5, selected : false, full : false }
		, { start_time : 5, end_time: 6, selected : false, full : false }
	];
	
	context['obj'].render_obj.time_selector = TB_func.objCopy(context['obj'].render_obj.time_selector_init);
	
	context['obj'].render_obj.form = {};
	context['obj'].render_obj.form.reqType = 'insert';
	context['obj'].render_obj.form.insertQuery = 'insert_reservation';
	context['obj'].render_obj.form.modifyQuery = 'modity_reservation';
	context['obj'].render_obj.time_complete_query = 'toggle_time_selector';//
	/*==/ 랜더링용 변수 ==*/
	
	context['obj'].queries = {
		'init' : 'init_reservation'
		, 'get_time_select' : 'get_time_select'//시간 선택 정보 가져오기
		, 'get_reservation_list' : 'get_reservation_list'//예약 정보 가져오기.
		, 'del_reservation_item' : 'del_reservation_item'//예약 정보 삭제
		, 'modify_status_change' : 'modify_status_change'//예약 상태 수정
	};
	/*==/ 쿼리들 ==*/
	
	context['obj'] = $.extend(context['obj'], context['param']);
	//console.log(context['obj']);
	/*======================================================
	 * 변수 설정
	 ======================================================*/
	
	var getTotalDate = function(year, month){
		if(month==4 || month==6 || month==9 || month==11){
			return 30;
		}else if(month==2){
			if(year%4 == 0){//윤년 2월일 경우.
				return 29;
			}else{
				return 28;
			}
		}else{
			return 31;
		}
	};
	/*============================================
	 * 월별 전체 날쨔 구하기
	 ============================================*/
	
	var printDate = function(){
		var year = context['obj'].render_obj.year
				, month  = context['obj'].render_obj.month;
		var selectDate = new Date(year, month-1,1);//입력된 월의 시작일을 알기 위한 값
		var _date=1;//날짜 테이블에 새겨질 날짜.
		var startDay = selectDate.getDay();//월의 시작 요일을 반환
		var totalDate = getTotalDate(year,month);
		var objHTML = '';//.name 클래스에 들어갈 내용
		var $dateBoxEl= context['parent'].find('.date_container').find('.date_box')
				, addClass = ''
				;
		
		$dateBoxEl
		.each(function(index){
			var $this = $(this);
			if(index >= startDay){//날짜의 시작일
				if(index <= totalDate+(startDay-1)){
					//$this.find('.num').html(_date);//달력에 표시될 날짜
					if(today.year == year && today.month == month){//이번달
						if(_date == today.date){//오늘
							addClass = 'bgc_sel selected';
						}else if(_date < today.date){//이전날들
							addClass = 'before_box';
						}else{//다음날들
							addClass = 'next_box';
						}
					}else if(today.year > year||(today.year >= year && today.month > month)){//이전날들
						addClass = 'before_box';
					}else if(today.year < year || (today.year <= year && today.month < month )){//다음 날들
						addClass = 'next_box';
					}
					$this
					.attr('class', 'date_box '+addClass+' date_box_'+_date)
					.attr('data-date', _date)
					.find('.num').html(_date);
					_date++;
				}else{
					$this.attr('class', 'date_box')
					.find('.num').html('').end()
					.find('.text').html('');
				}
			}else{//날짜가 안들어가는 칸
				$this.attr('class', 'date_box')
				.find('.num').html('').end()
				.find('.text').html('');
			}
			$this = null;
		});
	};
	/*============================================
	 * 달력날짜 출력
	 ============================================*/
	
	/*
	var chaMonthControler = function(month){
		if(month >=12){
			$el.find('.lastMonth').html(month-1+'월');		
			$el.find('.nextMonth').html(1+'월');		
		}else if(month <=1){
			$el.find('.lastMonth').html(12+'월');		
			$el.find('.nextMonth').html(month+1+'월');		
		}else{
			$el.find('.lastMonth').html(month-1+'월');		
			$el.find('.nextMonth').html(month+1+'월');		
		}
	};
	/*============================================
	 * 날짜 컨트롤러 월 변경
	 ============================================*/
	
	var changeContext = function(month, direction){
		context['obj'].render_obj.month = month;
		if(direction === 'last'){//이전달 이동 
			if(month ==12){//전해년도로 이동
				context['obj'].render_obj.lastMonth = month-1;
				context['obj'].render_obj.nextMonth = 1;
				context['obj'].render_obj.year--;
			}
			else{
				(month==1)?context['obj'].render_obj.lastMonth = 12:context['obj'].render_obj.lastMonth--;
				context['obj'].render_obj.nextMonth = month+1;
			}
		}else if(direction === 'next'){//다음달 이동
			if(month ==1){//다음년도로 이동
				context['obj'].render_obj.lastMonth = 12;
				context['obj'].render_obj.nextMonth = 2;
				context['obj'].render_obj.year++;
			}
			else{
				(month==12)?context['obj'].render_obj.nextMonth  = 1:context['obj'].render_obj.nextMonth++;
				context['obj'].render_obj.lastMonth = month -1;
			}
		}
		printDate();//달력 출력
	};
	/*============================================
	 * 날짜 변경시 context['obj']수정
	 ============================================*/
	
	var view_status_list = function(){
		var list = context['obj'].render_obj.status_list
				, len = list.length
				, $date_container = context['parent'].find('.date_container')
				, $date_box = $date_container.find('.date_box')
				, $num = $date_box.find('.num')
				, marina = context['obj'].render_obj.marinas[context['param'].selected_no];
				;
				
		//console.log(list);
		$date_container.find('.full').each(function(){$(this).removeClass('full');});
		
		if(!list.length){//해당하는 월의 리스트가 없으면 전체 삭제
			context['obj'].render_obj.reservation_status = 0;
			context['parts'].render('reservation_status');
			return false;
		}
		
		for (var i = 0 ; i < len ; i++){
			//달력에 빨간색 완료 표시
			if(list[i].year == context['obj'].render_obj.year 
				&&list[i].month ==  context['obj'].render_obj.month
				&&list[i].marina_no ==  marina.no
			){
				$date_container.find('.date_box_'+list[i].date).addClass('full');
			}
			//현재 날짜와 같으면 스테이터스 변경
			if(context['obj'].render_obj.year == list[i].year
				&& context['obj'].render_obj.month == list[i].month
				&& context['obj'].render_obj.date == list[i].date
				&&list[i].marina_no ==  marina.no
			){
				context['obj'].render_obj.reservation_status = 1;
				context['parts'].render('reservation_status');
			}
			
		};
		
		list = null, len = null;
	};
	/*=================================
	 * 달력에 스테이 터스 표시
	 =================================*/
	
	var selected_marina = function(no){
		var marinas = context['obj'].render_obj.marinas
				;
		
		for(var i = 0, len = marinas.length ; i < len ; i++){
			if(no){//마리나 변경
				if(no == marinas[i].no){
					marinas[i].selected = 'selected';
				}else if(marinas[i].selected && no != marinas[i].no){
					delete marinas[i].selected;
				}
			}else if(!no &&marinas[i].selected ){
				return marinas[i];
				break;
			}
		}
		marinas = null;
	};
	/*==/현재 선택되어진 마리나 정보 가져오기 ==*/
	
	var time_selector_toggle = function(type, start_time, end_time){
		var start_time_type = $.type(start_time)
				, selectors = context['obj'].render_obj.time_selector
				, selectors_len =selectors.length
				;
		//console.log(type,start_time_type );
		//console.log(start_time);
		if(type === 'selected' ){
			//추가
			if(start_time_type === 'array'){
				for(var i = 0; i < selectors_len ; i++){
					for(var j = 0, len_2 = start_time.length ; j <len_2 ; j++){
						if(selectors[i].start_time == start_time[j].start_time){
							selectors[i].selected = true;
						}
					}
				}
			}
			//삭제
			else if(start_time_type === 'string'){
				for(var i = 0;  i< selectors_len ; i ++){
					if(selectors[i].start_time == start_time){selectors[i].selected = false;}
				}
			}
		}
		//full
		else if(type === 'full'){
			if(start_time_type === 'array'){//배열일 경우는 재배열
				context['obj'].render_obj.time_selector = TB_func.objCopy(context['obj'].render_obj.time_selector_init);//초기화
				for(var j = 0, _len  = start_time.length ; j < _len ; j++){
					for(var i = 0, len = context['obj'].render_obj.time_selector.length ; i < len ; i++){
						if(context['obj'].render_obj.time_selector[i].start_time == start_time[j].start_time){
							context['obj'].render_obj.time_selector[i].full = (context['obj'].render_obj.time_selector[i].full === true)?false:true;
							break;
						}
					}
				}
			}
			else if(start_time_type === 'string'){//string 일 경우 처리
				for(var i = 0, len = context['obj'].render_obj.time_selector.length ; i < len ; i++){
					if(context['obj'].render_obj.time_selector[i].start_time == start_time){
						context['obj'].render_obj.time_selector[i].full = (context['obj'].render_obj.time_selector[i].full === true)?false:true;
						break;
					}
				}
			}
		}
		context['parts'].render('time_selector');
		start_time_type = null, selectors = null, selectors_len = null;
	};
	/*==/full, selected toggle==*/
	
	var init_date_selector_form_date = function(){
		context['obj'].render_obj.time_selector = TB_func.objCopy(context['obj'].render_obj.time_selector_init);
		context['obj'].render_obj.start_time = undefined;
		context['obj'].render_obj.end_time = undefined;
		context['parts'].render('reservation_form_date');
	}
	/*==/날짜선택과 폼의 날짜 초기화 하기==*/
	
	/*=================================
	 * 함수
	 =================================*/
	
	var change_reservation_status = function(status){
		var marina = context['obj'].render_obj.marinas[context['param'].selected_no];
		TB_ajax.get_json({
			'sendData' :{
				'year' : context['obj'].render_obj.year
				, 'month' : context['obj'].render_obj.month
				, 'date' : context['obj'].render_obj.date
				, 'marina_no' : marina.no
				, 'marina_name' : marina.marina_name
				, 'status' : context['obj'].render_obj.reservation_status
				, 'reqType' : 'select'
				, 'reqQuery' : context['obj'].queries.reservation_status.change
			}
			,'callback' : function(res){
				//console.log(res);
				context['obj'].render_obj.status_list = res.body.status_list;
				view_status_list();
				marina = null;
			}
		});
	};
	/*==/예약 상태 입력, 삭제==*/
	
	var get_time_select = function(callback){
		var marina = context['obj'].render_obj.marinas[context['param'].selected_no];
		TB_ajax.get_json({
			'sendData' :{
				'year' : context['obj'].render_obj.year
				, 'month' : context['obj'].render_obj.month
				, 'date' : context['obj'].render_obj.date
				, 'marina_no' : marina.no
				, 'reqType' : 'select'
				, 'reqQuery' : context['obj'].queries.get_time_select
			}
			,'callback' : function(res){
				time_selector_toggle('full', res.body.time_selector_list);
				//context['obj'].render_obj.status_list = res.body.status_list;
				//view_status_list();
				marina = null;
			}
		});
	};
	/*==/예약 상태 입력, 삭제==*/
	
	var get_reservation_list = function(){
		TB_ajax.get_json({
			'sendData' : {
				'reqType' : 'select'
				, 'reqQuery' : context['obj'].queries.get_reservation_list
				, 'marina_no' : context['obj'].render_obj.marina_no
				, 'page' : context['obj'].render_obj.reservation_list_page
				, 'status' : context['obj'].render_obj.selected_status
			}
			, 'callback' : function(res){
				// console.log(res.body);
				context['obj'].render_obj.reservation_list = res.body;
				context['parts'].render('reservation_list');
			}
		});
	};
	/*==/예약자 리스트==*/
	
	var del_reservation_item = function(no){
		if(!no){throw "not defined no";}
		TB_ajax.get_json({
			'sendData' : {
				'reqType' : 'delete'
				, 'reqQuery' : context['obj'].queries.del_reservation_item
				, 'no' : no
			}
			, callback : function(res){
				get_reservation_list();
			}
		});
	};
	/*==/예약자 삭제==*/
	
	var modify_status_change = function(no, status){
		TB_ajax.get_json({
			'sendData' : {
				'reqType' : 'modify'
				, 'reqQuery' : context['obj'].queries.modify_status_change
				, 'no' : no
				, 'status' : status
			}, callback : function(){
				get_reservation_list();
			}
		});
		
	};
	/*==/예약자 상태 수정==*/
	
	var get_init = function(callback){
		//console.log(context['param'].selected_no);
		var marina = context['obj'].render_obj.marinas[context['param'].selected_no];
		//console.log(marina);
		TB_ajax.get_json({
			'sendData' : {
				'year' : context['obj'].render_obj.year
				, 'month' : context['obj'].render_obj.month
				, 'date' : context['obj'].render_obj.date
				, 'marina_no' : marina.no
				, 'reqType' : 'select'
				, 'reqQuery' : context['obj'].queries.init
			}
			, 'callback' : function(res){
				
				//예약 정보 달력에 표시
				context['obj'].render_obj.status_list = res.body.status_list;
				
				time_selector_toggle('full', res.body.time_selector_list);
				
				view_status_list();
				
				get_reservation_list();//마리나 예약 리스트
				
				if(res.body.queries){//쿼리 
					$.extend(context['obj'].queries, res.body.queries);
				}
				if($.type(callback) === 'function'){callback(res);}
				marina= null;
			}
		});
	}
	/*==/초기정보 가져오기.==*/
	
	
	/*=================================
	 * DB 데이터 
	 =================================*/
	
	var eBinding = function(){
		context['parent']
		.find('.monthControler').find('.lastMonth, .nextMonth')
		.off('click.monthMove').on('click.monthMove', function(){
			var $this = $(this)
					, month = parseInt($this.attr('data-month'), 10)
					, direction = ''
					;
			direction = ($this.hasClass('lastMonth'))? 'last':'next'; 
			changeContext(month, direction);
			context['parts'].render({
				'parts' : 'month_controller'
				, 'obj' : context['obj']
				, 'callback' : function(){
					//console.log('당근');
					eBinding();
					// change_reservation_status();
					//시간선택 초기화
					init_date_selector_form_date();
					
					//데이터 초기화
					get_init();
				}
			});
		});
		/*==/달력 월별이동==*/
		
		context['parent']
		.find('.date_container').find('.date_box')
		.off('click.date_box').on('click.date_box', function(){
			var $this = $(this)
					, date = parseInt($this.find('.num').html(), 10);
			context['obj'].render_obj.date = date;
			context['obj'].render_obj.reservation_status = ($this.hasClass('full'))?1:0;
			context['obj'].render_obj.form.reqType = 'insert';//폼의 타입을 insert로 변경
			context['parts'].render(['reservation_status', 'reservation_form_date', 'form_hidden_var']);
			if(!context['obj'].TB_isAdmin){//일반인클릭
				if($this.hasClass('before_box')){//지난날 클릭 박스
					return false;
				}
				if($this.hasClass('full') ){//예약 완료 박스
					TB_func.popupDialog('죄송 합니다. <br/>이미 예약이 완료 되었습니다. <br/>다른 날짜를 선택해주십시요');
					return false;
				}
			}
			
			//날짜선태과, 폼 의 시간 초기환
			init_date_selector_form_date();
			
			context['parent'].find('.date_container').find('.selected').removeClass('selected');
			$this.addClass('selected');
			get_time_select();
		});
		/*==/달력 날짜 클릭 ==*/
		
	};
	
	context['parts'].func('reservation_status', function($el){
		$el.find('.button_cover')
		.off('click.reservation_status').on('click.reservation_status', function(){
			var $this = $(this)
					, status = parseInt($this.attr('data-status'), 10)
					;
			if(context['obj'].render_obj.reservation_status == status){return false;}
			// console.log(status);
			if(status == 0){//예약 가능 상태로 전환했을 경우 지우기
				context['parent'].find('.date_box_'+context['obj'].render_obj.date).removeClass('full');
			}
			context['obj'].render_obj.reservation_status = status;
			context['parts'].render('reservation_status');
			change_reservation_status();
			$this = null, status = null;
		});
	});
	/*==/admin : 달력 예약 완료 변경==*/
	
	context['parts'].func('time_selector', function($el){
		
		$el
		.find('.time_cover').on('click.time_complete', function(){
			var selected = []
					, $this = $(this)
					, $item = $this.closest('.item')
					, $selected = $el.find('.selected')
					, cur_index = parseInt($item.attr('data-index'), 10)
					, hasSelected = $item.hasClass('selected')
					, errText = '예약이 완료 되었습니다.<br/> 다른 시간을 선택해 주십시요.'
					;
				
			//현재 예약 상태 full
			if(context['obj'].render_obj.reservation_status ==1||$item.hasClass('full')){
				TB_func.popupDialog(errText);
				return false;
			}
			
			//아무것도 없을 때
			if(!$selected.length){
				selected.push({
					'index' : cur_index
					, 'start_time' : $item.attr('data-start_time')
					, 'end_time' : $item.attr('data-end_time')
				});
			}
			//선택된것을 해제.
			else if(hasSelected){
				selected = $item.attr('data-start_time');
			}
			//추가 선택
			else{
				$selected.each(function(){
					var $this = $(this);
					selected.push({
						'index' : parseInt($this.attr('data-index'), 10)
						, 'start_time' : $this.attr('data-start_time')
						, 'end_time' : $this.attr('data-end_time')
					})
				});
				//console.log(selected);
				if(selected[0].index-1 == cur_index ||selected[selected.length-1].index+1 ==cur_index){
					//console.log('앞이나 전');
					selected.push({
						'index' : cur_index
						, 'start_time' : $item.attr('data-start_time')
						, 'end_time' : $item.attr('data-end_time')
					});
				}else{
					TB_func.popupDialog('연속된 시간을<br/> 선택해 주셔야 합니다');
					return false;
				}
			}
			//시간선택 변경
			time_selector_toggle('selected', selected);
			
			//폼에 시간 입력
			$selected = $el.find('.selected');
			context['obj'].render_obj.start_time = $selected.first().attr('data-start_time');
			context['obj'].render_obj.end_time = $selected.last().attr('data-end_time');
			context['parts'].render('reservation_form_date');
			
		});
	})
	/*==/시간 선택, 시간 예약 완료.==*/
	
	context['parts'].func('reservation_form', function($el){
		
		
		//폼 reqType 변경
		$el.find('.type_changer').off('click.type_change')
		.on('click.type_change', function(){
			context['obj'].render_obj.form.reqType = $(this).attr('data-type');
			context['parts'].render('reservation_form');
		});
		
		//요트 설명 토글
		$el.find('.yachtBoatDescContainer').find('.icon_cover')
		.off('click.decTab')
		.on('click.decTab', function(){
			var $this = $(this)
					, $detail = $this.parent().find('.detail_wrap')
					, $fa = $this.find('.fa')
					, removeClass, addClass
					;
			if($fa.hasClass('fa-commenting')){//보이기
				removeClass = 'fa-commenting', addClass='fa-commenting-o';
				$detail.hide();
			}else{
				removeClass = 'fa-commenting-o', addClass='fa-commenting';
				$detail.show();
			}
			$fa.removeClass(removeClass).addClass(addClass);
			$this= null, $detail = null, $fa = null, removeClass = null, addClass = null;
		});
		
		
	});
	/*==/예약 폼==*/
	
	
	context['parts'].func('reservation_list', function($el){
		
		//예약 삭제
		$el.find('.remove_btn').off('click.reservation_item_remove')
		.on('click.reservation_item_remove', function(){
			del_reservation_item($(this).closest('.list_item').attr('data-no'));
		});
		
		//페이지 이동
		$el.find('.page_link').off('clck.reservation_list_page_move')
		.on('click.reservation_list_page_move', function(){
			context['obj'].render_obj.reservation_list_page = $(this).attr('data-page');
			get_reservation_list();
		});
		
		//스테이터스 체인지
		$el.find('.status').off('change.status_change')
		.on('change.status_change', function(){
			var $this = $(this);
			modify_status_change($this.closest('.list_item').attr('data-no'), $this.val());
			$this = null;
		});
		
		//수정 모드
		$el.find('.list_item').off('click.modify_reservation')
		.on('click.modify_reservation', function(){
			context['obj'].render_obj.form.reqType = 'modify';
			context['parts'].render('reservation_form');
			var item = context['obj'].render_obj.reservation_list.list[TB_func.json_inArray(context['obj'].render_obj.reservation_list.list, 'no', $(this).attr('data-no'))];
			TB_form.autoInsert(context['parent'].find('.reservation_form').find('.form_wrap'), item);
			item = null;
		});
		
		//스테이터스별 검색
		$el.find('.status_btn').off('click.selected_status')
		.on('click.selected_status', function(){
			context['obj'].render_obj.selected_status = $(this).attr('data-status');
			get_reservation_list();
		});
		
		//블럭 이동
		$el.find('.block_move').off('click.block_move')
		.on('click.block_move', function(){
			context['obj'].render_obj.reservation_list_page = $(this).attr('data-page');
			get_reservation_list();
		});
		
		
	});
	/*==/리스트 랜더링시 ==*/
	
	/*=================================
	 * 이벤트 바인딩
	 =================================*/
	
	TB_shared.set('contents--seayacht--reservation--time_selector_complete_before', function(data,$form){
		time_selector_toggle('full', data['5'].value);
		return true;
	});
	/*==/ 시간 완료 선택 ==*/
	
	TB_shared.set('contents--seayacht--reservation--time_selector_complete_aft', function(form){
	});
	/*==/ 시간 완료 선택 전송 후 ==*/
	
	TB_shared.set('contents--seayacht--reservation--reservation_before', function(form){
		//console.log(form);
		if(context['obj'].render_obj.reservation_status === 1){
			TB_func.popupDialog('죄송 합니다. <br/>이미 예약이 완료 되었습니다. <br/>다른 날짜를 선택해주십시요');
			return false;
		}
		return true;
	});
	/*==/폼 전송 전 예약이 꽉 찼는지 검사==*/
	
	TB_shared.set('contents--seayacht--reservation--reservation_aft', function(res){
		//console.log(res);
		get_reservation_list();
		//TB_func.popupDialog('예약이 정상 완료 되었습니다. <br/> 빠른 시일내로 연락 드리겠습니다.');
		var $complete_container = context['parent'].find('.reservation_form').find('.complete_container');
		TB_ani.ready($complete_container, function(){
			TB_ani.flow('scaleFull',$complete_container);
		});
	})
	/*==/폼 전송 후==*/
	
	/*=================================
	 * form 
	 =================================*/
	//console.log('예약', context['param']);
	
	TB_contents.render(context, function(){
		var $topEl = $('#contents--seayacht--local_marina')
				, $fullContainer = $topEl.find('.metro_fullContainer');
		printDate();
		get_init();
		
		context['parts'].func();
		eBinding();
		
		//베이킹 되어 있을 때 처리.
		if(TB_shared.get('contents--seayacht--local_marina--context')){
			if(TB_shared.get('contents--seayacht--local_marina--context')['obj'].TB_half_bake){
				$fullContainer = context['parent'].find('.metro_fullContainer');
				TB_shared.get('metroResponsive')(context['parent']);
				
				//첫 번째 이미지 정렬
				$topEl.find('.intro').find('img').load(function(){
					TB_func.fitAlign($topEl.find('.intro'), $topEl.find('.intro').find('.bg_img'));
				});
				
				TB_ani.flow('fadeIn', $fullContainer);
				
				return false;
			}
		}
		
		TB_ani.ready($fullContainer.find('img'), function(){
			TB_shared.get('metroResponsive')($topEl);
			
			TB_ani.flow('fadeIn', $fullContainer, function(){
				$fullContainer.find('img').removeClass('ST_opacity_0').show();
				
				//맵실행
				TB_shared.get('contents--seayacht--local_marina--map_init')();
				
				//첫 번째 이미지 정렬
				TB_func.fitAlign($topEl.find('.intro'), $topEl.find('.intro').find('.bg_img'));
				
			});
		});
	});
	//console.log(today);
	/*====================================================================
	 * 실행
	 ====================================================================*/

});


















