TB_shared.set('contents--seayacht--yacht_info--js', function(context){
	var sharedObjName='contents--seayacht--yacht_info--sendData'
			, _group = 'yacht_info'
			;
	
	
	TB_shared.set(sharedObjName, {
		'sharedObj' : sharedObjName
		, 'category_title':'요트·보트 안내'
		, 'categories' :[
			{'page' : 'yacht_introduce', 'name':'전곡마리나(화성)'}
			, {'page' : 'yacht_introduce','name':'왕산마리나(인천)'}
		]
		, 'list_options' : {//리스트 쿼리에 사용할 것
			'reqQuery' : 'get_yacht_info_list'
			, 'category' : '전곡마리나(화성)'
			, 'list_num' : 5
			, '_group' : _group
			, 'delete_reqQuery' : undefined//삭제 쿼리
		}
		, 'wvm_options': {//wvm에서 사용할 것.
			'none_hidden':'true'
			, 'none_notice':'true'
			, 'use_file': false//사용 유무
			, 'use_memo_in_image' : true //메모 내에 이미지 삽입이 가능하게
			, '_group' : _group
			, 'queries' : {
				'insert_reqQuery' : undefined
				, 'select_reqQuery' : undefined
				, 'modfiy_reqQuery' : undefined
			}
		}
	});
	/*===================================
	 * 리스트에서 사용될 객체 정의
	 ===================================*/
	
	//console.log(TB_shared.get(sharedObjName));
	//console.log(context);
	//if(context['TB_half_bake']){return false;}
	
	TB_contents.render(context);
	
});
