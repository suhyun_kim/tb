
TB_shared.set('contents--dreamyacht--sale--js', (function(){
	var context
		, show = {}
		, ani = {}
		;
	
	//show.ease = Power3.easeNone;
	show.ani_time = 0.3;
	show.delay = 0.2;
	show.callback = function(el){};
	
	
	
	/*==================================================
	 * 변수 설정
	 ==================================================*/
	
	var active_tab = function(page){
		var tab = '';
		if(!page){return false;}
		if(page === 'sale_yacht'){
			tab = 'tab_1';
		}else if(page === 'sale_boat'){
			tab = 'tab_2';
		}
		if(tab){
			context.parent.find('.r_2').find('.'+tab).find('a').trigger('click');
		}
	};
	
	var scroll_spy = function(param){
		var $scroll_spy = context.parent.find('.scroll_spy_buttons')
			, id = param&&param.scroll_spy 
			;
		
		//console.log(id);
		if(id){
			//$scroll_spy.find('.trigger_'+id).trigger('click');
			$scroll_spy.find('.trigger_'+id).trigger('click');
		}
	};

	
	var album= function(context, param){
		var context = context
			, param  = param||{}
			, options = {}
			, category = ''
			;
			
		if(param){
			if(param.scroll_spy === 'sale_boat'){category = '보트';}
			else if(param.scroll_spy === 'sale_yacht'){category = '요트';}
		}
		
		//console.log(category);
		
		options = {
			page : param.page||1
			, page_num : 5
			, list_type : 'change'
			, query_get_list : 'yacht_sale'
			, query_update_item : ''
			, query_modify_item : 'yacht_sale'
			, query_delete_item : 'yacht_sale'
			, list_convert : undefined
			, $parent : context.parent//★
			, infinity_scroll : false
			, category : category||''
			, search_field : param.search_field||''
			, search_value : param.search_value||''
		};
		
		//console.log(options);
		
		context.album = TB_bbs(options);//앨범 생성
		context.album.render_obj.TB_page = TB_page.locationDec().map.page;//현재 주소 정보
		context.album.render_obj.TB_root_path = TB_config.get('TB_root_path');//상단패스
		context.album.render_obj.TB_userInfo = TB_config.get('user');//유저정보
		context.album.render_obj.query_view_item = 'yacht_sale_item';
		context.album.render_obj.query_modify_item = 'yacht_sale';
		//console.log(context.album.render_obj);
		
		context.album.render(context.album, function(){
			//console.log('앨범 랜더 완료.');
			//console.log('sale', context.album.render_obj);
		});

	};
	/*==/앨범/==*/
	
	
	var init = function(_context){
		var param
			, page = TB_page.locationDec().map.page
			;
		context = $.extend(context, _context);
		
		param = _context.param||{}		
		
		context.obj = {};
		context.obj.query_insert_query = 'yacht_sale';
		context.obj.query_modify = 'yacht_sale';
		
		context.album = {};
		
		album(context, param);
				
		TB_contents.render(context, function($top){
			ani = new TB_ani.show($top, show);
			ani.init();
			
			$top.find('.tabs').tabs();
			context.parent.find('.scroll_spy').scrollSpy({scrollOffset:45});
			scroll_spy(context.param);
			evt(context.parent);
			
			active_tab(page);
		});
	};
	/*==/초기화/==*/
	
	var on =function(sendData){
		var param = sendData.param||{}
			, page = (param.page)?parseInt(param.page):param.page
			, category = param.category
			;
			
		//console.log('sale on');
		if(param){
			if(param.scroll_spy === 'sale_boat'){
				category = '보트';
			}else if(param.scroll_spy === 'sale_yacht'){
				category = '요트';
			}
			
		}
		
		ani.evt();
		scroll_spy(param);
		if(page || category){
			if(category === 'all'){
				category = '';
			}
			context.album.options.page = page||1;
			context.album.options.category = category||'';
			context.album.render_obj.category = category||'';
			context.album.render(context.album, function(){
				
				active_tab(TB_page.locationDec().map.page);
			});
		}

	};
	/*==/재방문/==*/
	
	
	
	/*==================================================
	 * 함수
	 ==================================================*/
	
	
	var evt = function($top){
		var $top = $top
			, $r_2 = $top.find('.r_2')
			;
		
		$r_2.find('.tab').off('click.tab_href')
		.on('click.tab_href', function(){
			var $this =$(this)
				, url = ''
				;
			
			$r_2.find('.tab').each(function(){
				var $tab = $(this);
				if($tab.hasClass('active')){
					$tab.removeClass('active');
				}
			});
			
			$this.addClass('active');

			if($this.hasClass('tab_all')){
				url = '#!/sale?category=all';
			}else if($this.hasClass('tab_1')){
				url = '#!/sale_yacht';
			}else if($this.hasClass('tab_2')){
				url = '#!/sale_boat';
			}
			window.location.href= url;
		});
		/*==/탭클릭/==*/
		
	};
	/*==================================================
	 * 이벤트
	 ==================================================*/
	
	
	
	/*==================================================
	 * 컨트롤
	 ==================================================*/
	return {
		init : init
		, on : on
	}
})());
