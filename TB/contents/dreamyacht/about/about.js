
TB_shared.set('contents--dreamyacht--about--js', (function(){
	var context
		, show = {}
		, album_options = { }
		, insert_form = {}
		, modify_form = {}
		;
	
	//show.ease = Power3.easeNone;
	show.ani_time = 0.4;
	show.delay = 0.2;
	show.callback = function(el){};
	/*==/show 애니메이션/==*/
	
	insert_form = {
		title : '요트사진'
		, req_type : 'insert'
		, req_query : 'about--yacht_album'
		, form : [
			{
				tag : 'select'
				, each : [
					{value : '1호정', text : '1호정' , selected : true}
					, {value : '2호정', text : '2호정' }
					, {value : '3호정', text : '3호정'}
				]
				, attr : {
					name : 'category'
					, class : 'browser-default'
				}
				
			}
			, {
				tag : 'input'
				, label : '제 목'
				, attr : {
					type : 'text'
					, name : 'subject'
					, class : 'TB_form_validate'
				}
			}
			, {
				tag : 'file'
				, attr : {
					type : 'file'
					, name : 'files'
					, class : 'TB_form_validate'
				}
			}
		]
		, submit : '입 력'
	};
	/*===/var : 입력 폼/==*/
	
	$.extend(true, modify_form, insert_form);
		
	//console.log(modify_form);
	
	modify_form.form.push(
		{
			tag : 'input'
			, attr : {
				name : 'no'
				, type : 'hidden'
			}
		}
	);
	delete modify_form.form[2].attr.class;//TB_form_validate 삭제
	modify_form.req_type = 'modify';
	modify_form.submit = '수 정';
	//TB_shared.set('about--yacht_album_modify', modify_form);
	/*===/ 수정 폼/==*/
	
	//TB_shared.set('about--yacht_album', insert_form);
	/*==================================================
	 * 변수 설정
	 ==================================================*/
	
	var scroll_spy = function(param){
		var $scroll_spy = context.parent.find('.scroll_spy_buttons')
			, id = param&&param.scroll_spy 
			;
		
		//console.log(id);
		$scroll_spy.find('.trigger_'+id).trigger('click')
	};
	/*==================================================
	 * scroll_spy
	 ==================================================*/
	
	
	var on = function(obj){
		var param = obj.param||{}
			, page = (param.page)?parseInt(param.page):param.page
			, category = param.category||'all'
			;
		
		//console.log('on param : ', param);
		scroll_spy(param);
		
		//console.log(context.album);
		if(page || category){
			if(category === 'all'){
				category = '';
			}
			context.album.options.page = page||1;
			context.album.options.category = category||'';
			context.album.render_obj.category = category||'';
			context.album.render(context.album, function(){
				context.parent.find('.r_3').find('.materialboxed').materialbox();
			});
		}
		
		
	};
	/*==/재 진입시 on함수/==*/
	
	var album = function(context, param){
		var context = context
			, param  = param||{}
			, options = {}
			;
		
		insert_before = function(data, $form){
			//console.log('전송');
			//console.log(data, $form)
		};
		
		insert_after = function(res){
			//console.log('전송 후');
			//console.log(res);
		};
		
		var modify_before = function(data, $form){
			//console.log('수정전', data, $form)
		};
		var modify_after = function(res){
			//console.log('수정 후 ', res);
		};

		options = {
			page : param.page||1
			, page_num : 5
			, list_type : 'change'
			, query_get_list : 'yacht_album'
			, query_update_item : ''
			, query_modify_item : 'yacht_album'
			, query_delete_item : 'yacht_album'
			, list_convert : undefined
			, $parent : context.parent//★
			, infinity_scroll : false
			, category : param.category||''
			, search_field : param.search_field||''
			, search_value : param.search_value||''
			, insert_form : insert_form
			, insert_before : insert_before
			, insert_after : insert_after
			, modify_names : ['no', 'category', 'subject']
			, modify_form : modify_form
			, modify_before : modify_before
			, modify_after : modify_after
		};
		
		//console.log(options);
		
		context.album = TB_bbs(options);//앨범 생성
		context.album.render_obj.TB_page = TB_page.locationDec().map.page;//현재 주소 정보
		context.album.render_obj.TB_root_path = TB_config.get('TB_root_path');//상단패스
		context.album.render_obj.TB_userInfo = TB_config.get('user');//상단패스
		context.album.render_obj.category = options.category;
		//console.log(context.obj);
		
		context.album.render(context.album, function(){
			//console.log('앨범 랜더 완료.');
			context.parent.find('.r_3').find('.materialboxed').materialbox();
		});
	};
	/*==
	 * /album_render/
	 * //$parent를 내부에 있는 엘리먼트로 하려면 랜더링 
	 * 된다음 해야 한는데 그럴 경우 TB_context.render()에 의해서 
	 * 이미 랜더링이 된다음이라 내부 소스가 아무것도 없게 된다.
	 * 해결법을 찾자.  다음에.
	 * ==*/
	
	
	var init = function(_context){
		var param = _context.param||{}
			, album_options
			;
			
		context = _context;
		
		context.obj = {};
		context.album = {};		
		
		//앨범 랜더링
		album(context, param);
		
		//console.log(_context);
		
		TB_contents.render(context, function($top){
			var ani = new TB_ani.show($top, show);
			
			ani.init();
			$top.find('.tabs').tabs();
			evt($top);
			context.parent.find('.scroll_spy').scrollSpy({scrollOffset:45});
			scroll_spy(context.param);
		});
	};
	/*==/초기화 함수/==*/

	
	
	/*==================================================
	 * 함수
	 ==================================================*/
	
	
	var evt = function($top){
		var $top = $top
			, $r_3 = $top.find('.r_3')
			;
		
		$r_3.find('.tab').off('click.tab_href')
		.on('click.tab_href', function(){
			var $this =$(this)
				, url = ''
				;
			
			$r_3.find('.tab').each(function(){
				var $tab = $(this);
				if($tab.hasClass('active')){
					$tab.removeClass('active');
				}
			});
			
			$this.addClass('active');
			
			if($this.hasClass('tab_all')){
				url = '#!/yacht_album?category=all';
			}else if($this.hasClass('tab_1')){
				url = '#!/yacht_album?category=1호정';
			}else if($this.hasClass('tab_2')){
				url = '#!/yacht_album?category=2호정';
			}else if($this.hasClass('tab_3')){
				url = '#!/yacht_album?category=3호정';
			}
			window.location.href= url;
		});
		/*==/탭클릭/==*/
		
	};
	
	/*==================================================
	 * 이벤트
	 ==================================================*/
	
	
	
	/*==================================================
	 * 컨트롤
	 ==================================================*/
	return {
		init : init
		, on : on
	};
})());
