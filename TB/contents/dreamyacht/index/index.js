
TB_shared.set('contents--dreamyacht--index--js', (function(){
	var context
		, show = {}
		, ani = {}
		;
	
	//show.ease = Power3.easeNone;
	show.ani_time = 0.3;
	show.delay = 0.2;
	show.callback = function(el){
		var title = el.getAttribute('title');
		if(title === '요트체험' ||title === '요트판매'||title === '요트교육'  ){
			el.className += 'ST_transition_all_03_s ';			
		}
	};
	
	/*==================================================
	 * 변수 설정
	 ==================================================*/
	
	
	var init = function(_context){
		context = $.extend(context, _context);
		TB_contents.render(context, function($top){
			ani = new TB_ani.show($top, show);
			ani.init();
		});
	};
	/*==/초기화/==*/
	
	var on =function(param){
		//console.log(param, ani);
		//console.log(ani);
		ani.evt();
		//console.log('index on', ani, ani.evt());
		
		//TB.binding(context.parent);
		//ani.run(context.parent, show);	
	};
	/*==/재방문/==*/
	
	
	var off = function(dec){
		//console.log('index off', dec);
	};
	/*==/나갈 시 /==*/
	
	/*==================================================
	 * 함수
	 ==================================================*/
	
	
	
	/*==================================================
	 * 이벤트
	 ==================================================*/
	
	
	
	/*==================================================
	 * 컨트롤
	 ==================================================*/
	return {
		init : init
		, on : on
		, off : off
	}
})());
