
TB_shared.set('contents--dreamyacht--education--js', (function(){
	var context
		, show = {}
		, ani = {}
		;
	
	//show.ease = Power3.easeNone;
	show.ani_time = 0.4;
	show.delay = 0.2;
	show.callback = function(el){
	};

	/*==================================================
	 * 변수 설정
	 ==================================================*/
	
	
	var scroll_spy = function(id){
		var $scroll_spy = context.parent.find('.scroll_spy_buttons')
			, id = id
			;
		
		//console.log('scroll_spy', id);
		$scroll_spy.find('.trigger_'+id).trigger('click')
	};
	/*==/scroll_spy/==*/

	
	var on = function(res){
		var id = res.param&&res.param.scroll_spy
			;
		
		if(id){ scroll_spy(id); }
		
		TB_ui.parallax_reset(context.parent);
		ani.evt();
	};
	/*==/on/==*/

	
	
	var init = function(_context){
		context = $.extend(context, _context);
		TB_contents.render(context, function($top){
			var id = context.param&&context.param.scroll_spy  
				;
			ani = new TB_ani.show($top, show)
			ani.init();
			context.parent.find('.scroll_spy').scrollSpy({scrollOffset:45});
			if(id){
				scroll_spy(id);
			}

		});
	};
	/*==================================================
	 * 함수
	 ==================================================*/
	
	
	
	/*==================================================
	 * 이벤트
	 ==================================================*/
	
	
	
	/*==================================================
	 * 컨트롤
	 ==================================================*/
	return {
		init : init
		, on: on
	}
})());
