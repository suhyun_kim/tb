TB_shared.set('contents--dreamyacht--community--js', (function(){
	var context
		, show = {}
		, ani = {}
		, common_render_obj = {
			TB_page : TB_page.locationDec().map.page//현재 페이지
			, TB_root_path : TB_config.get('TB_root_path')//최상단 주소
			, TB_userInfo : TB_config.get('user')//유저 정보
		}
		;
	
	//show.ease = Power3.easeNone;
	show.ani_time = 0.3;
	show.delay = 0.2;
	show.callback = function(el){};
	
	/*==================================================
	 * 변수 설정
	 ==================================================*/
	
	var list_convert= function(list){
		var i = 0, len = list.length
			, convert = []
			;
		for(; i < len ; i++){
			list[i].writetime = list[i].writetime.split(' ')[0];
		}
		return list;
	};
	/*==/리스트 날짜 줄이기/==*/
	
	var scroll_spy = function(param){
		var $scroll_spy = context.parent.find('.scroll_spy_buttons')
			, id = param&&param.scroll_spy 
			;
		
		//console.log(id);
		if(id){
			$scroll_spy.find('.trigger_'+id).trigger('click');
		}
	};


	var  map_init = function() {
		var myLatlng = new google.maps.LatLng(37.185654, 126.651543); 
		var mapOptions = { 
		      zoom: 17, 
		      center: myLatlng, 
		      scrollwheel : false,
		      mapTypeId: google.maps.MapTypeId.ROADMAP 
		} 
		var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
		var marker = new google.maps.Marker({ 
	            position: myLatlng, 
	            map: map, 
	            title: "전곡항마리나"
		        , animation : google.maps.Animation.DROP
		});
		
		var infowindow = new google.maps.InfoWindow();
		var infowindowLatlng = new google.maps.LatLng(37.1858940, 126.651543);
		infowindow.setContent('<b>드림요트 코리아</b><br/>031-355-1174');
		infowindow.setPosition(infowindowLatlng);
		infowindow.open(map);
		//console.log(marker);
	};
	/*==/맵 만들기/==*/


	
	var notice_set= function(context, param){
		var context = context
			, param  = param||{}
			, options = {}
			, category = ''
			;
		
		options = {
			page : param.notice_page||1
			, page_num : 5
			, list_type : 'change'
			, query_get_list : 'notice_list'
			, query_update_item : ''
			, query_modify_item : 'notice'
			, query_delete_item : 'notice'
			, list_convert : undefined
			, $parent : context.parent.find('#community_notice')
			, infinity_scroll : false
			, category : ''
			, search_field : param.search_field||''
			, search_value : param.search_value||''
		};
		//console.log(options);
		
		context.notice = TB_bbs(options);//앨범 생성
		context.notice.render_obj.query_view_item = 'notice_item';
		context.notice.render_obj.query_modify_item = 'notice';
		context.notice.render_obj = $.extend(context.notice.render_obj, common_render_obj);
		context.notice.list_convert = list_convert;
	};
	/*==/공지사항 셋팅/==*/
	
	var postscript_set= function(context, param){
		var context = context
			, param  = param||{}
			, options = {}
			, category = ''
			;
		
		options = {
			page : param.postscript_page||1
			, page_num : 5
			, list_type : 'change'
			, query_get_list : 'postscript_list'
			, query_update_item : ''
			, query_modify_item : 'postscript'
			, query_delete_item : 'postscript'
			, list_convert : undefined
			, $parent : context.parent.find('#community_postscript')
			, infinity_scroll : false
			, category : ''
			, search_field : param.search_field||''
			, search_value : param.search_value||''
		};
		
		//console.log(options);
		context.postscript = TB_bbs(options);//앨범 생성
		context.postscript.render_obj.query_view_item = 'postscript_item';
		context.postscript.render_obj.query_modify_item = 'postscript';
		context.postscript.render_obj = $.extend(context.postscript.render_obj, common_render_obj);
		context.postscript.list_convert = list_convert;
	};
	/*==/후기/==*/
	
	var qna_set= function(context, param){
		var context = context
			, param  = param||{}
			, options = {}
			, category = ''
			;
		
		options = {
			page : param.postscript_page||1
			, page_num : 5
			, list_type : 'change'
			, query_get_list : 'qna_list'
			, query_update_item : ''
			, query_modify_item : 'qna'
			, query_delete_item : 'qna'
			, list_convert : undefined
			, $parent : context.parent.find('#community_qna')
			, infinity_scroll : false
			, category : ''
			, search_field : param.search_field||''
			, search_value : param.search_value||''
		};
		
		//console.log(options);
		context.qna = TB_bbs(options);//앨범 생성
		context.qna.render_obj.query_view_item = 'qna_item';
		context.qna.render_obj.query_modify_item = 'qna';
		context.qna.render_obj = $.extend(context.qna.render_obj, common_render_obj);
		context.qna.list_convert = list_convert;
	};
	/*==/qna/==*/
	
	
	var init = function(_context){
		var param
			;
		context = $.extend(context, _context);
		
		param = _context.param||{}		
		
		context.obj = {};
		context.obj.query_insert_query = 'notice';
		context.obj.query_modify = 'notice';
		
		context.obj.query = {};
		context.obj.query.postscript_insert = 'postscript';
		context.obj.query.postscript_modify = 'postscript';
		context.obj.query.qna_insert = 'qna';
		context.obj.query.qna_modify = 'qna';
		
		context.notice = {};
		
		notice_set(context, param);
		postscript_set(context, param);
		qna_set(context, param);
		
		TB_contents.render(context, function($top){
			ani = new TB_ani.show($top, show);
			ani.init();
			
			context.notice.set_el(context.parent.find('#community_notice'));
			context.notice.render(context.notice);
			
			context.postscript.set_el(context.parent.find('#community_postscript'));
			context.postscript.render(context.postscript);
			
			context.qna.set_el(context.parent.find('#community_qna'));
			context.qna.render(context.qna);
			
			map_init();
			
			context.parent.find('.scroll_spy').scrollSpy({scrollOffset:45});
			scroll_spy(context.param);
			evt(context.parent);
		});
	};
	/*==/초기화/==*/
	
	var on =function(sendData){
		var param = sendData.param||{}
			, page = (param.page)?parseInt(param.page):param.page
			, prev_page = TB_page.sgmap().history[1].page
			;
			
		//console.log('sale on');
		//console.log(param, prev_page);
		
		if(param){
			if(param.scroll_spy === 'sale_boat'){category = '보트';}
			else if(param.scroll_spy === 'sale_yacht'){category = '요트';}
		}
		
		ani.evt();
		scroll_spy(param);
		
		
		//console.log(prev_page);
		if((param&&param.notice_page) || (prev_page === "modify_form_dreamyacht" || prev_page === "write_form_dreamyacht")){
			context.notice.options.page = param.notice_page||1;
			context.notice.render(context.notice);
		}
		
		if((param&&param.postscript_page) || (prev_page === "modify_form_dreamyacht" || prev_page === "write_form_dreamyacht")){
			context.postscript.options.page = param.postscript_page||1;
			context.postscript.render(context.postscript);
		}
		
		if((param&&param.qna_page) || (prev_page === "modify_form_dreamyacht" || prev_page === "write_form_dreamyacht")){
			context.qna.options.page = param.qna_page||1;
			context.qna.render(context.qna);
		}

	};
	/*==/재방문/==*/
	
	
	
	/*==================================================
	 * 함수
	 ==================================================*/
	
	
	var evt = function($top){
		var $top = $top
			;
		
	};
	/*==================================================
	 * 이벤트
	 ==================================================*/
	
	
	
	/*==================================================
	 * 컨트롤
	 ==================================================*/
	return {
		init : init
		, on : on
	}
})());
