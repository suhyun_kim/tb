
TB_shared.set('layout--metro--win_8--js', function(_context){
	var map = TB_page.locationDec();
	
	var metro = (function($){
		var scrollSw = true
				, curScrollPosi = 0
				, setResponsiveFunc = 0 
				, scrollEnd = 0
				, mode = 'mobile'
				, mouseWheelEvt = (navigator.userAgent.indexOf('Firefox') != -1)?'DOMMouseScroll.metro_scroll' : 'mousewheel.metro_scroll'
				;
				
		var init  = function(){
			eBinding();
			scrollEndFunc();
			TB_page.bakeFunc(function(){
				setTimeout(function(){
					responsive();
				}, 500);
			});//컨텐츠 마다 불러올 함수
		};
		/*==================================================
		 * 초기화
		 ==================================================*/
		
		
		var metro_box = function($box, mode, max_width, max_height){
			var $box = $box
				, mode = mode//pc or mobile
				, max_width = max_width //container width
				, max_height = max_height //container height
				, left = 0 , top = 0
				, row = 0
				, el_height = $box.outerHeight()
				, el_width = $box.outerWidth()
				, max_row = Math.floor(max_height/el_height)
				, add_top = Math.floor((max_height-(el_height*max_row))/2)
				, container_width
				, len = $box.length
				;
			
			$box.each(function(idx){
				var height = this.offsetHeight
					, width = this.offsetWidth
					;
				
				if(mode === 'mobile'){
					this.style.position ='';
					this.style.left ='';
					this.style.top ='';
				}else if(mode === 'pc'){
					this.style.position = 'absolute';
					this.style.left = left+'px';
					this.style.top = top+add_top+'px';
					top += height;
					//console.log(height, max_height, top, row, left);
					if(top+height > max_height){
						top = 0;
						row++;
						left = row*width;
					}
				}
				
			});
			
			//console.log('contaier_width :', len, max_row, len%max_row);
			if(mode === 'pc'){
				container_width = (len%max_row != 0)?left+el_width:left;
			}
			
			return container_width;
		};
		/*==/메트로 박스 셋팅/==*/
		
		/*==================================================
		 * 함수
		 ==================================================*/

		
		var metro_full_pic = function(obj){
			var css = {'position' : 'absolute'}
				;
			
			if(obj.$el[0].tagName != "IMG"){
				throw "metro_full_pic은 반드시 IMG 태그를 사용하여야 합니다. \n "+obj.$el[0].outerHTML;
			}
		
			if(obj.mode === 'pc'){
				css.height = obj.height;
				css.width = '';
				css.top  = '50%';
				css.left = '50%';
			}else if(obj.mode === 'mobile'){
				css.height = '';
				css.width = '150%';
				css.left = '50%';
				css.top = '50%';
			}
			var align = function(){
				//console.log(obj);
				if(Modernizr.csstransforms3d){
					obj.$container.on('transitionend', function(){
						obj.$el.css(css)
						.css({
							'margin-top': -obj.$el.height()/2
							, 'margin-left' : -obj.$el.width()/2
						});
					});
				}else{
					obj.$el.css(css)
					.css({
						'margin-top': -obj.$el.height()/2
						, 'margin-left' : -obj.$el.width()/2
					});
				}
			};
			
			obj.$el.load(function(){
				align();
			});
			align();
		};
		/*==================================================
		 * 자동으로 가운데 정렬을 시키고 metro_container 의 상태에 따라 크기를 가변적으로 변형 시킨다.
		 * pc 모드 일경우는 height 를 기준으로 크기를 잡고 
		 * mobile 모드일 경우는 width를 100%로 한다.  
		 * trigger Class
		 * 	. metro_full_pic
		 * params
		 * 	._obj(obj)
		 * 		. $el(jQuery) : 이미지  엘리먼트
		 * 		. alignObj(obj) : container의 정렬에 사용되는 오브젝트
		 * 		. mode (str) : pc 혹은 mobile인지 구분.
		 ==================================================*/
		
		var responsive = function(_obj, _callback){
			var option = {
						$scope : $('.TB_page_container_active'),
						containerWidth : 540,
					}
					, callback = _callback || undefined
					;
			
			if(arguments.length ==1 && $.type(agruments[0])=== 'function'){
				callback = arguments[0];
			}else{
				options = $.extend(option, _obj);
			}
			
			
			var winWid = TB_shared.get('winWid'),
					//pcContainerWid = parseInt(winWid/2, 10),
					winHei = TB_shared.get('winHei'),
					alignObj = {};
					
			if(winWid < 992){//모바일
				mode = 'mobile';
				alignObj.left = 0;
				alignObj['min-width'] = winWid;
				alignObj['width'] = winWid;
				alignObj.position = 'relative';
			}else{//피씨
				mode = 'pc';
				alignObj.top = 0;
				alignObj['min-width']= option.containerWidth;
				alignObj['width']= option.containerWidth;
			}
			
			
			
			option.$scope.find('.metro_wrap').each(function(){
				var $wrap  = $(this),
						align = []//정렬용값
						, addAlignWid = 0 //정렬시 누적되는 값
						, $metro_container = $wrap.find('.metro_container, .metro_fullContainer');
						;
				$wrap.find('.metro_container, .metro_fullContainer').each(function(_idx){
					var $this = $(this)
						, metro_wid = $this.attr('data-TB_metro_width')
						, metro_m_hei = $this.attr('data-TB_metro_m_hei')
						, isFullContainer = $this.hasClass('metro_fullContainer')//fullContainer인지 확인
						, isMinus_height = $this.attr('data-metro_minus_height')//metro_fullContainer일 때 pc 모드에서 지정된 height를 minus 시킨다.
						, $metro_minus_height = $this.find('.metro_minus_height')//해당 엘리먼트의 height를 minus시킨다.
						, $metro_sync_hei = $this.find('.metro_sync_height')//metro_container or metro_fullContainer 의 height와 높이를 맞춘다.
						, $metro_full_pic = $this.find('.metro_full_pic')//컨테이너에 꽉 차는 그림.
						, $metro_box= $this.find('.metro_box')//반응형 박스 <div class="metro_box" ></div>
						, setWid = option.containerWidth
						, thisWid  =$this.width()
						, alignWid = (_idx === 0)?0:option.containerWidth*_idx
						;
						
					if(!metro_wid){throw "'data-TB_metro_width' 속성이 존재 하지 않습니다.";}
							
					if(metro_wid){//비율에 따라 저장한다.
						metro_wid = metro_wid.split('_');
						align[_idx] = {
							value : metro_wid[0],
							type : metro_wid[1]
						};
					}
					
					 if(mode === 'pc'){//피씨모드일 경우.위치 조정 및 정리
					 	
					 	if(metro_wid){
						 	if(align[_idx].type === 'ratio'){//퍼센트
								setWid = Math.floor(winWid/align[_idx].value);
						 	}else{//퍼센트가 아닐경우단위 그래도 써준다. 
								//setWid = align[_idx].value+align[_idx].type;
								setWid = align[_idx].value+align[_idx].type;
						 	}
					 	}
					 	
						if(isFullContainer){//풀컨테이너 셋팅
							alignObj.height  =  Math.floor(winHei*0.98);
							if(isMinus_height && mode=== 'pc'){//minus_height
								alignObj.height = alignObj.height-(parseInt(isMinus_height, 10)); 
							}
						}
					 	
						alignObj['min-width']  =  setWid;
						alignObj['width']  =  setWid;
						/*==========/엘리먼트 셋팅===============*/
						
					 	if(align.length && _idx >0){
				 			if(align[_idx-1].type === 'ratio'){
					 			alignWid = Math.floor(winWid/align[_idx-1].value)*_idx; 
				 			}else{
					 			addAlignWid  += parseInt((align[_idx-1].value),10);
					 			alignWid = addAlignWid+align[_idx-1].type;
				 			}
					 	}
					 	alignObj.position = 'absolute';
						alignObj.left =  alignWid;
						
					}
					else if(mode === 'mobile'){//모바일 스타일
						alignObj['min-width']  =  '';
						alignObj['width']  =  '100%';
						if(isFullContainer && metro_m_hei){//height 지정
							metro_m_hei = metro_m_hei.split('_');
							if(metro_m_hei[1] === 'px'){metro_m_hei[1] = '';}
							alignObj.height  = metro_m_hei[0]+metro_m_hei[1];
						}else{//height 가 지정안되어 있을 경우는 height 삭제
							alignObj.height = '';
						}
					}
					/*==========/스타일 ===============*/

					//메트로 박스가 있을 때는 넓이 조정을 못하게 한다. 
					if($metro_box.length && mode === 'pc'){
						delete alignObj.width;	
						delete alignObj['min-width'];	
					}
					
					//현재 height 결정
					$this.css(alignObj);
					
					//minus height
					if($metro_minus_height.length){
						$metro_minus_height.each(function(){
							var $this = $(this)
									, minus = $this.attr('data-metro_minus_height')
									, height
									;
							if(minus && mode=== 'pc'){//minus_height
								height = alignObj.height-(parseInt(minus, 10)); 
							}else if(minus && mode=== 'mobile'){
								height = ''; 
							}
							$this.css('height',height);
							$this = null, minus = null, height = null;
						});
					}
					
					if($metro_sync_hei.length ){//
						//console.log(mode, metro_m_hei);
						
						$metro_sync_hei.each(function(){
							var $this = $(this)
									, ratio = parseInt($this.attr('data-metro_sync_height'), 10)
									, css
									;
							if(isNaN(ratio)){//숫자가 아닐경우
								ratio = undefined;
							}
							
							if(mode==='mobile' && !metro_m_hei){
								css = '';
							}else{
								if(ratio){//비율 값을 지정했을 경우
									css = alignObj.height*ratio/100;
								}else{//값이 없을 경우
									css = (alignObj.height)?alignObj.height:$this.height();
								}
							}
							
							//console.log(alignObj.height);
							$this.css('height', css);
							//$this = null, ratio = null, css =null;
						});
					}
					/*==========/메트로 컨테이어 height sync ===============*/
					
					if($metro_full_pic.length){
						$metro_full_pic.each(function(){
							var pic = {};
							if(mode === 'pc'){
								pic.width = parseInt(metro_wid[0], 10);
								pic.height = alignObj.height;
							}else if(mode === 'mobile'){
								pic.width = '';
								pic.height = metro_m_hei[0];
							}
							//console.log(pic);
							metro_full_pic({
								$el : $(this)
								, mode : mode
								, width :pic.width
								, height : pic.height
								, $container : $this
							});
							pic = null;
						});
					}
					/*==========/metro_full_pic ===============*/

					if($metro_box.length){
						var metro_box_width = metro_box($metro_box, mode, alignObj.width, alignObj.height);
						if(mode === 'pc'){
							$this.css({
								'width' : metro_box_width
								, 'min-width' : metro_box_width
							});
						}
					}
					/*==========/ metro_box /===============*/
					
				});
				
				if($metro_asyncWidth.length && mode === 'pc'){
					
					var metro_align = function(){
						var css = {width:0, left:0};
						
						$metro_container.each(function(){
							var $container = $(this)
									;
							css.width = parseInt($container.attr('data-TB_metro_width').split('_')[0], 10);
							css['min-width'] = css.width;
							$container.css(css);
							css.left += css.width;
						});
					};
					
					
					var metro_asyncWidth_align = function(){
						$metro_asyncWidth.each(function(idx){
							var $asyncWidth = $(this)
									, $target = $asyncWidth.find('.metro_asyncWidth_target')
									, sumWidth = 0
									;
							//타겟 순회
							$target.each(function(t_idx){
								var $target_el = $(this)
										, tagName = this.tagName
										, loaded = $target_el.attr('img_loaded')
										;
								var reposition = function(callback){
									sumWidth += $target_el.outerWidth();
									//타겟의 마지막이  로드되었을 때 넓이 값을 함산 후 metro_asyncWidth 속성 변경
									if($target.length === t_idx+1){
										$asyncWidth.attr('data-TB_metro_width', sumWidth+'_px');
									}
									//마지막 이미지 로드시 
									if(($metro_asyncWidth.length ===  idx+1) && ($target.length === t_idx+1)){
										//console.log('일반 마지막', sumWidth, $asyncWidth.attr('data-TB_metro_width'));
										metro_align();
										if(callback){callback();}
									}
								};
								/*===/ 넓이와 위치 재설정 ==*/
								//console.log($metro_asyncWidth.length , idx+1 , $target.length , t_idx+1, afterAlign, loaded, tagName);
								//이미지 파일인데 로드가 안됐을 경우.
								if(!loaded && tagName === 'IMG'){
									$target_el.load(function(){
										$target_el.attr('img_loaded', 'loaded');
										reposition(); 
									});
								}else{
									 setTimeout(function(){
									 	reposition();
									}, 500);
								}
							});
						});
					};
					
					metro_asyncWidth_align();
				}
				/*===================================
				 * metro_asyncWidth 
				 * ===================================*/

				
				
			});
			/*=========/metro_wrap============*/
			
		};
		/*======================================
		 * 미디어 분류해서 정렬
		 ======================================*/
		
		var scrollEndFunc = function(){
			scrollEnd = $(document).width()-$(window).width();
		};
		/*======================================
		 * 횡 스크롤 끝값 구하기.
		 ======================================*/
		
		var horizontalScroll = function(e){
			var scrollDistance,
			scrollDistance = curScrollPosi += e;
			
			 if(scrollDistance >= scrollEnd){//Max 좌측
				curScrollPosi = scrollEnd;
				scrollDistance = scrollEnd;
			}else if(scrollDistance <= 0 ){
				curScrollPosi = 0;
				scrollDistance = 0;
			}
			
			TweenMax.to(window, 0.5, {
				scrollTo : { x : scrollDistance},
				onComplete : function(){
					scrollSw = true;
				}
			});
		};
		/*=================================
		 * 마우스 스크롤시 수평 이동
		 =================================*/
		
		
		var horizontalScrollEvent = function(){
			if(!scrollSw)return false;
			scrollSw = false;
			
			$(document).on(mouseWheelEvt, function(e){
				//console.log(e);
				var num = 0;
				
				if(mouseWheelEvt === 'DOMMouseScroll.metro_scroll'){
					num = e.originalEvent.detail*40;
				}else if(mouseWheelEvt === 'mousewheel.metro_scroll'){
					num = (e.originalEvent.wheelDelta > 0)?-e.originalEvent.wheelDelta : e.originalEvent.wheelDelta*-1;
				}
				
				if(mode ==='pc'){
					horizontalScroll(num);
				}
				scrollEndFunc();
				num = null;
				
				if(mode==='pc'){//pc모드에서는 세로 스크롤 비활성화
					return false;
				}
				
			});
			
		};
		/*=================================
		 * 평행 스크롤 이벤트
		 =================================*/
		
		var select_scroll = function(_direction){
			var direction = _direction||'x';
			if(direction === 'x'){//가로축 스크롤
				scrollSw = true;
				horizontalScrollEvent();
			}
			else if(direction === 'y'){
				$(document).off(mouseWheelEvt);
			}
		};
		
		/*=================================
		 * 스크롤 방향 정하기.
		 * params
		 * 	. _direction (string) : 'x' or 'y'  스크롤될 방향을 결정.
		 =================================*/
		
		var eBinding = function(){
			horizontalScrollEvent();//수평 스크롤 
			
			$(window).on('resize', function(){
				clearTimeout(setResponsiveFunc);
				setResponsiveFunc = setTimeout(function(){
					responsive();
				}, 500);
			});
			/*==/metro_container 반응형 처리 ==*/
		};
		
		return {
			init : init,
			responsive : responsive
			, select_scroll : select_scroll
		};
		
	})(jQuery);
	/*===================================
	 * 메트로 UI 
	 ===================================*/
	
	metro.init();
	
	TB_shared.set('metroResponsive', metro.responsive);//반응형 공유 함수 등록
	
	TB_shared.set('metro_scroll', metro.select_scroll);//스크롤 방향 정하기 params('x' of 'y')
	
	if(!map){
		map = {
			'map' : TB_page.sgmap().menu[_context['param'].menu]
		};
	}
	
	_context['obj'] = map; //맵을 이용한 랜더링 가능.
	
	TB_contents.render(_context, function($topEl){
		TB_page.init();//페이지 초기화
	});
	
	
	
});
