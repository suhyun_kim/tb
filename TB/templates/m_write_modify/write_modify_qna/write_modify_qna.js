/*===========================
 * 필요변수
 * 	. type : insert or modify
 * 	. reqQuery : 어떤것을 처리할지 타입결정
 ===========================*/
TB_shared.func = function(context){
	//console.log(context);
	context['obj'] = {
		'submitBtn' : (context.param.reqType==='insert')?'입력' : '수정'
		, 'reqType' : context.param.reqType
		, 'reqQuery' : context.param.reqQuery
	};
	/*==/랜더링할 오브젝트 기본 셋팅==*/
	
	if(context.param.reqType==='modify'){//수정일 경우
		context['obj'].modify="modify";
		context['obj'].no=context.param.no;
		//랜더링할 오브젝트 추가 셋팅
		TB.adminAuth(function(res){
			if(res!==1)return false;
			context['obj'].auth = 'auth';
			context['obj'].reqQuery = 'answer_qna';
		});/*==/관리자계정일때 답변==*/
		
		TB_ajax.get_json(
			{
			'post' : 
				{
					'reqType' : 'select'
					, 'reqQuery' : context.param.selectReqQuery
					, 'no' : context.param.no
				}
			,'callback' : function(dbData){
				//console.log(dbData);
				var insertData = $.parseJSON(dbData.body);
				
				//console.log(insertData);
				TB_contents.render(context, function($topEl){
						context['$topEl'] = $topEl;
						TB_form.autoInsert($topEl,insertData);
						//이스케이프문자가 있을 경우 핸들바에서 오류가 나기 때문에 강제입력
						
						var phoneNum = context['$topEl'].find('.TB_form_join').val().split('-');
						context['$topEl'].find('.join_phone').each(function(idx){
							$(this).val(phoneNum[idx]);
						});
						//전화번호 나눠서 입력
						
						TB_form.binding($topEl);
						TB_form.eBinding($topEl);
						//이벤트 바인딩
				});/*==/랜더링==*/
			}
		});
	}else if(context.param.reqType==='insert'){//insert일 경우
		TB_contents.render(context, function($topEl){
			TB_form.binding($topEl);
			TB_form.eBinding($topEl);
		});
		/*==/랜더링==*/
	}
	
	
	
	if(!TB_shared.get('m_write_modify_aftAc')){
		TB_shared.set('m_write_modify_aftAc', function(){
			TB_shared.get('m_modal_1_off')();
			TB_contents.load($('.contents_wrap'));
		});
	};
	/*==/폼전송 후 액션==*/
};
