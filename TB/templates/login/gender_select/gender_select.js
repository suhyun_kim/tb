/*============================
 * nodejs 전용
 * params 
 * 	. GET 방식으로만 전달받음
 * 	. ex ) login?strategy = goolge,naver,facebook
 * 	. strategy(string) : 로그인할 전략
 ============================*/

TB_shared.set('templates--login--gender_select--js', function(context){
	var strategy = [];
	
	context['obj'] = {};
	
	/*=============================================
	 * 변수설정
	 =============================================*/

	TB_shared.set('templates--login--gender_select--beforeAC', function(data, $form){
		var gender;
		for(var i = 0, len = data.length ; i < len ; i++){
			if(data[i].name === 'gender'){
				gender = data[i].value; 
			}
		}
		if(!gender){
			TB_func.popupDialog('성별을 선택해주세요');
			return false;
		}
		return true;
	});
	
	TB_shared.set('templates--login--gender_select--aftAC', function(res){
		//console.log(res);
		if(res.result === 'select_success'){
			TB_ani.hide_backdrop({history_back:false});
		}else{
			throw '. gender_select Error \n ';
		}
	});
	/*=============================================
	 * 함수셋팅
	 =============================================*/
	
	var evt = function($topEl){
		
		$topEl.find('.icon').off('click.gender_select')
		.on('click.gender_select', function(){
			$(this).parent().find('[type=radio]').attr('checked', 'checked');
			$topEl.find('form').find('.submit').trigger('click');
		});
		/*==/성별 선택/==*/
		
	};
	/*=============================================
	 * 이벤트
	 =============================================*/
	
	
	TB_contents.render(context, function($topEl){
		evt($topEl);
	});
});

