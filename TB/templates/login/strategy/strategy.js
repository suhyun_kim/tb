/*============================
 * nodejs 전용
 * params 
 * 	. GET 방식으로만 전달받음
 * 	. ex ) login?strategy = goolge,naver,facebook
 * 	. strategy(string) : 로그인할 전략
 ============================*/

TB_shared.set('templates--login--strategy--js', function(context){
	var strategy = [];
	
	context['obj'] = {};
	
	//로그인 전략 설정
	if(context.param){
		context['obj'].strategy = [];
		strategy = context.param.strategy.split(',');
		for(var i = 0, len = strategy.length ; i < len ; i++){
			context.obj.strategy[i] = {company : strategy[i]};
		}
	}else{
		location.href = '#!/index';
		return false;
	}
	/*=============================================
	 * 변수 셋팅
	 =============================================*/
	
	
	/*=============================================
	 * 함수
	 =============================================*/
	
	var evt = function($topEl){
		$topEl.find('.login_button').off('click.login_button')
		.on('click.login_button', function(){
			var loading = "<div class='loading_container'><i class='fa fa-spinner fa-pulse fa-3x fa-fw ST_theme_color'></i></div>";
			$(this).closest('.login_button_wrap').empty().html(loading);
		});
		/*=====/구글 로그인 /==========*/
	};
	/*=============================================
	 * 이벤트
	 =============================================*/
	
	
	TB_contents.render(context, function($topEl){
		evt($topEl);
	});
});

