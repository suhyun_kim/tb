/*============================
 * nodejs 전용
 ============================*/

TB_shared.set('templates--search_form--backdrop_search_1--js', (function(context){
	var context = undefined;
	
	
	TB_shared.set('templates--search_form--backdrop_search_1--search_before',
		function(data, $form){
			var param = ''
					, page = ''
					;
			if(context.obj.page === 'mypage'){
				TB_shared.get('contents--nansolo--mypage--js').refresh();
			}
			
			
			for (var i= 0, len= data.length; i < len ; i++){
				if(data[i].name === 'category'){
					page = data[i].value;
				}
				if(data[i].value){
					param += (i>0)?'&':"";
					param += data[i].name+"="+data[i].value;
				}
			}
			
			location.replace("#!/"+context.obj.page+"?"+encodeURIComponent(param));
			
			//console.log(location.href);
			return false;
		}
	);
	/*===/검색 리다이렉트 함수/ ===*/
	
	var render = function(){
		TB_contents.render(context, function($topEl){
			$topEl.find('[name=search_value]').focus();
		});
	};
	/*===/랜더링 함수./ ===*/
	
	/*=============================================
	 * 함수
	 =============================================*/
	
	
	
	/*=============================================
	 * 이벤트
	 =============================================*/
	
	
	
	return {
		init : function(_context) {
			context = _context;
			//console.log(TB_page.locationDec());
			//console.log(TB_page.sgmap());
			context['obj'] = {};
			context.obj.page = context.param.page;
			context.obj.scope = (context.obj.page === 'mypage')?'private':'public';
			
			context.obj.render = {};
			context.obj.render.categories = [
				{text:'음 식', value:'food'}
				, {text:'여 행', value:'travel'}
				, {text:'기 타', value:'etc'}
			];
			
			render();
		}
		
		
		, TB_half_bake : function(p){
			
			context.obj.page = p.param.page;
			context.obj.search_type = (context.obj.page === 'mypage')?'private':'public';
			//계속 엘리먼트를 잡고 있는 메뉴의 것들과는 다르게 엘리먼트가 사라 지므로 다시 랜더링을 해줘야 한다.
			render();
			//context.parts.render('templates--search_form--backdrop_search_1--form');
		}
	}
})());


