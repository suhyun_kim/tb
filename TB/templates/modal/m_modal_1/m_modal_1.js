/*============================
 * 모달을 사용하기 위해서는 반드시 데이터를 넘겨야 한다.
 ============================*/
TB_shared.func = function(context){
	// console.log(context);
	context.param.sendData = JSON.stringify(context.param);
	//내부에 로드될 엘리먼트에 전송될 데이터
	
	
	var $modalWrap = $('#TB_modal');
	var click = "click";
	
	if(!TB_shared.get('m_modal_1_on')){
		TB_shared.set('m_modal_1_on', function(){
			$modalWrap.css({
				'position' : 'fixed'
				, 'background' : '#ffffff'
				, 'width' : TB_shared.get('winWid')
				, 'height' : TB_shared.get('winHei')
				, 'z-index' : '50'
				, 'display' : 'block'
			});
		});
	}/*================================
		  * 모달 on
		  ================================*/
	
	if(!TB_shared.get('m_modal_1_off')){
		TB_shared.set('m_modal_1_off', function(){
			$modalWrap.css({
				'width' : TB_shared.get('winWid')
				, 'height' : TB_shared.get('winHei')
				, 'z-index' : 'auto'
				, 'display' : 'none'
			});
		});
	}/*================================
		  * 모달 off
		  ================================*/
	 
	//console.log(JSON.stringify(sendData));
	
	//return false;
	TB_contents.render(context, function($topEl){
		TB_shared.get('m_modal_1_on')();
		$topEl
		.off(click, '.modal_header_closeBtn')
		.on(click, '.modal_header_closeBtn', function(){
			TB_shared.get('m_modal_1_off')();
		});
		//console.log($topEl.length);
		//TB.binding($topEl);
	});
};
