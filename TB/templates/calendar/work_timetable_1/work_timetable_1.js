/*============================
 * work_timetable 에 필요한 변수
 ============================*/

TB_shared.set('templates--calendar--work_timetable_1--js', function(context){
	var queries = {};
	
	context['obj'] = {};
	
	var d = new Date(),
			year = d.getFullYear(),
			month = d.getMonth()+1,
			date = d.getDate(),
			day = d.getDay()
			sharedObj  = TB_shared.get(context['param'].sharedObj)||null;
	
	context['obj'] = {
		'cur_year' : year
		, 'cur_month' : month
		, 'cur_date' : date
		, 'cur_day' : day
		, 'year' : year
		, 'month' : month
		, 'date' : date
		, 'day' : day
		, 'lastMonth' : (month == 1)?12:month-1
		, 'nextMonth' : (month < 12)?month+1:1
	};
	
	context['obj'] = $.extend(context['obj'], context['param']);
	
	var getTotalDate = function(year, month){
		if(month==4 || month==6 || month==9 || month==11){
			return 30;
		}else if(month==2){
			if(year%4 == 0){//윤년 2월일 경우.
				return 29;
			}else{
				return 28;
			}
		}else{
			return 31;
		}
	}
	/*============================================
	 * 월별 전체 날쨔 구하기
	 ============================================*/
	
	var toDayCheck = function(year, month, date, el){
		if(context['obj'].cur_year == year && context['obj'].cur_month == month && context['obj'].cur_date ==date){
			el.addClass('bgc_sel');
		}else{
			if(el.hasClass('bgc_sel')){
				el.removeClass('bgc_sel');
			}
		}					
	};
	/*============================================
	 * 오늘 날짜 표시
	 ============================================*/
	
	var printDate = function($el, year, month, obj){
		var selectDate = new Date(year, month-1,1);//입력된 월의 시작일을 알기 위한 값
		var _date=1;//날짜 테이블에 새겨질 날짜.
		var startDay = selectDate.getDay();//월의 시작 요일을 반환
		var totalDate = getTotalDate(year,month);
		var objHTML = '';//.name 클래스에 들어갈 내용
		var $dateBoxEl= $el.find('.date_container').find('.date_box');
		var i = 0
				, len= obj.length;
		// console.log(obj);
		
		$dateBoxEl
		.each(function(index){
			var $this = $(this);
			if(index >= startDay){//날짜의 시작일
				if(index <= totalDate+(startDay-1)){
					$this.find('.num').html(_date);//달력에 표시될 날짜
					toDayCheck(year, month, _date, $this);//현재 날짜 지정
					objHTML = '';
					if(len){
						for(;i < len ; i++){
							if(obj[i].date == _date){
								objHTML += "<div class='doctor' data-no='"+obj[i].no+"'>";
								objHTML += obj[i].doctor;
								objHTML += "</div>";
								break;
							}
						}
					}
					$this.find('.text').html(objHTML);
					_date++;
				}else{
					$this.find('.num').html('').end()
					.find('.text').html('');
				}
			}else{//날짜가 안들어가는 칸
				$this.find('.num').html('').end()
				.find('.text').html('');
			}
			
		});
	};
	/*============================================
	 * 달력날짜 출력
	 ============================================*/
	
	
	var chaMonthControler = function(month){
		if(month >=12){
			$el.find('.lastMonth').html(month-1+'월');		
			$el.find('.nextMonth').html(1+'월');		
		}else if(month <=1){
			$el.find('.lastMonth').html(12+'월');		
			$el.find('.nextMonth').html(month+1+'월');		
		}else{
			$el.find('.lastMonth').html(month-1+'월');		
			$el.find('.nextMonth').html(month+1+'월');		
		}
	};
	/*============================================
	 * 날짜 컨트롤러 월 변경
	 ============================================*/
	
	var changeContext = function(month, $el){
		context['obj'].month = month;
		if(month ==12){
			context['obj'].lastMonth = month-1;
			context['obj'].nextMonth = 1;
		}
		else if(month > 12){
			context['obj'].month = 1;
			context['obj'].year++;
			context['obj'].lastMonth = 12;
			context['obj'].nextMonth = 2;
		}
		else if(month ==1){
			context['obj'].lastMonth = 12;
			context['obj'].nextMonth = 2;
		}
		else if(month < 1){
			context['obj'].month = 12;
			context['obj'].year--;
			context['obj'].lastMonth = 11;
			context['obj'].nextMonth = 1;
		}
		else{
			context['obj'].lastMonth = month-1;
			context['obj'].nextMonth = month+1;
		}
		
		$el.find('.lastMonth').html(context['obj'].lastMonth+'월');		
		$el.find('.nextMonth').html(context['obj'].nextMonth+'월');		
		$el.find('.yearMonth_wrap').find('.month').html(context['obj'].month+'월');
		$el.find('.yearMonth_wrap').find('.year').html(context['obj'].year+'년');
	};
	/*============================================
	 * 날짜 변경시 context['obj']수정
	 ============================================*/
	
	
	var get_doctors =function(_callback){
		TB_ajax.get_json({
			'sendData' : {
				'reqQuery' :context['obj'].select_doctor_list_reqQuery 
			}
			, 'callback' : function(res){
				context['obj'].TB_root_path = TB_config.get('TB_root_path');
				
				context['obj'].doctors = res.body.doctors;
				//console.log(context['obj']);
				TB_handlebars.render({
					html : $(context['html']).find('.doctors_container').html()
					, obj : context['obj']
					, outputEl : context['parent'].find('.doctors_container')
				});
				
				del_doctor();
				
				if($.type(_callback)  === 'function')_callback(res.body);
				
			}
		});
	};
	/*============================================
	 * 의사 정보 출력
	 ============================================*/
	
	var del_doctor = function(){
		context['parent'].find('.doctors_wrap').find('.deleteBtn').off('click')
		.on('click', function(){
			TB_ajax.get_json({
				sendData : {
					'no' : $(this).closest('.item').find('.no').val()
					, 'reqType' : 'delete'
					, 'reqQuery' : queries.delete_reqQuery
				}
				,callback : function(res){
					get_doctors();
				}
			})
			;
		});
	}
	/*============================================
	 * 의사 정보 삭제
	 ============================================*/
	
	TB_shared.set('templates--calendar--work_timetable--memo', function(){
		get_db(context['parent'].find('.templates--calendar--work_timetable'));
	})
	/*============================================
	 * 월별 메모 출력
	 ============================================*/
	
	var get_db = function($el, _callback){
		TB_ajax.get_json({
			'sendData' : {
				'reqQuery' :context['obj'].select_reqQuery 
				, 'month' : context['obj'].month
				, 'year' : context['obj'].year
			}
			, 'callback' : function(res){
				var $detail_form = $el.find('.detail_container');
				printDate($el, context['obj'].year, context['obj'].month, res.body.list);
				
				if(res.body.queries&&$.isEmptyObject(queries) ){//쿼리 객체 보관
					queries = res.body.queries;
				}
				
				if(sharedObj && res.body.queries){
					sharedObj.queries = res.body.queries;
				}
				
				if(res.body.memo){//수정, 삭제
					res.body.memo.reqType = 'modify';
					res.body.memo.reqQuery = res.body.modify_detail_reqQuery;
					res.body.memo.delete_detail_reqQuery = res.body.delete_detail_reqQuery;
				}else{//입력
					res.body.memo = {};
					res.body.memo.year = context['obj'].year;
					res.body.memo.month = context['obj'].month;
					res.body.memo.reqType = 'insert';
					res.body.memo.reqQuery = res.body.insert_detail_reqQuery;
				}
				
				context['obj'].memo_wysiwyg_set = (context['obj'].TB_isAdmin)?'selection':'view';
				if(res.body.memo){
					context['obj'].memo_form = res.body.memo;
				}
				context['parts'].render({
					'parts':'templates--calendar--work_timetable--memo'
					, 'callback':function(){
						TB_form.autoInsert($detail_form , res.body.memo);
						TB_form.binding($detail_form);
						memo_delete();
					}
				})
				/*==/월별 메모 정보 출력 ==*/
				
				if($.type(_callback)  === 'function')_callback(res.body);
			}
		});
		
	};
	/*============================================
	 * db에서 데이터를 가져와 프린트를 달력과 설명을 출력한다.
	 ============================================*/
	
	var dateText_form = function($el){
		var $calendarText_form_wrap = context['parent'].find('.calendarText_form_wrap');
		var $this = $el
				, posi = $this.position()
				, text = $this.find('.text').html()
				, inputObj = {
					date : $this.find('.num').html()
					, month : context['obj'].month
					, year : context['obj'].year
				}
				,deleteBtn_display = 'none'
				, submit_text = '등록'
		;
		$calendarText_form_wrap.find('form')[0].reset();
		
		if(text){//수정, 삭제
			inputObj.reqType = 'modify';
			inputObj.doctor = $this.find('.doctor').html();
			inputObj.reqQuery = queries.modify_dateText_reqQuery;
			deleteBtn_display = 'block';
			submit_text = '수정';
		}else{//입력
			inputObj.reqType = 'insert';
			inputObj.reqQuery = queries.insert_dateText_reqQuery;
			deleteBtn_display = 'none';
			submit_text = '등록';
		}
		
		TB_form.autoInsert($calendarText_form_wrap.find('form'), inputObj);
		
		$calendarText_form_wrap.css({
			'left' : posi.left-80
			, 'top' : posi.top-180
		}).fadeIn().end()
		.find('.submit').val(submit_text).end()
		.find('.deleteBtn').css({
			'display': deleteBtn_display
		}).end()
		.find('.first_focus').focus()
		;
	};
	/*============================================
	 * 캘린더폼 보이기
	 ============================================*/
	
	TB_shared.set('templates--calendar--work_timetable--dateAftAc', function(data,$form){
		get_db(context['parent']);
		context['parent'].find('.calendarText_form_wrap').hide();
	});
	/*============================================
	 * 캘린더폼 전송 후 액션
	 ============================================*/
	
	var memo_delete = function(){
		context['parent'].find('.detail_container').find('.delete_container')
		.off('click')
		.on('click', function(){
			TB_ajax.get_json({
				'sendData' : {
					'reqType' : 'delete' 
					, 'reqQuery' : context['obj'].memo_form.delete_detail_reqQuery
					, 'month' : context['obj'].month
					, 'year' : context['obj'].year
				},
				'callback' : function(res){
					get_db(context['parent']);
				}
			});
		});
		/*==/이벤트 바인딩 : 메모 삭제==*/
	};
	/*============================================
	 * 메모 딜리트
	 ============================================*/
	
	
	TB_contents.render(context, function($topEl){
		
		get_doctors();
		/*====/의사 정보 출력 ====*/
		
		get_db($topEl);//달력 출력
		
		$topEl.find('.lastMonth').on('click', function(){
			changeContext(context['obj'].month-1, $topEl);
			get_db($topEl);
		});
		/*==/이벤트 바인딩 : 지난달==*/
		
		$topEl.find('.nextMonth').on('click', function(){
			changeContext(context['obj'].month+1, $topEl);
			get_db($topEl);
		});
		/*==/이벤트 바인딩 : 다음달==*/
		
		$topEl.find('.date_container').find('td').off('click')
		.on('click', function(){
			if(!context['obj'].TB_isAdmin){return false;}
			dateText_form($(this));
		});
		/*==/이벤트 바인딩 : 달력폼 보이기 ==*/
		
		$topEl.find('.calendarText_form_wrap').find('.ST_closeBtn_1').off('click')
		.on('click', function(){
			$topEl.find('.calendarText_form_wrap').fadeOut();
		});
		/*==/이벤트 바인딩 : 달력폼닫기==*/
		
		$topEl.find('.calendarText_form_wrap').find('.deleteBtn').off('click')
		.on('click', function(){
			var $form = $topEl.find('.calendarText_form_wrap').find('form');
			TB_ajax.get_json({
				sendData : {
					'year' : context['obj'].year
					, 'month' : context['obj'].month
					, 'date' : $form.find('[name=date]').val()
					, 'reqType' : 'delete'
					, 'reqQuery' : queries.delete_dateText_reqQuery
				}
				,callback : function(res){
					TB_shared.get('templates--calendar--work_timetable--dateAftAc')();
				}
			})
			;
		});
		/*==/이벤트 바인딩 : 당직자 삭제==*/
		
		
	});
	/*=================================
	 * 랜더링
	 =================================*/

});


















