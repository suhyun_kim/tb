/*===========================
 *dec
 *  write, view, modify 를 한번에 하는 템플릿.
 * 
 * 아래의 파라메터 를 포함한 wvm_options 객체가 있을 경우 그것도포함 시킨다.
 * 
 * params
 * 	. !reqType : insert  of view or modify of 'imageView'
 * 	. _group : '그룹 '
 * 	. categories : '여러 카테고리 그룹 category 와 둘중 하나만 '
 * 	. category : '카테고리'
 * 	. no : 'no'//modify 일때 삭제를 위한 값
 * 	. none_subject : 'true'//서브젝트 숨기기
 * 	. none_memo : 'true'//메모 숨기기
 * 	. none_file: 'true'//file 숨기기
 * 	. none_hidden: 'true'//hidden 오브젝트 랜더링 안함.
 * 	. none_notice: 'true'//제목 엘리먼트 랜더링 안함
 * 	. getInfo_query : ''//modify시 가져올 정보, 없을 경우 'get_bbs_item'가 입력 
 * 	. quries(obj) : {}//reqType에 따라 처리할 쿼리를 담을 배열
 * 		- insert_reqQuery : ''  //글 작성 쿼리
 * 		- select_reqQuery : '' //글 보기 쿼리
 * 		- delete_reqQuery : '' //글 삭제 쿼리
 * 		- modify_reqQuery : '' //글 수정 쿼리
 * 	. add_form(JSON array) : [{},{},{},...] //동적으로 input 등을 생성 하기 위한 변수, JSON 구종
 * 		- type(str) : '' //동적 생성될 엘리먼트 타임
 * 		- attr(JSON array) : [{},{},...] //생성된 엘리먼트의 속성들, 'kye'와 'value'의 쌍으로 이루어져 있다.
 * 		- validation(bool) : true or false //유효성 검사 여부.
 * 	. use_modify_change(bool) :  true of alse//view에서 modify로 변경하는 기능을 사용할지 결정.
 * 
 * ======reqType : view or modify===============
 * 	. hidden : '0' or '1' //숨김 유무 
 * 	. notice : '0' or '1' //공지 유무
 * 
 * ======reqType : imageView===============
 * 이미지 뷰일 경우 받아야될 파라 메터
 * 	. !image_src : 이미지 주소
 * 	. alt : 
 * 	. title : 
 ===========================*/

TB_shared.set('templates--wvm--wvm_1--js', function(_context){
	var path = false//제대로된 접근인지 확인 하기 위한 변수
			, sharedObj = {} //공유객체가 지정되어 있을경우 그것을 담을 변수.
			, context = {}
			, $topEl
			, passwordConfirm = false//패스워드를 사용할 패스워드가
			, submit_sw = true//폼 전송시 여러번 전송 안되게 사용할 스위치
			;
	
	//return false;
	//console.log(_context);
	//console.log(_context['param']);
	
	//$(window).trigger('resize');//#TB_modal 크기 조정
	context['obj']= {};
	
	var setDevice = function(){
		context['obj'].device = (TB_config.get('isMobile'))?'mobile':'pc';//디바이스(0:PC,1:모바일)
	};
	/*====================================================
	 * 현재 디바이스 정보 셋팅
	 ====================================================*/
	
	var path_check = function(_path){
		if($.type(_path) === 'boolean'){
			path = _path;
		}
		if(!path){//제대로된 경로가 아닐 경우 닫힘
			window.history.back();
			return false;
		}
	};
	/*========================
	 * 정상 경로 판단
	 ========================*/
	
	var password_chk = function(password, callback){
		var sendData = {
					'reqType':'select'
					, 'reqQuery':'bbs_hidden_password_chk'
					, 'no' : context['obj'].no
					, '_group' : context['obj']._group||null
					, 'category' : context['obj'].category||null
					, 'password' : ''
					, 'isComment':undefined//코멘트인지 체크
				}
				, param_type = $.type(password)
				;
		if(param_type === 'string'){
			sendData.password = password;
		}else if(param_type === 'object'){
			sendData = $.extend(sendData, password);
		}
		//console.log(context['obj']);
		TB_ajax.get_json({
			'sendData': sendData
			, 'callback' : function(res){
				//console.log(res.body)
				if($.type(callback) === 'function'){ callback(res.body.chk);}
			}
		});
	};
	/*========================
	 * 패스워드 확인
	 * param
	 * 	. password(string) : 확인할 비민 번호
	 * 	. callback(function) : 비민번호가 맞을 경우 trur 아닐경우 false를 파라메터로 갖는다.
	 ========================*/

	var toggle_action = function($sw){
		var value = $sw.find('[type=radio]:checked').val()
				;
		if(value === '1'){
			$sw.find('.on_container').show().end()
			.find('.off_container').hide();
		}else if(value === '0'){
			$sw.find('.off_container').show().end()
			.find('.on_container').hide();
		}
	};
	/*========================
	 * 토글 스위치의 값을 표현해 준다. 
	 * 한번 실행시 마다 변경
	 * params
	 * 	. $sw(jqueryObj) : 토글을 감싸고 있는 랩퍼
	 ========================*/
	
	var change_toggle = function($sw){
		$sw.find('[type=radio]')
		.each(function(){
			var $this = $(this);
			if(this.checked == false){
				$this.trigger('click');
				toggle_action($sw);
				$sw = null, $this = null;
				return false;
			}
		});
	};
	/*========================
	 * 토글 스위치를 클릭없이 바꿔준다.
	 * 지정된 값을 적용하기 위해 토글을 변경해 줄때 
	 * params
	 * 	. $sw(jqueryObj) : 토글을 감싸고 있는 랩퍼
	 ========================*/
	
	var article_delete = function(){
		var $password= $topEl.find('.password')
				, password = $password.val();
		
		var password_alert = function(){//패스워드 확이 표시
			TB_func.popupDialog('패스워드를 확인하십시요');
			$password.focus();
		};
		//console.log(context['obj'].TB_isAdmin);
		
		if(!password && !context['obj'].TB_isAdmin){//패스워드 칸에 값이 없을 경우.
			password_alert();
			return false;
		}
		
		password_chk(password, function(res){
			if(res === true){//패스워드가 일치할 경우.
				if(!confirm('정말 삭제 하시겠습니까?')){return false;}
				TB_ajax.get_json({
					'sendData' : {
						'reqType':'delete'
						, 'reqQuery': context['obj'].queries.delete_reqQuery
						, 'no' : context['obj'].no
						, 'password' : password
					}
					, 'callback' : function(res){
						TB_shared.set('templates--wvm--wvm_1--reqType', 'delete');//외부에 어떤작업을 알려주기 위한 변수
						window.history.back();
					}
				});
			}else{//패스워드가 일치 하지 않을 경우.
				password_alert();
			}
		});
	};
	/*========================
	 * 메인 글을 삭제한다.
	 ========================*/
	
	var eBinding = function($scope){
		var	$form = $scope.find('.main_form') 
				, $icons = $form.find('.add_icons_wrap')
				, $deleteBtn
				, $modify_change = $icons.find('.modify_change')
				, $comment_toggle = $icons.find('.comment_toggle')
				, $comment_list_wrap = $topEl.find('.comment_list_wrap')
				;
		
		$scope.find('.closeBtn').off('click')
		.on('click', function(){
			$('#TB_modal').hide();
			window.history.back();
		});
		/*====/모달 닫기 버튼스위치 ===*/
		
		$icons.find('.switchBtn_wrap').off('click')
		.on('click', function(){
			toggle_action($(this));
		});
		/*====/토글 스위치 ===*/
		
		if($icons.length){
			$deleteBtn = $icons.find('.delete_btn');
			if($deleteBtn.length){
				$deleteBtn.off('click.delete_wvm_1')
				.on('click.delete_wvm_1', function(){
					article_delete();
				});
			}
			
		}
		/*====/메인 글 삭제 ===*/
		
		$scope.find('.file').off('change')
		.on('change', function(){
			var val = $(this).val().split("\\")||'';
			if(val.length){
				val = val[val.length-1];
			}
			$scope.find('.file_name_text').html(val);
		});
		/*====/ 파일 값 변경===*/
		
		if($modify_change.length){
			$modify_change.off('click.modify_change')
			.on('click.modify_change', function(){
				var password = $form.find('.password').val()
						, $password_input = $scope.find('.hidden_pass_input_wrap')
						;
				//console.log(password);
				if(!password&&!context['obj'].TB_isAdmin){
					TB_func.popupDialog('비밀번호를 입력하세요.');
					$form.find('.password').focus();
					return false; 
				}
				
				password_chk(password, function(chk){
					if(chk){//비밀번호가 맞을 경우
						context['obj'].reqType = 'modify';
						init();
					}else{//비밀번호가 틀렸을 경우
						TB_func.popupDialog('비밀번호를 다시 확인해 주세요.');
						$form.find('.password').val('').focus();
					}
				});
			});
		}
		/*====/ 뷰 에서 modify모드로 전환.====*/
		
		if($comment_toggle.length){
			$comment_toggle.off('click.comment_toggle')
			.on('click.comment_toggle', function(){
				var $this = $(this)
						, className = 'comment_form_active'
						, $comment_form = $scope.find('.comment_form');
				if($this.hasClass(className)){//코멘트 폼 닫기
					$scope.find('.comment_form').hide()
					.find('textarea, .wysiwyg-editor').html('');
					$this.removeClass(className)
					.find('.fa')
					.removeClass('fa-commenting').addClass('fa-commenting-o');
				}else{//코멘트 폼 열기
					//console.log(context['obj']);
					if(context['obj'].TB_isAdmin){
						$comment_form.show().end()
						.find('[name=nick]').val('관리자').end()
						.find('[name=password]').val('sdfxze154s').end()
						.find('.wysiwyg-editor').focus();
					}else{
						$comment_form.show()
						.find('[name=nick]').focus();
					}
					$this.addClass(className)
					.find('.fa')
					.removeClass('fa-commenting-o').addClass('fa-commenting')
					.promise().done(function(){//코멘트 area로 스크롤
						$('#TB_modal').scrollTop($scope.find('.comment_form').offset().top);
					});
				}
			});
		}
		/*====/코멘트 달기 토글 버튼====*/
		
		if(context['obj']['use_comment']){//코멘트
			
			$comment_list_wrap.find('.show_password_input_btn').off('click.show_password_input')
			.on('click.show_password_input', function(){
				var $this = $(this)
						, $form = $this.parent().find('.comment_del_modify_form')
						, obj = $.parseJSON($this.attr('data-comment'))
						, submit_text = (obj.type === 'delete')?'삭제':'수정'
						;
					
				$topEl.find('.comment_del_modify_form').hide().find('[name=password]').val('');//초기화
				
				$form
				.find('.comment_del_modify_submit_btn').html(submit_text).end()
				.find('[name=type]').val(obj.type).end()
				.find('[name=no]').val(obj.no).end()
				.fadeIn().find('.password').focus().end()
				;
			});/*====/코멘트: 삭제 수정시 패스워드 폼 생성====*/
			
			$comment_list_wrap.find('.hide_form_btn')
			.off('click.hide_form').on('click.hide_form', function(){
				$(this).closest('.comment_del_modify_form').hide().find('[name=password]').val('');
			});			
			/*==/코멘트 패스워드 폼 닫기==*/
			
		}
		
	};
	/*==============================
	 * 이벤트 바인딩
	 * params
	 * 	. $scope($jQuery) : 범위 오브젝트
	 ==============================*/
	
	var render_pass_chk = function(){
		var parts = "<link href='"+TB_config.get('TB_root_path')+"templates/wvm/wvm_1/wvm_1.css' ";
				parts += "rel='stylesheet' type='text/css' />";
				parts += context['parts'].getHTML('templates--wvm--wvm_1--hidden_pass_input');
		context['parent'].html(parts).promise().done(function(){
			TB.binding(context['parent']);
			TB.eBinding(context['parent']);
			context['parent'].find('form').find('input').focus();
			context['parent'].find('.ST_closeBtn_1').off('click.history_back')
			.on('click.history_back', function(){//닫기 버튼 이벤트 바인딩
				window.history.back();
			});
			$('#TB_modal').find('[name=password]').focus();
			parts = null;
		});
	};
	/*==============================
	 * hidden 글일 경우 패스워드 체크 엘리먼트를 생성. 
	 ==============================*/
	
	var get_item = function(_callback){
		//console.log(context['param']);
		var sendData ={
					'no' : context['obj'].no,
					'_group' : context['obj']._group||null,
					'category' : context['obj'].category||null,
					'reqQuery' : context['obj'].getInfo_query||'get_bbs_item'
				}
				, view_cnt//view_cnt를 올릴지 안올릴지 결정.
				;
				
		//console.log(sendData);
		
		if(context['obj'].password){//패스워드 입력에 성공했을 경우
			sendData.password =  context['obj'].password;
		}
		
		//console.log(context);
		
		if(context['obj'].reqType === 'view'){//view 일 때 처리 할 일
			if(context['obj'].use_view_count && !context['obj'].TB_isAdmin){//본적이 없으면 카운트 올리기
				view_cnt = viewed({_group:sendData._group, category:sendData.category, no:sendData.no});
				if(!view_cnt){//본적이 없을 경우는 쿠키에 저장.
					viewed({_group:sendData._group, category:sendData.category, no:sendData.no, set:true});
					sendData.hit_cnt = 'up';
				}
			}
		}
		
		TB_ajax.get_json({//데이터 받아 오기.
			'sendData' : sendData 
			,'callback' : function(res){
				//console.log(res);
				
				if(res.body){
					if(res.body.hidden === '1'&&passwordConfirm === false&&!context['obj'].TB_isAdmin){//숨김글일경우
						render_pass_chk();
						return false;
					}
					context['obj'].form_data= res.body;
					path = true;
				}
				context['obj'].readonly = (context['obj'].reqType === 'view')?"readonly":'';
				setDevice();
				path_check();
				render();
				
					//console.log(context['obj']);
				//console.log(path);
				
				if($.type(_callback) === 'function'){ _callback(res); }
				sendData = null;
			}
		});
	};
	/*==============================
	 * 글 가져오기.
	 * params
	 * 	. $scope($jQuery) : 범위 오브젝트
	 ==============================*/
	
	var dynamic_el = function($topEl){
		$topEl.find('.dynamic_el').each(function(){
			var $this = $(this)
					, src = $this.text()
					;
			$this.html($('<div/>').html(src).html()).parent().removeClass('ST_visibility_hidden');
		});
	};
	/*==============================
	 * 동적으로 엘리먼트 추가
	 ==============================*/
	
	var render = function(){
		//console.log(context['obj']);
		TB_contents.render(context, function(_$topEl){
			$topEl = _$topEl;
			dynamic_el($topEl);//동적 엘리먼트 생성
		
			if(context['obj'].reqType === 'imageView'){
				//console.log(TB_shared.get(context['param'].sharedObj));
				
				var $image_cover = $topEl.find('.image_cover');
				$image_cover.find('img').load(function(){
					TB_ani.flow('fadeIn', $image_cover);
				});
				
			}else if(context['obj'].reqType === 'modify' || context['obj'].reqType === 'view'){//수정 or view일 경우
				if(context['obj'].reqType === 'modify'){
					var $toggle_cover = undefined;
					$topEl.find('.TB_form').find('.submit').val('수정');
					
					if(context['obj'].none_hidden != 'true' && context['obj'].hidden== '1'){//숨김으로 되어 있을 시 값설정.
						$toggle_cover =$topEl.find('.main_form').find('.hidden_cover'); 
					}
					
					if(context['obj'].none_notice != 'true' && context['obj'].notice == '1'){//공지사항
						$toggle_cover =$topEl.find('.main_form').find('.notice_cover'); 
					}
					
					if($toggle_cover){
						change_toggle($toggle_cover);
					}
					
					$toggle_cover = null;
				}
				//console.log(context['obj']);
				TB_form.autoInsert($topEl.find('.main_form'), context['obj'].form_data);
				//console.log($topEl.find('.main_form').val());
				TB_ani.flow('fadeIn', $topEl);
				
			}else{//insert 같은 경우
				TB_ani.flow('fadeIn', $topEl, function(){
					if($topEl.find('[name=subject]').length){
						$topEl.find('[name=subject]').focus();
					}
				});
				TB_form.binding($topEl);
				TB_form.eBinding($topEl);
			}
			
			context['parts'].func();
			
			eBinding($topEl);
		});
		
	};
	/*==============================
	 * 랜더링
	 ==============================*/
	
	$.extend(true, context, _context);
	//console.log(context['obj']);
	if(context['param'].sharedObj){//GET방식이 아니라 TB_shared에 정의된 것이 있으면 가져다 쓴다.
		sharedObj = TB_shared.get(context['param'].sharedObj);
		if(sharedObj){
			$.extend(context['obj'], sharedObj);
			if(sharedObj.wvm_options){//wvm옵션이 정의 되어 있다면 그것을 추가로 가져다 쓴다.
				$.extend(context['obj'], sharedObj.wvm_options);
			}
		}
	}
	
	$.extend(context['obj'], context['param']);//파라메터가 우선된다.
	
	//wvm에서 어떤 작업을 했는지 외부에 알려 주기 위한 외부 노출 변수.
	TB_shared.set('templates--wvm--wvm_1--reqType', context['param'].reqType+'__'+context['param'].sharedObj);
	/*==============================
	 * 사용해야할 변수 셋팅 
	 ==============================*/
	
	var set_query = function(type){
		//console.log(context['obj'].queries);
		//console.log('테스트');
		if(!context['obj'].queries && !(type === 'imageView'||type === 'view')){//쿼리가 없을 경우 돌려 보낸다.
			throw 'wvm_1 Error \n not defined queries or not defined type';
			path_check(false);
			return false;
		}
		if(type === 'insert'){
			if(!context['obj'].queries.insert_reqQuery){
				throw 'wvm_1 Error \n not defined queries in insert reqQuery';
				path_check(false);
				return false;
			}
			context['obj'].reqQuery = context['obj'].queries.insert_reqQuery;
		}else if(type === 'modify'){
			// console.log(context['obj']);
			if(!context['obj'].queries.modify_reqQuery){
				throw 'wvm_1 Error \n not defined queries in modify reqQuery';
				path_check(false);
				return false;
			}
			context['obj'].reqQuery = context['obj'].queries.modify_reqQuery;
		}
		//console.log('테스트');
	};
	/*==============================
	 * queries에서 reqQuery셋팅 
	 ==============================*/
	
	var insert_wysiwyg = function($form, html){
		$form.find('[name=memo]').val(html).end()
		.find('.wysiwyg-editor').html(html);
		$main_form = null;
	};
	/*==============================
	 * memo 내에 html 삽입
	 ==============================*/
	
	var get_async_images = function($form){
		var arr = [];
		//console.log($form.find('.async_image').length);
		$form.find('.async_image').each(function(){
			var $this = $(this);
			arr.push({
				'images' : $this.attr('data-async_images')
				, 'images_name' : $this.attr('title')
				, 'images_thumbnail_src' : $this.attr('data-async_images_thumbnail_src')
			});
			$this= null;
		});
		return arr;
	};
	/*==============================
	 * memo 내에 삽입된 async_image 객체 반환
	 ==============================*/
	
	
	TB_shared.set('templates--wvm--wvm_1--async_file_form_before', function(data, $form){
		//console.log(res);
		return true;
	});
	
	TB_shared.set('templates--wvm--wvm_1--async_file_form_aft', function(res){
		//console.log('aft',res);
		var $main_form = context['parent'].find('.main_form');
		
		if(res.async_image === true){//비동기 위지윅 일 경우
			var memo_val = $main_form.find('[name=memo]').val()
					, img = ''
					;
			img += "<img src='"+context['obj'].TB_root_path+res.file_src+"' ";
			img += " title='"+res.file_name+"' ";
			img += " data-async_images='"+res.file_src+"' ";
			img += " data-async_images_thumbnail_src='"+res.thumbnail_src+"' ";
			img += " class='async_image' ";
			img += " style='max-width:90%;' ";
			img += "/>";
			
			//메모내에 이미지 삽입
			insert_wysiwyg($main_form, memo_val+img);
		}
		submit_sw = true;
	});
	/*==============================
	 * async_file_form 전송 후 함수 
	 ==============================*/
	
	context['parts'].func('add_icons_wrap', function($el){
		var $memo_in_image_btn = $el.find('.add_memo_in_image_btn_wrap');
			//console.log('파츠실행');
		
		$el.find('.add_memo_in_image_btn_wrap').off('click.memo_in_image')
		.on('click.memo_in_image', function(){
			if(submit_sw  == false){return false;}
			submit_sw = false;
			context['parent'].find('.another_form_wrap').find('[type=file]').trigger('click');
		});
		/*==/메모내 이미시 삽입 버튼클릭 ==*/
		
		context['parent'].find('.another_form_wrap').find('[name=file]').off('change.async_image')
		.on('change.async_image', function(){
			//console.log('파일명 변경');
			$(this).closest('form').find('[type=submit]').trigger('click');
		});
		/*==/메모내 이미시 삽입: 파일 내용이 변경 되었을 때 서브밋 버튼 클릭. ==*/
		
	});
	/*==============================
	 * 추가 기능 아이콘 들
	 ==============================*/
	
	TB_shared.set('templates--wvm--wvm_1--formBeforeAc', function(data, $form){
		//console.log('st');
		if(context['obj'].use_memo_in_image){//비동기로 이미지 업로드하기.
			var add_arr = get_async_images($form)
					, images = '' ,images_name = '', images_thumbnail_src = ''
					, len = add_arr.length
					;
			if(!len){
				TB_func.popupDialog('이미지를 추가해 주십시요.');
				return false;
			}
			for( var i = 0; i < len ; i++){
				if(i ===0){
					images = add_arr[i].images;
					images_name = add_arr[i].images_name;
					images_thumbnail_src = add_arr[i].images_thumbnail_src;
				}else{
					images += '||'+add_arr[i].images;
					images_name += '||'+add_arr[i].images_name;
					images_thumbnail_src += '||'+add_arr[i].images_thumbnail_src;
				}
			}
			data.push({
				'name' : 'images'
				, 'value' : images
			});
			data.push({
				'name' : 'images_name'
				, 'value' : images_name
			});
			data.push({
				'name' : 'images_thumbnail_src'
				, 'value' : images_thumbnail_src
			});
			
			data.push({'name':'use_memo_in_images','value':'true'});
		}
		//console.log('dfasf');
		return true;
	});
	/*========================
	 * main_form before
	 ========================*/
	
	TB_shared.set('templates--wvm--wvm_1--formAftAc', function(res){
		context = null;
		sharedObj = null;
		path = null;
		TB_page.reload('activePage');
		window.history.back();
	});
	/*========================
	 * 폼 전송 후 aftAc 셋팅
	 * dec 
	 * 	너무 일찌 리로드 시키면 내용이 갱신이 안된다.
	 ========================*/
	
	TB_shared.set('templates--wvm--wvm_1--hidden_pass_input', function(data){
		password_chk(data[0].value, function(chk){
			if(!chk){//패스워드 틀림
				TB_func.popupDialog('패스워드를 확인해 주십시요.');
				context['parent'].find('form')[0].reset();
			}else{//패스워드 맞음.
				passwordConfirm = true;
				context['obj'].password = data[0].value;
				get_item();
			}
		});
		return false;
	});
	/*========================
	 * 비밀글일 경우 패스워드 확인 절차
	 ========================*/
	
	var get_comment_list = function(_callback){
		//console.log(context['obj']);
		var sendData = {
					'reqType': 'select'
					, 'reqQuery':context['obj'].queries.comment_select_reqQuery
					, 'parent_no' : context['obj'].no
					, 'page':1
				};
		TB_ajax.get_json({
			'sendData' : sendData
			, 'callback' : function(data){
				if($.type(_callback) === 'function'){_callback(data);}
			}
		});
	};
	/*==============================
	 * 답글 리스트 정보 가져오기.
	 ==============================*/
	
	var comment_render = function(){
		if(!context['obj'].use_comment){return false;}
		//console.log(TB_shared.get(res.body.sharedObj));
		get_comment_list(function(res){
			context['obj'].comment_list = res.body.comment_list;
			context['parts'].render({
				'parts':'templates--wvm--wvm_1--comment_list'
				, 'obj' : context['obj']
				, 'callback' : function(){
					var $comment_list_wrap = $topEl.find('.comment_list_wrap')
							, $comment_list_container = $comment_list_wrap.find('.comment_list_container');
					//console.log($comment_list_container.css('display'));
					TB_ani.flow('fadeIn', $comment_list_container);
					TB.eBinding($comment_list_wrap);
					TB.binding($comment_list_wrap);
					eBinding($topEl);
					$comment_list_wrap = null;
				}
			});
		});	
	};
	/*==============================
	 * 코멘트 리스트 랜더링 
	 ==============================*/
	
	TB_shared.set('templates--wvm--wvm_1--comment_form_input', function(data, $form, c){
		$form.find('.wysiwyg-editor').html('');//폼 클리어
		
		if(data.insert_success){//
			
			comment_render();
			//console.log($topEl.find('.add_icons_wrap').find('.comment_toggle').length);
			$topEl.find('.add_icons_wrap').find('.comment_toggle ').trigger('click');//코멘트 폼 닫기.
			TB_shared.set('templates--wvm--wvm_1--reqType', 'comment_insert');//코멘트 작성 외부에 알려주기 위한 변수.
		}else{
			TB_func.popupDialog('코멘트 작성에 실패 하였습니다.');
		}
	});
	/*================================================
	 * 답글쓰기
	 ================================================*/
	
	TB_shared.set('templates--wvm--wvm_1--comment_list_modify', function(res){
		TB_shared.set('templates--wvm--wvm_1--reqType', 'comment_modify');//코멘트 작성 외부에 알려주기 위한 변수.
		comment_render();
	});
	/*==============================
	 * 코멘트 수정 
	 ==============================*/
	
	var comment_delete = function(obj, callback){
		var sendData = {
					'reqType':'delete'
					, 'reqQuery': ''
					, 'no' : undefined
					, 'password' : ''
					, 'isComment':undefined//코멘트인지 체크
				}
				;
		sendData.reqQuery = context['obj'].queries.comment_delete_reqQuery; 
		sendData = $.extend(sendData, obj);
		//console.log(sendData);
		//return false;
		TB_ajax.get_json({
			'sendData': sendData
			, 'callback' : function(res){
				TB_shared.set('templates--wvm--wvm_1--reqType', 'comment_delete');//코멘트 작성 외부에 알려주기 위한 변수.
				if($.type(callback) === 'function'){ callback(res.body.chk);}
			}
		});
	};
	/*==============================
	 * 코멘트 db에서 삭제 
	 ==============================*/
	
	TB_shared.set('templates--wvm--wvm_1--comment_list_del_modify', function(data, $form){
		var obj = {
					type : data[0].value
					, no : data[1].value
					, password : data[2].value
					, isComment : true
				}
				, $modify_form = $form.closest('li').find('.comment_modify_form_wrap')
				;
		
		$topEl.find('.comment_modify_form_wrap').find('.modify_btn_wrap').hide();
		
		password_chk(obj, function(res){
			if(obj.type === 'delete' && res){//삭제시 패스워드 일치
				if(!confirm('정말 삭제 하시 겠습니까?')){return false;}
				$form.closest('li').remove();
				delete obj.type;
				comment_delete(obj);
			}else if(obj.type === 'modify' && res){//수정
				$form.parent().hide().find('.password').val('');
				$modify_form.find('.memo').attr('data-TB_form_set', 'wysiwyg:modify').end()
				.find('[name=password]').val(obj.password);
				TB_form.binding($modify_form);
				$modify_form.show().find('.wysiwyg-editor').focus().end()
				.find('.modify_btn_wrap').show();
			}else{//패스워드가 안 맞을 경우.
				TB_func.popupDialog('패스워드를 다시 확인하십시요.');
				$form.find('.password').val('').focus();
			}
		});
		return false;
	});
	/*==============================
	 * 코멘트 삭제 , 수정 전환 시 패스워드 체크
	 ==============================*/
	
	context['parts'].func('list_move_btn', function($el){
		if(context['obj'].use_list !== true || $.type(context['obj'].list) !== 'array' ){
			return false;
		}
		
		//무브 버튼 show & hide 설정
		var show_hide_btn = function(){
			if(context['obj'].list_index <= 0){
				$el.find('.next_list_btn').fadeIn().end()
				.find('.prev_list_btn').hide();
			}else if(context['obj'].list_index >=  context['obj'].list.length-1){
				$el.find('.next_list_btn').hide().end()
				.find('.prev_list_btn').fadeIn();
			}else{
				$el.find('.next_list_btn').fadeIn().end()
				.find('.prev_list_btn').fadeIn();
			}
		};
		
		//이미지 를 셋팅할 함수
		var set_item = function(index){
			var selected//선택된 리스트를 담을 객체
					;
			selected = context['obj'].list[index];
			context['obj'].image_src = TB_config.get('TB_root_path')+selected.file_src;
			context['obj'].header = selected.subject;
			context['obj'].list_index = index;
			init();
			selected = null;
		};
		
		var eBinding = function(){
			TB_ani.ready($el, function(){
				TB_ani.flow('fadeIn', $el, function(){
					$el.find('.next_list_btn').off('click.wvm1ListMoveNext')
					.on('click.wvm1ListMoveNext', function(){
						set_item(parseInt(context['obj'].list_index, 10)+1);
					}).end()
					.find('.prev_list_btn').off('click.wvm1ListMovePrev')
					.on('click.wvm1ListMovePrev', function(){
						set_item(parseInt(context['obj'].list_index, 10)-1);
					});
				});
			});
		};
		
		if(context['obj'].reqType == 'imageView'){
			context['parent'].find('img').load(function(){
				show_hide_btn();
				eBinding();
			});
		}else{
			show_hide_btn();
			eBinding();
		}
		
		//console.log(context);
	});
	/*==/리스트 이동 ==*/
	
	/*==============================
	 * wvm에서 리스트 이동.
	 ==============================*/
	
	var viewed = function(_obj){
		var obj = {
					_group : '' ,  no : '' , category : '' , set : false
				}
				, cookie_name//검색할 쿠키명
				, search_val//검색 값
				, data //검색되어진 쿠키값
				;
		
		obj = $.extend(obj, _obj);
		
		cookie_name = 'wvm_1_'+obj._group+'_viwed';
		search_val = obj._group+'_'+obj.category+'_'+obj.no;
		
		//console.log(cookie_name, search_val);
		data = $.cookie(cookie_name);
		if(obj.set){//셋팅할때
			if(!data){data = '';}
			$.cookie(cookie_name, data+','+search_val,{expires:7,path:'/'});
			data = true;
		}else{//값을 돌려 줄때
			if(!data){return false;}
			data = data.split(',');
			data = (data.indexOf(search_val) < 0)?false:true;
		}
		
		cookie_name =null, obj = null, search_val= null;
		return data;
	};
	/*==============================
	 * 해당하는 글을 본적이 있는지 확인
	 * param
	 * 	. obj._group(string) : 그룹명
	 * 	. obj.category(string) : category 명
	 * 	. obj.no(string) : no
	 * 	. obj.set(bool) : true 일경우 해당 값을 셋팅하고 false 일경우 찾아서 내보낸다. 기본값 false
	 * return 
	 * 	. True or False(bool) : 저장된 쿠키에 값이 true를 없을 경우 false를 리턴.
	 ==============================*/
	
	var init = function(){
		//console.log(context['obj']);
		set_query(context['obj'].reqType);
		if(context['obj'].reqType  === 'insert'){//reqType에 따라 쿼리를 골라 사용한다, 미리 해당 오브젝트에 쿼리가 정의 되어 있어야 한다.
			setDevice();
			render();
		}else if(context['obj'].reqType  === 'modify'){//수정일 경우
			get_item();//데이터를 가져와 랜더링
		}else if(context['obj'].reqType === 'view'){//일반 뷰'
			
			get_item(function(){
				comment_render();
			});
		}else if(context['obj'].reqType === 'imageView'){
			render();
		}else{
			path_check(false);
		}
	};
	/*==============================
	 * 초기화  
	 ==============================*/
	
	TB_auth.isAdmin(function(res){//관리자로 로그인되어 있는 상태인지 확인.
		//console.log(res);
		context['obj'].TB_isAdmin = res;
		//console.log(context['obj']);
		
		init();//초기화
		//console.log(context['obj']);
	});
	
});
















































