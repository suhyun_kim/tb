/*======================================================
 * 팝업 띄우기
 * params
 * 	. type : 'show' or 'confirm'  of 'alert' 기본값은 'show'
 * 	. title : ''//제목
 * 	. subtitle : ''//부제목
 * 
 * ===========/common ============
 * 
 * 	. template : '<input/>'  //폼 생성시 사용될 것
 * 	. submitBtnText : ''//확인 버튼 텍스트
 * 	. cancelBtnText : ''//캔슬버튼 텍스트
 * 	. callback(function) : ''//TB_form.beforeAc와 같은 내용을 param으로 넘김.
 * ===========/show ============
 ======================================================*/

TB_shared.set('templates--popup--popup_1--js', function(context){
	
	console.log(context);
	
	TB_shared.set('templates--popup--popup_1--show', function(options){
		TB_contents.render(context);
	});
	
});
