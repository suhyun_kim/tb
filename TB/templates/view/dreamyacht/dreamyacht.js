/*===========================
 *dec
 *  write 전용 템플릿 wvm으로 하다 보니 너무 코드가 복잡해져서 따로 분리 시켰다.
 * 
 * params
 * 
 ===========================*/

TB_shared.set('templates--view--dreamyacht--js', function(context){
	var param = context.param||{}
		, reqQuery = param.reqQuery 
		, no = param.no
		, category = param.category
		, user = TB_config.get('user')
		, now = new Date().format('yyyy-MM-dd')
		, $TB_modal = $('#TB_modal')
		;
	
	//console.log(reqQuery, no, category);
	
	context['obj'] = {};
	
	context.obj.device = TB_config.get('device');
	
	context.obj.is_hidden = false;//숨김글을 돌파 여부
	
	context.obj.render = {};
	context.obj.render.comment_list = [];//코멘트리스트
	
	context.obj.comment = {};
	context.obj.comment.use = false;
	context.obj.comment.writetime_disabled = 'disabled';
	context.obj.comment.use_password = true;
	context.obj.comment.reqType = 'insert';
	context.obj.comment.reqQuery = 'comment';
	context.obj.comment.no = '';
	context.obj.comment.parent_no = no;
	context.obj.comment.nick = user&&user.nick;
	context.obj.comment.writetime = now.split(' ')[0];
	context.obj.comment.memo = '';
	context.obj.comment.submit = '입  력';
	
	//답글 기능을 활성화 할지 안할지
	if(reqQuery === 'postscript_item' || reqQuery === 'qna_item'){
		context.obj.comment.use = true;
	}
	if(user&&user.member_grade === 'supervisor'){
		context.obj.comment.writetime_disabled = '';
		context.obj.comment.use_password = false;
	}
	
	/*============================================
	 * 변수설정
	 ============================================*/
	
	var delete_comment = function(no, password){
		var no = no
			, password = password
			;
		//console.log(no);
		// //console.log(password);
		TB_ajax.get_json({
			sendData : {
				reqType : 'delete'
				, reqQuery : 'comment'
				, no : no
				, password : password
			}	
			, callback : function(res){
				if(!res.body){
					TB_func.popupDialog('비밀번호를 확인하세요.');
					return false;
				}
				TB_func.json_arr_delete(context.obj.render.comment_list, 'no',no);
				//console.log(context.obj.render.comment_list);
				context.parts.render('comment_list');
			}
		});
	};
	/*==//==*/
	
	var set_comment_var = function(type, item){
		
		if(user&&user.member_grade === 'supervisor'){
			context.obj.comment.writetime_disabled = '';
			context.obj.comment.use_password = false;
		}else{
			context.obj.comment.writetime_disabled = 'disabled';
			context.obj.comment.use_password = true;
		}
		
		if(type === 'modify'){
			context.obj.comment.reqType = 'modify';
			context.obj.comment.no = item.no;
			context.obj.comment.nick = item.nick;
			context.obj.comment.writetime = item.writetime;
			context.obj.comment.memo = item.memo;
			context.obj.comment.submit = '수  정';
			return false;
		}
		
		context.obj.comment.reqType = 'insert';
		context.obj.comment.reqQuery = 'comment';
		context.obj.comment.no = '';
		context.obj.comment.parent_no = no;
		context.obj.comment.nick = (user&&user.nick)||'';
		context.obj.comment.writetime = now.split(' ')[0];
		context.obj.comment.memo = '';
		context.obj.comment.submit = '입  력';
		//console.log(context.obj);		
	};
	/*===/ 코멘트 변수설정 /===*/
	
	var modify_input = function(no){
		var no = no
			, item
			, list = context.obj.render.comment_list
			, i = 0, len=  list.length
			;
		
		for(;i < len ; i++){
			if(no == list[i].no){
				item  = list[i];
				break;
			}
		}
		
		set_comment_var('modify', item);
		
		context.parts.render('temp--view_comment_form', function($el){
			//TB_shared.get('$window').scrollTop(TB_shared.get('documentHei'));
			//TB_shared.get('$window').scrollTop(500);
			$TB_modal.scrollTop($('.templates--view--dreamyacht').height());
		});
	};
	/*==/코멘트 수정폼에 입력 및 스크롤 이동/==*/
	
	
	TB_shared.set('temp--view--dreamyacht--comment_form_aft', function(res){
		
		if(res.result != 'success'){
			set_comment_var();//코멘트폼 초기화
			context.parts.render('temp--view_comment_form');
			return false;
		}
		
		if(context.obj.comment.reqType === 'insert'){
			context.obj.render.comment_list = res.list;
		}else if(context.obj.comment.reqType === 'modify'){
			TB_func.json_arr_update(context.obj.render.comment_list, res.item, 'no');
		}
		
		
		set_comment_var();//코멘트폼 초기화
		context.parts.render('comment_list');
		context.parts.render('temp--view_comment_form');
	});
	/*==/코멘트 작성 및 수정 폼/==*/
	
	var go_prev_page = function(req_query){
		var prev_page = '#!/index';
		
		//console.log(context.param);
		if(req_query === 'yacht_sale_item'){
			prev_page = '#!/sale?category=all';
		}else if(req_query === 'qna_item'){
			prev_page = '#!/community_qna';
		}else if(req_query === 'notice_item'){
			prev_page = '#!/community_notice';
		}else if(req_query === 'postscript_item'){
			prev_page = '#!/community_postscript';
		}
		
		//console.log(prev_page);
		location.href = prev_page;
	};
	/*==/이전 페이지 이동하기./==*/
	
	
	var get_detail = function(reqQuery, category, no, callback){
		TB_ajax.get_json({
			sendData : {
				reqQuery : reqQuery
				, category : category
				, no : no
			}
			, callback : function(data){
				if($.type(callback) === 'function'){callback(data.body);}
			}
		});
	};
	/*== / 세부내역 가져오기 ==*/
	
	
	TB_shared.set('temp--view--dreamyacht--aft', function(res){
		//console.log(res);
		if(!res){
			TB_func.popupDialog('패스워드가 틀렸습니다.');
			return false;
		}
		context.obj.is_hidden = false;
		context.obj.render = res;
		TB_contents.render(context);
	});
	/*== / 세부내역 가져오기 ==*/
	
	
	
	/*============================================
	 * 함 수
	 ============================================*/
	
	
	context.parent.off('click.back_history', '.closeBtn')
	.on('click.back_history', '.closeBtn', function(){
		go_prev_page(reqQuery);
		//history.back();
	});
	
	
	context.parts.func('comment_list', function($el){
		
		//코멘트 수정 버튼 클릭
		$el.find('.modify_btn').off('click.modify')
		.on('click.modify', function(){
			modify_input(this.getAttribute('data-no'));
		});
		
		//코멘트 삭제 버튼
		$el.find('.delete_btn').off('click.delete')
		.on('click.delete', function(){
			var password = '';
			
			if(user && user.member_grade === 'supervisor'){
				if(!confirm('정말삭제 하시겠습니까?')){return false;}
			}else{
				password = prompt('비밀번호를 입력 하세요.');
				if(!password){
					TB_func.popupDialog('비밀번호를 입력하세요.');
					return false;
				}
			}
			//if(user && user.member_grade != 'supervisor' && !password){return false;}
			delete_comment(this.getAttribute('data-no'), password);
		});
		
	});
	/*==/코멘트 리스트/==*/
	
	context.parts.func('temp--view_comment_form', function($el){
		//입력모드 변경
		$el.find('.insert_mode').off('click.insert_mode')
		.on('click.insert_mode', function(){
			set_comment_var();//코멘트폼 초기화
			context.parts.render('temp--view_comment_form');
		});

	});
	/*==/코멘트 폼/==*/
	
	/*============================================
	 * 이벤트
	 ============================================*/
	
	
	//디테일 정보를 받아온뒤 랜더링
	get_detail(reqQuery, category, no, function(data){
		//console.log('get_detail_aft : ', data);
		if(data === false){
			context.obj.render.subject = '비밀번호입력';
			context.obj.form = {};
			context.obj.form.reqQuery = reqQuery;
			context.obj.form.category = category;
			context.obj.form.no = no;
			context.obj.is_hidden = true;
		}else{
			data.writetime = data.writetime.split(' ')[0];
			context.obj.render = data;
			context.obj.is_hidden = false;
		}
		TB_contents.render(context, function(){
			context.parts.render();
		});
		
	});
	/*============================================
	 * 컨트롤러
	 ============================================*/
});
















































