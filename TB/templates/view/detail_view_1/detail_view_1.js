/*===========================
 *dec
 *  write 전용 템플릿 wvm으로 하다 보니 너무 코드가 복잡해져서 따로 분리 시켰다.
 * 
 * params
 * 
 ===========================*/

TB_shared.set('templates--view--detail_view_1--js', function(context){
	context['obj'] = {};
	
	/*============================================
	 * 변수설정
	 ============================================*/
	
	
	
	
	
	
	var convert_render_data= function(_data){
		var data = _data
				, i = 0
				;
				
		data.stars_point = [];
				
		for(;i < data.stars_cnt ; i++){
			data.stars_point[i] = {};
			data.stars_point[i].class_name = 'star_point_'+(i+1);
		}
		
		return data;
	};
	/*==/리스트 배열에 랜더링에 사용될 값 추가./==*/
	
	var get_detail = function(category, no, callback){
		TB_ajax.get_json({
			sendData : {
				reqQuery : 'get_detail_story'
				, category : category
				, no : no
			}
			, callback : function(data){
				context.obj.render = data.body;
				if($.type(callback) === 'function'){callback(data);}
			}
		});
	};
	/*== / 세부내역 가져오기 ==*/
	
	var validate = function(param){
		if($.type(parseInt(param.no,10)) != 'number'){return false;}
		if($.type(param.category) != 'string'){return false;}	
	};
	/*== / 유효성 검사 ==*/
	
	
	var create_map = function(_data, $output){
		var $map_wrap = $output
				, data = _data
				, latlng = []
				, k = 0, i = 0, len = data.length
				, map
				, map_options = {
					center : undefined
					, level : 3
				}
				, location_info = []
				, marker, position_info = [], polyline
				, geocoder = new daum.maps.services.Geocoder()
				;
		//좌표, 시간 추출
		for(; i < len ; i++){
			if(data[i].lat ){
				latlng[k] = {};
				latlng[k].originalname = data[i].originalname;
				latlng[k].lat = data[i].lat;
				latlng[k].lng = data[i].lon;
				latlng[k].position = new daum.maps.LatLng(latlng[k].lat, latlng[k].lng);
				//console.log(data[i])	;
				//console.log(geocoder.coord2addr(latlng[k].position));
				if(data[i].pictured_time){
					latlng[k].pictured_time = data[i].pictured_time;
					latlng[k].pictured_date = TB_func.convert_string_date(data[i].pictured_time);
				}
				k++;
			}
		}
		
		if(!latlng.length){
			return false;
		}
		
		//시간순 정렬
		latlng = TB_func.json_sort(latlng, 'pictured_date');
		
		//센터 설정
		map_options.center = new daum.maps.LatLng(latlng[0].lat, latlng[0].lng);
		
		//맵 생성
		map = new daum.maps.Map($map_wrap[0],map_options);
		
		//지도 중심 영역
		var bounds = new daum.maps.LatLngBounds();
		
		//줌 컨트롤러 생성
		map.addControl(new daum.maps.ZoomControl(), daum.maps.ControlPosition.RIGHT);
		
		i = 0, k = 0, len = latlng.length; 
		
		//마커 생성, 주소저장, 포지션 객체 생성
		for(;i<len;i++){
			
			//포지션 정보
			position_info[i] = latlng[i].position;
			
			bounds.extend(latlng[i].position);
			
			//주소 저장.
		 	geocoder.coord2detailaddr(latlng[i].position, function(status, result){
		 		var title = '';
				latlng[k].addr = result[0];
				
				//console.log(latlng[k]);
				title += '파일명 : '+latlng[k].originalname+'\n';
				title += '장   소 : '+latlng[k].addr.jibunAddress.name+'\n';
				title += '시   간 : '+latlng[k].pictured_time;
				
				//마커 생성
				marker  = new daum.maps.Marker({
					map : map
					, position : latlng[k].position
					, title : title
				}); 
				
				bounds.extend(latlng[k].position);
				
				k++;
				
				if(len === k ){
					map.setBounds(bounds);//중심좌표 설정
					map.setZoomable(false);//줌 막기 
				}
			});
		}
		
		//선셋팅
		polyline = new daum.maps.Polyline({
			path : position_info
			, strokeWeight : 3//선의 두께
			, strokeColor : '#539AF1'//선의 색상
			, strokeOpacity : 0.7 //선의 불투명도
			, strokeStyle : 'solid'//선의 타입
		});
		
		polyline.setMap(map);
		
		return latlng;
	};
	/*== / 맵 로드==*/
	
	/*============================================
	 * 함 수
	 ============================================*/
	
	
	
	
	
	context.parent.off('click.back_history', '.closeBtn')
	.on('click.back_history', '.closeBtn', function(){
		history.back();
	});
	/*============================================
	 * 이벤트
	 ============================================*/
	
	
	
	
	
	//유효성 검사
	if(validate(context.param) === false){location.replace('#!/index');}
	
	TB_func.fall([
		//세부 정보 받아오기.
		function(next, param){
			get_detail(context.param.category, context.param.no, function(data){
				context.obj.render = convert_render_data(context.obj.render);
				next(data);
			});
		}
		//랜더링
		, function(next, param){
			TB_contents.render(context, function($topEl){
				var is_map = false;
				if(context.obj.render.images_JSON 
						&&(context.obj.TB_userInfo.member_grade === 'supervisor'
						||context.obj.TB_userInfo.id === param.body.writer_id
						)
					){
					is_map = create_map(JSON.parse(context.obj.render.images_JSON), context.parent.find('.map_container'));
					//맵이 없을 경우 삭제
					if(!is_map){
						context.parent.find('.map_wrap').remove();
					}
				}
			});
		}
	]);
	//디테일 정보를 받아온뒤 랜더링
	/*============================================
	 * 컨트롤러
	 ============================================*/
});
















































