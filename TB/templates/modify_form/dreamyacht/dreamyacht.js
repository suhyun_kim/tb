/*===========================
 *dec
 *  write 전용 템플릿 wvm으로 하다 보니 너무 코드가 복잡해져서 따로 분리 시켰다.
 * 
 * params
 * 
 ===========================*/

TB_shared.set('templates--modify_form--dreamyacht--js', function(context){
	var tl = new TimelineLite()
		, insert_sw = false
		, param = context.param
		, images_json = []//이미지 정보를 담고 있다가 업로드 시에 업로드 한다.
		, user = TB_config.get('user')
		;
	
	context['obj'] = {};
	
	context.obj.confirm = (user&&user.member_grade === 'supervisor')?true:false;
	
	
	context.obj.render = {};
	context.obj.render.reqQuey = param.reqQuery;
	context.obj.render.subject = '';
	context.obj.render.memo = '';
	context.obj.render.no = param.no;
	context.obj.render.nick = '';
	context.obj.render.writetime = '';
	context.obj.render.category = param.category;
	context.obj.render.passwod = '';
	context.obj.render.hidden_checked = '';
	context.obj.writetime_disabled = '';
	
	//console.log(param);
	if(context.obj.confirm != true){
		context.obj.writetime_disabled = 'disabled';
	}
	
	console.log(context.obj.writetime_disabled);
	
	
	if(param.reqQuery === 'yacht_sale'){
		context.obj.selected_category = param.category||'요트';
		context.obj.categories = [
			{name : '요트', value : '요트'}
			, {name : '보트', value : '보트'}
		];
		context.obj.device = TB_config.get('device');
		param.query_get_item = 'yacht_sale_item';
		//console.log(param);
	}else if(param.reqQuery === 'notice'){
		param.query_get_item = 'notice_item';
	}else if(param.reqQuery === 'postscript'){
		param.query_get_item = 'postscript_item';
	}else if(param.reqQuery === 'qna'){
		param.query_get_item = 'qna_item';
	}

	/*============================================
	 * 변수설정
	 ============================================*/
	
	var delete_item = function(no, nick, password){
		//console.log(arguments);
		if(!confirm('정말삭제 하시겠습니까?')){return false;}
		TB_ajax.get_json({
			sendData : {
				reqQuery : 'bbs_item'
				, reqType : 'delete'
				, no : no
				, nick : nick 
				, password : password
			}
			, callback : function(res){
				//console.log('delete_item_res : ',res);
				go_prev_page(context.param.reqQuery);
			}
		});
	};
	/*==/삭제하기/==*/
	
	var go_prev_page = function(req_query){
		var prev_page = '#!/index';
		
		if(req_query === 'yacht_sale'){
			prev_page = '#!/sale?category=all';
			
		}else if(req_query === 'qna'){
			prev_page = '#!/community_qna';
			
		}else if(req_query === 'notice'){
			prev_page = '#!/community_notice';
			
		}else if(req_query === 'postscript'){
			prev_page = '#!/community_postscript';
		}
		
		//console.log(context.param, req_query, prev_page);
		//console.log(prev_page);
		location.replace(prev_page);
		//location.href = prev_page;
	};
	/*==/이전 페이지 이동하기./==*/

	
	var get_item= function(_obj){
		var obj = {
				reqQuery : ''
				, no : ''
				, category : ''
				, callback : undefined
			}
			;
			
		obj = $.extend(obj, _obj);
		
		TB_ajax.get_json({
			sendData : {
				reqQuery : obj.reqQuery+'_item'
				, no : obj.no
				, category : obj.category
			}
			, callback : function(res){
				//console.log('get_item () : ', res.body);
				
				if(res.body){//패스워드 확인되었을 경우
					images_json = (res.body.images_JSON)?JSON.parse(res.body.images_JSON):[];
					context.obj.render.nick = res.body.nick;
					context.obj.render.writetime = (res.body.writetime)?res.body.writetime.split(' ')[0]:'';
					context.obj.render.subject = res.body.subject;
					context.obj.render.memo = res.body.memo;
					context.obj.render.hidden_checked = (res.body.hidden === '1')?'checked':'';
					//context.obj.render.hidden = 'on';
				}
				
				
				if($.type(obj.callback) === 'function'){
					obj.callback(res.body);
				}
				//console.log(res);
			}
		});
	};
	/*==/아이템 가져오기/==*/
	
	
	function placeCaretAtEnd(el) {
	    el.focus();
	    if (typeof window.getSelection != "undefined"
	            && typeof document.createRange != "undefined") {
	        var range = document.createRange();
	        range.selectNodeContents(el);
	        range.collapse(false);
	        var sel = window.getSelection();
	        sel.removeAllRanges();
	        sel.addRange(range);
	    } else if (typeof document.body.createTextRange != "undefined") {
	        var textRange = document.body.createTextRange();
	        textRange.moveToElementText(el);
	        textRange.collapse(false);
	        textRange.select();
	    }
	}
	/*==/content editable 에서 커서 마지막으로 이동.==*/
	
	var insert_wysiwyg = function($form, html){
		$form.find('[name=memo]').val(html).end()
		.find('.wysiwyg-editor').html(html)
		.promise().done(function(){
			var $wysiwyg =$form.find('.wysiwyg-editor'); 
			$wysiwyg.find('img').last().load(function(){
				$wysiwyg.focus().animate({scrollTop : 10000},0);
				placeCaretAtEnd($wysiwyg[0]);
			});
		});
		;
	};
	/*== /위지윅에 HTML 삽입==*/
	
	var preview_local_image = function(input){
		var r = new FileReader()
			, $main_form = context.parent.find('.main_form')
			, memo_val = $main_form.find('[name=memo]').val()
			, img = ''
				;

		r.onload = function(e){
			img += "<img src='"+e.target.result+"' ";
			img += " title='"+input.files[0].name+"' ";
			img += " data-async_images_no='' ";
			img += " class='async_image async_image_uploading' ";
			img += " style='max-width:100%;' ";
			img += "/><br/><br/>";
			insert_wysiwyg($main_form, memo_val+img);
			/*
			setTimeout(function(){
				console.log($main_form.find('.async_image_uploading').length);
				$main_form.find('.async_image_uploading').attr('src', 'test.jpg');
				console.log($main_form.find('.wysiwyg-editor').html());
				insert_wysiwyg($main_form, $main_form.find('.wysiwyg-editor').html());
				console.log($main_form.find('[name=memo]').val());
			}, 2000);
			*/
		};
		
		r.readAsDataURL(input.files[0]);
	};
	/*== /프리뷰 이미지를 업로드.==*/
	
	var async_image_uploaded = function(src, originalname, no){
		var $main_form = context.parent.find('.main_form')
				, $memo = $main_form.find('[name=memo]')
				, html = ''
				;
		$main_form.find('.async_image_uploading').each(function(){
			var $this = $(this)
					, name = $this.attr('title')
					;
			if(name === originalname){
				$this.attr('src', src).attr('data-async_images_no',no).removeClass('async_image_uploading');
			}
		});
		html = $main_form.find('.wysiwyg-editor').html();
		insert_wysiwyg($main_form, html);
		
	};
	/*== /업로드 완료 후 이미지 주소 변경==*/
	
	var insert_wysiwyg_image = function(src, originalname, no){
		var $main_form = context.parent.find('.main_form')
			, memo_val = $main_form.find('[name=memo]').val()
			, img = ''
			;
			
		img += "<img src='"+src+"' ";
		img += " title='"+originalname+"' ";
		img += " data-async_images_no='"+no+"' ";
		img += " class='async_image ' ";
		img += " style='max-width:100%;' ";
		img += "/><br/><br/>"
		;
		insert_wysiwyg($main_form, memo_val+img);
	};
	/*== /위지윅에 이미지 삽입==*/
	
	TB_shared.set('templates--write_form--write_form_1--async_image_upload_beforeAC', function(data, $form){
		if($form.find('.async_image_uploading').length){
			TB_func.popupDialog('이미지를 업로드 중입니다.');
			return false;
		}
		return true;		
	});
	/*==/비동기 이미지 전송전 함수==*/
	
	TB_shared.set('templates--write_form--write_form_1--async_image_upload_aftAC', function(res){
		if(!res.src){
			TB_func.loading.off();
			location.replace('#!/index');
			return false;
		}
		
		res.src=TB_config.get('TB_root_path')+res.src;
		
		//wysiwyg 이미지 
		insert_wysiwyg_image(res.src, res.originalname, res.no);
		
		//비동기 이미지 업로드 완료로 변경.
		//async_image_uploaded(res.src, res.originalname, res.no);
		
		
		//이미지정보를 변수에 담기.
		images_json.push({
			no : res.no
			, src : res.src
			, originalname : res.originalname
			, pictured_time : res.pictured_time
			, lat : res.lat
			, lon : res.lon
		});
		
		//로딩 끝내기
		TB_func.loading.off();
	});
	/*==/비동기 이미지 전송후 함수==*/
	
	TB_shared.set('templates--write_form--write_form_1--beforeAc', function(data, $form){
		
		//insert_sw = true;
		//console.log(data);
		//이미지 정보 찾아 넣기.
		if(images_json.length){
			//console.log(images_json);
			var inserted = []//삭제되지 않고 남아있는 이미지의 no 
				, final_images = []
				;
			//삭제 되지 않고 남아있는 이미지만 추려낸다.
			$form.find('.async_image').each(function(){
				inserted.push({no : $(this).attr('data-async_images_no')});
			});
			
			//console.log(inserted);
			for(var i =0, len = images_json.length; i < len ; i++){
				for(var j= 0, _len = inserted.length ; j < _len ; j++){
					if(images_json[i].no == inserted[j].no){
						final_images.push(images_json[i]);
					}
				}
			}
			
			//console.log(final_images);			
			data.push({
				name : 'images_JSON'
				, requires:false
				, type:'textarea'
				, value : JSON.stringify(final_images)
			});
			//console.log(data);	
		}
		//console.log('업로드 전');
		return true;		
	});
	/*==/폼 전송전 함수==*/
	
	TB_shared.set('templates--write_form--write_form_1--aftAc', function(res){
		//console.log('수정완료',res);
		if(res.result === 'success'){
			go_prev_page(context.param.reqQuery);
			//history.back();
		}
	});
	/*==/폼 전송 후 함수==*/
	
	
	TB_shared.set('templates--write_form--write_form_1--password_check', function(res){
		if(!res){return true;}
		
		context.obj.confirm = true;
		images_json = (res.images_JSON)?JSON.parse(res.images_JSON):[];
		context.obj.render.subject = res.subject;
		context.obj.render.nick = res.nick;
		context.obj.render.writetime = res.writetime.split(' ')[0];
		context.obj.render.memo = res.memo;
		context.obj.render.password = context.parent.find('.password_check_form').find('.password').val();
		context.obj.render.hidden_checked = (res.hidden === '1')?'checked':'';
		
		
		TB_contents.render(context, function($topEl){
			//console.log(context);
			//console.log(context.obj.render);
			$topEl.find('.body_wrap').removeClass('ST_dis_none');
			context.parts.func();
			evt($topEl);
		});
		
	});
	/*==/패스워드 체크 후/==*/
	
	
	var convert_render_data = function(_data){
		var data = _data
				, i = 0
				;
				
		data.render.stars_point = [];
				
		for(;i < data.render.stars_cnt ; i++){
			data.render.stars_point[i] = {};
			data.render.stars_point[i].class_name = 'star_point_'+(i+1);
		}
		
		return data;
	};
	/*==/랜더링 랜더링 하기위한 값 추가../==*/

	/*============================================
	 * 함 수
	 ============================================*/
	
	
	var evt = function($top){
		
		$top.find('.delete_button').off('click.delete_item')
		.on('click.delete_item', function(){
			delete_item(context.obj.render.no, context.obj.render.nick, context.obj.render.password);
		});
		/*==/삭제 버튼 클릭/==*/
	};
	
	context.parts.func('templates--write_form--write_form_1--header', function($el){
		$el.find('.closeBtn').off('click.history_back')
		.on('click.history_back', function(){
			go_prev_page(context.param.reqQuery);
			//history.go(-1);
		});
	});
	/*==/닫기/==*/
	
	context.parts.func('templates--write_form--write_form_1--category_selector', function($el){
		$el.find('select').off('change.category_change')
		.on('change.category_change', function(){
			context.obj.selected_category = $(this).val();
			if(context.obj.selected_category === 'etc'){
				context.obj.render.view.price = false;
				context.obj.render.view.itinerary = false;
			}else if(context.obj.selected_category === 'food'){
				context.obj.render.view.price = true;
				context.obj.render.view.itinerary = false;
			}else if(context.obj.selected_category === 'travel'){
				context.obj.render.view.price = true;
				context.obj.render.view.itinerary = true;
			}
			context.parts.render(
				['templates--write_form--write_form_1--add_info'
					, 'templates--write_form--write_form_1--add_info_view'
				]
			);
		});
	});
	/*==/카테고리 선택/==*/
	
	context.parts.func('templates--write_form--write_form_1--add_info', function($el){
		
		$el.find('.async_image_upload').off('click.async_image_click')
		.on('click.async_image_click', function(){
			context.parent.find('.async_image_upload_form_wrap').find('.file').trigger('click');
		});
		/*==/비동기 이미지 업로드 버튼 클릭==*/
		
		context.parent.find('.async_image_upload_form_wrap').find('.file').off('change.async_image_change')
		.on('change.async_image_change', function(){
			
			var $this = $(this)
				, val = $this.val()
				, size//MB 단위
				, type// 파일 타입
				, allow_type = ['jpeg', 'png', 'gif', 'bmp']
				, allow = false
				, max_width = 900
				, max_size = 4
				;
			
			//이미지가 없으면 업로드 안함.
			if(!val){return false;}
			
			TB_func.loading.on('full');
			
			
			//변수 설정
			size = Math.floor($this[0].files[0].size/1024/1024);//MB 단위
			type = $this[0].files[0].type;
			
			//이미지 타입 유효성 검사
			for(var i= 0, len = allow_type.length; i < len ; i++){
				if(type.match(allow_type[i])){
					allow = true;
				}
			}
			
			if(!allow){
				TB_func.popupDialog('이미지 파일을 선택하세요.');
				return false;
			}
			
			//이미지 크기 제한
			if(size > max_size){
				TB_func.popupDialog(max_size+'MB 이하의 파일을 선택하세요.');
				return false;
			}
			
			//임시로 로컬 이미지 업로드
			//preview_local_image($this[0]);
			
			//이미지 업로드
			$this.closest('form').find('.submit').trigger('click');
		});
		/*==/비동기 파일의 값이 변했을 때 업로드==*/
		
	});
	/*==/아이콘 클릭 시 세부 입력 정보 활성화/==*/
	
	/*============================================
	 * 이벤트
	 ============================================*/
	
		
	//console.log(param);
	
	TB_func.fall([
		function(next, param){
			if(!user || user.member_grade != 'supervisor'){next();}
			get_item({
				no : context.param.no
				, reqQuery : context.param.reqQuery
				, callback : function(res){
					//console.log(res);
					if(res){
						next(res);
					}
				}
			})
		}
		, function(next, param){
			TB_contents.render(context, function($topEl){
				//console.log(context);
				$topEl.find('.body_wrap').removeClass('ST_dis_none');
				context.parts.func();
				evt($topEl);
			});
			
		}
	]);
	
	
	
	
});
















































