TB_shared.func = function(context){
	var backupName = 'templates||m_list||list_1'+context['param'].reqQuery+'||backup';
	var backupData = TB_shared.get(backupName);
	var $topEl = '';
	var topObj = {};
	//console.log(context);
	
	if(!backupData){//백업한 내용이 없을 경우는 로딩바 표시
		TB_el.loadingBar(context['parent'], {'icon' : 'fa fa-3x fa-cog fa-spin'});
	}else{//이전에 들렸을 경우 페이지 출력
		context['parent'].html(backupData);
	}
	/*==/데이터를 가져올 동안 보여줄 화면 결정==*/
	
	TB_ajax.get_json({
		'post' : {
			'reqType' : 'select'
			, 'reqQuery' : context.param.reqQuery
			, 'page' : context.param.page
			, 'listNum' : context.param.listNum
			, 'adminAuthKey':$.cookie('adminAuthKey')||''
		}
		,'callback' : function(dbData){
			context['obj'] = dbData.body;//랜더링의 위한 데이터 파일
			if(context['param'].showBtns) context['obj'].showBtns=true;
			 if(context.param.title)context['obj'].title = context.param.title; 
			/*==/모달 타이틀 설정==*/
			
			TB_contents.render(context, function($topEl){
				context['$topEl'] = $topEl;
				
				context['$topEl'].ready(function(){
					topObj.bntsMoveDistance = $topEl.find('li').find('.btn_wrap').width()-60;
					renderAfter(context['$topEl'], context['obj']);
				});
			});/*==/랜더링==*/
		}
	});/*==/리스트 가져와서 뿌리기===*/
	
	var renderAfter = function($topEl, obj){
		//console.log(btn_wrap_wid);
		$topEl.hide();//처리 완료돼기까지 엘리먼트 감춤
		TB_form.binding($topEl);//바인딩
		TB_form.eBinding($topEl);//이벤트 바인딩
		toggleBtn($topEl);//토글 애니메이션
		moreItems($topEl);//아이템 더 출력하기.
		TweenLite
		.to($topEl.find('.btn_wrap'),0,{
			right : "-"+topObj.bntsMoveDistance
		});
		/*==/초기 셋팅 ==*/
		
		 $(document).ready(function(){
			 $topEl.show();
			if(obj.list_info.total_pg <= context.param.page){
				$topEl.find('.moreItemsBtn').hide();
				return false;
			}/*==/더보기 버튼 보여주기 유무 ==*/
			TB_shared.set(backupName, $topEl[0].outerHTML);//리스트 데이터 캐쉬
			
			if(context['param'].showBtns){
				$topEl.find('li')
				.off('click', '.submitBtn_container')	
				.on('click', '.submitBtn_container', function(){
					var $this = $(this);
					var $li = $this.closest('li');
					var password = $li.find('.password').val(), no = $li.find('.no').val();
					var confirm = false;
					if(!password)return false;
					for(var i in context['obj']['list']){
						if(context['obj']['list'].hasOwnProperty(i)){
							if(no===context['obj']['list'][i].no){
								context['obj']['list'][i].auth = 'auth';
								if(password==context['obj']['list'][i].password) confirm = context['obj']['list'][i];
								break;
							}
						}
					}/*==/패스워드가 동일한지 검사 ==*/
					
					if(!confirm)return false;
					var src = $(context['html']).find('.btn_wrap').html();
					var _obj = {};
					$.extend(_obj, context);
					_obj['obj'].list = [confirm];
					var output = TB_handlebars.returnRender(src,_obj['obj'].list[0]);
					var $btnWrap =$li.find('.btn_wrap'); 
					$btnWrap.html(output).children('.toggleBtns')
					.remove()
					.promise().done(function(){
						TB.binding($btnWrap);
						TB.eBinding($btnWrap);
						TB_form.binding($btnWrap);
						TB_form.eBinding($btnWrap);
						confirm = null, i = null, $this = null, $li = null, src = null, _obj = null, output= null, $btnWrap = null;
					});
					
				});
			}/*==/패스워드 체크==*/
		 	
		 });
	};
	
	var moreItems = function($topEl){
		$topEl.find('.moreItemsBtn').off(TB_config.get('click'))
		.on(TB_config.get('click'), function(){
			context.param.page++;
			TB_ajax.get_json(
				{
				'post' : {
					'reqType' : 'select'
					, 'reqQuery' : context.param.reqQuery
					, 'page' : context.param.page
					, 'listNum' : context.param.listNum
				}
				,'callback' : function(dbData){
					
					for (var i = 0,len =  dbData.body.list.length; i < len ; i++){
						context['obj']['list'].push(dbData.body.list[i]);
					}
					
					TB_contents.render(context, function($topEl){
						$topEl.ready(function(){
							topObj.bntsMoveDistance = $topEl.find('li').find('.btn_wrap').width()-60;
							renderAfter($topEl, context['obj']);
						});
					});/*==/랜더링==*/
				}
			});
		});
	}/*===/아이템 더 출력하기 ===*/
	
	
	var toggleBtn = function($el){
			var $toggleBtn = $el.find('.toggleBtns');
			var click = "click";
			
			$toggleBtn
			.off(click)
			.on(click, function(e){
				if(e.stopPropagation) e.stopPropagation();
				topObj.bntsMoveDistance = $el.find('li').find('.btn_wrap').width()-60;
				var $this = $(this);
				var $btnWrap  =$this.closest('.btn_wrap');
				var btnsView = $btnWrap.attr('data-isView')?true:false;
				var btnsMoveType = '';
				var $icon = $this.children('.fa');
				if(btnsView===false){//보일때
					$icon.addClass('fa-rotate-180');
					$btnWrap.attr('data-isView', 'true');
					btnsMoveType="+";
				}else{
					$icon.removeClass('fa-rotate-180');
					$btnWrap.removeAttr('data-isView');
					btnsMoveType="-";
				} 
				
				TweenLite
				.to($btnWrap,0.5,{
					right : btnsMoveType+'='+topObj.bntsMoveDistance
				});
				
				btnsView = !btnsView;
			});
	};/*==/토글 버튼 애니메이션==*/
	
	var bbsDelete = function(_obj){
		var obj = {no : _obj.on, category : _obj.category};
	};
	/*====================*
	 * 게시판의 내용을 지운다.
	 * no값과 category 값이 반드시 필요.
	 ====================**/
};
