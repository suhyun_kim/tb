TB_shared.set('templates--backdrop--popup--js', (function() {
	var context
			, $bg = $('#TB_backdrop_bg')
			, $wrapper = $('#TB_backdrop_content')
			, user = TB_config.get('user')
			;
		
	//console.log(TB_auth.getAdminInfo());
	//console.log('asdf');
	/*====================================================
	 * 변수설정
	 ====================================================*/
	
	var close = function(period){
		var period = period
				, now= new Date()
				, limit_date
				, over_time
				;
		if(period === 'today'){
			limit_date = now.getTime()+(1*24*60*60*1000);
			over_time = (limit_date-now.getTime())/60/60/1000;
			//console.log(now.getTime(), limit_date, (limit_date-now.getTime())/60/60/1000);
			$.cookie('backdrop_view_period' , limit_date, {path:'/', expires:7});
		}
		
		TB_page.sgmap().templates.backdrop_popup.off();
	};
	
	var delete_item= function(no, callback){
		if(!no){return false;}
		//return false;
		TB_ajax.get_json({
			sendData : {
				reqType : 'delete'
				, reqQuery : context.obj.query.delete
				, no : no
			}
			, callback : function(data){
				
				context.obj.render.slides = TB_func.json_arr_delete(context.obj.render.slides, 'no', no);
				//console.log(context.obj.render.slides);
				if(typeof callback === 'function'){callback();}
			}
		});		
	};
	
	
	var change_form = function(reqType, no){
		var item
				, today = new Date().format('yyyy-MM-dd')		
				;
				
		if(no){
			context.obj.render.selected_no = no;
			item = context.obj.render.slides.filter(function(item){
				if(item.no === no){return item;}
			})[0];
		}else{
			context.obj.render.selected_no = '';
		}
		
		if(reqType === 'insert'){
			context.obj.render.form.reqType = 'insert';
			context.obj.render.form.reqQuery = context.obj.query.insert;
			context.obj.render.form.subject = '';
			context.obj.render.form.start_date = today;
			context.obj.render.form.end_date = today;
			context.obj.render.form.submit = '입  력';
		}else if(reqType === 'modify'){
			context.obj.render.form.reqType = 'modify';
			context.obj.render.form.reqQuery = context.obj.query.modify;
			context.obj.render.form.subject = item.subject;
			context.obj.render.form.start_date = item.start_date;
			context.obj.render.form.end_date = item.end_date;
			context.obj.render.form.submit = '수  정';
		}
		//console.log(context.obj.render);
		
		context.parts.render();
		
	};
	/*========/랜더링 폼 변경/=========*/
	
	var get_list = function(callback){
		var reqQuery = context.obj.query.select
				, callback  = ($.type(callback) === 'function')?callback:undefined
				;
		//console.log(context);
		TB_ajax.get_json({
			sendData : {
				reqQuery : reqQuery
			}
			, callback : function(data){
				context.obj.render.slides = data.body;
				
				if(callback){callback(data.body);}
			}
		});
	};
	/*==/리스트 가져오기==*/
	
	
	var init = function(context){
		var today = new Date().format('yyyy-MM-dd')
				, context = context
				;
		//console.log(context)
		context.obj = {};
		
		context.obj.query = {};
		context.obj.query.insert = context.param.query.insert;
		context.obj.query.modify = context.param.query.modify;
		context.obj.query.delete = context.param.query.delete;
		context.obj.query.select = context.param.query.select;
		
		context.obj.render = {};
		
		context.obj.render.selected_no = '';
		//console.log(context.param.slides);
		context.obj.render.slides = context.param.slides||[];
		
		context.obj.render.form = {};
		context.obj.render.form.reqType = 'insert';
		context.obj.render.form.reqQuery = context.obj.query.insert;
		context.obj.render.form.subject = '';
		context.obj.render.form.start_date = today;
		context.obj.render.form.end_date = today;
		context.obj.render.form.submit = '등록';
		
		set_parts_func();
		//TB_ani.ready($bg);
		//console.log(context)
		
		TB_func.fall([
			function(next, param){//1주일 쿠키확인, 데이터 가져오기
				var limit_date = parseInt($.cookie('backdrop_view_period'), 10)
					, now = new Date()
					, over_time =  (limit_date)?(limit_date-now.getTime())/60/60/1000:0
					;
				//console.log(limit_date, over_time);
				//console.log(user, limit_date, over_time);
				/*
				if(over_time > 0){
					context = null;
					close();
					return false;
				}
				*/
				
				next(over_time);
			}
			, function(next, over_time){//데이터 가져오기
				//console.log(over_time)
				/*
				// return false;
				if(context.obj.render.slides.length > 0){
					next();
					return false;
				}
				*/
				
				get_list(function(){
					//console.log(context.obj.render.slides.length);
					if(user&& user.member_grade === 'supervisor'){
						next();
					}else {
						if(!context.obj.render.slides.length || over_time > 0){
							context= null;
							close();
							return false;
						}
					}
					next();
				});
			}
			, function(next, param){//랜더링
				TB_contents.render(context, function($topEl){
					//console.log(context)
					//return false;
					//context.parts.render('backdrop--popup_form')
					context.parts.render({
						parts : ['backdrop--popup_form', 'backdrop--popup_slider']
						, callback : function($el){
							$el.find('img').eq(0).load(function(){
								var $this = $(this)
									, wid= $this.width()
									, hei = $this.height()
									;
								//console.log($this);
								//console.log(this.naturalWidth,this.naturalHeight);
							});
						}
					});
						// TB_ani.ready($wrapper, function(){
							// TB_ani.flow('fadeIn', $wrapper);
						// });
					
				});
			}
		]);
	};
	/*==/초기화 함수 ==*/
	
	
	TB_shared.set('templates--backdrop--popup_form_after', function(data){
		if(context.obj.render.form.reqType === 'insert'){
			get_list(function(){
				change_form('insert');
			});
		}else if(context.obj.render.form.reqType === 'modify'){
			if(data.length){
				context.obj.render.slides = TB_func.json_arr_update(context.obj.render.slides, data[0], 'no');
			}else{
				TB_func.dbMessage('수정에 실패 하였습니다');
			}
		}
		
		change_form('insert');
	});
	
	/*====================================================
	 * 함수
	 ====================================================*/
	
	
	var set_parts_func = function(){
		
		context.parts.func('backdrop--popup_slider', function($el){
			var $el = $el
					;
			
			//닫기 버튼
			$el.find('.close_button').off('click.close_popup')
			.on('click.close_popup', function(){
				//TB_page.sgmap().templates.backdrop_popup.off();
				close();
			});
			
			//1주일 닫기 버튼
			$el.find('.today_close_button').off('click.today_close_button')
			.on('click.today_close_button', function(){
				//TB_page.sgmap().templates.backdrop_popup.off();
				close('today');
			});
			
		});
		/*====/슬라이드/=====*/		
		
		context.parts.func('backdrop--popup_form', function($el){
			var $form = $el.find('.TB_form')
					, $fold_button = $el.find('.folder_wrap')
					, $thumbnail_item = $el.find('.thumbnail_item')
					, $i_mode_btn = $el.find('.insert_mode_button_wrap')
					, $del_btn = $el.find('.delete_button')
					, $off_btn = $el.find('.off_button_wrap')
					;
			
			//폼 바인딩
			TB_form.binding($el);
					
			//set form func 
			if($.type(context.param.before) === 'function'){
				$form.data('before_func', context.param.before);
			}
			if($.type(context.param.after) === 'function'){
				$form.data('after_func', context.param.after);
			}
			
			//off 버튼
			$off_btn.off('click.off')
			.on('click.off', function(){
				TB_page.sgmap().templates.backdrop_popup.off();
			});
			
			//폴드 버튼
			$fold_button.off('click.fold')
			.on('click.fold', function(e){
				var $this = $(this)
						, is_show = $el.hasClass('show')
						;
				if(is_show){
					$el.removeClass('show');
					$this.find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-up');
				}else{
					change_form('insert');
					$el.addClass('show');
					$this.find('.fa').removeClass('fa-chevron-up').addClass('fa-chevron-down');
				}
			});
			
			//썸네일 아이템 클릭
			$thumbnail_item.off('click.thumbnail_item')
			.on('click.thumbnail_item', function(e){
				change_form('modify', $(this).attr('data-no'));
			});
			
			//입력모드
			$i_mode_btn.off('click.insert_mode')
			.on('click.insert_mode', function(){
				$i_mode_btn.removeClass('modify');
				change_form('insert');
			});
			
			//아이템 삭제
			$del_btn.off('click.delete_button')
			.on('click.delete_button', function(){
				if(!confirm('정말 삭제 하시겠습니까?')){ return false; }
				delete_item(context.obj.render.selected_no, function(){
					change_form('insert');
				});
			});
			
		});
		/*====/폼/=====*/		
		
	};
	/*====================================================
	 * 이벤트
	 ====================================================*/
	
	
	return {
		init : function(p_context) {
			context = p_context;
			
			//console.log(context.obj);
			init(context);
			
		}
		, off : function(){
			//console.log('off');
		}
	};
})());
