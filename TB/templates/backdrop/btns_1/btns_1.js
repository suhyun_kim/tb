/*============================
 * 백드롭을 사용하기을 사용하기 위해서는 반드시 데이터를 넘겨야 한다.
 * {	"title" : "대제목"
 * 	, "sub_title" : "대제목 아래 글"
 * 	, "type" : "show" 
 * 	,"btns" 
 * 	[
 * 		"text" : "text"
 * 		, "color" : "#000000"
 * 		, "background" : "#ffffff"
 * 		, "class" : "ST_class"
 * 		, "onClick" : "버튼을 눌렀을 때 실행될 함수."
 * 	], []....
 * }
 ============================*/
TB_shared.func = function(context){
	context.param.sendData = JSON.stringify(context.param);
	
	
	var $backdropWrap = $('#TB_backdrop');
	
	if(!TB_shared.get('m_backdrop_btns_1_on')){
		TB_shared.set('m_backdrop_btns_1_on', function(){
			$backdropWrap.children('#TB_backdrop_bg').show();
		});
	}/*================================
		  * 백드롭 on
		  ================================*/
	
	if(!TB_shared.get('m_backdrop_btns_1_off')){
		TB_shared.set('m_backdrop_btns_1_off', function(){
			$backdropWrap.children('#TB_backdrop_bg')
			.hide().end()
			.find('#TB_backdrop_content').empty();
		});
	}/*================================
		  * 백드롭 off
		  ================================*/
	 
	//console.log(JSON.stringify(sendData));
	
	//return false;
	TB_contents.render(context, function($btnsEl){
		TB_shared.get('m_modal_1_on')();
		
	});
};
