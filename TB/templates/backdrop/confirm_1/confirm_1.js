/*============================
 * nodejs 전용
 * params 
 * 	. GET 방식으로만 전달받음
 * 	. ex ) login?strategy = goolge,naver,facebook
 * 	. strategy(string) : 로그인할 전략
 ============================*/

TB_shared.set('templates--backdrop--confirm_1--js', function(context){
	context['obj'] = {};
	$.extend(context['obj'], context['param']);
		
	/*=============================================
	 * 변수 셋팅
	 =============================================*/
	
	/*=============================================
	 * 함수
	 =============================================*/
	
	var evt = function($topEl){
		$topEl.find('.button').off('click.confirm_button')
		.on('click.confirm_button', function(){
			var $this = $(this)
				, value = $this.attr('data-value')
				, func_name = context.param.shared_func
				;
				
			
			if($.type(TB_shared.get(func_name)) === 'function'){
				TB_shared.get(func_name)(value);
			}
			
		});
		/*=====/확인버튼 /==========*/
	};
	/*=============================================
	 * 이벤트
	 =============================================*/
	
	
	TB_contents.render(context, function($topEl){
		evt($topEl);
		$topEl.removeClass('ST_opacity_0');
	});
});

