
TB_shared.set('templates--backdrop--dynamic_form--js', (function(context){
	var context = undefined;
	
	var init = function(){
		
		TB_contents.render(context, function($topEl){
			var $form = $topEl.find('.TB_form');		
				
			if(typeof context.obj.before === 'function'){
				$form.data('before_func', context.obj.before);
			}
			if(typeof context.obj.after === 'function'){
				$form.data('after_func', context.obj.after);
			}
			
			$topEl.removeClass('ST_opacity_0');
			
			TB_form.binding($topEl);
			
			$topEl.find('input').each(function(){
				$(this).trigger('focus');
			});
			
		});
	};
	/*===/랜더링 함수./ ===*/
	
	
	var receive_obj = function(param){
		
		if(!param || !param.sharedObj){
			//외부에서 들어왔을 경우 index 페이지로 이동
			if(TB_page.sgmap().history.length === 1){
				location.replace('/');
			}
			//이전 페이지가 있을 경우 이전페이지로 이동.
			else{
				location.history.back();
			}
			return false;
		}
		
		context.obj = TB_shared.get(param.sharedObj);
		//context.obj.no = param.no;
		//return false;
		
		init();
	};
	/*==/ 동적 폼 생성에 사용될 변수 받아 오기 ==*/
	
	/*=============================================
	 * 함수
	 =============================================*/
	
	
	
	/*=============================================
	 * 이벤트
	 =============================================*/
	
	
	
	return {
		init : function(_context) {
			context = _context;
			context.obj = {};
			
			//console.log(context.param.sharedObj);
			
			receive_obj(context.param);
			
			//init();
		}
		, on : function(param){
			//console.log(param);
			receive_obj(param);
			
		}
		, off : function(){
			//console.log('off');
		}
	}
})());


