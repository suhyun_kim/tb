/*============================
 * slider_1에 필요한 변수
 * {
 * 	아래와 같음.
 * 	!name : 슬라이더의 고유 값. 중복 되어서는 안된다.,
 * 	sliderHeight : 슬라이더의 height지정. 지정하였을 경우 작동안됨. 
 * }
 ============================*/
TB_shared.set('templates--slider--slider_1--js', function(_context){
	var top_name =  'templates--slider--slider_1--js';
	var slider_name = top_name+'--'+_context['param'].name;
	
	if(!TB_shared.get(slider_name)){
		TB_shared.set(slider_name, {
			slider : {},
			full_name : slider_name,
			context : _context,
			init : function(){
				
				var slider = this.slider,
						context = this.context
						;
				
				var set = function(){
					slider.name;
					slider.full_name = slider_name;
					slider.easing = 'easeNone';
					slider.delay = 5000;
					slider.movespeed = 1000;
					slider.type = 'slideShow';//slideShow, fadeInOut
					slider.direction = 'left';
					slider.anchor;//배열
					slider.src;//배열
					slider.text;//슬라이더별 문자 배열
					
					context['obj'] = {};
					
					TB_ajax.get_json({
						'sendData' : {
							'reqType' : 'select',
							'reqQuery' : 'getSlider',
							'name' : context['param'].name
						},
						"callback" : function(res){
							$.extend(slider, res.body.slider);
							$.extend(slider, context['param']);//수동으로 옵션을 설정했을 경우 DB정보도 무시하고 적용된다.
							
							if($.type(slider.before)=== 'function'){//before 함수가 지정 되어 있을 경우.
								if(slider.before(res.body) === false){return false;}
							}
							
							
							if(res.body.isCreate &&res.body.slider.images){
								slider.images = res.body.slider.images;
								if(slider.images.length > 1){//한장의 이미지 밖에 없을 경우  슬라이드가 작동을 안한다.
									context['obj'].move = true;
								}
							}
							
							context['obj'].sliderHeight = _context['param'].sliderHeight||undefined;
							context['obj'].images =(res.body.slider?res.body.slider.images:undefined);
							context['obj'].isCreate = res.body.isCreate;
							context['obj'].submit = (res.body.isCreate)?'수정':'생성';
							context['obj'].name = slider.name;
							context['obj'].no = slider.no;
							context['obj'].images_cnt = (slider.images)?slider.images.length:0;
							context['obj'].root_path = TB_config.get('TB_root_path');
							context['obj'].slider_form_reqType = res.body.reqType;
							context['obj'].slider_form_reqQuery = res.body.reqQuery;
							render();
						}
					});
				};
				/*==================================
				 * 초기화
				 ==================================*/
				
				var render = function(){
					TB_contents.render(context, function($topEl){
						var $form = $topEl.find('.form_wrap')
								, $slider_wrap = $topEl.find('.slider_wrap')
								, $li = $slider_wrap.find('li')
								, $img = $li.find('img')
								;
						
						TB_form.autoInsert($form, slider);
						TB_form.eBinding($topEl);
						
						if(context['obj'].move === true){
							move.init($slider_wrap, slider);
						}
						
						if(!$img.length){return false;}//이미지가 없을 경우 
						
						if(context['param'].sliderHeight){//height를 지정했을 경우
							$slider_wrap.css('height', context['param'].sliderHeight).end()
							.find('.slider_container').css('height',context['param'].sliderHeight)
							.find('li').css('height', context['param'].sliderHeight);
						}else{//height를 지정하지 않았을 경우
							$img.load(function(){
								TB_style.binding($topEl);
							});
						}
						
					});
					
				};
				/*====================================
				 * 랜더링
				 ====================================*/
				set();
			}
			
		});
		
		TB_shared.get(slider_name).init();
	}else{//캐시해놓은 것이 있을 때.
		TB_shared.get(slider_name).context = _context;//parent가 작동을 안해서 새로운 걸로 지정.
		TB_shared.get(slider_name).init();
	}
	
	TB_shared.set('templates--slider--slider_1--beforeAc_del_image', function(res, $form){
		var $parent = $form.parent();
		TB_ani.flow('widHei_0_fadeOut', $parent, function(){
			$parent.remove();
			$parent = null;
		});
		//console.log(_context['obj']);
		return true;
	});	
	/*====================================
	 * 이미지 삭제전 함수
	 ====================================*/
	
	TB_shared.set('templates--slider--slider_1--sliderFormAft', function(res, $form){
		var name = $form.closest('.templates--slider--slider_1').attr('data-TB_templates_name');
		TB_shared.get(top_name+'--'+name).init();
		name = null;
	});	
	/*====================================
	 * 폼전송 후 함수.
	 ====================================*/
	
	var move = (function(TweenLite, $){
		var slider,
				$slider_wrap,
				$slider_container,
				$nav_wrap,
				$nav_btn,
				slider_wid,
				slider_hei,
				$slides,
				$move_slides,
				$next_btn, 
				$prev_btn, 
				view_idx =0,
				prev_idx,
				next_idx,
				slide_len,
				move_interval,
				move_sw = false,//슬라이더의 움직을 알기 위한 스위치 슬라이딩 동안은 flase, 끝나면 true
				first_align = true,//슬라이드시 최초 움직임식 정렬이 안되는데 이를 해결하기 위한것. 
				tl
				;
				
		var init = function($el, _slider){
			$slider_wrap =  $.extend(true, $slider_wrap, $el),
			$slider_container = $el.find('.slider_container'),
			$nav_wrap = $el.find('.slider_nav_wrap'),
			$nav_btn = $nav_wrap.find('li'),
			$slides = $slider_container.find('li'),
			$next_btn = $el.find('.next_btn'),
			$prev_btn = $el.find('.prev_btn'),
			slider =  _slider;
			
			if(Modernizr.csstransforms3d){
				tl = new TimelineMax;
			}else{//올드 브라우져 셋팅
				slider.type='fadeInOut';
			}
			
			if($slides.length ===2 && slider.type === 'slideShow'){
				$slides.clone().appendTo($slider_container);
				$slides = $slider_container.find('li');
			}
			
			slide_len = $slides.length;
			autoPlay(false);
			set_nav();
			slider_wid = $slider_wrap.width();
			slider_hei = $slider_wrap.height();
			set_position();
			
			$slides.first().find('img').load(function(){
				autoPlay(true);
				eBinding();
				move_sw = true;
			});
		};
		/*====================================
		 * 초기화
		 * params
		 * 	- $el(jQueryObj) : jQuery 오브젝트
		 * 	- slider(obj) : slider에 관한 정보가 담기 오브젝트
		 ====================================*/
		
		var autoPlay =function(sw){
			clearTimeout(move_interval);
			if(!sw){
				return false;
			}
			move_interval = setTimeout(function(){
				play();
				autoPlay(true);
			}, slider.delay);
		};
		/*====================================
		 * 자동 플레이 유무
		 * params
		 * 	- sw(bool) : true 일경우 자동 플레이 on, false일 경우 자동 플레이 off
		 ====================================*/
		
		var play = function(_obj){
			var obj = {
						type : slider.type,
						direction : slider.direction,
						idxSw : true//인덱스를 자동으로 변경하지 못하게 하는 스위치
					},
					distance = 0,
					movespeed = slider.movespeed/1000;
			move_sw  = false;
			
			if(first_align){
				first_align = false;
				slider_wid = $slider_wrap.width();
				slider_hei = $slider_wrap.height();
				set_position();
			}
			
			if(typeof _obj === 'object'){
				obj = $.extend(obj, _obj);
			}
			if(obj.direction === 'left'){
				if(obj.idxSw){
					if(view_idx >= slide_len-1){view_idx = 0;}
					else{ view_idx++; }
				}
			}else if(obj.direction === 'right'){
				if(obj.idxSw){
					if(view_idx <= 0){view_idx = slide_len-1;}
					else{view_idx--;}
				}
			}
			
			if(obj.type === 'slideShow'){
				if(obj.direction === 'left'){
					distance = -slider_wid;
				}else if(obj.direction === 'right'){
					distance = slider_wid;
				}
				set_nav();
				tl.clear();
				tl.to($move_slides, movespeed, {
					left :  "+="+distance,
					ease : Power4[slider.easing],
					onComplete : function(i){
						set_position();
						swBtn(true);
						navBtn(true);
						move_sw = true;
					}
				});
			}else if(obj.type ==='fadeInOut'){
				$slides.each(function(idx){
					var $this = $(this);
					if(idx === view_idx){
						$this.css({
							'z-index' : '1',
							'display' : 'none'
						}).fadeIn();
					}else{
						$this.fadeOut().css({
							'z-index' : 'auto'
						});
					}
				});
				set_nav();
				swBtn(true);
				navBtn(true);
				move_sw = true;
			}
		};
		
		var set_position = function(_obj){
			if(slider.type === 'slideShow'){
				var prev_posi,
						next_posi;
				$move_slides = [];
				if(!_obj){//지정된 인덱스가 없을 경
					(view_idx ===0)?prev_idx =slide_len-1:prev_idx=view_idx-1;
					(view_idx===slide_len-1)?next_idx = 0 : next_idx = view_idx+1;
				}else{//지정된 인덱스가 있을 경우
					if(_obj.prev_idx){
						prev_idx = _obj.prev_idx;
						next_idx = undefined;
					 }else if(_obj.next_idx||_obj.next_idx ===0){
						prev_idx = undefined;
						next_idx =_obj.next_idx;
					}
				}
				if(slider.direction === 'left'){//left slide
					prev_posi = -slider_wid,
					next_posi = slider_wid;
				}
				$slides.each(function(idx){
					var $this= $(this);
					if(idx === view_idx ){
						$move_slides.push($this);
						$this.css({
							'z-index' : '2',
							'left' : '0', 'top' : '0'
						});
					}else if(prev_idx === idx){
						//console.log('이전', idx);
						$move_slides.push($this);
						$this.css({
							'z-index' : '1',
							'left' : prev_posi 
						});
					}else if(next_idx === idx){
						//console.log('넥스트', idx);
						$move_slides.push($this);
						$this.css({
							"z-index" : '1',
							'left' : next_posi
						});
					}else{
						$this.css('z-index', 'auto');
					}
				});
			}else if(slider.type === 'fadeInOut'){
				$slides.first().css('z-index','2');
			}
		};
		
		var set_nav = function(_idx){
			var idx = _idx||view_idx,
					len = $nav_wrap.length,
					$li = $nav_wrap.find('li');
			if($li.length===2 && slider.type ==='slideShow'){
				if(idx%2 === 0){
					$li.first().find('span')
					.removeClass('fa-circle').addClass('fa-dot-circle-o');
					$li.last().find('span')
					.removeClass('fa-dot-circle-o').addClass('fa-circle');
				}else{
					$li.last().find('span')
					.removeClass('fa-circle').addClass('fa-dot-circle-o');
					$li.first().find('span')
					.removeClass('fa-dot-circle-o').addClass('fa-circle');
				}
				return false;
			}
			
			$li.each(function(each_idx){
				var $span = $(this).find('span');
				if(idx === each_idx){
					$span.removeClass('fa-circle').addClass('fa-dot-circle-o');
				}else{
					$span.removeClass('fa-dot-circle-o').addClass('fa-circle');
				}
			});
		};
		
		var swBtn = function(sw){
			if(sw){
				$next_btn.off('click')
				.on('click', function(){
					swBtn(false);
					play({direction : 'left'});
				});
				$prev_btn.off('click')
				.on('click', function(){
					swBtn(false);
					play({direction : 'right'});
				});
			}else{
				$next_btn.off('click');
				$prev_btn.off('click');
			}
		};
		/*===================================
		 * 버튼을 작동 시키고 끈다.
		 ===================================*/
		
		var navBtn = function(sw){
			if(sw){
				$nav_btn.off('click')
				.on('click', function(){
					var idx = $(this).index(), 
							direction,
							posi_idx = {}; 
					//console.log(idx, view_idx);
					if(idx > view_idx){
						direction = 'right';
						posi_idx.prev_idx = idx;
					}else if(idx < view_idx){
						direction = 'left';
						posi_idx.next_idx = idx;
					}else{
						return false;
					}
					set_position(posi_idx);
					view_idx = idx;
					play({direction:direction, idxSw : false});	
					navBtn(false);
				});
			}else{
				$nav_btn.off('click');
			}
		};
		/*===================================
		 * 네비게이션 버튼
		 * ===================================*/
		
		var draggable = function(){
			var first_posi,
					end_posi,
					result_posi;
			Draggable.create($slider_container, {
				type : 'x',
				bounds : $slider_wrap,
				lockAxis:true,
				edgeResistance:0,//탄력
				throwProps : true,
				onClick : function(e){//클릭 이벤트
					//console.log(e);
				},
				onDragStart : function(e){
					if(e.type === 'touchmove'){//핸드폰 드래그 이벤트
						first_posi = e.changedTouches[0].clientX;
					}else{
						first_posi = e.x;
					}
				},
				onDragEnd : function(e){
					e.stopImmediatePropagation();
					e.stopPropagation();
					e.preventDefault();
					if(!move_sw){
						return false;
					}
					if(e.type === 'touchend'){
						end_posi = e.changedTouches[0].clientX;
					}else{
						end_posi = e.x;
					}
					result_posi = first_posi - end_posi;
					//console.log(first_posi, end_posi, result_posi);
					//return false;
					if(result_posi > 100){
						play({direction : 'left'});
					}else if(result_posi < -100){
						play({direction : 'right'});
					}
				}
			});
		};
		/*===================================
		 * 드래그블
		 * 드래그 해서도 슬라이드 작동.
		 * ===================================*/
		
		var resize;
		
		var eBinding = function(){
			$slider_wrap.off('mouseenter.slider_1')
			.on('mouseenter.slider_1', function(){
				autoPlay(false);
			});
			
			$slider_wrap.off('mouseleave.slider_1')
			.on('mouseleave.slider_1', function(){
				autoPlay(true);
			});
			/*====/slider_wrap에 관한것==*/
			
			swBtn(true);
			/*====/슬라이드 버튼==*/
			
			navBtn(true);
			/*====/네비게이션버튼==*/
			
			if(Modernizr.csstransforms3d){
				draggable();
			}
			/*====/드래그블==*/
			
			$(window).on('resize.templates_slider_1', function(){
				clearTimeout(resize);
				resize = setTimeout(function(){
					slider_wid = $slider_wrap.width();
					slider_hei = $slider_wrap.height();
					set_position();
				}, 700);
			});
			/*==/리사이즈 시 슬라이더 위치 재정렬 ==*/
			
		};
		
		return {
			init : init
		}
	})(TweenLite, jQuery);
	
	
});

