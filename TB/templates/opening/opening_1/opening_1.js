/**
 * @author user
 */
TB_shared.set('templates--opening--opening_1--js', function(context){
			
	var bg_style
			tl = new TimelineMax()
			;
			
	var skip = function(_scope){
		var $scope = _scope
				, $skip_btn = $scope.find('.skip_btn');
		if($skip_btn.length){
			$skip_btn.off('click.opening_skip')
			.on('click.opening_skip', function(){
				tl.clear();
				$('#TB_opening_wrap').remove();
				TB.contents_load();
				TB_shared.set('templates--opening--opening_1--js', null);
			});
		}
		/*==/ 오프닝 스킵 하기 ==*/
	};
	/*==========================================
	 * 이벤트 바인딩
	 ==========================================*/

			
	context['obj']= {};
	context['obj'].text = context.param.text.split('');
	context['obj'].logo = context['param'].logo;
	
	bg_style = {'background': '#ff9547'};
	
	TB_contents.render(context, function($topEl){
		var $logo = $topEl.find('.logo_wrap')
				, $text_wrap = $topEl.find('.text_wrap')
				, $word = $text_wrap.find('.word')
				
		skip($topEl);
		
		context['parent'].css(bg_style);
		TB_ani.flow('fadeIn', $topEl);
		tl.set($logo,{opacity:0})
			.set($word, {
				css : {
					'scale': 5
					, 'color':'transparent'
					, 'opacity':0
				}
			})
			.set($topEl, {
				'css' : {
					'display':'block'
				}
			})
			.to($topEl, 0.5, {opacity:1})
			.to($logo, 1, {opacity:1})
			.staggerTo($word, 1, {
				css : {
					'color':'#ffffff'
					, 'scale':1
					, 'opacity':1
				}
				, scale : 1
			}, 0.2)
			.to(context['parent'], 1, {
				opacity:0
				, ease  : Elastic.easeOut
				, onComplete : function(){
					tl.clear();
					$('#TB_opening_wrap').remove();
					TB.contents_load();
					TB_shared.set('templates--opening--opening_1--js', null);
				}
			})
			;	
	});
	
	
});


























