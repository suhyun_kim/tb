/*============================
 * metro_album_1 에 필요한 변수
 * . TB_bbs에서 데이터를 가져온다.
 *  . 네비게이션시  sharedObj와 context 2개를 같이 변경해준다. 
 * {
 * 	!"reqQuery" : '', //리스트를 불러울 리퀘스트 쿼리
 * 	"!group" : ""//요청 할 그룹명TB_bbs사용.
 * 	"insert_reqQuery": "" //앨범 생성시 사용할 쿼리
 * 	"page" : "", // 보여질 페이지의 수
 * 	"list_num":"페이지당 글의 수"
 * 	"category_title" : ""//카테고리 타이틀
 * }
 ============================*/

TB_shared.set('templates--albums--metro_album_1--js', function(context){
	
	var listSrc= $(context['html']).find('.list_wrap').html();//리스트 소스
	
	context['param'].page = context['param'].page||'1';
	
	context['obj'] = {};
	
	$.extend(context['obj'], context['param']);
	
	var Album = (function(){
		var $topEl = {},
				$list_wrap = {},//false
				$box = {},
				$pc_nav = undefined,
				$mobile_nav = undefined,
				loaded = false//캐시된 상태인지 확인.
				, end = false
				, srcStorage = {};
		
		var getSrc = function(body){
			if(context['param']['page']==1&&loaded ===false){//처음 리스트 출력시
				if(body.queries||body._group){//쿼리가 있을 경우 관련 쿼리를 sharedObj에 업데이트 시킨다.
					var sharedObj = TB_shared.get(context['param'].sharedObj);
					var extendObj = {
						queries : body.queries,
						_group : body._group
					}
					TB_shared.set(context['param'].sharedObj, $.extend(sharedObj, extendObj));
					sharedObj = null, extendObj;
				}
				
				$.extend(context['obj'], body);
				
				context['obj'].TB_root_path =  TB_config.get('TB_root_path');
				//console.log(context['obj']);
				TB_contents.render(context, function($el){
					$topEl = context['parent'].find('.templates--albums--metro_algum_1');
					
					if(!$pc_nav){ $pc_nav = context['parent'].find('.pc_nav_container'); }
					if(!$mobile_nav){ $mobile_nav = context['parent'].find('.mobile_nav_container'); }
					
					$list_wrap = $topEl.find('.list_wrap');
					$box = $list_wrap.find('.box');
					eBinding();
					TB_shared.get('metroResponsive')({$scope : $topEl});
					
					TB_ani.flow('fadeIn', $topEl, function(){
						TB_func.imageReady($topEl, function(){
							TB_ani.flow('staggerTo_up', $box);	
							$topEl.closest('.TB_page_container').css('transform','initial');//fixed 방지 제거용
						});
					});
					
				});
				curNav();
				loaded = true;
				
			}else if(context['param']['page']==1&&loaded === true){//한번 로드 시킨상태에서 카테고리를 변경하거나 할때.
				
				$.extend(context['obj'], body);
				
				addSrc = TB_handlebars.returnRender(listSrc, context['obj']);
				
				$list_wrap.html(addSrc).promise().done(function(){
					TB_shared.get('metroResponsive')({$scope : $topEl});
					eBinding();
					$box = $list_wrap.find('.box');
					$topEl.find('img').last().load(function(){
						TB_ani.flow('staggerTo_up', $box);
					});
					TB.binding($list_wrap);
				});
				
			}else{//리스트가 남았을 경우
				addSrc = TB_handlebars.returnRender(listSrc, context['obj']);
				$list_wrap.append(addSrc);
			}
			//console.log(context['obj']);
		};
		
		var render = function(){
			if(srcStorage[context['obj'].category]&&context['obj'].page == 1){//기존에 데이터를 저장했던 경우.
				getSrc(srcStorage[context['obj'].category]);
			}else if(context['obj'].page == 1){//신규데이터
				TB_ajax.get_json({
					'sendData' : TB_func.objCopy(context.param),
					'callback' : function(res){
						srcStorage[context['obj'].category] = res.body;
						getSrc(res.body);
					}
				});
			}
		};
		/*==================================
		 * 앨범 랜더링
		 ==================================*/
		
		var curNav = function(category, callback){
			var curCategory = category||context['obj'].category;
			
			$pc_nav.find('li').each(function(){
				var $this = $(this);
				if($this.attr('data-category') === curCategory){
					$this.addClass('pc_nav_active')
				}else{
					$this.removeClass('pc_nav_active');
				}
			});
			$mobile_nav.find('li').each(function(){
				var $this = $(this);
				if($this.attr('data-category') === curCategory){
					$this.addClass('mobile_nav_active')
				}else{
					$this.removeClass('mobile_nav_active');
				}
			});
			
			context['obj'].category = curCategory;
			if(context['obj'].sharedObj){
				TB_shared.extend(context['obj'].sharedObj, {
					category : curCategory
				});
			}
			if($.type(callback) === 'function'){
				callback();
			}
		};
		/*==================================
		 * 네비게이션 표시
		 ==================================*/
		
		var eBinding = function(){
			$topEl.find('.list_wrap').find('.box').find('.ST_pattern')
			.off('mouseenter.album_1').on('mouseenter.album_1', function(){
				var $img=  $(this).closest('.box').find('img');
				if(Modernizr.csstransitions){
					TweenMax.to($img, 2,{
						scale : 1.3
						, ease : Power4.easeOut
					});
				}
			});
			
			$topEl.find('.list_wrap').find('.box').find('.ST_pattern')
			.off('mouseleave.album_1').on('mouseleave.album_1', function(){
				var $img =  $(this).closest('.box').find('img');
				if(Modernizr.csstransitions){
					TweenMax.to($img, 0.5,{
						scale : 1
					});
				}
			});
			/*===/마우스 오버 애니메이션 ==*/
			
			$topEl.find('.navBtn').off('click')
			.on('click', function(){
				var $this = $(this)
						, selectCategory = $this.attr('data-category')
						, isMobileNav = $this.hasClass('mobile_nav');
						
				if(selectCategory === context['obj'].category){//현재 카테고리가 같을 때
					return false;
				}
				if(isMobileNav){//모바일 네비게이션일 경우 메뉴 폴딩
					$mobile_nav.find('.nav_foldBtn').trigger('click');	
				}
				context['obj'].page = '1';
				if(context['obj'].sharedObj){
					TB_shared.extend(context['obj'].sharedObj, {page : '1'});
				}
				curNav(selectCategory, function(){
					render();
				});
			});
			/*===/카테고리 변경==*/
			
			$topEl.find('.nav_foldBtn').off('click')
			.on('click', function(){
				var $this = $(this);
				if($this.hasClass('fa-caret-down')){
					$this.removeClass('fa-caret-down').addClass('fa-caret-up');
				}else if($this.hasClass('fa-caret-up')){
					$this.removeClass('fa-caret-up').addClass('fa-caret-down');
				}
				$(this).closest('.nav_container').find('ul').slideToggle();
			});
			/*===/메뉴 폴딩==*/
			
			$topEl.find('.box').find('.viewImageBtn')
			.off('click').on('click', function(){
				var $box = $(this).parent(),
						$image = $box.find('.image');
						
				TB_shared.set('templates--albums--metro_algum_1--viewImage'
				, {
					image_src : $image.attr('src'),
					alt : $image.attr('alt'),
					title : $image.attr('title'),
					header : $box.find('.subject').html()
				});
			});
			/*===/이미지 클릭==*/
			
			$topEl.find('.box').find('.removeBtn')
			.off('click').on('click', function(){
				var $this = $(this),
						$thisBox = $this.closest('.box'),
						sendData = $.parseJSON($this.attr('data-sendData'))
						, selected_category = context['obj'].category
						;
						
				sendData.reqType = 'delete';
				sendData.reqQuery = context['obj'].queries.delete_reqQuery;
				loaded = false;//캐쉬 풀기
				//console.log(sendData);
				if(Modernizr.cssanimations){//삭제 애니메이션
					TweenLite
					.to($thisBox, 0.3,{
						scale : 0, opacity: 0, height: 0,
						ease : Power2.easeOut
						, onComplete : function(){
							$thisBox.remove();
						}
					});
				}else{
					$thisBox.fadeOut().remove();
				}
				
				//저장소에서 삭제
				for (var i = 0, len = srcStorage[selected_category].list.length ; i < len ; i++){
					if(sendData.no === srcStorage[selected_category].list[i].no){
						srcStorage[selected_category].list.splice(i,1);
						break;
					}
				}
				
				TB_ajax.get_json({
					'sendData' : sendData,
					'callback' : function(res){
						$this = null, sendData = null;
					}
				});
			});
			/*===/삭제 버튼==*/
			
			$topEl.find('.moreBtn').off('click')
			.on('click', function(){
				if(end) return false;
				context['param']['page']++;
				if(context['obj']['list_info']['total_page'] <= context['param']['page']){
					end = true;
					$topEl.find('.moreBtn').fadeOut();
				}
				render();
			});
			/*==/더 보기 ==*/
		};
		
		return {
			render : render,
			eBinding : eBinding
		}
	})();
	
	
	Album.render();
	

});

