/*============================
 * album_1에 필요한 변수
 * {
 * 	!"reqQuery" : '', //리퀘스트 쿼리
 * 	!"page" : "", // 보여질 페이지의 수
 * 	"category" : "", 
 * 	"searchKey" : "",
 * 	"searchVal" : ""
 * }
 ============================*/

TB_shared.set('templates--albums--album_1--js', function(_context){
	var context = _context;
	var listSrc= $(context['html']).find('.list_wrap').html();//리스트 소스
	
	var Album = (function(){
		var $topEl = {},
				$list_wrap = {},
				end = false;;
		
		var render = function(_callback){
			TB_ajax.get_json({
				'sendData' : context.param,
				'callback' : function(res){
					var addSrc;
					context['obj'] = res.body;
					context['obj'].TB_root_path =  TB_config.get('TB_root_path');
					
					if(context['param']['page']==1){//처음 리스트 출력시
						TB_contents.render(context, function($el){
							$topEl = $el;
							$list_wrap = $topEl.find('.list_wrap');
							eBinding();
						});
					}else{//리스트가 남았을 경우
						addSrc = TB_handlebars.returnRender(listSrc, context['obj']);
						$list_wrap.append(addSrc);
						eBinding();
					}
				}
			});
		};
		/*==================================
		 * 앨범 랜더링
		 ==================================*/
		
		var eBinding = function(){
			if($topEl.find('.list_wrap').find('.coptyBtn').length){
				$topEl.find('.list_wrap').find('.coptyBtn')
				.off('click').on('click', function(){
					TB_func.copyClipboard($(this).attr('data-src'));
				});
			}
			/*===/그림 주소 복사 ==*/
			
			if($topEl.find('.list_wrap').find('.delBtn').length){
				$topEl.find('.list_wrap').find('.delBtn')
				.off('click').on('click', function(){
					var $this = $(this),
							$li = $this.closest('li'),
							sendData = $.parseJSON($this.attr('data-sendData'));
							sendData.reqType = 'delete';
							//console.log(sendData);
					TB_ajax.get_json({
						'sendData' : sendData,
						'callback' : function(res){
							//console.log(res);
							if(Modernizr.cssanimations){
								TweenLite
								.to($li, 0.3,{
									width : 0
									, scale :0
									, opacity: 0
									, ease : Power4.easeIn
									, onComplete : function(){
										setTimeout(function(){
											$li.remove();
										}, 500)
									}
								});
							}else{
								$li.fadeOut().remove();
							}
							$this = null, sendData = null;
						}
					});
				});
			}
			/*===/삭제 버튼==*/
			
			$topEl.find('.moreBtn').off('click')
			.on('click', function(){
				if(end) return false;
				context['param']['page']++;
				if(context['obj']['list_info']['total_page'] <= context['param']['page']){
					end = true;
					$topEl.find('.moreBtn').fadeOut();
				}
				render();
			});
		};
		
		return {
			render : render,
			eBinding : eBinding,
		}
	})();
	
	
	context['param']['page'] = 1;
	Album.render();
	

});

