/*============================
 * metro_bbs_1 에 필요한 변수
 * . TB_bbs에서 데이터를 가져온다.
 *  . 네비게이션시  context['param]은 sharedObj를 참조 하고 있고
		context['obj']는 랜더링 데이터 이므로 신경쓰자
	context['param]은 넘어온 원본을 참조 하고 있음.
 ============================*/

TB_shared.set('templates--albums--metro_album_2--js', function(context){
	var listSrc= $(context['html']).find('.list_wrap').html()//리스트 소스
			, href_param = TB_func.getToParams()||{}//현재 url의 GET변수 객체
			;
	
	//console.log('asdfasf');
	context['obj'] = $.extend(true, context['obj'], context['param']);
	context['obj'].list_options.page = context['param'].list_options.page||1;
	context['obj'].list_options.list_num = context['param'].list_options.list_num||20;
	
	var Bbs = (function(){
		var $topEl = {},
				$list_wrap = {},//false
				$box = {},
				$pc_nav = undefined,
				$mobile_nav = undefined
				, loaded = false//캐시된 상태인지 확인.
				, end = false
				, srcStorage = {}//한번 가져온 데이터를 저장할 객체
				;
		
		var getSrc = function(type){
			
			if(context['obj'].list_options.page == 1 && loaded ===false){//처음 리스트 출력시
				context['obj'].TB_root_path =  TB_config.get('TB_root_path');
				
				TB_contents.render(context, function(_$topEl){
					$topEl = _$topEl;
					//$topEl = context['parent'].find('.templates--albums--metro_algum_1');
					//console.log($topEl.find('.list_wrap')[0].outerHTML)
					
					if(!$pc_nav){ $pc_nav = $topEl.find('.pc_nav_container'); }
					if(!$mobile_nav){ $mobile_nav = $topEl.find('.mobile_nav_container'); }
					
					$list_wrap = $topEl.find('.list_wrap');
					$box = $list_wrap.find('.box');
					eBinding();
					
					TB_shared.get('metroResponsive')({$scope : $topEl});
					
					TB_ani.flow('fadeIn', $topEl, function(){
						TB_ani.flow('staggerTo_up', $box);	
						$topEl.closest('.TB_page_container').css('transform','initial');//fixed 방지 제거용
					});
					
					context['obj'].list_options.use_metro_introduce = false;//소개글은 한번 출력 뒤 parts에서 출력안함.
				});
				
				curNav();
				
				loaded = true;
			}else if(context['obj'].list_options.page ==1&&loaded === true&&srcStorage[context['obj'].list_options.category]){//한번 로드 시킨상태에서 카테고리를 변경하거나 할때.
				
				var renderObj = srcStorage[context['obj'].list_options.category]||context['obj'];
				renderObj.sharedObj = context['obj'].sharedObj;
				addSrc = TB_handlebars.returnRender(listSrc, renderObj);
				context['obj'] = context['obj']
				
				$list_wrap.html(addSrc).promise().done(function(){
					TB_shared.get('metroResponsive')({$scope : $topEl});
					eBinding();
					$box = $list_wrap.find('.box');
					TB_ani.flow('staggerTo_up', $box);
					TB.binding($list_wrap);
					renderObj = null;
				});
				
			}else{//리스트가 남았을 경우
				
				addSrc = TB_handlebars.returnRender(listSrc, context['obj']);
				if(type === 'add'){//리스트를 유지한채 갤러리를 추가
					$topEl.find('.list_wrap').append(addSrc).promise().done(function(){
						$box = $list_wrap.find('.box');
						TB_shared.get('metroResponsive')({$scope : $topEl});
						$box.show();
						//console.log(context['obj'].list_info.page, context['obj'].list_info.total_page);
						context['parts'].render({
							'parts':'templates--albums--metro_album_2--tools'
							, 'obj':context['obj']
							, 'callback':function(){
								//console.log(TB_shared.get('winWid'));
								eBinding();
								setTimeout(function(){
									page_scroll(context['obj'].list_options.page);
								}, 500);
							}
						});
					});
				}else{
					$topEl.find('.list_wrap').html(addSrc).promise().done(function(){
						$box = $list_wrap.find('.box');
						TB_shared.get('metroResponsive')({$scope : $topEl});
						TB_ani.flow('staggerTo_up', $box);	
						//console.log(context['obj'].list_info.page, context['obj'].list_info.total_page);
						context['parts'].render({
							'parts':'templates--albums--metro_album_2--tools'
							, 'obj':context['obj']
							, 'callback':function(){
								eBinding();
							}
						});
					});
				}
				
			}
			//console.log(context['obj']);
		};
		/*=======================================================
		 * 데이터를 바인딩 하고 
		 =======================================================*/
		
		var getListData = function(_callback){
			TB_ajax.get_json({
				'sendData' : TB_func.objCopy(context['obj'].list_options) ,
				'callback' : function(res){
					if(res.body.delete_reqQuery){//삭제 쿼리
						context['obj'].list_options.delete_reqQuery = res.body.delete_reqQuery;
						context['param'].list_options.delete_reqQuery = res.body.delete_reqQuery;
						delete res.body.delete_reqQuery;
					}
					if(res.body.wvm_queries){//wvm쿼리
						context['obj'].wvm_options.queries = res.body.wvm_queries;
						context['param'].wvm_options.queries = res.body.wvm_queries;
						delete res.body.wvm_queries;
					}
					$.extend(context['obj'], res.body);
					if($.type(_callback) === 'function'){
						_callback(context['obj']);
					}
					//WVM에서 list를 사용할 시 list 데이터 넘김
					if(context['obj'].wvm_options.use_list === true){
						context['param'].wvm_options.list = context['obj'].list;
					}
				}
			});
		};
		/*===/list_options를 기준으로 데이터를 받아와 콜백으로 넘긴다.==*/
		
		var render = function(type){
			if(srcStorage[context['obj'].list_options.category]&&context['obj'].list_options.page == 1){//기존에 데이터를 저장했던 경우.
				getSrc();
				return false;
			}
			
			getListData(function(data){
			 	if( context['obj'].list_options.page == 1 &&context['obj'].list_options.category){//신규데이터 저장
					srcStorage[context['obj'].list_options.category] = res.body;
				}
				getSrc(type);
			});
		};
		/*==================================
		 * DB에서 리스트 데이터 받아옴 -> 스로리지에 캐쉬 ->랜더링
		 ==================================*/
		
		var curNav = function(category, callback){
			var curCategory = category||context['obj'].list_options.category;
			
			$pc_nav.find('li').each(function(){
				var $this = $(this);
				if($this.attr('data-category') === curCategory){
					$this.addClass('pc_nav_active')
				}else{
					$this.removeClass('pc_nav_active');
				}
			});
			$mobile_nav.find('li').each(function(){
				var $this = $(this);
				if($this.attr('data-category') === curCategory){
					$this.addClass('mobile_nav_active')
				}else{
					$this.removeClass('mobile_nav_active');
				}
			});
			
			context['obj'].list_options.category = curCategory;
			
			if(context['obj'].sharedObj && category){//insert버튼 클릭시 category를 넘기기 위해 shareObj수정
				TB_shared.get(context['obj'].sharedObj).wvm_options.category = category
			}
			if($.type(callback) === 'function'){
				callback();
			}
		};
		/*==================================
		 * 네비게이션 표시
		 ==================================*/
		
		TB_shared.set('templates--albums--metro_album_2--page_jump', function(data){
			var page = parseInt(data[0].value, 10);
			if(!$.isNumeric(page) ||page <1 ||page > context['obj'].list_info.total_page){
				return false;
			}
			context['obj'].list_options.page = page;
			render();
			return false;
		});
		/*==================================
		 * 지정된 페이지 이동
		 ==================================*/
		
		var page_scroll = function(_page){
			var $page = $topEl.find('.metro_wrap').find('.page_'+_page);
			if(!$page.length){return false;}
			//console.log($page.offset());
			if(TB_shared.get('winWid') > 992){//pc 스크롤
				$(window).scrollLeft($page.offset().left);
			}else{//모바일 스크롤
				$(window).scrollTop($page.offset().top);
			}
			return true
		};
		/*==================================
		 * 페이지 스크롤
		 ==================================*/
		
		var eBinding = function(){
			
			$topEl.find('.navBtn').off('click')
			.on('click', function(){
				var $this = $(this)
						, selectCategory = $this.attr('data-category')
						, isMobileNav = $this.hasClass('mobile_nav');
				
				if(selectCategory === context['obj'].category){//현재 카테고리가 같을 때
					return false;
				}
				if(isMobileNav){//모바일 네비게이션일 경우 메뉴 폴딩
					$mobile_nav.find('.nav_foldBtn').trigger('click');	
				}
				//context['obj'].page = '1';
				context['obj'].list_options.page = 1;
				context['obj'].list_options.category = selectCategory;
				// if(context['obj'].sharedObj){
					// //TB_shared.extend(context['obj'].sharedObj, {page : '1'});
				// }
				curNav(selectCategory, function(){
					render();
				});
			});
			/*===/카테고리 변경==*/
			
			$topEl.find('.nav_foldBtn').off('click')
			.on('click', function(){
				var $this = $(this);
				if($this.hasClass('fa-caret-down')){
					$this.removeClass('fa-caret-down').addClass('fa-caret-up');
				}else if($this.hasClass('fa-caret-up')){
					$this.removeClass('fa-caret-up').addClass('fa-caret-down');
				}
				$(this).closest('.nav_container').find('ul').slideToggle();
			});
			/*===/메뉴 폴딩==*/
			
			$topEl.find('.box').find('.removeBtn')
			.off('click.remove_album').on('click.remove_album', function(){
				var $this = $(this),
						$thisBox = $this.closest('.box'),
						sendData = $.parseJSON($this.attr('data-sendData'))
						, selected_category = context['obj'].list_options.category
						;
						
				sendData.reqType = 'delete';
				sendData.reqQuery = context['obj'].list_options.delete_reqQuery;
				loaded = false;//캐쉬 풀기
				
				if(Modernizr.cssanimations){
					TweenLite
					.to($thisBox, 0.3,{
						scale : 0, opacity: 0, height: 0,
						ease : Expo.easeIn
						, onComplete : function(){
							$thisBox.remove();
						}
					});
				}else{
					$thisBox.fadeOut().remove();
				}
				
				if(srcStorage[selected_category]){//저장소에서 삭제
					for (var i = 0, len = srcStorage[selected_category].list.length ; i < len ; i++){
						if(sendData.no === srcStorage[selected_category].list[i].no){
							srcStorage[selected_category].list.splice(i,1);
							break;
						}
					}
				}
				
				TB_ajax.get_json({
					'sendData' : sendData,
					'callback' : function(res){
						$this = null, sendData = null;
					}
				});
			});
			/*===/삭제 버튼==*/
			
			$topEl.find('.tools_wrap').find('.next_page_btn').off('click.next_page_btn')
			.on('click.next_page_btn', function(){
				if(context['obj'].list_info.total_page <= context['obj'].list_options.page){return false;}
				context['obj'].list_options.page++;
				if(!page_scroll(context['obj'].list_options.page)){//이전 페이지가 없을 경우
					render('add');
				}else{//이전 페이지가 있을 경우.
					context['parts'].render({
						'parts':'templates--albums--metro_album_2--tools'
						, 'callback':function(){
							eBinding();
						} 
					});
				}
				if(context['obj'].list_options.page >= context['obj'].list_info.total_page){//모바일 더보기 버튼 비활성화
					$topEl.find('.mobile_add_btn').hide();
				}
			});
			/*===/다음페이지 버튼==*/
			
			$topEl.find('.tools_wrap').find('.prev_page_btn').off('click.prev_page_btn')
			.on('click.prev_page_btn', function(){
				if(context['obj'].list_options.page <= 1){return false;}
				context['obj'].list_options.page--;
				if(!page_scroll(context['obj'].list_options.page)){//이전 페이지가 없을 경우
					render();
				}else{//이전 페이지가 있을 경우.
					context['parts'].render({
						'parts':'templates--albums--metro_album_2--tools'
						, 'callback':function(){
							eBinding();
						} 
					});
				}
			});
			/*===/이전 페이지 버튼==*/
			
			
			$topEl.find('.mobile_add_btn_wrap').children('.mobile_add_btn')
			.off('click.mobile_add')
			.on('click.mobile_add', function(){
				$topEl.find('.tools_wrap').find('.next_page_btn').trigger('click');
			})
		};
		
		return {
			render : render,
			eBinding : eBinding
		}
	})();
	
	
	Bbs.render();
	

});

