/*============================
 * metro_bbs_1 에 필요한 변수
 * . TB_bbs에서 데이터를 가져온다.
 *  . 네비게이션시  context['param]은 sharedObj를 참조 하고 있고
		context['obj']는 랜더링 데이터 이므로 신경쓰자
 * {
 * }
 ============================*/

TB_shared.set('templates--albums--metro_album_3--js', function(context){
	var listSrc= $(context['html']).find('.list_wrap').html()//리스트 소스
			, href_param = TB_func.getToParams()||{}//현재 url의 GET변수 객체
			;
	
	//console.log('asdfasf');
	//console.log(context['param']);
	
	context['obj'] = $.extend(true, context['obj'], context['param']);
	context['obj'].list_options.page = context['param'].list_options.page||1;
	context['obj'].list_options.list_num = context['param'].list_options.list_num||20;
	
	var Bbs = (function(){
		var $topEl = {},
				$list_wrap = {},//false
				$box = {},
				$pc_nav = undefined,
				$mobile_nav = undefined
				, loaded = false//캐시된 상태인지 확인.
				, end = false
				, srcStorage = {}//한번 가져온 데이터를 저장할 객체
				;
				
		var 	imagesArray_convert = function(list){
			if($.type(list) !== 'array'){return false;}
			var i = 0, len = list.length, arr = [], $memo, $img;
			for(;i < len ; i++){
				arr[i] = list[i];
				arr[i].images = list[i].images.split('||');
				arr[i].images_name = list[i].images_name.split('||');
				arr[i].images_thumbnail_src = list[i].images_thumbnail_src.split('||');
				$memo = $('<div/>').addClass('wrapper').html(list[i].memo);
				$img= $memo.find('img');
				if($img.length){
					$memo.find('img').each(function(){
						$(this).remove();
					});
				}
				arr[i].rm_memo = $memo.html();
			}
			return arr;
		};
		/*=====================================
		 * string 으로 된 images값을 배열로 만들고 memo에서 img가 삭제된 rm_memo를 추가한다. 
		 ======================================*/
		
		var getSrc = function(type){
			
			if(context['obj'].list_options.page == 1 && loaded ===false){//처음 리스트 출력시
				context['obj'].TB_root_path =  TB_config.get('TB_root_path');
				
				TB_contents.render(context, function(_$topEl){
					$topEl = _$topEl;
					//$topEl = context['parent'].find('.templates--albums--metro_algum_1');
					//console.log($topEl.find('.list_wrap')[0].outerHTML)
					
					if(!$pc_nav){ $pc_nav = $topEl.find('.pc_nav_container'); }
					if(!$mobile_nav){ $mobile_nav = $topEl.find('.mobile_nav_container'); }
					
					$list_wrap = $topEl.find('.list_wrap');
					$box = $list_wrap.find('.box');
					eBinding();
					
					TB_shared.get('metroResponsive')({$scope : $topEl});
					
					TB_ani.flow('fadeIn', $topEl, function(){
						TB_ani.flow('staggerTo_up', $box);	
						$topEl.closest('.TB_page_container').css('transform','initial');//fixed 방지 제거용
					});
					
					context['obj'].list_options.use_metro_introduce = false;//소개글은 한번 출력 뒤 parts에서 출력안함.
					context['parts'].func();//파츠 함수 작동
				});
				
				curNav();
				
				TB_contents.init(context['parent'].find('.footer'));
				
				loaded = true;
			}
			//한번 로드 시킨상태에서 카테고리를 변경하거나 할때.
			else if(context['obj'].list_options.page ==1&&loaded === true&&srcStorage[context['obj'].list_options.category]){
				var renderObj = srcStorage[context['obj'].list_options.category]||context['obj'];
				renderObj.sharedObj = context['obj'].sharedObj;
				addSrc = TB_handlebars.returnRender(listSrc, renderObj);
				context['obj'] = context['obj'];
				
				$list_wrap.html(addSrc).promise().done(function(){
					var $addPage = $topEl.find('.page_'+context['obj'].list_options.page).closest('.list_parts ');
					
					//기본 바인딩
					TB.binding($addPage);
					TB.eBinding($addPage);
					TB_shared.get('metroResponsive')({$scope : $topEl});
					eBinding();
					
					TB.binding($list_wrap);
					context['parts'].func();//파츠 함수 작동
					renderObj = null;
				});
				
			}
			else{//리스트가 남았을 경우 리스트 추가
				
				addSrc = TB_handlebars.returnRender(listSrc, context['obj']);
				if(type === 'add'){//리스트를 유지한채 갤러리를 추가
					$topEl.find('.list_wrap').find('.album_item').last().after(addSrc).promise().done(function(){
						var $addPage = $topEl.find('.page_'+context['obj'].list_options.page).closest('.list_parts ');
						
						//기본 바인딩
						TB.binding($addPage);
						TB.eBinding($addPage);
						TB_shared.get('metroResponsive')({$scope : $topEl});
						
						//console.log(context['obj'].list_info.page, context['obj'].list_info.total_page);
						context['parts'].render({
							'parts':'templates--albums--metro_album_3--tools'
							, 'obj':context['obj']
							, 'callback':function(){
								//console.log(TB_shared.get('winWid'));
								eBinding();
								setTimeout(function(){
									page_scroll(context['obj'].list_options.page);
								}, 500);
								context['parts'].func();//파츠 함수 작동
							}
						});
						
						$addPage= null;
					});
				}
				//강제 페이지 이동.
				else{
					$topEl.find('.list_wrap').html(addSrc).promise().done(function(){
						var $addPage = $topEl.find('.page_'+context['obj'].list_options.page).closest('.list_parts ');
						//기본 바인딩
						TB.binding($addPage);
						TB.eBinding($addPage);
						TB_shared.get('metroResponsive')({$scope : $topEl});
						//console.log(context['obj'].list_info.page, context['obj'].list_info.total_page);
						context['parts'].render({
							'parts':'templates--albums--metro_album_3--tools'
							, 'obj':context['obj']
							, 'callback':function(){
								eBinding();
								context['parts'].func();//파츠 함수 작동
							}
						});
					});
				}
				
			}
			//console.log(context['obj']);
		};
		/*=======================================================
		 * 데이터를 바인딩 하고 
		 =======================================================*/
		
		var getListData = function(_callback){
			TB_ajax.get_json({
				'sendData' : TB_func.objCopy(context['obj'].list_options) ,
				'callback' : function(res){
					//console.log(res);
					if(res.body.delete_reqQuery){//삭제 쿼리
						context['obj'].list_options.delete_reqQuery = res.body.delete_reqQuery;
						context['param'].list_options.delete_reqQuery = res.body.delete_reqQuery;
						delete res.body.delete_reqQuery;
					}
					if(res.body.wvm_queries){//wvm쿼리
						context['obj'].wvm_options.queries = res.body.wvm_queries;
						context['param'].wvm_options.queries = res.body.wvm_queries;
						delete res.body.wvm_queries;
					}
					res.body.list = imagesArray_convert(res.body.list);//images 의 배열화 및 memo 내의 img 삭제
					
					$.extend(context['obj'], res.body);
					if($.type(_callback) === 'function'){
						_callback(context['obj']);
					}
					//console.log(context['obj']);
				}
			});
		};
		/*===/list_options를 기준으로 데이터를 받아와 콜백으로 넘긴다.==*/
		
		var render = function(type){
			
			//기존에 데이터를 저장했던 경우.
			if(srcStorage[context['obj'].list_options.category]&&context['obj'].list_options.page == 1){
				getSrc();
				return false;
			}
			
			getListData(function(data){
				
			 	if( context['obj'].list_options.page == 1 &&context['obj'].list_options.category){
					srcStorage[context['obj'].list_options.category] = data.body;
				}
				
				getSrc(type);
			});
			/*==/ 신규데이터 저장==*/
			
			
		};
		/*==================================
		 * DB에서 리스트 데이터 받아옴 -> 스로리지에 캐쉬 ->랜더링
		 ==================================*/
		
		var curNav = function(category, callback){
			var curCategory = category||context['obj'].list_options.category;
			
			$pc_nav.find('li').each(function(){
				var $this = $(this);
				if($this.attr('data-category') === curCategory){
					$this.addClass('active');
				}else{
					$this.removeClass('active');
				}
			});
			$mobile_nav.find('li').each(function(){
				var $this = $(this);
				if($this.attr('data-category') === curCategory){
					$this.addClass('active');
				}else{
					$this.removeClass('active');
				}
			});
			
			context['obj'].list_options.category = curCategory;
			
			if(context['obj'].sharedObj && category){//insert버튼 클릭시 category를 넘기기 위해 shareObj수정
				TB_shared.get(context['obj'].sharedObj).wvm_options.category = category;
			}
			if($.type(callback) === 'function'){
				callback();
			}
		};
		/*==================================
		 * 네비게이션 표시
		 ==================================*/
		
		
		TB_shared.set('templates--albums--metro_album_3--page_jump', function(data){
			var page = parseInt(data[0].value, 10);
			if(!$.isNumeric(page) ||page <1 ||page > context['obj'].list_info.total_page){
				return false;
			}
			context['obj'].list_options.page = page;
			render();
			return false;
		});
		/*==================================
		 * 지정된 페이지 이동
		 ==================================*/
		
		var page_scroll = function(_page){
			var $page = $topEl.find('.metro_wrap').find('.page_'+_page);
			if(!$page.length){return false;}
			if(TB_shared.get('winWid') > 992){//pc 스크롤
				$(window).scrollLeft($page.offset().left);
			}else{//모바일 스크롤
				$(window).scrollTop($page.offset().top);
			}
			return true;
		};
		/*==================================
		 * 페이지 스크롤
		 ==================================*/
		
		var eBinding = function(){
			
			$topEl.find('.navBtn').off('click')
			.on('click', function(){
				var $this = $(this)
						, selectCategory = $this.attr('data-category')
						, isMobileNav = $this.hasClass('mobile_nav');
				
				if(selectCategory === context['obj'].category){//현재 카테고리가 같을 때
					return false;
				}
				if(isMobileNav){//모바일 네비게이션일 경우 메뉴 폴딩
					$mobile_nav.find('.nav_foldBtn').trigger('click');	
				}
				//context['obj'].page = '1';
				context['obj'].list_options.page = 1;
				context['obj'].list_options.category = selectCategory;
				// if(context['obj'].sharedObj){
					// //TB_shared.extend(context['obj'].sharedObj, {page : '1'});
				// }
				curNav(selectCategory, function(){
					render();
				});
			});
			/*===/카테고리 변경==*/
			
			$topEl.find('.nav_foldBtn').off('click')
			.on('click', function(){
				var $this = $(this);
				if($this.hasClass('fa-caret-down')){
					$this.removeClass('fa-caret-down').addClass('fa-caret-up');
				}else if($this.hasClass('fa-caret-up')){
					$this.removeClass('fa-caret-up').addClass('fa-caret-down');
				}
				$(this).closest('.nav_container').find('ul').slideToggle();
			});
			/*===/메뉴 폴딩==*/
			
			$topEl.find('.removeBtn')
			.off('click.remove_album').on('click.remove_album', function(){
				var $this = $(this),
						$thisBox = $this.closest('.metro_fullContainer'),
						sendData = $.parseJSON($this.attr('data-sendData'))
						, selected_category = context['obj'].list_options.category
						;
						
				//delete 쿼리 데이터 작성
				sendData.reqType = 'delete';
				sendData.reqQuery = context['obj'].list_options.delete_reqQuery;
				loaded = false;//캐쉬 풀기
				
				if(Modernizr.cssanimations){
					TweenLite
					.to($thisBox, 0.3,{
						scale : 0, opacity: 0, height: 0,
						ease : Expo.easeIn
						, onComplete : function(){
							$thisBox.remove();
							TB_shared.get('metroResponsive')({$scope : $topEl});
						}
					});
				}else{
					$thisBox.fadeOut().remove();
				}
				
				if(srcStorage[selected_category]){//저장소에서 삭제
					for (var i = 0, len = srcStorage[selected_category].list.length ; i < len ; i++){
						if(sendData.no === srcStorage[selected_category].list[i].no){
							srcStorage[selected_category].list.splice(i,1);
							break;
						}
					}
				}
				//console.log(sendData);
				// console.log(context['obj'].list);
				// return false;
				//테이블 삭제와 서버의 데이터 삭제
				TB_ajax.get_json({
					'sendData' : sendData,
					'callback' : function(res){
						$this = null, sendData = null;
					}
				});
			});
			/*===/삭제 버튼==*/
			
			$topEl.find('.tools_wrap').find('.next_page_btn').off('click.next_page_btn')
			.on('click.next_page_btn', function(){
				if(context['obj'].list_info.total_page <= context['obj'].list_options.page){return false;}
				context['obj'].list_options.page++;
				//console.log(context['obj'].list_options.page);
				if(!page_scroll(context['obj'].list_options.page)){//이전 페이지가 없을 경우
					render('add');
				}else{//이전 페이지가 있을 경우.
					context['parts'].render({
						'parts':'templates--albums--metro_album_3--tools'
						, 'callback':function(){
							eBinding();
						} 
					});
				}
				if(context['obj'].list_options.page >= context['obj'].list_info.total_page){//모바일 더보기 버튼 비활성화
					$topEl.find('.mobile_add_btn').hide();
				}
			});
			/*===/다음페이지 버튼==*/
			
			$topEl.find('.tools_wrap').find('.prev_page_btn').off('click.prev_page_btn')
			.on('click.prev_page_btn', function(){
				if(context['obj'].list_options.page <= 1){return false;}
				context['obj'].list_options.page--;
				if(!page_scroll(context['obj'].list_options.page)){//이전 페이지가 없을 경우
					render();
				}else{//이전 페이지가 있을 경우.
					context['parts'].render({
						'parts':'templates--albums--metro_album_3--tools'
						, 'callback':function(){
							eBinding();
						} 
					});
				}
			});
			/*===/이전 페이지 버튼==*/
			
			$topEl.find('.mobile_add_btn_wrap').children('.mobile_add_btn')
			.off('click.mobile_add')
			.on('click.mobile_add', function(){
				$topEl.find('.tools_wrap').find('.next_page_btn').trigger('click');
			});
			/*===/moblie : 리스트 더보기==*/
			
		};
		/*=========================================
		 * 이벤트 바인딩
		 =========================================*/
		
		
		context['parts'].func('page', function($el){
			
			//console.log('테스트');
			$el.find('.thumbnail_img').off('click.to_image')
			.on('click.to_image', function(){
				var $this = $(this)
						, index = $this.attr('data-index').split('_')
						, $img = $this.closest('.item_container').find('.main_img')
						, selected_item = context['obj'].list[index[0]]
						;

				//메인 이미지변경
				$img.attr('src', context['obj'].TB_root_path+selected_item.images[index[1]])
				.attr('title', selected_item.images_name[index[1]])
				;
				//active 변경
				$this.parent().find('.active').removeClass('active');
				$this.addClass('active');
				
			});
			/*==/섬네일 클릭으로 메인 이미지 변경==*/
			
			//console.log(context['obj']);
			
		});
		/*=========================================
		 * 파츠 : 페이지 표시
		 =========================================*/
		
		
		return {
			render : render,
			eBinding : eBinding
		};
		
	})();
	
	
	
	Bbs.render();
	

});

