/*============================
 ============================*/
TB_shared.set('templates--panorama--webgl--js', function(_context){
	TB_contents.render(_context, function($topEl){
		var camera, 
				scene, 
				renderer,
				textureSrc = TB_config.get('TB_root_path')+'img/panorama/house_1.jpg';
		
		var isUserInteracting = false,
				onMouseDownMouseX = 0,
				onMouseDownMouseY = 0,
				lon = 0, 
				onMouseDownLon = 0,
				lat = 0, 
				onMouseDownLat = 0,
				phi = 0,
				theta = 0;
				
		init();
		animate();
		
		function init(){
			var container,
					mesh;
			
			//스튜디오 지정
			container = document.getElementById('container');
			// container = $('#container')[0].outerHTML;
			
			//카메라 생성
			camera = new THREE.PerspectiveCamera( 80, window.innerWidth / window.innerHeight, 1, 1100 );
			
			//카메라 타겟 지정
			camera.target = new THREE.Vector3( 0, 0, 0 );
			
			scene = new THREE.Scene();
			
			//스피어 생성
			var geometry = new THREE.SphereGeometry( 500, 60, 40 );
			geometry.applyMatrix( new THREE.Matrix4().makeScale( -1, 1, 1 ) );
			
			//텍스쳐 셋팅
			var material = new THREE.MeshBasicMaterial( {
				map: THREE.ImageUtils.loadTexture(textureSrc)
			} );
			
			//지오메트리와 텍스쳐를 가지고 메쉬 생성.										
			mesh = new THREE.Mesh( geometry, material );
			
			//씬에 메쉬 추가
			scene.add( mesh );
			
			//랜더러 생성 후 엘리먼트 추가
			renderer = new THREE.WebGLRenderer();
			renderer.setPixelRatio( window.devicePixelRatio );
			renderer.setSize( window.innerWidth, window.innerHeight );
			container.appendChild( renderer.domElement );
			
			//이벤트 바인딩
			document.addEventListener('mousedown', onDocumentMouseDown , false);
			document.addEventListener('mousemove', onDocumentMouseMove, false);
			document.addEventListener('mouseup', onDocumentMouseUp, false);
			//document.addEventListener('mousewheel', onDocumentMouseWheel, false);
			document.addEventListener('DOMMouseScroll', onDocumentMouseWheel, false);
			
			document.addEventListener('dragover', function(event){
				event.preventDefault();
				event.dataTransfer.dropEffect = 'copy';
			}, false);
			
			document.addEventListener('dragenter', function(event){
				document.body.style.opacity = 0.5;
			}, false);
			
			document.addEventListener('dragleave', function(event){
				document.body.style.opacity = 1;
			}, false);
			
			document.addEventListener('drop', function(event){
				event.preventDefault();
				
				var reader = new FileReader();
				
				reader.addEventlistener('load', function(event){
					material.map.image.src = event.target.result;
					material.map.needsUpdate = true;
				}, false);
				
				reader.readAsDataURL(event.dataTransfer.files[0]);
				
				document.body.style.opacity = 1;
				
			}, false);
			
			window.addEventListener('resize', onWindowResize, false);
			
		}
		/*===========================================
		 * 초기화 함수
		 ===========================================*/
		
		function onWindowResize(){
			camera.aspect = window.innerWidth/window.innerHeight;//카메라 비율
			camera.updateProjectionMatrix();
			
			renderer.setSize(window.innerWidth, window.innerHeight);
		}
		/*===============================================
		 * 윈도우 리사이즈시 화면 비율 조정
		 ===============================================*/
		
		function onDocumentMouseDown(event){
			event.preventDefault();
			isUserInteracting = true;
			onPointerDownPointerX = event.clientX;
			onPointerDownPointerY = event.clientY;
			onPointerDownLon = lon;
			onPointerDownLat = lat;
		}
		/*===============================================
		 * 마우스가 카메라 안으로 들어 왔을 때.
		 ===============================================*/
		
		function onDocumentMouseMove(event){
			if(isUserInteracting === true){
				lon = (onPointerDownPointerX - event.clientX) * 0.1 + onPointerDownLon;
				lat = (onPointerDownPointerY - event.clientY) * 0.1 + onPointerDownLat;
			}
		}
		/*===============================================
		 * 마우스 움직일 시
		 ===============================================*/
		
		function onDocumentMouseUp(event){
			isUserInteracting = false; 
		}
		/*===============================================
		 * 마우스가 카메라에서 벗어 났을 때.
		 ===============================================*/
		
		function onDocumentMouseWheel(event){
			if(event.wheelDeltaY){//webkit
				camera.fov -= event.wheelDeltaY * 0.05;
			}else if(event.wheelDelta){//opera, ie9
				camera.fov -= event.wheelDelta * 0.05;
			}else if(event.detail){
				camera.fov += event.detail * 1.0;
			}
			camera.updateProjectionMatrix();
		} 
		/*===============================================
		 * 마우스 휠시 카메라
		 ===============================================*/
		
		
		function animate(){
			requestAnimationFrame(animate);//프레인 재생
			update();
		}
		/*===============================================
		 * 애니메이션
		 ===============================================*/
		
		
		function update(){
			if(isUserInteracting === false){//자동 회전
				lon +=0.1;
			}
			
			lat = Math.max(-85, Math.min(85, lat));
			phi = THREE.Math.degToRad(90 - lat);
			theta = THREE.Math.degToRad(lon);
			
			camera.target.x = 500*Math.sin(phi) * Math.cos(theta);
			camera.target.y = 500*Math.cos(phi);
			camera.target.z = 500*Math.sin(phi) * Math.sin(theta);
			
			camera.lookAt(camera.target);
			
			//왜곡
			//camera.position.copy(camera.target).negate();
			
			renderer.render(scene, camera);
			
		}
		/*===============================================
		 * 
		 ===============================================*/
	});
});































