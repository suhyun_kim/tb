/*============================
 * metro_bbs_1 에 필요한 변수
 * . TB_bbs에서 데이터를 가져온다.
 *  . 네비게이션시  context['param]은 sharedObj를 참조 하고 있고
		context['obj']는 랜더링 데이터 이므로 신경쓰자
 * {
 * }
 ============================*/

TB_shared.set('templates--bbs--metro_bbs_1--js', function(context){
	
	//console.log(TB_shared.get('templates--bbs--metro_bbs_1--baked'));
	// if(TB_shared.set('templates--bbs--metro_bbs_1--baked')){//베이킹 안되어 있을 때.
		// TB_shared.set('templates--bbs--metro_bbs_1--baked',true);
	// }else{
		// console.log(context);
		// return false;
	// }
	var listSrc= $(context['html']).find('.list_wrap').html()//리스트 소스
			, href_param = TB_func.getToParams()||{}//현재 url의 GET변수 객체
			;
			
	/*
	if(href_param.category){//GET변수 있을 경우 sharedObj와 params 변경
		context['param'].list_options.category = href_param.category;
	}
	*/
	
	context['obj'] = $.extend(true, context['obj'], context['param']);
	context['obj'].list_options.page = context['param'].list_options.page||1;
	context['obj'].list_options.list_num = context['param'].list_options.list_num||20;
	
	var Bbs = (function(){
		var $topEl = {},
				$list_wrap = {},//false
				$box = {},
				$pc_nav = undefined,
				$mobile_nav = undefined
				, loaded = false//캐시된 상태인지 확인.
				, end = false
				, srcStorage = {}//한번 가져온 데이터를 저장할 객체
				;
				
		var list_render = function(){
		};
		/*=====================================
		 * 리스트를 랜더링 하기.
		 =====================================*/
		
		
		var getSrc = function(body){
			
			if(context['obj'].list_options.page == 1 && loaded ===false){//처음 리스트 출력시
				context['obj'].TB_root_path =  TB_config.get('TB_root_path');
				//console.log(context.obj);
				TB_contents.render(context, function(_$topEl){
					$topEl = _$topEl;
					//$topEl = context['parent'].find('.templates--albums--metro_algum_1');
					//console.log($topEl.find('.list_wrap')[0].outerHTML)
					
					if(!$pc_nav){ $pc_nav = $topEl.find('.pc_nav_container'); }
					if(!$mobile_nav){ $mobile_nav = $topEl.find('.mobile_nav_container'); }
					
					$list_wrap = $topEl.find('.list_wrap');
					$box = $list_wrap.find('.box');
					eBinding();
					
					TB_shared.get('metroResponsive')({$scope : $topEl});
					
					TB_ani.flow('fadeIn', $topEl, function(){
						TB_ani.flow('staggerTo_up', $box);	
						$topEl.closest('.TB_page_container').css('transform','initial');//fixed 방지 제거용
					});
					
				});
				
				curNav();
				
				loaded = true;
			}else if(context['obj'].list_options.page ==1&&loaded === true){//한번 로드 시킨상태에서 카테고리를 변경하거나 할때.
				var renderObj = srcStorage[context['obj'].list_options.category]||context['obj'];
				context['obj'].list = renderObj.list;
				context['obj'].list_info = renderObj.list_info;
				//context['obj'] = $.extend(context['obj'], );
				context['parts'].render({
					'parts':'templates--bbs--metro_bbs_1--list'
					, 'callback' : function(){
						TB_shared.get('metroResponsive')({$scope : $topEl});
						eBinding();
						$box = $list_wrap.find('.box');
						TB_ani.flow('staggerTo_up', $box);
						TB.binding($list_wrap);
					}
				});
			}else{//리스트가 남았을 경우
				addSrc = TB_handlebars.returnRender(listSrc, context['obj']);
				$list_wrap.append(addSrc);
			}
			//console.log(context['obj']);
		};
		
		var render = function(){
			
			if(srcStorage[context['obj'].list_options.category]&&context['obj'].list_options.page == 1){//기존에 데이터를 저장했던 경우.
				//console.log(srcStorage[context['obj'].list_options.category]);
				getSrc();
				
			}else if( context['obj'].list_options.page == 1){//신규데이터
				//console.log(context['obj'].list_options);
				
				TB_ajax.get_json({
					'sendData' : TB_func.objCopy(context['obj'].list_options) ,
					'callback' : function(res){
						//console.log(res);
						if(res.body.delete_reqQuery){//삭제 쿼리
							context['obj'].list_options.delete_reqQuery = res.body.delete_reqQuery;
							context['param'].list_options.delete_reqQuery = res.body.delete_reqQuery;
							delete res.body.delete_reqQuery;
						}
						if(res.body.wvm_queries){//wvm쿼리
							context['obj'].wvm_options.queries = res.body.wvm_queries;
							context['param'].wvm_options.queries = res.body.wvm_queries;
							delete res.body.wvm_queries;
						}
						$.extend(context['obj'], res.body);
						//console.log(res.body);
						//console.log(context['obj']);
						
						srcStorage[context['obj'].list_options.category] = res.body;
						//console.log(srcStorage[context['obj'].list_options.category]);
						
						getSrc();
					}
				});
			}
			
		};
		/*==================================
		 * DB에서 리스트 데이터 받아옴 -> 스로리지에 캐쉬 ->랜더링
		 ==================================*/
		
		var curNav = function(category, callback){
			var curCategory = category||context['obj'].list_options.category;
			
			$pc_nav.find('li').each(function(){
				var $this = $(this);
				if($this.attr('data-category') === curCategory){
					$this.addClass('pc_nav_active')
				}else{
					$this.removeClass('pc_nav_active');
				}
			});
			$mobile_nav.find('li').each(function(){
				var $this = $(this);
				if($this.attr('data-category') === curCategory){
					$this.addClass('mobile_nav_active')
				}else{
					$this.removeClass('mobile_nav_active');
				}
			});
			
			context['obj'].list_options.category = curCategory;
			
			if(context['obj'].sharedObj && category){//insert버튼 클릭시 category를 넘기기 위해 shareObj수정
				TB_shared.get(context['obj'].sharedObj).wvm_options.category = category
			}
			if($.type(callback) === 'function'){
				callback();
			}
		};
		/*==================================
		 * 네비게이션 표시
		 ==================================*/
		
		var eBinding = function(){
			
			$topEl.find('.navBtn').off('click')
			.on('click', function(){
				var $this = $(this)
						, selectCategory = $this.attr('data-category')
						, isMobileNav = $this.hasClass('mobile_nav');
				
				if(selectCategory === context['obj'].category){//현재 카테고리가 같을 때
					return false;
				}
				if(isMobileNav){//모바일 네비게이션일 경우 메뉴 폴딩
					$mobile_nav.find('.nav_foldBtn').trigger('click');	
				}
				//context['obj'].page = '1';
				context['obj'].list_options.page = 1;
				context['obj'].list_options.category = selectCategory;
				// if(context['obj'].sharedObj){
					// //TB_shared.extend(context['obj'].sharedObj, {page : '1'});
				// }
				curNav(selectCategory, function(){
					render();
				});
			});
			/*===/카테고리 변경==*/
			
			$topEl.find('.nav_foldBtn').off('click')
			.on('click', function(){
				var $this = $(this);
				if($this.hasClass('fa-caret-down')){
					$this.removeClass('fa-caret-down').addClass('fa-caret-up');
				}else if($this.hasClass('fa-caret-up')){
					$this.removeClass('fa-caret-up').addClass('fa-caret-down');
				}
				$(this).closest('.nav_container').find('ul').slideToggle();
			});
			/*===/메뉴 폴딩==*/
			
			$topEl.find('.box').find('.contextBtn')
			.off('click').on('click', function(){
				var $box = $(this).parent(),
						$image = $box.find('.image');
						
				TB_shared.set('templates--bbs--metro_bbs_1--view'
				, {
					image_src : $image.attr('src'),
					alt : $image.attr('alt'),
					title : $image.attr('title'),
					header : $box.find('.subject').html()
				});
				
			});
			/*===/이미지 클릭==*/
			
			$topEl.find('.box').find('.removeBtn')
			.off('click').on('click', function(){
				var $this = $(this),
						$thisBox = $this.closest('.box'),
						sendData = $.parseJSON($this.attr('data-sendData'))
						, selected_category = context['obj'].list_options.category
						;
						
				sendData.reqType = 'delete';
				sendData.reqQuery = context['obj'].list_options.delete_reqQuery;
				loaded = false;//캐쉬 풀기
				
				if(Modernizr.cssanimations){
					TweenLite
					.to($thisBox, 0.3,{
						scale : 0, opacity: 0, height: 0,
						ease : Expo.easeIn
						, onComplete : function(){
							$thisBox.remove();
						}
					});
				}else{
					$thisBox.fadeOut().remove();
				}
				
				//저장소에서 삭제
				for (var i = 0, len = srcStorage[selected_category].list.length ; i < len ; i++){
					if(sendData.no === srcStorage[selected_category].list[i].no){
						srcStorage[selected_category].list.splice(i,1);
						break;
					}
				}
				
				TB_ajax.get_json({
					'sendData' : sendData,
					'callback' : function(res){
						$this = null, sendData = null, selected_category = null;
					}
				});
			});
			/*===/삭제 버튼==*/
			
			/*==/더 보기 ==*/
		};
		
		return {
			render : render,
			eBinding : eBinding
		}
	})();
	
	
	Bbs.render();
	

});

