/*============================
 ============================*/

TB_shared.set('templates--bbs--bbs_1--js', function(context){
	var listSrc= $(context['html']).find('.list_wrap').html()//리스트 소스
			, href_param = TB_func.getToParams()||{}//현재 url의 GET변수 객체
			, $topEl
			;
	
	context['obj'] = $.extend(true, context['obj'], context['param']);
	context['obj'].list_options.page = context['param'].list_options.page||1;
	context['obj'].list_options.list_num = context['param'].list_options.list_num||15;
	
	
	var Bbs = (function(){
		var $topEl = {}
				, $list_wrap = {}//false
				, loaded = false//로드된 상태인지 확인.
				, srcStorage = {}//한번 가져온 데이터를 저장할 객체
				;
		
		var getSrc = function(){
			//console.log(context['obj'].list_options.no);
			//return false;
			if(loaded === false){
				loaded === true;
				TB_contents.render(context, function(topEl){
					$topEl = topEl;
					$list_wrap = $topEl.find('.list_wrap')
					eBinding();
					TB_ani.flow('fadeIn', $topEl, function(){
						$topEl.find('.mobile_add_btn_wrap').removeClass('ST_dis_none');
					});
				});
			}else{
				context['parts'].render({
					'parts':'templates--bbs--bbs_1--list'
					, 'callback':function(data){
						eBinding();
					}
				});
			}
			return false;
		};
		/*=======================================================
		 * 데이터를 가지고 랜더링한다.
		 =======================================================*/
		
		var parts_renewal = function(){
			$('#TB_page_container_'+TB_page.locationDec().map.src)
			.find('.templates--bbs--bbs_1').each(function(){
				context['parts'].renewal($(this));
			});
		};
		/*==================================
		 * 파츠 리뉴얼
		 ==================================*/

		
		var getListData = function(_callback){
			//console.log('asdfsadfsad');
			//console.log(context['obj']);
			TB_ajax.get_json({
				'sendData' : TB_func.objCopy(context['obj'].list_options) ,
				'callback' : function(res){
					
					var today = TB_func.getDate('Y-M-D')
							, writetime_arr = [];
					//console.log(res.body);
					if(res.body.delete_reqQuery){//삭제 쿼리
						context['obj'].list_options.delete_reqQuery = res.body.delete_reqQuery;
						context['param'].list_options.delete_reqQuery = res.body.delete_reqQuery;
						//delete res.body.delete_reqQuery;
					}
					if(res.body.wvm_queries){//wvm쿼리
						context['obj'].wvm_options.queries = res.body.wvm_queries;
						context['param'].wvm_options.queries = res.body.wvm_queries;
						delete res.body.wvm_queries;
					}
					$.extend(context['obj'], res.body);
					
					for (var i = 0, len = context['obj'].list.length ; i < len ; i++){//시간 표시 설정
						writetime_arr = context['obj'].list[i].writetime.split(' ');
						
						if(today == writetime_arr[0]){//오늘 작성한 글은 시간만 표시
							context['obj'].list[i].writetime = writetime_arr[1];
						}else{//오늘 작성한 글이 아닌경우 날짜만 표시
							context['obj'].list[i].writetime = writetime_arr[0];
						}
					}
					//console.log(context['obj']);
					
					if(context['obj'].list_options.page > 1){//이전 페이지
						context['obj'].list_info.prev_page = context['obj'].list_info.page-1;
					}
					if(context['obj'].list_options.page < context['obj'].list_info.total_page){//다음 페이지
						context['obj'].list_info.next_page = parseInt(context['obj'].list_info.page,10)+1;
					}
					
					if(context['obj'].list_info.cur_block > 1){//이전 블럭 페이지 
						context['obj'].list_info.prev_block = context['obj'].list_info.page-context['obj'].list_info.page_num
					}
					
					if(context['obj'].list_info.cur_block < context['obj'].list_info.totalBlock ){//다음 블럭
						context['obj'].list_info.next_block = context['obj'].list_info.cur_block*context['obj'].list_info.page_num+1
					}
					
					if($.type(_callback) === 'function'){
						_callback(context['obj']);
					}
				}
			});
		};
		/*===/list_options를 기준으로 데이터를 받아와 콜백으로 넘긴다.==*/
		
		var render = function(type){
			getListData(function(data){
				//console.log(data);
				getSrc();
			});
		};
		/*==================================
		 * 데이터를 받아와서 랜더링
		 ==================================*/
		
		var group_render = function(param){
			//console.log(param);
			context['obj'].list_options['no'] = param['no']||undefined;
			context['obj'].list_options['page'] = param['page']||1;
			context['obj'].list_options['_group'] = param['_group'];
			context['obj'].list_options['admin_only'] = param['admin_only'];
			context['obj'].wvm_options['_group'] = param['_group'];
			TB_shared.get(context['obj'].sharedObj).wvm_options._group = param['_group']; 
			/*==/파라메터 적용==*/
			
			context['parent'].find('.search_form').find('.ST_list_search_input').val('');
			parts_render(context['obj'].list_options['page']);
		};
		/*==================================
		 * 외부에서 _group명을 바꿔서 랜더링 할때 노출 시킬 함수.
		 ==================================*/
		
		var parts_render = function(page, search_sw){
			context['obj'].list_options.page = page||context['obj'].list_options.page;
			context['obj'].list_options.use_search = search_sw||false;
			getListData(function(){
				var arr = [];
				arr.push('templates--bbs--bbs_1--pc_page_link');
				arr.push('templates--bbs--bbs_1--list');
				arr.push('templates--bbs--bbs_1--tools');
				arr.push('templates--bbs--bbs_1--footer_btns');
				
				context['parts'].render({
					'parts':arr
					, 'callback':function(){
						eBinding();
					}
				});
			});
		};
		/*==================================
		 * 일부분만 랜더링
		 * params
		 * 	. page(int) : 검색할 페이지
		 * 	. search_sw(boolean) : 
		 ==================================*/
		
		var search_render = function(page, field, value){
			if(!value){//검색 값이 없을 경우.
				parts_render(1, false);
			}
			//console.log(field, value);
			context['obj'].list_options.search_field = field;//검색 필드 설정
			context['obj'].list_options.search_value = value;//검색 값 설정
			parts_render(page,true);
		};
		/*==================================
		 * 검색
		 ==================================*/
		
		TB_shared.set('templates--bbs--bbs_1--search', function(data){
			//console.log(data);
			search_render(1,data[0].value, data[1].value);
			return false;
		});
		/*==================================
		 * 검색
		 ==================================*/
		
		TB_shared.set('templates--bbs--bbs_1--page_jump', function(data){
			var page = parseInt(data[0].value, 10);
			if(!$.isNumeric(page) ||page <1 ||page > context['obj'].list_info.total_page){
				return false;
			}
			parts_render(page);
			return false;
		});
		/*==================================
		 * 페이지 점프
		 ==================================*/
		
		var view = function(no){
			var $item =  $topEl.find('.ST_list').find('li').last().find('a')
					, href = $item.attr('href').split('/')[1].split('?')
					, data = href[1]
					, i
					, new_location = ''
					, arr = []
					, num = 0
					;
			data = TB_func.sendDataConvert('?'+data);
			data.no = no;
			$.each(data, function(key, val){
				new_location += (num === 0)?key+'='+val:'&'+key+'='+val;
				num++;
			});
			new_location = '#!/'+href[0]+'?'+new_location;
			window.location.href = new_location;
			$item = null, href = null, data = null, i = null, new_location = null, arr = null, num = null;
		};
		/*==================================
		 * no 값이 있을 경우.view 하기.
		 * params
		 * 	. no(int or string) :  view 할 no 값.
		 ==================================*/
		
		TB_shared.set('templates--bbs--bbs_1--delete_list_beforeFunc', function(data){
			var $numbers = $topEl.find('.list_del_no:checked')
					, numbers = ''
					;
			if(!$numbers.length){//체크가 안되어 있을 경우
				TB_func.popupDialog('삭제할 항목을 선택하십시요.');
				return false;
			}
			$numbers.each(function(idx){
				var no = $(this).val();
				numbers += (idx <= 0)?no:','+no; 
			});
			TB_ajax.get_json({
				'sendData':{
					'reqType' : 'delete'
					, 'reqQuery' : context['obj'].list_options.delete_reqQuery
					, 'numbers' : numbers
				}
				, 'callback' : function(res){
					parts_render();
				}
			})
		});
		/*==================================
		 * 다중 리스트 삭제
		 ==================================*/
		
		var eBinding = function($scope){
			var $scope  = $scope||$topEl;
			
			$topEl.find('.box').find('.removeBtn')
			.off('click.remove_album').on('click.remove_album', function(){
				var $this = $(this),
						$thisBox = $this.closest('.box'),
						sendData = $.parseJSON($this.attr('data-sendData'));
						
				sendData.reqType = 'delete';
				sendData.reqQuery = context['obj'].list_options.delete_reqQuery;
				loaded = false;//캐쉬 풀기
				
				if(Modernizr.cssanimations){
					TweenLite
					.to($thisBox, 0.3,{
						scale : 0, opacity: 0, height: 0,
						ease : Expo.easeIn
						, onComplete : function(){
							$thisBox.remove();
						}
					});
				}else{
					$thisBox.fadeOut().remove();
				}
				//console.log(sendData);
				TB_ajax.get_json({
					'sendData' : sendData,
					'callback' : function(res){
						$this = null, sendData = null;
					}
				});
			});
			/*===/삭제 버튼==*/
			
			$topEl.find('.link')
			.off('click.page_move')
			.on('click.page_move', function(){
				var page = $(this).attr('data-page')
						, $search_form= undefined
						, search_field = undefined
						, search_value = undefined
						;
				if(page == context['obj'].list_options.page){
					return false;
				}
				if(context['obj'].list_options.use_search === true){//한번 검색 했을 경우.
					$search_form =$topEl.find('.search_form') ;
					search_field = $search_form.find('.search_field').val();
					search_value = $search_form.find('.search_value').val();
					//console.log(search_field, search_value);
					search_render(page,search_field, search_value);
				}else{//검색을 안하고 순서대로
					$topEl.find('.tools_wrap').find('.page_move').find('.input_page_num')
					.val(page).closest('form').find('.submit').trigger('click');
				}
				
				page = null, $search_form = null, search_field = null, search_value= null;
			});
			/*===/pc:페이지 버튼==*/
			
			$topEl.find('.mobile_add_btn_wrap').children('.mobile_add_btn')
			.off('click.mobile_add')
			.on('click.mobile_add', function(){
				$topEl.find('.tools_wrap').find('.next_page_btn').trigger('click');
			});/*==/모바일 다음 페이지 버튼 ==*/
			
			if(context['obj'].list_options.no){//no 값이 있을 경우 view 
				view(context['obj'].list_options.no);
			}
		};
		
		TB_shared.set('templates--bbs--bbs_1--group_render', function(param){
			parts_renewal();
			Bbs.group_render(param);
		});
		//외부에서 그룹 변수만 가지고 랜더링 할수있게.
	
		return {
			render : render,
			eBinding : eBinding
			, group_render: group_render
		}
	})();
	
	
	Bbs.render();
	
	
});

