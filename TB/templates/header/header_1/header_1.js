
TB_shared.set('contents--dreamyacht--header--js', function(context){
	var context = context
		, $nav_link = {}//네게이션 버튼 엘리먼트
		, $pc = {}
		, $cursor_l = {}//커서 left line
		, $cursor_c = {}//커서 가운데 동그라미
		, $window = TB_shared.get('$window')
		;
	
	
	context.obj = {};
	
	context.obj.menu = TB_func.objCopy(TB_page.sgmap().menu);//메뉴 복사
	//index 삭제
	if(context.obj.menu.length){
		context.obj.menu.splice(0,1);
	}
	//console.log(TB_page.sgmap());
	
	/*==================================================
	 * 변수 설정
	 ==================================================*/
	
	
	var init = function(context){
		$pc = context.parent.find('.pc_header_wrap');
		
		var $pc_cursor = $pc.find('.cursor_container')
			, menu = TB_page.sgmap().menu;
		
		$nav_link = context.parent.find('.nav').find('a');
		
		$cursor_l = $pc.find('.cursor_line');
		$cursor_c = $pc_cursor.find('.center');
		
		if(!menu.length){return false;}
		
		//현재 페이지 표시
		cur_page();
		evt(context.parent);
		cursor_on();
		
		//mobile_nav
		context.parent.find('.button-collapse').sideNav();
		
		//팝업
		TB_page.sgmap().templates.backdrop_popup.on();
	};
	
	
	//현재 페이지 표시
	var cur_page = function(map){
		try{
			var map = map||TB_page.locationDec().map;
			$nav_link.each(function(){
				var $this = $(this)
					, this_page = $this.attr('data-page')
					;
				if(this_page === map.page || this_page === map.parent){
					$this.addClass('active');
				}else{
					$this.removeClass('active');
				}
			});
		}catch(e){
			throw e;
		}
	};
	
	//마우스 오버시 작동들
	var cursor_on = function($selected_el, hover){
		var $selected_el = $selected_el||$pc.find('.page_link').first()
			, x = $selected_el.offset().left+Math.floor($selected_el.outerWidth()/2)
			, tl = (Modernizr.csstransforms3d)?new TimelineMax():undefined
			, $pc_nav = $pc.find('.nav_container')
			, $child_container
			, $child_bg = $pc.find('.child_bg')
			, left_limit = $pc_nav.offset().left
			, right_limit = left_limit+$pc_nav.width()
			, options_l = {
				transformOrigin:"50% 50%"
				, scaleX : (hover)?1:0
				, autoAlpha : (hover)?1:0
				, ease : Expo.easeOut
			}
			, options_c = {
				x : x-7
				, autoAlpha : (hover)?1:0
				, ease : Expo.easeOut
			}
			;
			
		
		if(!tl){return false;}
		
		//마우스 리브시 
		if(hover === false){
			$child_container = $pc.find('.childMenu_container')
			delete options_c.x;
			$child_bg.addClass('ST_opacity_0').removeClass('active');
			//$child_container.addClass('ST_dis_none');
		}else if(hover === true){
			$child_container = $selected_el.find('.childMenu_container');
			
			$child_bg.removeClass('ST_opacity_0').addClass('active');
			//$child_container.removeClass('ST_dis_none');
		}
		
		tl
		.clear()
		.to($cursor_c, 1, options_c)
		.to($cursor_l, 1, options_l, '-='+1)
		;
	};
	/*==/parent_menu에 마우스 오버시 /==*/
	
	
	/*==================================================
	 * 함수
	 ==================================================*/
	
	
	var evt = function($top){
		var $top = $top
			, prevent_scroll = false
			;
		
		
		//PC 네비게이션 마우스 오버
		$pc.find('.nav').find('.parent_menu').off('mouseenter.pc_nav_enter')
		.on('mouseenter.pc_nav_enter', function(){
			cursor_on($(this),true);
		});
		
		//PC 네비게이션 마우스 아웃
		$pc.find('.nav_container').off('mouseleave.pc_nav_leave')
		.on('mouseleave.pc_nav_leave', function(){
			var $active = $pc.find('.nav').find('.active');
			$active = ($active.length)?$active:$pc.find('.page_link').first();
			cursor_on($pc.find('.page_link').first(),false);
		});
		
		//스크롤시 최상단 벗어날 경우 헤더 애니메이션
		$window.off('scroll.pc_header')
		.on('scroll.pc_header', function(e){
			if(prevent_scroll === false){
				prevent_scroll = true;
				
				var scroll = $window.scrollTop();
				
				setTimeout(function(){
					prevent_scroll = false;
				}, 0);
				
				if(scroll > 0){
					$top.find('.pc_header_wrap, .mobile_header_wrap, .child_bg').addClass('scrolled');
				}else{
					$top.find('.pc_header_wrap, .mobile_header_wrap, .child_bg').removeClass('scrolled');
				}
				
			}
		});
		
		
		$top.find('.mobile_header_wrap, .pc_header_container').find('.parent_link, .m_child_link').off('click.m_link')
		.on('click.m_link', function(){
			$window.scrollTop(0);
			$('#sidenav-overlay').trigger('click');
		});
		/*==/네비게이션 클릭/==*/
		
	};
	/*==/이벤트 함수/==*/
	
	
	TB_page.moveAc(function(map){
		cur_page(map.map);
	});
	/*==/페이지 변경시 이벤트 추가./==*/
	
	/*==================================================
	 * 이벤트
	 ==================================================*/
	
	
	
	TB_contents.render(context, function(){
		init(context);
		
	});
	/*==================================================
	 * 컨트롤
	 ==================================================*/
});
