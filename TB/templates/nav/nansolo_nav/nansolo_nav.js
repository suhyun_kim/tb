TB_shared.set('templates--nav--nansolo_nav--js', function(context){
	var $topEl = {},
			$oldPointer = [];
	
	context['obj'] = {};
	context['obj'].menu = TB_page.sgmap().menu;
	
	context['obj'].shape = {};
	
	context['obj'].shape.select_user = {}//모양 폼의 유저 정보 
	context['obj'].shape.select_user.list = [
		{
			"member_grade" : "admin"
			, "user":"총관리자"
		},{
			"member_grade" : "manager"
			, "user":"메니져"
		},{
			"member_grade" : "user"
			, "user":"일반유저"
		}
	];
	
	context.obj.TB_userInfo = context.obj.TB_userInfo||TB_config.get('user');
	context.obj.TB_root_path = context.obj.TB_root_path||TB_config.get('TB_root_path');
	
	/*=====================================================
	 * 변수 설정.
	 =====================================================*/
	
	
	var set_icons = function(_menu){
		var menu = _menu
				, i = 0, j = 0
				, len = menu.length
				, icons = ['user','cutlery', 'map', 'book', 'list-alt', 'trophy']
				;
		for(; i < len ; i++){
			 if(menu[i].page != 'index'){//인덱스 페이지를 제외한 페이지
			 	menu[i].icon = icons[j];
			 	j++;
			 }
		}
		return menu;
	};
	/*==/메뉴 아이콘 셋팅 ==*/
	
	var render = function(){
		TB_contents.render(context, function(_$topEl){
			$topEl = _$topEl;
			context['parts'].func();
			evt();
			
			//로그인 후 성별 정보가 없을 경우 처리
			if(context.obj.TB_userInfo){
				if(!context.obj.TB_userInfo.gender ||context.obj.TB_userInfo.gender === 'undefined'){
					TB_ani.show_backdrop({
						src : 'templates--login--gender_select'
						, width: 500
						, height: 300
						, history_back : false
						, hide_lock : true
					});
				}
			}
		});
	};
	/*==/메뉴 랜더링 함수==*/
	
	var activeMenu = function(map){
		var map = map||TB_page.locationDec().map;
		if(!map){map = 'index';}
		for(var i = 0 , len = context['obj'].menu.length ; i < len ; i++){
			context['obj'].menu[i].active = (map.page == context['obj'].menu[i].page)?'active':'';
			
			//비활성화할 페이지
			if(context['obj'].menu[i].page == 'index' 
				|| (!context['obj'].TB_userInfo &&context['obj'].menu[i].page ==='mypage')
			){
				context['obj'].menu[i].visibility = false;
			}else{
				context['obj'].menu[i].visibility = true;
			}
			
			//자식 메뉴가 있을 경우.
			if(context['obj'].menu[i].childMenu){
				for(var k = 0, len_2 =context['obj'].menu[i].childMenu.length ; k < len_2 ; k++){
					if(map.page ==  context['obj'].menu[i].childMenu[k].page){
						context['obj'].menu[i].active = 'active';
					}else {
						context['obj'].menu[i].active = '';
						
					}
				}
			}
		}

		
		i = null, len = null, k = null, len_2 = null;
	};
	/*==/현재 메뉴 표시==*/
	
	var init = function(){
		//초기페이지는 인덱스
		activeMenu();
		
		//메뉴 아이콘 셋팅
		context['obj'].menu = set_icons(context['obj'].menu);
		
		//메뉴 맵을 받아 왔을 경우와 받아오지 않았을 경우 따로 처리
		if(TB_page.sgmap().menu.length > 0){
			render();
		}else {
			TB.menuSet(function(){
				render();
			});
		}
		

	};
	/*==/초기화/==*/
	
	/*=====================================================
	 * 함수
	 =====================================================*/
	
	
	
	context['parts'].func('select_user', function($el){
		$el.find('.user_level').off('click.change_user_level')
		.on('click.change_user_level', function(){
			context['obj'].shape.select_user.level = $(this).attr('data-user_level');
			context['parts'].render('select_user');
		});
	});
	/*==/관리자 변경 ==*/
	
	var evt = function(){
		
		context.parent.off('mouseenter.pc_nav_scroll_on')
		.on('mouseenter.pc_nav_scroll_on', function(){
			TB_shared.get('metro_scroll')('y');
		});
		/*==/pc nav 마우스 오버 ==*/
		
		context.parent.off('mouseleave.pc_nav_scroll_off')
		.on('mouseleave.pc_nav_scroll_off', function(){
			TB_shared.get('metro_scroll')('x');
		});
		/*==/pc nav 마우스 leave ==*/
		
		context.parent.find('.link_mypage').off('click.mypage_link')
		.on('click.mypage_link', function(){
			if(TB_shared.get('contents--nansolo--mypage--js')){
				TB_shared.get('contents--nansolo--mypage--js').refresh();
				(TB_shared.get('winWid')>991)?TB_shared.get('$window').scrollLeft(0):TB_shared.get('$window').scrollTop(0);
				TB_shared.get('metro_set_curPosi')(0);
			}
		});
		/*==/mystory 클릭 ==*/
		
	};

	/*================================================
	 * 이벤트
	 * ================================================*/
	
	
	
	
	TB_page.moveAc(function(page){
		activeMenu(page.map);//현재 페이지 표시.
		
		//활성화 페이지 표시
		context['parts'].render({
			parts : 'menu'
			, callback : function(){
				evt();
			}
		});
	});
	/*================================================
	 * 페이지 움직일시 현재 위치 표시
	 ================================================*/
	
	
	
	init();
	/*================================================
	 * 초기화
	 ================================================*/
	
	
});
