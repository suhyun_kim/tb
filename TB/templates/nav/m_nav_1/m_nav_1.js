/*==============================================
 * 
 ==============================================*/
TB_shared.set('templates--nav--m_nav_1--js', function(_context){
	var $topEl = {},
			$oldPointer = []
			, $childMenu_toggle//메뉴 전체 컨데
			;
	
	TB_page.moveAc(function(data){
		cur_location(data);
	});
	
	var cur_location = function(_map){
		var map =_map||TB_page.locationDec()
				, $pointer = (map)?$topEl.find('[data-tb_link_page='+map.map.page+']'):[]
				, level
				;
				
		if(!$pointer.length){//선택된 것이 없을 경우 index페이지 같은 경우.
			clear();
		}else{
			level = $pointer.attr('data-TB_link_level');
		}
		
		if($oldPointer.length){//이전 기록이 있을 경우.
			if(level === '1'){//이동한것 level 1일때
				$oldPointer.parent()
				.removeClass('.topMenu_active').removeClass('selected')
				.find('.childMenu_active').removeClass('childMenu_active')
				.find('.active_li').removeClass('active_li');
			}else if(level === '2'){//레벨 2 이동시
				var old_topMenu_idx  = $oldPointer.closest('.menu_topMenu').index()
						, topMenu_idx = $pointer.closest('.menu_topMenu').index()
						;
				if(old_topMenu_idx !== topMenu_idx){//같은 탑메뉴가 아닐경우.
					clear($oldPointer.closest('.menu_topMenu'));
				}
			}
		}
		
		$topEl.find('.menu_topMenu').each(function(idx){
			var $this = $(this);
			
			if(!$this.hasClass('selected')){//초기화
				if($this.hasClass('topMenu_active')){
					$this.removeClass('topMenu_active');
				}
				if($this.find('.childMenu_active').length){
					$this.find('.childMenu_active').removeClass('childMenu_active');
				}
				//$this.find('.childMenu_Container').css('z-index', 'auto').fadeOut();
			}
			
			if($this.find('.active_li').length){
				$this.find('.active_li').removeClass('active_li');
			}
		});
		
		if(level === '1'){
			$pointer.parent().addClass('topMenu_active').addClass('selected')
			.find('.childMenu_Container').addClass('childMenu_active')
			.find('li').first().addClass('active_li');
		}else if(level === '2'){
			$pointer.parent().addClass('active_li')
			.closest('.childMenu_Container').addClass('childMenu_active')
			.closest('.menu_topMenu').addClass('topMenu_active').addClass('selected');
		}
		$oldPointer = $pointer;
	};
	/*==========================================
	 * 현재 위치 표시 
	 ==========================================*/
	
	var clear = function($menu){
		if($menu instanceof $){
			if($menu.hasClass('menu_topMenu')){//탑메뉴와 하위 메뉴 클리어
				$menu
				.removeClass('.topMenu_active').removeClass('selected')
				.find('.childMenu_active').removeClass('childMenu_active')
				.find('.active_li').removeClass('active_li');
			}
		}else{//전체 클리어
			$topEl.find('.selected').removeClass('selected').end()
			.find('.topMenu_active').removeClass('topMenu_active').end()
			.find('.childMenu_active').removeClass('childMenu_active').end()
			.find('.active_li').removeClass('active_li').end()
			;
		}
	};
	/*==========================================
	 *  메뉴 클리어.
	 ==========================================*/
	
	var setNav = function($topEl){
		var $bg = $topEl.find('.bg'),
				$nav = $topEl.find('.nav_wrap')
				;
		$nav.css('right', '-300px');		
	};
	/*==========================================
	 *  네비 보이기
	 ==========================================*/
	
	var showNav = function($topEl){
		var $bg = $topEl.find('.bg'),
				$nav = $topEl.find('.nav_wrap')
				, tl
				, speed = 0.3
				;
		if(Modernizr.csstransforms3d){
			tl = new TimelineMax({ease:Power4.easeIn})
			TB_ani.ready($topEl, function(){
				TB_ani.ready($bg, function(){
					tl
					.to($topEl, speed,{autoAlpha:1})
					.to($bg, speed, {autoAlpha:0.5}, '-='+speed)
					.to($nav, speed, {right:0}, '-='+speed)
					;
				})
			});
		}else{
			$($topEl, $bg).fadeIn();
			$nav.css('right', 0);
		}
		//TB_ani.flow('fadeIn', $topEl);
		// TweenMax
		// .to($nav, 1,{right : 0, ease : Back.easeIn});
	};
	/*==========================================
	 *  네비 보이기
	 ==========================================*/
	
	var child_nav_show = function($el){
		$el.find('.fa').removeClass('fa-caret-down').addClass('fa-caret-up').end()
		.closest('li').find('.childMenu_Container').slideDown();
	};
	/*==========================================
	 *  자식 네비 보이기
	 * params
	 * 	.$el(jquery obj) : 토글 버튼 객체
	 ==========================================*/
	
	var child_nav_hide = function($el){
		$el.find('.fa').removeClass('fa-caret-up').addClass('fa-caret-down').end()
		.closest('li').find('.childMenu_Container').slideUp();
	};
	/*==========================================
	 *  자식 네비 보이기
	 * params
	 * 	.$el(jquery obj) : 토글 버튼 객체
	 ==========================================*/
	
	var hideNav = function(){
		$topEl.css('display','none')
		.find('.nav_wrap').css('right',-300)
		;
	};
	/*==========================================
	 *  네비 숨기기
	 ==========================================*/
	
	var eBinding = function(){
		
		$topEl.find('.showContentBtn')
		.on('click', function(){
			hideNav();
		});
		
		$topEl.find('.nav_wrap').find('a')
		.on('click.hide_m_nav', function(){
			hideNav();
		});
		
		$topEl.find('.bg').on('click.hide_m_nav', function(){
			hideNav();
		});
		/*==/네비게이션 전체 보기 숨기기==*/
		
		if($childMenu_toggle.length){
			$childMenu_toggle.off('click.child_show')
			.on('click.child_show', function(){
				var $this = $(this)
						, direction = $this.find('.fa').hasClass('fa-caret-down')
						;
				(direction)?child_nav_show($this):child_nav_hide($this);
			});
		}
		/*==/childMenu 토글 이벤트==*/
		
	};
	/*==========================================
	 *이벤트 바인딩  
	 ==========================================*/
	
	var set_sharedNav_show= function(){
		TB_shared.set('templates--nav--m_nav_1--showNav',function(param){
			//console.log(param);
			showNav($topEl);
		});
	};
	/*==========================================
	 * 모바일 네비 공용 함수 만들기  
	 ==========================================*/
	
	var render = function(){
		_context['obj'] = {};
		_context['obj'].menu = TB_page.sgmap().menu;
		TB_contents.render(_context, function(_$topEl){
			$topEl = _$topEl;
			$childMenu_toggle = $topEl.find('.childMenu_toggle_btn');
			
			setNav($topEl);
			cur_location();
			//hoverAc($topEl);
			eBinding();
			set_sharedNav_show();
		});
	};
	/*==/메뉴 랜더링 함수==*/
	
	if(TB_page.sgmap().menu.length > 0){
		render();
	}else {
		TB.menuSet(function(){
			render();
		});
	}
	/*==/메뉴 맵을 받아 왔을 경우와 받아오지 않았을 경우 따로 처리==*/

	
});








































