TB_shared.set('templates--nav--dongneNav_1--js', function(_context){
	var $topEl = {},
			$oldPointer = [];
			
	_context['obj'] = {};
	_context['obj'].menu = TB_page.sgmap().menu;
	
	_context['obj'].shape = {};
	
	_context['obj'].shape.current_user = {}
	_context['obj'].shape.current_user.list = [
		{"user_level":"admin", "user":"총관리자"}
		,{"user_level":"manager", "user":"관리자"}
		,{"user_level":"client", "user":"업주"}
		,{"user_level":"customer", "user":"일반고객"}   
	];
	_context['obj'].shape.current_user.level = TB_auth.getUserInfo().user_level;
	/*=====================================================
	 * 변수 설정.
	 =====================================================*/
	

/*	
	var clear = function($menu){
		if($menu instanceof $){
			if($menu.hasClass('menu_topMenu')){//탑메뉴와 하위 메뉴 클리어
				$menu
				.removeClass('.topMenu_active').removeClass('selected')
				.find('.childMenu_active').removeClass('childMenu_active')
				.find('.active_li').removeClass('active_li');
			}
		}else{//전체 클리어
			$topEl.find('.selected').removeClass('selected').end()
			.find('.topMenu_active').removeClass('topMenu_active').end()
			.find('.childMenu_active').removeClass('childMenu_active').end()
			.find('.active_li').removeClass('active_li').end()
			;
		}
		
	};
	/*==========================================
	 *  메뉴 클리어.
	 ==========================================*/
	
	/*
	var hoverAc = function($el){
		//return false;
		$el.find('.nav_container').hover(function(){
			var $this = $(this);
			$this.find('ul').slideDown();
		}, function(){
			$(this).find('ul').stop().slideUp();
		});
		/*==/컨테이너에 마우스 오버시 ==*/
		/*
		$el.find('.menu_topMenu').off('mouseenter.nav_enter')
		.on('mouseenter.nav_enter', function(){
			var $this = $(this)
			;
			$this.addClass('active').find('.childMenu_Container').css('opacity', '');
		});
		$el.find('.menu_topMenu').off('mouseleave.nav_leave')
		.on('mouseleave.nav_leave', function(){
			var $this = $(this)
			;
			$this.removeClass('active');
		});
		/*==/상단 메뉴 마우스 오버시 ==*/
		/*
	};
	/*==========================================
	 * hover시  
	 ==========================================*/
	
	/*
	var eBinding = function(){
		$(document).find('a').off('click.nav')
		.on('click.nav', function(){
			var $this = $(this)
					, level = $this.attr('data-TB_link_level')
					;
			$topEl.find('.selected').removeClass('selected');
			if(level ==='1'){
				$this.parent().addClass('selected')
				.find('.childMenu_Container').css('z-index','auto');
			}else if(level === '2'){
				$this.closest('.childMenu_Container').css('z-index', 'auto')
				.closest('.menu_topMenu').addClass('selected')
				.find('.active_li').removeClass('active_li').end()
				.addClass('active_li');
			}
		});
	};
	/*==========================================
	 * ebinding  
	 ==========================================*/
	
	/*==
	var activeMenu = function(map){
		var map = map||TB_page.locationDec().map;
		//console.log(map);
		//console.log(_context['obj']);
		for(var i = 0 , len = _context['obj'].menu.length ; i < len ; i++){
			_context['obj'].menu[i].active = (map.page == _context['obj'].menu[i].page)?'active':'';
			
			//자식 메뉴가 있을 경우.
			if(_context['obj'].menu[i].childMenu){
				for(var k = 0, len_2 =_context['obj'].menu[i].childMenu.length ; k < len_2 ; k++){
					if(map.page ==  _context['obj'].menu[i].childMenu[k].page){
						_context['obj'].menu[i].active = 'active';
					}
				}
			}
		}
		i = null, len = null, k = null, len_2 = null;
	};
	/*==/메뉴 맵을 받아 왔을 경우와 받아오지 않았을 경우 따로 처리==*/
	
	
	_context['parts'].func('current_user', function($el){
		$el.find('.user_level').off('click.change_user_level')
		.on('click.change_user_level', function(){
			_context['obj'].shape.current_user.level = $(this).attr('data-user_level');
			_context['parts'].render('current_user');
		});
	});
	/*==/관리자 변경 ==*/
	
	
	/*================================================
	 * 이벤트
	 * ================================================*/

	
	var render = function(){
		
		//activeMenu();
		TB_contents.render(_context, function(_$topEl){
			//console.log(TB_auth.getUserInfo());
			//console.log(_context);
			//console.log(_$topEl);
			$topEl = _$topEl;
			//cur_location();
			//hoverAc($topEl);
			//eBinding();
			_context['parts'].func();
		});
	};
	/*==/메뉴 랜더링 함수==*/
	
	if(TB_page.sgmap().menu.length > 0){
		render();
	}else {
		TB.menuSet(function(){
			render();
		});
	}
	/*==/메뉴 맵을 받아 왔을 경우와 받아오지 않았을 경우 따로 처리==*/
	
	TB_page.moveAc(function(data){
		console.log(data);
//		activeMenu(data.map);
		//활성화 페이지 표시
		_context['parts'].render({
			parts : 'dongneNav_1'
			, callback : function(d){
				//cur_location(data);
				//eBinding();
			}
		});
	});
	/*================================================
	 * 페이지 움직일시 현재 위치 표시
	 ================================================*/
	
	
	
});
