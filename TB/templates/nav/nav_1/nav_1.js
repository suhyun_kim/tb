TB_shared.set('templates--nav--nav_1--js', function(_context){
	var $topEl = {},
			$oldPointer = [];
	
	TB_page.moveAc(function(data){
		//console.log(data);
		cur_location(data);
	});
	
	var cur_location = function(_map){
		var map =_map||TB_page.locationDec()
				, $pointer = (map)?$topEl.find('[data-tb_link_page='+map.map.page+']'):[]
				, level
				;
				
		if(!$pointer.length){//선택된 것이 없을 경우 index페이지 같은 경우.
			clear();
		}else{
			level = $pointer.attr('data-TB_link_level');
		}
		
		//console.log($pointer.length);
		if($oldPointer.length){//이전 기록이 있을 경우.
			if(level === '1'){//이동한것 level 1일때
				$oldPointer.parent()
				.removeClass('.topMenu_active').removeClass('selected')
				.find('.childMenu_active').removeClass('childMenu_active')
				.find('.active_li').removeClass('active_li');
			}else if(level === '2'){//레벨 2 이동시
				var old_topMenu_idx  = $oldPointer.closest('.menu_topMenu').index()
						, topMenu_idx = $pointer.closest('.menu_topMenu').index()
						;
				if(old_topMenu_idx !== topMenu_idx){//같은 탑메뉴가 아닐경우.
					clear($oldPointer.closest('.menu_topMenu'));
				}
			}
		}
		
		$topEl.find('.menu_topMenu').each(function(idx){
			var $this = $(this);
			if(!$this.hasClass('selected')){//초기화
				if($this.hasClass('topMenu_active')){
					$this.removeClass('topMenu_active');
				}
				if($this.find('.childMenu_active').length){
					$this.find('.childMenu_active').removeClass('childMenu_active');
				}
				$this.find('.childMenu_Container').css('z-index', 'auto').fadeOut();
			}
			
			if($this.find('.active_li').length){
				$this.find('.active_li').removeClass('active_li');
			}
		});
		
		if(level === '1'){
			$pointer.parent().addClass('topMenu_active').addClass('selected')
			.find('.childMenu_Container').addClass('childMenu_active')
			.find('li').first().addClass('active_li');
		}else if(level === '2'){
			$pointer.parent().addClass('active_li')
			.closest('.childMenu_Container').addClass('childMenu_active')
			.closest('.menu_topMenu').addClass('topMenu_active').addClass('selected');
		}
		$oldPointer = $pointer;
	};
	/*==========================================
	 * 현재 위치 표시 
	 ==========================================*/
	
	var clear = function($menu){
		if($menu instanceof $){
			if($menu.hasClass('menu_topMenu')){//탑메뉴와 하위 메뉴 클리어
				$menu
				.removeClass('.topMenu_active').removeClass('selected')
				.find('.childMenu_active').removeClass('childMenu_active')
				.find('.active_li').removeClass('active_li');
			}
		}else{//전체 클리어
			$topEl.find('.selected').removeClass('selected').end()
			.find('.topMenu_active').removeClass('topMenu_active').end()
			.find('.childMenu_active').removeClass('childMenu_active').end()
			.find('.active_li').removeClass('active_li').end()
			;
		}
		
	};
	/*==========================================
	 *  메뉴 클리어.
	 ==========================================*/
	
	var hoverAc = function($el){
		$el.find('.menu_topMenu').off('mouseenter.nav_enter')
		.on('mouseenter.nav_enter', function(){
			var $this = $(this);
			if(!$this.hasClass('selected')){
				TB_ani.flow('fadeIn', $this.addClass('topMenu_active')
				.find('.childMenu_Container').addClass('childMenu_active')
				.css({
					'z-index':'50'
					, 'opacity':1
				}));
			}
		});
		$el.find('.menu_topMenu').off('mouseleave.nav_leave')
		.on('mouseleave.nav_leave', function(){
			var $this = $(this);
			if(!$this.hasClass('selected')){
				TB_ani.flow('fadeOut', $this.removeClass('topMenu_active')
				.find('.childMenu_Container').removeClass('childMenu_active').css({
					'z-index': 'auto'
					, 'display':'none'
				}));
			}
		});
	};
	/*==========================================
	 * hover시  
	 ==========================================*/
	
	var eBinding = function(){
		$(document).find('a').off('click.nav')
		.on('click.nav', function(){
			var level = $(this).attr('data-TB_link_level');
			$topEl.find('.selected').removeClass('selected');
			if(level ==='1'){
				$(this).parent().addClass('selected')
				.find('.childMenu_Container').css('z-index','auto');
			}else if(level === '2'){
				$(this).closest('.childMenu_Container').css('z-index', 'auto')
				.closest('.menu_topMenu').addClass('selected')
				.find('.active_li').removeClass('active_li').end()
				.addClass('active_li');
			}
			
		});
	};
	/*==========================================
	 * ebinding  
	 ==========================================*/
	
	var render = function(){
		_context['obj'] = {};
		_context['obj'].menu = TB_page.sgmap().menu;
		TB_contents.render(_context, function(_$topEl){
			$topEl = _$topEl;
			cur_location();
			hoverAc($topEl)
			eBinding();
		});
	};
	/*==/메뉴 랜더링 함수==*/
	
	if(TB_page.sgmap().menu.length > 0){
		render();
	}else {
		TB.menuSet(function(){
			render();
		});
	}
	/*==/메뉴 맵을 받아 왔을 경우와 받아오지 않았을 경우 따로 처리==*/
	
	
	
	
});
