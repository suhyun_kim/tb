/*===========================
 * desc : bbs의 정보를 불러와 로드한다.
 * 필요변수
 * 	. category : bbs 분류
 * 	. no : 글번호
 ===========================*/
TB_shared.func = function(context){
	var reqQuery = 'bbsInfo';
	
	//console.log(context);
	
	if(context.param.category==='noticeBbs'){
		reqQuery = 'bbsInfo';
	}
	
	TB_ajax.get_json(
		{
		'post' : 
			{
				'reqType' : 'select'
				, 'reqQuery' : reqQuery
				, 'no' : context.param.no
			}
		,'callback' : function(dbData){
			//console.log(dbData);
			var insertData = $.parseJSON(dbData.body);
			
			TB_contents.render(context, function($topEl){
					
					TB_form.autoInsert($topEl,insertData);
					//이스케이프문자가 있을 경우 핸들바에서 오류가 나기 때문에 강제입력
					
					TB_form.binding($topEl);
					TB_form.eBinding($topEl);
					/*==/이벤트 바인딩==*/
					
			});/*==/랜더링==*/
		}
	});
	
};
