/*===========================
 *dec
 *  write 전용 템플릿 wvm으로 하다 보니 너무 코드가 복잡해져서 따로 분리 시켰다.
 * 
 * params
 * 
 ===========================*/

TB_shared.set('templates--write_form--dreamyacht--js', function(context){
	var tl = new TimelineLite()
		, insert_sw = false
		, param = context.param
		, user = TB_config.get('user')
		, write_date = new Date().format('yyyy-MM-dd') 
		, prev_page = '#!/index'
		;
	
	//console.log(param);
	
	//console.log(TB_config.get('user'));
	//console.log(write_date);
	
	context['obj'] = {};
	
	context.obj.supervisor = (user&&user.member_grade==='supervisor')?true:false;
	
	context.obj.render = {};
	context.obj.render.view = {};
	context.obj.render.view.price = false;
	context.obj.render.view.itinerary = false;
	context.obj.render.input = {};
	context.obj.render.input.price = 0;
	context.obj.render.input.itinerary = 0;
	
	context.obj.render.reqQuey = param.reqQuery;
	context.obj.device = TB_config.get('device');
	
	context.obj.form = {};
	context.obj.form.writer = (user)?'관리자':'';
	context.obj.form.write_date = write_date;
	context.obj.form.disabled = (context.obj.supervisor)?'':'disabled';
	
	
	if(param.reqQuery === 'yacht_sale'){
		context.obj.selected_category = '요트';
		context.obj.categories = [
			{name : '요트', value : '요트'}
			, {name : '보트', value : '보트'}
		];
		
	}
	
	images_json = [];//이미지 정보를 담고 있다가 업로드 시에 업로드 한다.
	/*============================================
	 * 변수설정
	 ============================================*/
	
	var go_prev_page = function(req_query){
		var prev_page = '#!/index';
		
		if(req_query === 'yacht_sale'){
			prev_page = '#!/sale?category=all';
			
		}else if(req_query === 'qna'){
			prev_page = '#!/community_qna';
			
		}else if(req_query === 'notice'){
			prev_page = '#!/community_notice';
			
		}else if(req_query === 'postscript'){
			prev_page = '#!/community_postscript';
		}
		
		//console.log(prev_page);
		location.replace(prev_page);
		//location.href = prev_page;
	};
	/*==/이전 페이지 이동하기./==*/
	
	function placeCaretAtEnd(el) {
	    el.focus();
	    if (typeof window.getSelection != "undefined"
	            && typeof document.createRange != "undefined") {
	        var range = document.createRange();
	        range.selectNodeContents(el);
	        range.collapse(false);
	        var sel = window.getSelection();
	        sel.removeAllRanges();
	        sel.addRange(range);
	    } else if (typeof document.body.createTextRange != "undefined") {
	        var textRange = document.body.createTextRange();
	        textRange.moveToElementText(el);
	        textRange.collapse(false);
	        textRange.select();
	    }
	}
	/*==/content editable 에서 커서 마지막으로 이동.==*/
	
	var insert_wysiwyg = function($form, html){
		$form.find('[name=memo]').val(html).end()
		.find('.wysiwyg-editor').html(html)
		.promise().done(function(){
			var $wysiwyg =$form.find('.wysiwyg-editor'); 
			$wysiwyg.find('img').last().load(function(){
				$wysiwyg.focus().animate({scrollTop : 10000},0);
				placeCaretAtEnd($wysiwyg[0]);
			});
		});
		;
	};
	/*== /위지윅에 HTML 삽입==*/
	
	var preview_local_image = function(input){
		var r = new FileReader()
			, $main_form = context.parent.find('.main_form')
			, memo_val = $main_form.find('[name=memo]').val()
			, img = ''
				;

		r.onload = function(e){
			img += "<img src='"+e.target.result+"' ";
			img += " title='"+input.files[0].name+"' ";
			img += " data-async_images_no='' ";
			img += " class='async_image async_image_uploading' ";
			img += " style='max-width:100%;' ";
			img += "/><br/><br/>";
			insert_wysiwyg($main_form, memo_val+img);
			/*
			setTimeout(function(){
				console.log($main_form.find('.async_image_uploading').length);
				$main_form.find('.async_image_uploading').attr('src', 'test.jpg');
				console.log($main_form.find('.wysiwyg-editor').html());
				insert_wysiwyg($main_form, $main_form.find('.wysiwyg-editor').html());
				console.log($main_form.find('[name=memo]').val());
			}, 2000);
			*/
		};
		
		r.readAsDataURL(input.files[0]);
	};
	/*== /프리뷰 이미지를 업로드.==*/
	
	var async_image_uploaded = function(src, originalname, no){
		var $main_form = context.parent.find('.main_form')
				, $memo = $main_form.find('[name=memo]')
				, html = ''
				;
		$main_form.find('.async_image_uploading').each(function(){
			var $this = $(this)
					, name = $this.attr('title')
					;
			if(name === originalname){
				$this.attr('src', src).attr('data-async_images_no',no).removeClass('async_image_uploading');
			}
		});
		html = $main_form.find('.wysiwyg-editor').html();
		insert_wysiwyg($main_form, html);
		
	};
	/*== /업로드 완료 후 이미지 주소 변경==*/
	
	var insert_wysiwyg_image = function(src, originalname, no){
		var $main_form = context.parent.find('.main_form')
			, memo_val = $main_form.find('[name=memo]').val()
			, img = ''
			;
			
		img += "<img src='"+src+"' ";
		img += " title='"+originalname+"' ";
		img += " data-async_images_no='"+no+"' ";
		img += " class='async_image ' ";
		img += " style='max-width:100%;' ";
		img += "/><br/><br/>"
		;
		insert_wysiwyg($main_form, memo_val+img);
	};
	/*== /위지윅에 이미지 삽입==*/
	
	TB_shared.set('templates--write_form--write_form_1--async_image_upload_beforeAC', function(data, $form){
		if($form.find('.async_image_uploading').length){
			TB_func.popupDialog('이미지를 업로드 중입니다.');
			return false;
		}
		return true;		
	});
	/*==/비동기 이미지 전송전 함수==*/
	
	TB_shared.set('templates--write_form--write_form_1--async_image_upload_aftAC', function(res){
		if(!res.src){
			TB_func.loading.off();
			location.replace('#!/index');
			return false;
		}
		
		res.src=TB_config.get('TB_root_path')+res.src;
		
		//wysiwyg 이미지 
		insert_wysiwyg_image(res.src, res.originalname, res.no);
		
		//비동기 이미지 업로드 완료로 변경.
		//async_image_uploaded(res.src, res.originalname, res.no);
		
		
		//이미지정보를 변수에 담기.
		images_json.push({
			no : res.no
			, src : res.src
			, originalname : res.originalname
			, pictured_time : res.pictured_time
			, lat : res.lat
			, lon : res.lon
		});
		
		//로딩 끝내기
		TB_func.loading.off();
	});
	/*==/비동기 이미지 전송후 함수==*/
	
	TB_shared.set('templates--write_form--write_form_1--beforeAc', function(data, $form){
		
		//insert_sw = true;
		//console.log(data);
		//이미지 정보 찾아 넣기.
		if(images_json.length){
			//console.log(images_json);
			var inserted = []//삭제되지 않고 남아있는 이미지의 no 
				, final_images = []
				;
			//삭제 되지 않고 남아있는 이미지만 추려낸다.
			$form.find('.async_image').each(function(){
				inserted.push({no : $(this).attr('data-async_images_no')});
			});
			
			//console.log(inserted);
			for(var i =0, len = images_json.length; i < len ; i++){
				for(var j= 0, _len = inserted.length ; j < _len ; j++){
					if(images_json[i].no == inserted[j].no){
						final_images.push(images_json[i]);
					}
				}
			}
			
			//console.log(final_images);			
			data.push({
				name : 'images_JSON'
				, requires:false
				, type:'textarea'
				, value : JSON.stringify(final_images)
			});
			//console.log(data);	
		}
		return true;		
	});
	/*==/폼 전송전 함수==*/
	
	TB_shared.set('templates--write_form--write_form_1--aftAc', function(res){
		if(res.result === 'success'){
			//history.back();
			//console.log(prev_page);
			go_prev_page(context.param.reqQuery);
			//location.href = prev_page;
		}
	});
	/*==/폼 전송 후 함수==*/
	
	
	var convert_render_data= function(_data){
		var data = _data
				, i = 0
				;
				
		data.render.stars_point = [];
				
		for(;i < data.render.stars_cnt ; i++){
			data.render.stars_point[i] = {};
			data.render.stars_point[i].class_name = 'star_point_'+(i+1);
		}
		
		return data;
	};
	/*==/랜더링 랜더링 하기위한 값 추가../==*/

	/*============================================
	 * 함 수
	 ============================================*/
	
	
	
	
	
	
	
	context.parts.func('templates--write_form--write_form_1--header', function($el){
		$el.find('.closeBtn').off('click.history_back')
		.on('click.history_back', function(){
			//history.go(-1);
			go_prev_page(context.param.reqQuery);
		});
	});
	/*==/닫기/==*/
	
	context.parts.func('templates--write_form--write_form_1--category_selector', function($el){
		$el.find('select').off('change.category_change')
		.on('change.category_change', function(){
			context.obj.selected_category = $(this).val();
			if(context.obj.selected_category === 'etc'){
				context.obj.render.view.price = false;
				context.obj.render.view.itinerary = false;
			}else if(context.obj.selected_category === 'food'){
				context.obj.render.view.price = true;
				context.obj.render.view.itinerary = false;
			}else if(context.obj.selected_category === 'travel'){
				context.obj.render.view.price = true;
				context.obj.render.view.itinerary = true;
			}
			context.parts.render(
				['templates--write_form--write_form_1--add_info'
					, 'templates--write_form--write_form_1--add_info_view'
				]
			);
		});
	});
	/*==/카테고리 선택/==*/
	
	context.parts.func('templates--write_form--write_form_1--add_info', function($el){
		
		$el.find('.popup_detail').off('click.popup_detail')
		.on('click.popup_detail', function(){
			var $popup_detail = $(this)
				, $col = $popup_detail.parent()
				, $add_info_wrap = $col.closest('.add_info_wrap')
				, $input_wrap = $col.find('.input_wrap')
				, viewed = $input_wrap.attr('data-viewed')
				;
			//console.log(viewed);
			if(viewed == 'true'){
				$input_wrap.hide().removeAttr('data-viewed')
				.parent().find('.popup_detail').removeClass('active')
				;
			}else{
				$add_info_wrap.find('.input_wrap').each(function(){
					var $this= $(this)
						, _viewed = $this.attr('data-viewed');
					if(_viewed){
						$this.removeAttr('data-viewed')
						.parent().find('.popup_detail').removeClass('active')
						;
					}
					$this.hide();
				});
				$input_wrap.attr('data-viewed', 'true').find('input').focus();
				$popup_detail.addClass('active');
				TB_ani.ready($input_wrap, function(){
					tl.set($input_wrap, {scale:0, opacity:0})
					.to(
						$input_wrap
						, 0.2
						, {scale : 1, opacity: 1, ease:Back.easeOut
								, onComplete: function(){
									$input_wrap.find('input').focus();
								} 
							}
					);
				});
			}
		});
		/*==/입력창 팝업 ==*/
		
		/*
		$el.find('.price_input, .itinerary_input').off('focusout.input_leave')
		.on('focusout.input_leave', function(){
			var $this = $(this)
				, name = $this.attr('name')
				, val = $this.val()
				;
			val = parseInt(val, 10);
			if(isNaN(val)){
				return false;
			}
			if(name === 'price'){context.obj.render.input.price = val;}
			else if(name === 'itinerary'){context.obj.render.input.itinerary = val;}
			context.parts.render('templates--write_form--write_form_1--add_info_view');
			$this.closest('.col').find('.popup_detail').trigger('click');
		});
		/*==/비용, 기간 focusout==*/
		
		/*
		var star_hover;
		
		$el.find('.star_input_wrap').find('.fa').off('mouseenter.star_mouseenter')
		.on('mouseenter.star_mouseenter', function(){
			var $this = $(this)
				, point = parseInt($this.attr('data-point'), 10)
				, $input_wrap = $this.closest('.input_wrap')
				, lock_p = parseInt($input_wrap.find('.star_lock').attr('data-point'),10)
				;
				
			clearTimeout(star_hover);
			$input_wrap.find('.fa').each(function(){
				var $star = $(this)
					, p = parseInt($star.attr('data-point'), 10)
					;
				if(point >= p ||  lock_p >= p){
					$star.addClass('active_star').addClass('fa-star').removeClass('fa-star-o');
				}else{
					$star.removeClass('active_star').removeClass('fa-star').addClass('fa-star-o');
				}
			});
		});
		/*==/별점 마우스 오버==*/
		
		/*
		$el.find('.star_input_wrap').find('.start_cover').off('mouseleave.star_mouseleave')
		.on('mouseleave.star_mouseleave', function(){
			var $this = $(this)
				, $input_wrap = $this.closest('.input_wrap')
				, $active = $input_wrap.find('.active_star')
				, lock_p = parseInt($input_wrap.find('.star_lock').attr('data-point'),10)||0
				;
			star_hover = setTimeout(function(){
				$active.each(function(){
					var $star = $(this)
						, p = parseInt($star.attr('data-point'), 10);
					if(lock_p < p){
						$star.removeClass('active_star').removeClass('fa-star').addClass('fa-star-o');
					} 
				});
			}, 300);
		});
		/*==/별점 마우스 아웃==*/
		
		/*
		$el.find('.star_input_wrap').find('.fa').off('click.star_click')
		.on('click.star_click', function(){
			var $this = $(this)
				, $input_wrap = $this.closest('.input_wrap')
				, lock_p 
				;
			//console.log($this.hasClass('star_lock'), $this.attr('data-point'));
			if($this.hasClass('star_lock') && $this.attr('data-point') == '1'){
				$this.removeClass('star_lock');
			}else{
				$input_wrap.find('.fa').each(function(){
					$(this).removeClass('star_lock');
				});
				$this.addClass('star_lock');
			}
			
			lock_p = parseInt($input_wrap.find('.star_lock').attr('data-point'),10)||0;
			
			$input_wrap.find('.fa').each(function(){
				var $star = $(this)
					, p = parseInt($star.attr('data-point'), 10);
				if(lock_p < p){
					$star.removeClass('active_star').removeClass('fa-star').addClass('fa-star-o');
				} 
			});
			$input_wrap.find('input').val(lock_p).end()
			.closest('.col').find('.icon_wrap').trigger('click');
			
			context.obj.render.stars_cnt = lock_p;
			context.obj = convert_render_data(context.obj);
			
			context.parts.render('templates--write_form--write_form_1--add_info_view');

		});
		/*==/별점 클릭==*/
		
		$el.find('.async_image_upload').off('click.async_image_click')
		.on('click.async_image_click', function(){
			context.parent.find('.async_image_upload_form_wrap').find('.file').trigger('click');
		});
		/*==/비동기 이미지 업로드 버튼 클릭==*/
		
		
		context.parent.find('.async_image_upload_form_wrap').find('.file').off('change.async_image_change')
		.on('change.async_image_change', function(){
			
			var $this = $(this)
				, val = $this.val()
				, size//MB 단위
				, type// 파일 타입
				, allow_type = ['jpeg', 'png', 'gif', 'bmp']
				, allow = false
				, max_width = 900
				, max_size = 5
				;
			
			//이미지가 없으면 업로드 안함.
			if(!val){return false;}
			
			TB_func.loading.on('full');
			
			
			//변수 설정
			size = Math.floor($this[0].files[0].size/1024/1024);//MB 단위
			type = $this[0].files[0].type;
			
			//이미지 타입 유효성 검사
			for(var i= 0, len = allow_type.length; i < len ; i++){
				if(type.match(allow_type[i])){
					allow = true;
				}
			}
			
			if(!allow){
				TB_func.popupDialog('이미지 파일을 선택하세요.');
				return false;
			}
			
			//이미지 크기 제한
			if(size > max_size){
				TB_func.popupDialog(max_size+'MB 이하의 파일을 선택하세요.');
				return false;
			}
			
			//임시로 로컬 이미지 업로드
			//preview_local_image($this[0]);
			
			//이미지 업로드
			$this.closest('form').find('.submit').trigger('click');
		});
		/*==/비동기 파일의 값이 변했을 때 업로드==*/
		
	});
	/*==/아이콘 클릭 시 세부 입력 정보 활성화/==*/
	
	/*============================================
	 * 이벤트
	 ============================================*/
	
	
	
	
	
	
	
	TB_contents.render(context, function($topEl){
		
		//console.log(context.obj);
		
		TB_form.binding($topEl);
		TB_form.eBinding($topEl);
		//console.log(context);
		$topEl.find('.body_wrap').removeClass('ST_dis_none');
		
		
		context.parts.func();
	});
	/*============================================
	 * 컨트롤
	 ============================================*/

	
	
});
















































