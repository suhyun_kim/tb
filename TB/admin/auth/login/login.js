TB_shared.set('admin--auth--login--js', function(context){
	
	TB_shared.set('adminCreateAft', function(res){
		if(res.result === 'createAdmin'){
			window.location.replace(TB_config.get('TB_root_path')+'admin/');		
		}
	});
	/*==================================
	 * 관리자 생성 유무 판단
	 ==================================*/
	
	var load_adminController = function(){
		TB_contents.impt({//관리자 컨트롤러 임포트
			'src' : 'admin--controller--index'
			, 'target' : '#adminController'
		});
	};
	/*==================================
	 * 관리자 컨트롤러 로드
	 ==================================*/
	
	var load_layout = function(){
		$('#TB_index_wrap')
		.attr({
			'data-TB_contents_src' :TB_config.get('defautLayout'),
			'data-TB_contents_sendData' : "?menu=0" 
		});
		
		TB_contents.load($('#TB_index_wrap'));
	};
	/*==================================
	 * 레이아웃 로드
	 ==================================*/
	
	TB_shared.set('adminLogin', function(_res){
		//console.log('로컬', _res);
		//console.log(_res);
		if(_res.result === "success"){
			delete _res.result;
			//console.log(_res);
			TB_auth.setAdmin(_res);//관리자 정보 셋팅
			load_layout();
			load_adminController();
			TB_config.set('user', _res);
		}else if(_res.result === 'faield'){
			TB_func.popupDialog('로그인에 실패 하였습니다.');
		}
	});
	/*==================================
	 * 관리자 로그인 처리
	 ==================================*/
	
	TB_ajax.get_json({
		'sendData' : {
			'reqQuery' : 'isCreate'
		}, 
		'callback' : function(res){
			context.obj = res.body;
			//console.log('확인', res.body);
			TB_auth.isAdmin(function(isAdmin){
				//console.log('관리자 확인 : '+isAdmin);
				//console.log('관리자 확인 : '+isAdmin);
				if(isAdmin){
					$adminController =$('#adminController'); 
					load_layout();
					load_adminController();
					//console.log(isAdmin);
				}else{
					//console.log('생성폼');
					TB_contents.render(context, function($top){
						$top.removeClass('ST_opacity_0');
					});
					//TB.contents_load();
				}
			});
		}
	});
	
		
});
