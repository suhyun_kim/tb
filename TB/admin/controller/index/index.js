/**
 * @author user
 */
$(document).ready(function(){
	var $topEl = $('#admin--controller--index');
	var $parent = $topEl.parent();
	var $nav_wrap = $topEl.find('.index_nav_wrap');
	var $nav_list_cover = $nav_wrap.find('.nav_list_cover');
	var $nav_closeBtn = $nav_wrap.find('.nav_closeBtn');
	var $ctrl_wrap = $topEl.find('.index_ctrl_wrap')
		, parts
		;
		
	console.log(TB);
	
	parts = TB_contents.create_parts({
		parent : $parent
		, html : $parent.html()
		, obj : {}
	});
	/*==/파츠 생성/==*/
	
	console.log(parts);
	parts.render(function($top){
		var $top = $top;
		
		//상단메뉴 드롭다운
		TB_ui.dropdown($top.find('.ST_dropdown'));
		
	});
	
	
	
	
	//TB.binding($topEl);
	//TB.eBinding($topEl);
	/*===========================================
	 * 이벤트 바인딩 
	============================================*/
	
	$nav_closeBtn.off('click')
	.on('click', function(){
		$nav_closeBtn.toggleClass('fa-caret-up');
		$ctrl_wrap.toggle('hide');
		$nav_list_cover.toggle('hide');
	});
	/*==/토글 버튼 ==*/
	
	$topEl.find('.index_nav_wrap').draggable({handle : '.nav_closeBtn_wrap'});
	$topEl.find('.index_ctrl_wrap').draggable({handle : '.draggable_handle'});
	/*==/드래그블==*/
	
	$nav_list_cover.children('li').off('click')
	.on('click', function(){
		var $this  = $(this),
				src = $this.attr('data-TB_contents_src')
				admin_menu = $this.attr('data-TB_admin_menu')
				;
				
		if(admin_menu === 'reload'){//리로드 버튼 눌렀을 경우
			window.location.reload(true);
		}else if(admin_menu === 'logout'){//로그아웃 버튼
			TB_auth.delAdminInfo(function(){
				window.location.replace(TB_config.get('TB_domain'));
			});
		}
				
		$('.TB_el_marked').remove();
		$('a').off('click');/*==/a속성 해제 ==*/
				
		if(src ==='admin--controller--element'){//엘리먼트 관련인 경우
			$('a').on('click', function(e){
				e.preventDefault();
			});/*==/a속성 죽이기 ==*/
		}
		
		$topEl.find('.index_ctrl_wrap').fadeIn();
	});
	/*==/관리자 메뉴 버튼 클릭 ==*/
	
	$nav_list_cover.find('.slider_edit_btn').click(function(){
		$('.templates--slider--slider_1').find('.ST_popup_form_1').toggle().end()
		.find('.thumbnail_wrap').toggle();
	});
	/*==/슬라이더 버튼 ==*/
	
	$nav_wrap.find('.logout').off('click.adminLogout')
	.on('click.adminLogout', function(){
		TB_auth.delAdminInfo(function(){
			window.location.replace(TB_config.get('TB_domain'));
		});
	});
	/*===========================================
	 * 로그아웃
	 ===========================================*/
	
});
