/*======================================
 * 메뉴의 수정은 TB_page 의 맵을 수정한뒤 그 파일을 TB_config에 업로드 하는걸 기본 골자로 한다.
 ======================================*/

TB_shared.set('admin--controller--map--js', function(_context){
	var context = _context;
	
	//context.parent.find('.map_menu_wrap').empty();
	//_context['html'] = $html[0].outerHTML;
	
	/*==/메뉴 소스 저장==*/
	
	
	/*========================================================
	 * 변수 설정
	 ========================================================*/
	
	
	var modify_menu = function(index){
		var index = index
			, item 
			;
		item = search_menu(index);
		item.before_func = 'TB_admin_menu_modify';
		item.submit = '수 정';
		item.sendData = ($.type(item.sendData) === 'object')?JSON.stringify(item.sendData):item.sendData;
		context.obj.render.cm_form = $.extend(context.obj.render.cm_form, item);
		context.obj.render.cm_form.bake = [
			{value : 'none' , checked : (item.bake === 'none')?true:false}
			, {value : 'half_bake' , checked : (item.bake === 'half_bake')?true:false}
			, {value : 'bake' , checked : (item.bake === 'bake')?true:false}
		];
		//console.log(item);
		context.parts.render({
			parts : 'cm_form'
			, callback : function(data){
				context.parent.find('.create_form_wrap').show();
			}
		});
		//console.log(item);
	};
	/*==/메뉴 수정 폼 보이기/==*/
	
	
	
	var find_index = function(index){
		var result
			, menu = TB_page.sgmap().menu
			, index, len, child, child_len
			, type = $.type(index)
			;
		
		if(index){
			index = (type === 'string')?index.split('_'):index;
			len =index.length; 
		}else {return false;}
		
		//chidMenu를 만들 때.
		if(len == 1){
			child = menu[index[0]].childMenu;
		}else if(len == 2){
			child = menu[index[0]].childMenu[index[1]].childMenu;
		}
		
		child_len = (child)?child.length:0;
		return index+'_'+child_len;
	};
	/*==/해당하는 메뉴의 최대 length를 리턴 한다./==*/
	
	
	
	var search_menu = function(idx, menu){
		var idx = idx.split('_')
			, menu = TB_page.sgmap().menu||menu
			, len= idx.length
			;
		
		//console.log(idx);
		if(len == 1){ 
			return menu[idx[0]]; 
		}else if(len == 2){
			return menu[idx[0]].childMenu[idx[1]]; 
		}else if(len ==3){
			return menu[idx[0]].childMenu[idx[1]].childMenu[idx[2]]; 
		}
	};
	/*==/'1_2_4 '와 같은 형태의 인덱스로 트이에서 해당메뉴를 찾아낸다. /==*/
	
	var add_child = function(parent_idx){
		var parent_idx = parent_idx
			, menu  = TB_page.sgmap().menu
			, data = {} , parent
			;
		
		parent = search_menu(parent_idx);
		
		//console.log(parent);
		
		data.before_func = 'TB_admin_menu_insert';
		data.page = '';
		data.title = '';
		data.bake = [
			{value : 'none' , checked : (parent.bake === 'none')?true:false}
			, {value : 'half_bake' , checked : (parent.bake === 'half_bake')?true:false}
			, {value : 'bake' , checked : (parent.bake === 'bake')?true:false}
		];
		data.index = find_index(parent.index);
		data.depth = parseInt(parent.depth, 10)+1;
		data.parent = parent.page;
		data.src = TB_config.get('default_contents_src');
		data.sendData = '';
		data.submit = '입 력';
		
		//console.log(data);
		//console.log(idx, menu);
		
		context.obj.render.cm_form = $.extend(context.obj.render.cm_form, data);
		//console.log(context.obj.render.cm_form);
		
		//폼 랜더링
		context.parts.render({
			parts : 'cm_form'
			, callback : function(data){
				context.parent.find('.create_form_wrap').show();
			}
		});

		
	};
	/*==/자식 메뉴 찾기/*==*/
	
	var save_menu = function(menu){
		var menu = menu||TB_page.sgmap().menu;
		
		if($.type(menu) != 'array'){
			throw 'type check failed';
			return false;
		}
		context.obj.render.upload_map = JSON.stringify(menu);
		context.parts.render('TB_admin_save_map', function($el){
			context.parent.find('.menu_save_form').find('[type=submit]').trigger('click');
			//init(context);
		});
	};
	/*==/메뉴 저장 하기./==*/
	
	var get_menu_item= function(idx){
		var idx = idx// 0_2_3 과 같은 형식으로 들어온다.
			, depth
			, item
			, menu = TB_page.sgmap().menu
			;
		
		if(!idx){ return false; }
		
		idx = idx.split('_');// 0_2_3 과 같은 형식으로 들어온다.
		depth = idx.length-1;
		
		if(depth === 0){//
			return menu[idx[0]];
		}else if(depth === 1){
			
		}else if(depth === 2){
			
		}
		
	};
	/*==/(func)인덱스를 기준으로 메뉴 아이템을 반환/*==*/
	
	
	
	var cm_form = function(type, idx){
		var type = type 
			, idx = idx
			, data = get_menu_item(idx)
			, menu_len = TB_page.sgmap().menu.length;
			;
		//console.log(idx);
		
		if(data === false){
			data = {
				depth : 0
				, src : TB_config.get('default_contents_src')
				, target : '#contents_wrap'
				, bake : 'half_bake'
			};
		}
		
		context.obj.render.cm_form.before_func = (type === 'insert')?'TB_admin_menu_insert':'TB_admin_menu_modify';
		context.obj.render.cm_form.page = data.page;
		context.obj.render.cm_form.title = data.title;
		context.obj.render.cm_form.bake = [
			{value : 'none' , checked : (data.bake === 'none')?true:false}
			, {value : 'half_bake' , checked : (data.bake === 'half_bake')?true:false}
			, {value : 'bake' , checked : (data.bake === 'bake')?true:false}
		];
		context.obj.render.cm_form.index = (type==='insert')?menu_len:data.index;
		context.obj.render.cm_form.depth = data.depth;
		context.obj.render.cm_form.parent = data.parent; //parent_child 형태로 2단계까지만 존재. 최상위는 없다.
		context.obj.render.cm_form.src = data.src;
		context.obj.render.cm_form.target = data.target;
		context.obj.render.cm_form.sendData = data.sendData;
		context.obj.render.cm_form.submit = (type === 'insert')?'입력':'수 정';
		
		//폼 랜더링
		context.parts.render({
			parts : 'cm_form'
			, callback : function(data){
				context.parent.find('.create_form_wrap').show();
			}
		});
		
	};
	/*==/입력, 수정 폼 보이기/*==*/
	
	
	TB_shared.set('TB_admin_menu_insert', function(data, $form){
		TB_page.formAddMap($form);//메뉴 추가
		save_menu();
	});
	/*==/맵 입력 /*==*/
	
	
	TB_shared.set('TB_admin_menu_modify', function(data, $form){
		var menu = {}
			, data = data
			, modify = {}
			, i = 0 , len = data.length
			;

		for(; i < len ; i++){
			modify[data[i].name] = data[i].value;
		}
		
		menu = TB_page.modify_menu(modify.page, modify);
		
		if(!menu){return false}
		
		save_menu(menu);
	});
	/*==/폼 전송 전 맵 수정 함수/*==*/
	
	
	/*========================================================
	 * 함수 설정
	 ========================================================*/
	
	/*========================================================
	 * 이벤트
	 ========================================================*/
	
	
	var init = function(){
		context.obj = {};
		
		context.obj.render = {};
		
		context.obj.render.cm_form = {};
		context.obj.render.cm_form.before_func = 'TB_admin_menu_insert';
		context.obj.render.cm_form.page = '';
		context.obj.render.cm_form.title = '';
		context.obj.render.cm_form.bake = [
			{value : 'none' , checked : false}
			, {value : 'half_bake' , checked : false}
			, {value : 'bake' , checked : true}
		];
		context.obj.render.cm_form.index = 0;//메뉴를 찾기 편하도록 하기위해 
		context.obj.render.cm_form.depth = 0;
		context.obj.render.cm_form.parent = '';
		context.obj.render.cm_form.src = TB_config.get('default_contents_src');
		context.obj.render.cm_form.target = '#contents_wrap';
		context.obj.render.cm_form.sendData = '';
		context.obj.render.cm_form.submit = '입 력';
		/*====/생성, 수정 폼 /======*/
		
		context.obj.render.upload_map = '';//실제 업로드에 사용될 menu 데이터
		/*====/맵 업로드 폼/======*/
		
		/*
		[
			{"page":"education","title":"요트교육","bake":"half_bake","index":0,"depth":"0","parent":"","src":"contents--dreamyacht--education","target":"#contents_wrap","sendData":""
				,"childMenu":[
					{
						"page":"education_basic"
						,"title":"외양항해초급교육","bake":"half_bake","index":"3_0","depth":"1","parent":"education"
						,"src":"contents--dreamyacht--education","target":"#contents_wrap"
						,"sendData":{"scroll_spy":"education_basic"}
					},{
						"page":"education_medium","title":"외양항해중급교육","bake":"half_bake","index":"3_1"
						,"depth":"1","parent":"education","src":"contents--dreamyacht--education","target":"#contents_wrap"
						,"sendData":
						{
							"scroll_spy":"education_medium"
						}
					}
				]
			},{
				"page":"sale","title":"요트·보트매매","bake":"half_bake","index":1,"depth":"0","parent":"","src":"contents--dreamyacht--sale"
				,"target":"#contents_wrap","sendData":""
				,"childMenu":[
					{
						"page":"sale_yacht","title":"요트매매","bake":"half_bake","index":"4_0","depth":"1","parent":"sale"
						,"src":"contents--dreamyacht--sale","target":"#contents_wrap"
						,"sendData":{
							"scroll_spy":"sale_yacht"
						}
					},{
						"page":"sale_boat","title":"보트매매","bake":"half_bake","index":"4_1","depth":"1","parent":"sale","src":"contents--dreamyacht--sale","target":"#contents_wrap"
						,"sendData":{"scroll_spy":"sale_boat"}
					}
				]
			},{
				"page":"community","title":"커뮤니티","bake":"half_bake","index":2,"depth":"0","parent":"","src":"contents--dreamyacht--community","target":"#contents_wrap","sendData":""
				,"childMenu":[
					{
						"page":"community_notice","title":"공지사항","bake":"half_bake","index":"5_0","depth":"1","parent":"community"
						,"src":"contents--dreamyacht--community"
						,"target":"#contents_wrap"
						,"sendData":{
							"scroll_spy":"community_notice"
							,"before_func":"TB_admin_menu_modify","submit":"수 정"
						}
					},{
						"page":"community_postscript","title":"후기","bake":"half_bake","index":"5_1","depth":"1","parent":"community","src":"contents--dreamyacht--community"
						,"target":"#contents_wrap"
						,"sendData":{"scroll_spy":"community_postscript"}
					},{
						"page":"community_price","title":"요금안내","bake":"half_bake","index":"5_2","depth":"1","parent":"community","src":"contents--dreamyacht--community"
						,"target":"#contents_wrap","sendData":{"scroll_spy":"community_price"}
					},{
						"page":"community_qna","title":"문의","bake":"half_bake","index":"5_3","depth":"1","parent":"community","src":"contents--dreamyacht--community"
						,"target":"#contents_wrap"
						,"sendData":{"scroll_spy":"community_qna"}
					},{
						"page":"community_location","title":"위치","bake":"half_bake","index":"5_4","depth":"1","parent":"community","src":"contents--dreamyacht--community"
						,"target":"#contents_wrap","sendData":{"scroll_spy":"community_location"}
					}
				]
			},{
				"page":"about","title":"드림요트","bake":"half_bake","index":"4","depth":"0","parent":"","src":"contents--dreamyacht--about","target":"#contents_wrap","sendData":""
			}
		]
		*/

		//console.log(_context);
		var menuHTML = context.parent.find('.map_menu_wrap').html();
	
		
		TB_contents.render(context, function($topEl){
			var $createFormWrap = $topEl.find('.create_form_wrap');
			var $createBtn = $topEl.find('.createBtn');
			var $map_menu_wrap = $topEl.find('.map_menu_wrap');
			var form_initData = {
						src : TB_config.get('default_contents_src'), 
						target : '#contents_wrap',
						page : '' 
					};
			var select_idx;//메뉴 수정시에 사용될 메뉴 인덱스 정보
			
					
			var printMenu = function(){
				var $menu_wrap = $topEl.find('.map_menu_wrap');
				//console.log(TB_page.sgmap());
				//console.log(TB_page.sgmap());
				TB_handlebars.render({
					html : menuHTML
					, obj : TB_page.sgmap()
					, outputEl : $menu_wrap
				}, function(){
					eBinding($topEl);
					TB_page.convertMap();
					setTimeout(function(){
						hei_balance($menu_wrap.find('.parentLevel'));
						TB_ani.flow('fadeIn',$menu_wrap);
					}, 300);//*==/높이 맞추기
				});
			};
			/*==/메뉴 리스트 출력 ==*/
			
			TB_shared.set('save_menu', function(data, $form){
				var menu = TB_page.sgmap().menu
					, value;
				
				if(!menu.length){return false;};
				
				for(var i = 0, len = menu.length; i < len ; i++){
					if(menu[i].sendData){
						if($.type(menu[i].sendData) === 'string'){
							menu[i].sendData = $.parseJSON(menu[i].sendData);
						}
					}
				}
				value = JSON.stringify(menu);
				
				data[2].value =  value;
				$form.find('[name=menu]').val(value);
				i = null, len = null;
				return true;
			});
			/*==/메뉴 DB 에 저장==*/
			
			TB_shared.set('menuUpAft', function(_res){
				
				// TB.eBinding($topEl.find('.menu_save_form'));
								
				//location.reload(true);
			});		
			/*==/메뉴 DB 에 저장 한 후 ==*/
			
			TB_shared.set('map--menuUpBefore', function(data, $form){
				//데이터가 제대로 되었는지 검사
				var menu = JSON.parse(data[2].value);
				
				
				return true;
			});
			/*==/메뉴 DB 에 저장 한 후 ==*/
			
			
			var showForm = function(_posi){
				$createFormWrap.show().css({
					'position' : 'absolute',
				});
			};
			/*==/인풋 폼 show==*/
			
			var findMenu_idx = function(_idx){
				if(!arguments.length)return false;
				var arr = _idx.split('_'), len = arr.length, menu = TB_page.sgmap().menu, returnData;
				if(len===1) returnData =  menu[arr[0]];
				else if(len === 2) returnData =  menu[arr[0]].childMenu[arr[1]];
				else if(len === 3) returnData = menu[arr[0]].childMenu[arr[1]].childMenu[arr[2]];
				arr = null, len = null, menu = null;
				return returnData;
			};
			/*==============================================
			 * 인덱스를 기준으로 메뉴를 찾는다.
			 * params
			 * 	. _idx(string) : 0_1_2 와 같은 형태로 받는다.
			 * return 
			 * 	. mapArray (obj) : inddex에 해당하는 메뉴 오브젝트.
			 *											원본 menu를 참조하고 있으므로 수정시 원본도 수정된다. 
			 ==============================================*/
			
			var showCreatModifyForm = function(_$el){
				var $this = _$el
					, posi = $this.position()
					, formData = {}
					, idx = $this.attr('data-menu_index')//메뉴를 찾을 수 있는 인덱스
					, type = $this.attr('data-menu_formType')
					;
				
				//console.log(idx, type);
						
				formData = $.extend(formData, form_initData);//초기 데이터 입력
				
				if(idx) $.extend(formData, findMenu_idx(idx));//선택한 데이터 입력
				
				//console.log(formData);
				
				if(type==='insert'){
					$createFormWrap.find('.submit').val('등록');
					if(idx){
						formData.parent = formData.page;
						formData.page = formData.page+'_'; 
						formData.depth = parseInt(formData.depth,10)+1; 
					}
				}else if(type==='modify'){
					$createFormWrap.find('.submit').val('수정');
					select_idx = idx;
				}
				if($.type(formData.sendData) === 'object'){
					formData.sendData = JSON.stringify(formData.sendData);
				}
				$createFormWrap.find('form')[0].reset();//폼 클리어
				TB_form.autoInsert($createFormWrap.find('form'), formData);//
				showForm(posi);
				formData = null, $this =null, posi = null, idx = null, type = null;
			};
			/*==============================================
			 * showCreatModifyForm을 보여줄때 
			 ==============================================*/
			
			var hei_balance = function($menus, _callback){
				var top_hei = 0;
				$menus.each(function(){
					var hei = $(this).height();
					if(top_hei < hei){
						top_hei = hei;
					}
				}).each(function(){
					$(this).height(top_hei);
				});
				if(typeof _callback === 'function'){_callback();}
				top_hei = null;
			};
			/*==============================================
			 * 메뉴의 높이 맞추기.
			 * dec
			 * 	. 메뉴중 가장 높은 엘리먼트에 나머지 엘리먼트의 높이를 맞춘다.
			 * params
			 * 	. $menus(jQuery Obj) : 메뉴 오브젝트
			 * 
			 ==============================================*/
			
			var saveMap = function(){
				$topEl.find('.menu_save_form').find('.submit').trigger('click');
				printMenu();
				TB_contents.load($('#header_wrap'));
			};
			/*==============================================
			 * 맵저장 트리거를 작동 시킨다.
			 ==============================================*/
			
			var delMenu = function(_idx){
				if(!arguments.length)return false;
				
				var arr = _idx.split('_'), len = arr.length, menu = TB_page.sgmap().menu;
				//console.log(arr);
				if(!confirm('정말삭제 하시겠습니까?')){return false;}
				if(len===1) delete  menu.splice([arr[0]],1);
				else if(len === 2) delete  menu[arr[0]].childMenu.splice([arr[1]],1);
				else if(len === 3){
					delete  menu[arr[0]].childMenu[arr[1]].childMenu.splice([arr[2]],1);
				} 
				//console.log(menu);
				//printMenu();
				save_menu(menu);
				arr = null, len = null, menu = null;
			};
			/*==============================================
			 * 맵 삭제
			 * params
			 * 	. _idx(string) :  삭제할메뉴의 인덱스
			 ==============================================*/
			
			
			var eBinding = function(_$scope){
				var $scope = _$scope||$(document);
				
				
				$topEl.find('.modify_menu_btn').off('click.modify_menu_btn')
				.on('click.modify_menu_btn', function(){
					modify_menu(this.getAttribute('data-menu_index'));
				});
				/*==/자식 메뉴 수정==*/
				
				$topEl.find('.add_child_btn').off('click.add_chid')
				.on('click.add_child', function(){
					add_child(this.getAttribute('data-menu_index'));
				});
				/*==/자식 메뉴 만들기==*/
				
				$topEl.find('.toggle_menu_btn').off('click.toggle_menu')
				.on('click.toggle_menu', function(){
					var type = $(this).attr('data-menu_formType')
						, idx = this.getAttribute('data-menu_index')
						;
					cm_form(type, idx);
					//showCreatModifyForm($(this));
				});
				/*==/ 폼 생성 양식 ==*/
				
				$map_menu_wrap.find('.del_btn').off('click.menu_delete')
				.on('click.menu_delete', function(){
					
					TB_page.delete_menu(this.getAttribute('data-page'));
					save_menu();
				});
				/*==/ 메뉴 삭제 /==*/
				
				
				$scope.find('.map_menu_wrap').sortable({
					connectWith : '.parentLevel'//이동 범위
					, haldle : '.sort_handle' //핸들지정
					//, dropOnEmpty : false //이동시 원래자리 빠지게 만들기
					, update : function(e, ui){
						var $item = ui.item
								, arr = []//변경된 메뉴를 담을 배열
								;
						
						$topEl.find('.parentLevel').each(function(){
							var index = this.getAttribute('data-menu_idx');
								;
							arr.push(TB_page.sgmap().menu[index]);
						});
						TB_page.sgmap({
							menu : arr
						});//메뉴 저장.
						//saveMap();
						save_menu();
					}
				});
				$scope.find('.map_menu_wrap').disableSelection();
				/*==/정렬==*/
				//$scope.find('.parentLevel').disableSelection();
				
				$scope.find('.create_form_closeBtn').off('click.create_form_closeBtn')
				.on('click.create_form_closeBtn', function(){
					$scope.find('.create_form_wrap').fadeOut();
				});
				/*==/클릭 : 폼 닫기 버튼 ==*/
			};
			/*===================================
			 * 이벤트 관련 바인딩
			 ===================================*/
			
			eBinding($topEl);
			TB_form.binding($topEl);
			TB_form.eBinding($topEl);
			printMenu();//트리구조 메뉴 출력하기.
			
			//evt($topEl);//이벤트 
			
		});
	};
	/*==/초기화 함수/==*/
	
	
	
	init();
	
	
});
