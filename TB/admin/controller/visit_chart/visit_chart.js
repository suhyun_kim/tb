TB_shared.set('admin--controller--visit_chart--js', function(context){
	var graph_data = {//
			year : []//년도 데이터
		}
		, $src = $(context['html'])
		, html = {//f랜더링시 사용될 HTML 소스
			year : $src.find('.year').html()
			, month : $src.find('.month').html()
		}
		, obj = {//랜더링시 사용될 데이터 
			cur_tab : 'period'
			, cur_period : 'year'
			, cur_detail_period : undefined
			, cur_page : 1
		}
		, $topEl
		, $period_container//기간 셀렉터 컨테이너
		, $tabs
	;
	
	var init = function(){
		print_period(function(){//프린트 출력
			
			create_date_selector();//날짜선택 생성
			
			e_period();//이벤트 바인딩
			
			selected_tab();
			//console.log('당근');
			TB_ani.flow('fadeIn', $topEl);
		});
		
	};
	/*=================================================
	 * 초기 엘리먼트 셋팅
	 =================================================*/
	
	var getData = function(_callback){
		var sendData = {
			reqQuery : 'get_visitor_info'
			, tab : obj.cur_tab
		};
		
		if(sendData.tab === 'period'){
			sendData.period = obj.cur_period;
			sendData.period_detail = obj.cur_detail_period;
		}else if(sendData.tab === 'ip'){
			sendData.page = obj.cur_page;
		}else if(sendData.tab === 'referrer'){
			sendData.page = obj.cur_page;
		}
		
		
		TB_ajax.get_json({
			'sendData' : sendData
			, 'callback' : function(res){
				//console.log(res);
				if($.type(_callback) === 'function'){ _callback(res); }
			}
		})
	};
	/*=================================================
	 * 차트 데이터 가져오기
	 =================================================*/
	
	var controller_set = function( _callback){
		var text = {};
		if(obj.cur_tab ==='period'){//기간 탭
			// console.log(obj);
			var d = get_selected_date();//현재 선택되어 있는 날짜값
			
			if(obj.cur_period === 'year'){//년선택
				obj.cur_detail_period = '';
				$period_container.find('.date_selector_wrap').hide();
			}else if(obj.cur_period === 'month'){//월선택
				$period_container
				.find('.date, .month').hide().end()
				.find('.date_selector_wrap').fadeIn();
				
				obj.cur_detail_period = {};
				obj.cur_detail_period.year = d[0];
			}else if(obj.cur_period ==='date'){//날짜별
				$period_container
				.find('.month').show().end()
				.find('.date').hide().end()
				.find('.date_selector_wrap').fadeIn();
				
				obj.cur_detail_period = {};
				obj.cur_detail_period.year = d[0];
				obj.cur_detail_period.month = d[1];
				
			}else if(obj.cur_period ==='hour'){//시간별
				
				$period_container
				.find('.month').show().end()
				.find('.date').html(text.date).show().end()
				.find('.date_selector_wrap').fadeIn();
				
				obj.cur_detail_period = {};
				obj.cur_detail_period.year = d[0];
				obj.cur_detail_period.month = d[1];
				obj.cur_detail_period.date = d[2];
				
				lastDay = null, today_date = null;
			}
			
			
		}
		
		if($.type(_callback) === 'function'){
			_callback();
		}
		
	};
	/*=================================================
	 * 그래프를 그리는 것을 제외한 컨트롤러 셋팅
	 =================================================*/
	
	
	var create_date_selector = function(_type){
		var text = {};//select 생성될 HTML
		text.year =undefined;
		text.month = undefined;
		text.date = undefined;
		var d = new Date();
		var cur_time = {};
		cur_time.year = d.getFullYear();
		cur_time.month = d.getMonth()+1;
		cur_time.date = d.getDate();
		cur_time.last_date = ( new Date(cur_time.year , cur_time.month, 0) ).getDate()//월의 마지막 날
		;
		
		for( var j = 0, len = graph_data.year.length; j < len ; j++){//데이터 셋팅
			text.year += "<option value='"+graph_data.year[j].year+"' >"+graph_data.year[j].year+"년</option>";
		}
			
		for(var i = 1; i <=12 ; i++){//월생성
			text.month += "<option value='"+i+"' >"+i+'월'+"</option>";
		}
		
		for(var i = 1; i <= cur_time.last_date ; i++){//날짜생성
			text.date += "<option value='"+i+"' >"+i+'일'+"</option>";
		}
		
		
		$period_container
		.find('.year').html(text.year)
		.val(cur_time.year).end()
		.find('.month').html(text.month)
		.val(cur_time.month).end()
		.find('.date').html(text.date)
		.val(cur_time.date)
		.promise().done(function(){
			controller_set();
		});	
	};
	/*=================================================
	 * 달력 선택기 생성
	 * . 일은 월 기준에 맞춰야 하기 때문에 년, 월만 기초적으로 생성한다. 
	 =================================================*/
	
	var create_dateOnly = function(){
		var d = get_selected_date();
		var last_day = ( new Date(d[0] , d[1], 0) ).getDate()//월의 마지막 날
		var text = ''; 
		for(var i = 1; i <= last_day ; i++){//날짜생성
			text += "<option value='"+i+"' >"+i+'일'+"</option>";
		}
		$period_container.find('.date').html(text);
	};
	/*=================================================
	 * 달력 선택기 생성
	 * 	. 오직 날짜만 생성
	 =================================================*/
	
	
	var get_selected_date = function(){
		var arr = [];
		$period_container.find('.date_selector_wrap').find('select')
		.each(function(){
			arr.push($(this).val());
		});
		return arr;
	};
	/*=================================================
	 * 선택되어 있는 날짜를 반환
	 =================================================*/
	
	var e_period = function(){
		$tabs.find('.tab')
		.off('click.admin--controller--visit_chart--tab_evt')
		.on('click.admin--controller--visit_chart--tab_evt', function(){
			var tab_value =$(this).attr('data-tab_value'); 
			obj.cur_tab = tab_value;
			obj.cur_page = 1;
			selected_tab();
			tab_action();
			print_period();
		});
		/*==/탭==*/
		
		
		$period_container.find('.period_selector')
		.off('change.period')
		.on('change.period', function(){
			obj.cur_period = $(this).val();
			//console.log(obj);
			controller_set();//셋팅
			print_period();//그래프 출력
		});
		/*==/기간:대분류 선택 ==*/
		
		
		$period_container.find('.date_selector_wrap').find('.month, .year, .date')
		.off('change.selected_month')
		.on('change.selected_month', function(){
			if($(this).hasClass('month')){//월을 변경할 경우 날짜를 다시 생성한다.
				create_dateOnly();
			}
			controller_set();
			print_period();//그래프 출력
		});
		/*==/기간:세부 선택==*/
		
		$topEl.find('.list_wrap').find('.prev_btn')
		.off('click.admin--controller--visit_chart--list_prev_btn')
		.on('click.admin--controller--visit_chart--list_prev_btn', function(){
			obj.cur_page--;
			print_period();
		});
		/*==/페이지 이동 : 이전 페이지==*/
		
		$topEl.find('.list_wrap').find('.next_btn')
		.off('click.admin--controller--visit_chart--list_next_btn')
		.on('click.admin--controller--visit_chart--list_next_btn', function(){
			obj.cur_page++;
			print_period();
		});
		/*==/페이지 이동 : 다음 페이지==*/
	};
	/*=================================================
	 * 차트 선택시 이벤트 처리
	 =================================================*/
	
	var print_period = function(_callback){
		cur_period = $topEl.find('.period_selector').val();
		
		getData(function(res){
			//console.log(res.body);
			//console.log(obj);
			var chart_arr = []//차트 값
					, row = []//행 데이터 
					, config = {
						title:''
						, type:'bar'
						, useVal : false
					}//차트에 들어갈 각종 설정값
					, field = obj.cur_tab//row 출력시 사용될 값
					, chart_data = []
					, render_set = {}//list_랜더링시 사용하기 위한 오브젝트
					;
			if(obj.cur_tab ==='period'){
				if(obj.cur_period === 'year'){//년도별
					
					graph_data.year = res.body.graph_data;//스토리지 저장
					chart_arr = ['방문자', 0];
					row = ['연도',0];
					field = 'year';
					config.title = '년도별 방문자 차트';
					
				}else if(obj.cur_period === 'month'){//월
					
					graph_data.month = res.body.graph_data;//스토리지 저장
					chart_arr = ['방문자', 0];
					row = ['월', 0];
					field = 'month';
					config.title = '월별 방문자 차트';
					
				}else if(obj.cur_period === 'date'){//일별
					for(var i = 0, len = res.body.graph_data.length ; i < len ; i++){//날짜 자르기.
						res.body.graph_data[i].date = res.body.graph_data[i].date.split('-')[2];
					}
					
					graph_data.date = res.body.graph_data;//스토리지 저장
					chart_arr = ['방문자', 0];
					row = ['일', 0];
					field = 'date';
					config.title = '일별 방문자 차트';
					
				}else if(obj.cur_period === 'hour'){//시간별
					for(var i = 0, len = res.body.graph_data.length ; i < len ; i++){//날짜 자르기.
						res.body.graph_data[i].datetime = res.body.graph_data[i].datetime.split(' ')[1];
					}
					
					graph_data.date = res.body.graph_data;//스토리지 저장
					chart_arr = ['방문자', 0];
					row = ['시간', 0];
					field = 'datetime';
					config.title = '시간별 방문자 차트';
				}
				
			}else if(obj.cur_tab ==='day'){//요일별 보기
				var sort_data = []
						, i = 0
						, len= res.body.graph_data.length
						, day_arr = ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일']
						, dumy = {day : '', 'COUNT(*)':'0'}//
						;
				for(; i < 7 ; i++){
					dumy.day = day_arr[i];
					dumy['COUNT(*)'] = 0;
					for(var j =0; j < len ; j++){
						if(day_arr[i] == res.body.graph_data[j].day){
							dumy['COUNT(*)'] = res.body.graph_data[j]['COUNT(*)'];
						}
					}
					sort_data[i]  = TB_func.objCopy(dumy);
				}
				res.body.graph_data = sort_data;
				graph_data.day = res.body.graph_data;
				chart_arr = ['방문자', 0];
				row = ['요일', 0];
				config.title = '요일별 방문자 차트';
				sort_data = null, i = null, len = null, day_arr = null, dumy = null;
			}else if(obj.cur_tab === 'platform'){//기기별 보기
				graph_data.platform = res.body.graph_data;
				chart_arr = ['방문자'];
				config.title = '기기별 접속현황';
				config.useVal = true;
				config.type = 'pie';
				field ='platform';
				 
			}else if(obj.cur_tab === 'ip'){//아이피별 보기
				graph_data.ip = res.body.graph_data;
				chart_arr = ['방문자'];
				config.title = '아이피별 접속현황';
				field ='ip';
				config.type ='list';
				//console.log(res.body.graph_data);
				render_set.header_col = [
					{category : '아이피'}
					, {category : '방문횟수'}
				];
			}else if(obj.cur_tab === 'referrer'){//이전 접속 경로
				graph_data.referrer = res.body.graph_data;
				chart_arr = ['방문자'];
				config.title = '이전경로별 접속현황';
				field ='before_url';
				config.type ='list';
				//console.log(res.body.graph_data);
				render_set.header_col = [
					{category : '이전경로'}
					, {category : '방문횟수'}
				];
			}
			
			if(config.type==='bar'){//막대 그래프
				for(var i = 0, len  =res.body.graph_data.length ; i < len ; i++){
					row.push(res.body.graph_data[i][field]);
					chart_arr.push(res.body.graph_data[i]['COUNT(*)']);
				}
				chart_data = [row, chart_arr];
				$topEl.find('.list_wrap').hide().end()
				.find('.TB_visitor_chart_wrap').show();
				
			}else if(config.type === 'pie'){//pie 그래프
				chart_data.push(chart_arr);
				for(var i = 0, len  =res.body.graph_data.length ; i < len ; i++){
					//row.push(res.body.graph_data[i][field]);
					if(res.body.graph_data[i][field] === ''){//그외의 경우 ETC처리
						res.body.graph_data[i][field] = 'etc';
					}
					chart_arr = [res.body.graph_data[i][field]];
					chart_arr.push(res.body.graph_data[i]['COUNT(*)']);
					chart_data.push(chart_arr);
				}
				//console.log(res.body.graph_data);
				$topEl.find('.list_wrap').hide().end()
				.find('.TB_visitor_chart_wrap').show();
				
			}else if(config.type === 'list'){//리스트 형태
				var len= res.body.graph_data.length;
				if(!len){//가져온 데이터가 없을 경우.
					TB_func.popupDialog('더이상 자료가 없습니다');
					if(obj.cur_page > 0) {obj.cur_page--;}
					return false;
				}
				
				for(var i = 0; i < len ; i++){
					res.body.graph_data[i].col_1= res.body.graph_data[i][field]; 
					res.body.graph_data[i].cnt= res.body.graph_data[i]['COUNT(*)']; 
				}
				
				render_set.list = res.body.graph_data;
				render_set.page = obj.cur_page;
				
				context['parts'].render({
					parts : 'list'
					, obj : render_set
					, callback : function(){
						e_period();//이벤트 재바인딩
					}
				});
				$topEl.find('.TB_visitor_chart_wrap').hide().end()
				.find('.list_wrap').show();
				
				//return false;
			}
			
			var chart_config = {
			  "config": {
				    "title": config.title
				    , "subTitle": ""
				    , "type": config.type
				    , "barWidth": 7
				    , "useShadow" : "no"
				    , "colorSet": 
				         ["#ff9547" ,'red', 'skyblue', 'pulple']
				    , "bg" : '#ffffff'
				    , 'textColor' : '#555555'
					, 'paddingBottom' : 65
					, 'useVal':config.useVal
			  }
			  , "data": chart_data
			};
			
			if($.type(_callback) === 'function'){ _callback(res); }
			
			if(config.type != 'list'){
				ccchart.init('hoge', chart_config);
			}
		});
	};
	/*=================================================
	 * 기간 별 통계 차트 출력
	 =================================================*/
	
	var selected_tab = function(_tab){
		$tabs.find('.tab').each(function(){
			var $this = $(this)
					tab_value = $this.attr('data-tab_value');
			if(tab_value === obj.cur_tab){
				$this.addClass('selected');
			}else{
				$this.removeClass('selected');
			}
			$this = null, tab_value = null;
		});
	};
	/*=================================================
	 * 선택되어 있는 탭 표시
	 =================================================*/
	
	var tab_action = function(){
		if(obj.cur_tab != 'period'){//기간별 보기 
			$period_container.slideUp();
		}else{
			$period_container.slideDown();
		}
		
		
	};
	/*=================================================
	 * 탭별 지정된 액션
	 =================================================*/
	

	context['obj'] = obj;
	//console.log(context['obj']);
	
	TB_contents.render(context, function(_$topEl){
		$topEl = _$topEl;
		$period_container = $topEl.find('.period_container');
		$tabs = $topEl.find('.tabs_wrap');
		
		
		init();//초기화
	});
});

























