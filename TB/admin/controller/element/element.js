TB_shared.set('admin--controller--element--js', function(_context){
	
	var directEl = (function($, TB_ajax, TB_el){
		var $ctrl, 
				type,
				$topEl,//컨트롤러 내부 엘리먼트
				$ctrlEl,
				$form,
				$image_element_form;
				TB_el_marked_html = "<div class='TB_el_marked ST_opacity_20_p TB_style' ";
				TB_el_marked_html += "data-TB_style='ST_parentWid_100_p' > ";
				TB_el_marked_html += "<div class='btnsCover'>"; 
				TB_el_marked_html += "<div class='create markedBtn' data-markedBtn='insert'>생성</div>"; 
				TB_el_marked_html += "<div class='modify markedBtn' data-markedBtn='modify'>수정</div>"; 
				TB_el_marked_html += "<div class='del markedBtn' data-markedBtn='delete'>삭제</div>"; 
				TB_el_marked_html += "</div>";
				TB_el_marked_html += "</div>";
				
		var init = function(_$topEl){
			$topEl = $.extend(true, $topEl, _$topEl);
			type = _context['param'].type;
			$ctrlEl = $topEl.children('.ctrl_wrp');
			$form = $topEl.find('.img_ctrl_wrap'),
			$image_element_form = $topEl.find('.image_element_form');
			//viewCtrl(type);
			directView();
			eBinding();
			etcFuncSet();
		};
		
		var etcFuncSet = function(){
			TB_shared.set('directEl_form_beforeAc', function(data){
				return true;
			});
			/*=====================================
			 * 폼 업로드 전 함수.
			 =====================================*/
			
			TB_shared.set('admin--controller--element--formUploadAft', function(_res){
				// _res.reqType = 'modify';
				setTB_el( _res);
				TB_contents.load($('#admin_controller_contents').find('.TB_contents_load'));
				setForm(_res);
				_res = null;
			});
			/*=====================================
			 * 폼 업로드 후 함수.
			 =====================================*/
		};
		/*=====================================
		 * 기타 함수 셋팅
		 =====================================*/
		
		var setTB_el = function(_obj){
			var $el = $('[data-TB_el_name='+_obj.name+']');
			if(_obj.type==='image' || _obj.type === 'element'){
				_obj.attr = _obj.attr||{};
				_obj.style = _obj.style||{};
				_obj.attr.src = _obj.src;
				delete _obj.src; 
				delete _obj.writer; 
				delete _obj.writetime;
				delete _obj.modifytime;
				delete _obj.modifier;
			}
			
			if(typeof _obj.attr ==='string'){
				_obj.attr = TB_func.removeBackslash(_obj.attr);
			}
			if(typeof _obj.style ==='string'){
				_obj.style = TB_func.removeBackslash(_obj.style);
			}
			//console.log(_obj);
			TB_el.sgStorage(_obj.name, _obj);
			TB_el.binding($el.parent());
			directView($el.parent());
			$el = null;
		};
		/*=================================
		 * db에서 받아온 오브젝트를 이용해 엘리먼트를 재 셋팅 한다.
		 =================================*/
		
		var viewCtrl = function($el){
			$ctrlEl.show().children('div').show();
			/*
			$ctrlEl.show().children('div').each(function() {
				var $this = $(this);
				if($this.attr('data-ctrl_type') === type){
					$this.show();
				}else {
					$this.hide();
				}
			});
			*/
		};
		/*==========================================
		 * 폼 컨트롤러를 활성화 한다.
		 ==========================================*/
		
		var delEl = function(_obj){
			var obj = TB_el.sgStorage(_obj.name),//TB_el 스토리지 객체
					sendData = {
						'reqType' : 'delete'
					},
					$delObj = $('[data-TB_el_name='+obj.name+']');
			if(!obj){
				return false;
			}
			if(obj.type==='image'){//이미지 일시
				sendData.reqType = 'delete';
				sendData.reqQuery = 'TB_el_img_del';
				sendData.name = obj.name;
				sendData.src = obj.attr.src;
			}else if(obj.type === 'element'){
				sendData.reqType = 'delete';
				sendData.reqQuery = 'TB_el_img_del';
				sendData.name = obj.name;
			}else if(obj.type === 'text'){
				sendData.reqType = 'delete';
				sendData.reqQuery = 'TB_el_img_del';
				sendData.name = obj.name;
				$delObj.unwrap();
				//_obj.$el.unwrap();
			}
			TB_ajax.get_json({//DB에서 테이블과 실제 이미지 파일 삭제
				'sendData' : sendData
			});
			TB_el.sgStorage(obj.name, null);//TB_el객체 삭제
			//console.log(_obj.$el.attr('style'));
			//console.log(_obj.$el.attr('class'));
			//_obj.$el.removeAttr('style').empty();//엘리먼트 스타일 삭제와 내부 비우기
			_obj.$el.empty();//엘리먼트 스타일 삭제와 내부 비우기
			//TB_el.binding();//TB_el 다시 적용
			directView($delObj.parent());//마크 다시 표시
			//'console.log(_obj.$el.attr('style'));
			//return false;
			$form.find('.ST_closeBtn_1').trigger('click');//폼 닫기
		};
		/*==========================================
		 * 동적 엘리먼트 삭제
		 * 실제 삭제 되지는 않고 초기화 시킨다.
		 * prams
		 * 	. _obj(obj) : 선택된 객체와 TB_el 객체와 기본 정보등을 포함한다. 
		 ==========================================*/
		
		var stringifyChain = function(_$el){
			var $el  = {};
			$el = $.extend($el, _$el);
			var $cover = $el.closest('form'),
					key = $el.attr('data-stringify_key'),
					val = $el.val(),
					$target = $cover.find($el.attr('data-stringify_target')),
					targetObj = $.parseJSON($target.val());
			
			targetObj[key] = val;
			$target.val(JSON.stringify(targetObj));
		};
		/*==========================================
		 * 폼내의 값을 오브젝트로 만든뒤  타켓에 해당하는 textara에 추가해서 다시 풀어 놓는다.
		 * parmas
		 * 	. _$el(jqueryObj) : 인풋값을 가지고 있는 오브젝트
		 ==========================================*/
		
		var stringifyChain_autoInsert = function(_$cover, _obj){
			var $cover = {},
					obj = {};
			$.extend($cover, _$cover);
			$.extend(obj, _obj);
			
			$cover.find('.stringifyChain').each(function(){
				var $this = $(this),
						key = $this.attr('data-stringify_key');
				if(obj[key]){
					$this.val(obj[key]);
				}
			});
			$cover = null, obj = null;
		};
		/*==========================================
		 * stringifryChin클래스를 기준으로 해당하는 키를 가지고 있을 경우 입력 시켜준다.
		 * parmas
		 * 	. $cover(jqueryObj) : top 엘리먼트
		 * 	. obj (obj) : stringChain에 입력시켜줄 오브젝트
		 ==========================================*/
		
		var setForm = function(_obj, $el){
			var obj = {},
					submit,
					$img_el_form = $form.find('.image_element_form');
			$.extend(obj, _obj);
			$topEl.find('.image_element_form')[0].reset();
			
			obj.elObj = TB_el.sgStorage(obj.name);//TB_el. 스토리지 데이터
			
			if(obj.type === 'image'){obj.reqQuery= "directEl_image"; }
			else if(obj.type === 'element'){obj.reqQuery= "directEl_element";}
			else if(obj.type === 'text'){obj.reqQuery= "directEl_text";}
			
			
			if(!obj.elObj){//생성폼
				$img_el_form.find('.file_item').show();
				obj.reqType==='insert';
				submit = '등록';
				TB_form.autoInsert($img_el_form, obj);
				$img_el_form.find('.viewImg').removeAttr('src').hide();
			}else{//수정폼
				$img_el_form.find('.file_item').hide();//파일 폼 감추기
				obj.elObj.reqType = 'modify';
				obj.elObj.reqQuery = obj.reqQuery;
				if($.type(obj.elObj.attr) === 'object'){
					obj.elObj.src = obj.elObj.attr.src;
					obj.elObj.attr = JSON.stringify(obj.elObj.attr);
				}
				if($.type(obj.elObj.style) === 'object'){
					obj.elObj.style = JSON.stringify(obj.elObj.style);
				}
				submit = '수정';
				TB_form.autoInsert($img_el_form, obj.elObj);//폼에 기본 항목 입력
				
				if(obj.elObj.attr && obj.elObj.attr !== ''){
					obj.elObj.attr = $.parseJSON(obj.elObj.attr); 
					if(obj.elObj.attr.src){//폼에 사진 있을 시 사진 입력
						$img_el_form.find('.viewImg').attr('src', TB_config.get('TB_root_path')+obj.elObj.attr.src).show();
					}
				}
				if(obj.elObj.style && obj.elObj.style !== ''){
					obj.elObj.style = $.parseJSON(obj.elObj.style);
				}
				if(obj.elObj.attr && obj.elObj.attr !== ''){
					stringifyChain_autoInsert($img_el_form, obj.elObj.attr);
				}
				if(obj.elObj.style && obj.elObj.style !== ''){
					stringifyChain_autoInsert($img_el_form, obj.elObj.style);
				}
			}
			
			viewCtrl(type);
			$form.find('.image_element_form').find('input[type=submit]').val(submit);
			obj  = null, submit = null, $img_el_form = null;
		};
		/*==========================================
		 * 객체를 가지고 폼을 셋팅 한다.
		 * params
		 * 	. _obj(obj) : TB_el 객체
		 ==========================================*/
		
		var directView = function($el){
			var $scope = $el||$(document),
					sw = true;
			if(!sw){
				return false;
			} 
			sw = !sw;
			$scope.find('.TB_el')
			.each(function(_idx){
				var $this = $(this),
						wid = $this.outerWidth(),
						hei = $this.outerHeight(),
						$img = $this.find('img'),
						name = $this.attr('data-TB_el_name'),
						TB_el_storage = TB_el.sgStorage(name);
				
				//console.log(name , wid, hei);
				if(!TB_el_storage || TB_el_storage.type == _context['param'].type){
					if($this.css('position') ==='static'){ $this.css('position', 'relative'); }
					if(!$this.children('.TB_el_marked').length){
						$this.append(TB_el_marked_html);
					}
					$this.children('.TB_el_marked')
					.css({
						'width' : wid,
						'height' : hei,
						'z-index' : 10
					}).fadeIn();
					if($img.length){//이미지가 있을 경우
						$img.load(function(){//이미지를 로딩 후 다시한번 함수 호출
							directView($scope);
						});
					}
				}
			});
			
			
			sw = !sw;
		};
		/*=============================================
		 * 마크 표시
		 =============================================*/
		
		var showBtn = function($el, isCreate){
			$el.children('.btnsCover').children('div')
			.each(function(){
				var $this = $(this), 
						btn = $this.attr('data-markedBtn');
				if($this.hasClass('create')&&isCreate==='create'){//생성버튼 보기
					$this.fadeIn();
				}else if(isCreate =='modify' && ($this.hasClass('modify')||$this.hasClass('del'))){
					$this.fadeIn();	
				}else if(isCreate ==='hide'){
					$this.hide();
				}else{
					$this.hide();
				}
				$this = null;
			});
		};
		/*=============================================
		 * 마크 엘리먼트에 마우스 오버시 보여질 버튼 결정
		 * params
		 * 	. $el(jqueryObj) : 마크 엘리먼트
		 * 	. type(str) : 보여질 버튼. 'create'가 아닌 경우는 '삭제'버튼과'수정버튼'을 보여준다.
		 * 		- 'create' , 'modify', 'del'
		 ======================================The Horizontal Way=======*/
		
		var eBinding = function($el){
			var $scope = $el||$(document);
			
			$topEl.find('.complete_button').off('click.complete')
			.on('click.complete', function(){
				//console.log('click');
				$topEl.find('textarea').focus().end()
				;
				//ie 계열의 경우 바로 트리거가 작동되면  form에 값이 입력이 안된채로 입력되어 버린다. 
				setTimeout(function(){
					$topEl.find('.submit').trigger('click');
				}, 300);
			});
			/*==/완료 버튼 클릭/==*/
			
			
			$scope.off('mouseenter', '.TB_el_marked')
			.on('mouseenter', '.TB_el_marked', function(){
				var $this = $(this), 
						name = $this.parent().attr('data-TB_el_name'), 
						obj = TB_el.sgStorage(name), 
						isCreate;
				if(!obj){//오브젝트가 없어서 새로 생성시
					isCreate = 'create';
				}else{//수정, 삭제시
					isCreate = 'modify';
				}
				showBtn($this, isCreate);
				
				$this = null, name = null, obj = null, isCreate =null;
			});
			/*==/다이렉트 마크 마우스 오버==*/
			
			$scope.off('mouseleave', '.TB_el_marked')
			.on('mouseleave', '.TB_el_marked', function(){
				var $this = $(this);
				showBtn($this, 'hide');
				$this = null;
			});
			/*==/다이렉트 마크 마우스 아웃==*/
			
			$scope.off('click', '.markedBtn')
			.on('click', '.markedBtn', function(){
				var $this  = $(this), 
						$el = $this.closest('.TB_el'),
						obj = {
							$el : $el,
							reqType :  $this.attr('data-markedBtn'),
							name : $el.attr('data-TB_el_name'),
							type : _context['param'].type
						}, 
						$text = $form.find('[name=memo]');
						
				//console.log(obj);
				//console.log($el[0].outerHTML);
				if(!obj.name){throw 'not defined attribution : \'data-TB_el_name\'  ';}
				
				if(obj.reqType === 'delete'){
					delEl(obj);
				}else{
					if(obj.type === 'text'&&$el){//텍스트 일 경우 엘리먼트에 표시
						var memo = $el.find('.TB_el_marked').remove().end().html();
						$el.empty();
						// console.log($el);
						TB_form.wysiwygSet($el, 'selection', {
							'blur' : function(){
								$form.find('[name=memo]').val($el.html());
							}
						});
						if(obj.reqType ==='modify'){//수정일 경우
							$el.html(memo);
						}
						$el.focus();
					}
					
					setForm(obj, $el);
				}
				obj = null;
			});
			/*==/다이렉트 마크 클릭==*/
			
			$topEl.find( '.image_element_form').off('change', '.file')
			.on('change', '.file', function(){
				$(this).closest('form').find('[type=submit]').trigger('click');
			});
			/*==/이미지 선택시==*/
			
			$scope.off('click', '.directEl_fofm_closeBtn')
			.on('click', '.directEl_fofm_closeBtn', function(){
				$('.image_element_form')[0].reset();
			});
			/*==/클로즈 버튼 누를시에  폼 리셋==*/
			
			$topEl.find( '.image_element_form').off('focusout', '.stringifyChain')
			.on(' focusout', '.stringifyChain', function(){
				//console.log(this);
				stringifyChain($(this));
			});
			/*==/객체화 시켜서 값 연동==*/
		};
		
		return {
			init : init
		};
	})(jQuery, TB_ajax, TB_el);
	/*===================================================
	 * 다이렉트 객체
	 ===================================================*/
	
	TB_shared.set('admin--controller--element--get_album', {
										"reqQuery" : "get_imageTable"
									});
	/*===================================================
	 * 앨범을 출력하기 위해 넘길 객체
	 ===================================================*/
	
	TB_contents.render(_context, function($topEl){
		directEl.init($topEl);
	});
});
