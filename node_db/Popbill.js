/**
 * @author user
 */
var Popbill = (function(){
	var popbill = require('popbill')
			, TB_func = require('../TB/lib/js/TB_func')
			, messageService//문자 API 모듈
			;
	
	function Popbill(){
		console.log('start popbill');
		//설정
		popbill.config({
			LinkID : 'LINKAPI'
			, SecretKey : 'NPwdoT9ZR57Y7daWJMrZS4UjOY4dUmZ8p+z0K+/Xl7E='
			, IsTest : true
			, defaultErrorHandler : function(Error){
				console.log('Error Occur : [' + Error.code + '] ' + Error.message);				
			}
		});
		
		//문자 API 셋팅
		messageService = popbill.MessageService();
	}
	/*====================================
	 * 생성자 함수.
	 ====================================*/
	
	Popbill.prototype.message = function(_options){
		var options = {
			type : 'sms'
			, copNum : '1234567890'
			, sendNum : '03180598866'//발신번호
			, recieveNum : '010-2064-6586'//수신번호
			, receiveName : '김수현'
			, contents :'문자 메시지 테스트'
		};
		
		options = TB_func.merge(options, _options);
		
		if(options.type === 'sms'){
			  var testCorpNum = '1234567890';           // 팝빌회원 사업자번호, '-' 제외 10자리
			  var sendNum = '07075103710';              // 발신번호
			  var receiveNum = '01020646586';             // 수신번호
			  var receiveName = '김수현';               // 수신자명
			  var contents = 'SMS 단건전송 메시지 테스트';   // 메시지 내용, 90Byte 초과시 길이가 조정되어 전송
			  var reserveDT = '';                      // 예약전송일시(yyyyMMddHHmmss), 미기재시 즉시전송
			  var adsYN = false;                        // 광고문자 전송여부
			
			  messageService.sendSMS(testCorpNum, sendNum, receiveNum, receiveName, contents, reserveDT, adsYN,
			    function(receiptNum){
			      console.log('success');
			      console.log(arguments);
			    }, function(Error){
			      console.log(arguments);
			  });
		}
		
	};
	/*====================================
	 * 생성자 함수.
	 ====================================*/
	
	
	return Popbill;
})();

module.exports= Popbill;