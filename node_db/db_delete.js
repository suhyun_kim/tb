var DB_delete = (function(){
	var _db= require('./db')
			, MD5 = require('../TB/lib/js/plugin/md5')
			, TB_func=  require('../TB/lib/js/TB_func')
			, db_config = require('./db_config')
			, when = require('when')
			, fall = require('../node_lib/async_fall')
			, Db_file = require('./db_file')
			, db_file
			;
	
	function DB_delete(mysql_connection_info){
		db = new _db(mysql_connection_info);
		db_file = new Db_file();
		//TB_func.init();
		//console.log(JSON.stringify(db));;
	}
	/*====================================
	 * 셀렉트
	 ====================================*/
	
	var delete_story = function(no, user, callback){
		fall([
			//스토리 데이터를 불러와서 삭제 가능한지 검사
			function(next, param){
				db.select({
					table : 'nansolo_story'
					, where : "WHERE no='"+no+"' "
					, callback : function(data){
						if(user.member_grade !='supervisor'){
							if(data[0].writer_id != user.id){
								if(typeof callback === 'function'){
									callback({'TB_dbMessage':'삭제 권한이 없습니다.','result':false});
								}
								return false;
							}
						}
						next(data[0]);
					}
				});
			}
			//파일 삭제 , DB 삭제
			, function(next, data){
				var images
					, s3_keys = ''
					, contents = ''
					, where = {etc:'', food:'',travel:'', }
					, delete_imageTable_query = ''
					;
				
				//piece db 삭제
				if(data.is_group === 'group'){
					contents = JSON.parse(data.contents_JSON);
					for(var i= 0, len = contents[i]; i < len ; i++){
						if(contents[i].category === 'etc'){
							if(!where.etc){where.etc += "WHERE no='"+contents[i].no+"' "}
							else{where.etc += "OR no='"+contents[i].no+"' "; }
						}else if(contents[i].category === 'food'){
							if(!where.food){where.food += "WHERE no='"+contents[i].no+"' "}
							else{where.food += "OR no='"+contents[i].no+"' "; }
						}else if(contents[i].category === 'travel'){
							if(!where.travel){where.travel += "WHERE no='"+contents[i].no+"' "}
							else{where.travel += "OR no='"+contents[i].no+"' "; }
						}
					}
				}else{
					if(data.category === 'etc'){
						where.etc = "WHERE no='"+data.article_no+"' ";
					}else if(data.category === 'food'){
						where.food = "WHERE no='"+data.article_no+"' ";
					}else if(data.category === 'travel'){
						where.travel = "WHERE no='"+data.article_no+"' ";
					}
				}
				//console.log(where);
				
				if(where.etc){
					db.delete({ table : 'nansolo_piece_etc', where : where.etc});
				}
				if(where.food){
					db.delete({ table : 'nansolo_piece_food', where : where.food});
				}
				if(where.travel){
					db.delete({ table : 'nansolo_piece_travel' , where : where.travel});
				}
				
				//story_db삭제
				db.delete({
					table : 'nansolo_story'
					, where : "WHERE no='"+data.no+"' "
				});

				//console.log(data);
				//파일삭제
				if(data.images_JSON){
					images = JSON.parse(data.images_JSON);
					s3_keys = db_file.s3_convert_srcToKeys(images);
					db_file.s3_delete_objects(s3_keys, function(data){
							//console.log(data);
					});
					for(var i= 0, len= images ; i < len ; i++){
						delete_imageTable_query = (i<=0)?"WHERE no='"+images[i].no+"' '":" OR no='"+images[i].no+"' ";
					}
					
					db.delete({
						table : 'TB_imageTable'
						, where : delete_imageTable_query
					});
				}
				
			}
		]);
	};
	/*====================================
	 * 함수
	 ====================================*/
	
	DB_delete.prototype.setQuery = function(req, callback, _passport){
		var result = []
				, query_obj = req.body
				, query = (typeof query_obj == 'object')?query_obj.reqQuery:query_obj
				, now = new Date().format('yyyy-MM-dd hh:mm:ss')
				, auth
				;
		//insert 시 필요 없는 부분 삭제
		delete query_obj.reqType;		
		delete query_obj.reqQuery;
		if(query_obj.TB_adminAuthKey){
			auth = TB_func.objCopy(query_obj.TB_adminAuthKey);
			delete query_obj.TB_adminAuthKey;
		}
		
		switch(query){
			case 'delete_achievement_category'://분류 삭제
				db.delete({
					table : 'nansolo_achievement_category'
					, where : "WHERE no='"+req.body.no+"' "
					, callback : function(result){
						callback(result);
					}
				});
			break;
			case 'delete_story':
				if(req.user){
					delete_story(req.body.no, req.user, function(result){
						//consol.log(result);
						callback(result);
					});
				}else{
					callback({"TB_dbMessage":"권한오류입니다.","result":false});
				}
			break;
			case 'TB_el_img_del'://다이렉트 엘리먼트 삭제
				if(session.user_level != 'admin'){
					callback({'TB_dbMessage':"권한 오류 입니다.", "TB_dbAuthError" : true});
					return false;
				}

				db.delete({ table : 'TB_el', where : " WHERE name='"+query_obj.name+"' " 
					, callback : function(){
						if(typeof callback == 'function'){ callback({"TB_result":"success"});}
					}
				});
			break;
			
			default://지정된 쿼리가 없을 경우.
				console.log('not defined delete query : '+query);
			break;
		}
	};
	/*====================================
	 * 쿼리 셋팅
	 ====================================*/
	
	return DB_delete;
})();

module.exports = DB_delete;
