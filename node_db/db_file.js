var DB_file = (function(){
	var	TB_func=  require('../TB/lib/js/TB_func')
			, db_config = require('./db_config')
			, AWS = require('aws-sdk')
			, s3 = new AWS.S3({region : 'ap-northeast-2'})
			, s3Bucket = 'nansolo.images'
			, fs = require('fs')
			, when = require('when')
			, mime = require('mime')
			, im = require('imagemagick')
			, gm = require('gm').subClass({imageMagick:true})
			, fall = require('../node_lib/async_fall')
			;
			
			
	function DB_file(){
		//TB_func.init();
	}
	/*===============================================
	 * 생성자 함수.
	 ===============================================*/
	
	DB_file.prototype.isImage = function(mimetype){
		if(['image/jpeg','image/jpg','image/gif','image/png' ].indexOf(mimetype) >= 0){
			return true;
		}
		return false;
	};
	/*===============================================
	 * 이미지 인지 확인
	 * params
	 *  . mimetype(str) : 이미지 타입
	 ===============================================*/
	
	
	DB_file.prototype.convertDD = function(lat, latref, lon, lonref){
		var gps = {
			lat : lat.split(',')
			, latref : latref
			, lon : lon.split(',')
			, lonref : lonref
		};
		
		//분수 타입의 위도와 경로를 실수로 변경한다.
		var divide = function(fra_num){
			var numArr = [];
			for(var i = 0, len = fra_num.length; i < len ; i++){
				//fra_num[]
				var arr = fra_num[i].split('/');
				numArr[i] = arr[0]/arr[1];
			}
			return numArr;
		};
		
		//DMS(각도.분,초) 타입의 gps정보를 DD 타입으로 변경해준다. 
		var convertDMSToDD = function(degrees, minutes, seconds, direction){
			var dd = degrees + minutes/60+seconds/(60*60);
			if(direction == 'S'||direction == 'W'){
				dd = dd*-1;
			}
			return dd;
		};
		
		gps.lat = divide(gps.lat);
		gps.lon = divide(gps.lon);
		
		gps.lat = convertDMSToDD(gps.lat[0],gps.lat[1], gps.lat[2], gps.latref).toFixed(5);
		gps.lon = convertDMSToDD(gps.lon[0],gps.lon[1], gps.lon[2], gps.lonref).toFixed(5);
		return gps;
	};
	/*===============================================
	 * imagemagick에서의 DD타입의 gps정보로 변경한다.
	 ===============================================*/
	
	var orientationToRotation = function(orien){
		if(orien == 1){
			return 0;
		}else if(orien == 6){
			return 90;
		}else if(orien == 3){
			return 180;
		}else if(orien == 8){
			return -90;
		}
		
	};
	/*===============================================
	 * 오리엔테이션 정보를 보고 이미지를 어느 정도 회전 시킬지 리턴한다.
	 ===============================================*/
	
	DB_file.prototype.resizeImageS3 = function( _options){
		var options = {
					folder : undefined
					, filename : undefined
					, resize : {
						width : undefined
						, height: undefined
						, maxWidth : undefined
						, maxHeight : undefined
						, ratioLock : true//비율을 고정할 것인지 정하는것.
					}
					, ogInfo : {}//원본 이미지의 정보.
					, rotate : 0//회전각도
					, callback : undefined
				}
				, ratioLock = true
				;
		
		options = TB_func.merge(options, _options);
		
		//비율락의 해제 와 
		if((options.resize.maxWidth && options.resize.maxHeight)&&(options.resize.width && options.resize.height)){
			ratioLock = false;
		}
		
		//maxWidth를 width로 변경.
		if(options.resize.maxWidth){options.resize.width = options.resize.maxWidth;}
		if(options.resize.maxHeight){options.resize.height = options.resize.maxHeight;}
		
		//비율제한
		if(ratioLock){
			
			if(options.resize.width){
				options.resize.width = options.resize.width;
				options.resize.height = Math.round(options.ogInfo.height*options.resize.width/options.ogInfo.width);
				
			}else if(options.resize.height){
				options.resize.height = options.height;
				options.resize.width = Math.round(options.ogInfo.width*options.resize.height/options.ogInfo.height);
				
			}
		
		}else{//비율 제한 없음
			
			if(!options.resize.width){ options.resize.width = options.ogInfo.width; }
			else if(!options.resize.height) { options.resize.height = options.ogInfo.height; }
		}
		
		fall([
			//이미지 가져오기 
			function(next){
				s3.getObject({
					Bucket : s3Bucket
					, Key : options.folder+options.filename 
				}, function(err, data){
					if(err){ throw err; }
					next(data);
				});
			}
			//리사이즈
			, function(next, param){
				gm(param.Body)
				.autoOrient()
				.resize(options.resize.width)
				.toBuffer(function(err, buffer){
					if(err){throw err;}
					next(buffer);
				});
			}
			//재업로드
			, function(next, param){
				s3.putObject({
					Bucket : s3Bucket
					, Key : options.folder+options.filename
					, Body : new Buffer(param ,'binary')
					, ContentType : mime.lookup(options.filename)
				}, function(err, data){
					if(err){throw err; }
					if(typeof options.callback === 'function'){options.callback();}
				});
			}
			
		]);
		
	};
	/*===============================================
	 * 이미지 리사이즈
	 ===============================================*/
	
	DB_file.prototype.uploadS3= function(file, _options){
		var options = {
					folder_url : ''//폴더 url
					, autoCreateDate : false //폴더 생성시 년,월,일 형태의 폴더를 자동으로 만든다.(yyyy-MM-dd 형태의 데이트)
					, type : 'image'//파일의 형태 체크
					, callback : undefined
					, resize : {
 						width : undefined , height: undefined
						, maxWidth : undefined , maxHeight: undefined
					}
				}
				, mimetype = mime.lookup(file.originalname)//이미지 타입인지 확인.
				, typepath = false
				, resize = false
				, rotate = false
				, db_file = this
				, fileInfo
				, exif = undefined //exif meta data
				, now
				;
		
		options = TB_func.merge(options, _options);
		
		if(!options.folder_url){
			throw '파일의 경로를 지정해 주십시요.';
			return false;
		}
		
		//일자별 폴더 경로 자동 생성.
		if(options.autoCreateDate){
			now = new Date().format('yyyy-MM-dd');
			now = now.split('-');
			options.folder_url = options.folder_url+'/'+now[0]+'/'+now[1]+'/'+now[2];
		}
		
		//리사이징 작업을 해야 하는지 체크
		resize = (options.resize.width||options.resize.height||options.resize.maxWidth||options.resize.maxHeight)?true:false;
		
		//파일 지정.
		if(!file){throw '파일을 지정해 주십시요.'; return false;}
		
		//폴더의 경로가 지정 안되어 있을 때.
		if(options.folder_url === ''){ 
			throw 'not defined Error uploadFolder url';
			return false;
		}
		
		//타입 제한.
		if(options.type){
			
			//이미지 타입 체크
			if(options.type === 'image'){
				typepath = this.isImage(mimetype);
			}
			
			//타입을 검사를 통과하지 못 했을 경우
			if(typepath === false){
				options.callback({'TB_dbMessage':'이미지 파일이 아닙니다.', result : false});
				return false;
			}
		}
		
		//끝에 슬래쉬가 안 붙어 있을 경우 붙이기.
		if(options.folder_url.substr(-1,1) != '/'){ options.folder_url += '/';}
		
		fs.readFile(file.path, function(err, data){
			if(err){console.log(err); return false;}
			var filename = file.originalname.split('.');
			
			//파일의 확장자 검사.
			if(filename.length <= 1){
				options.callback({'TB_dbMessage':'파일의 확장자를 확인해 주십시요', result:false});
				return false;
			}
			
			filename = file.filename+'.'+filename[filename.length-1];
			
			fall([
				//파일의 세부 정보를 획득(exif등)
				function(next){
					im.identify( file.path , function(err, data){
						if(err){ throw err; }
						next(data);
					});
				}
				
				//파일 업로드, 임시파일 삭제
				, function(next, param){
					s3.putObject(
						{
							Bucket : s3Bucket
							, Key : options.folder_url+filename
							, Body : data
							, ContentType : mimetype
						}, function(err, data){
							//파일 삭제
							fs.unlink('TB/tmp/uploadFiles/'+file.filename, function(_err){
								if(_err){ throw _err; return false; }
							});
							
							if(err){ throw err; }
							next(param);
						}
					);
				}
				//파일 별 작업
				, function(next,_fileinfo){
					var width;
					//이미지일 경우 리사이즈
					if(options.type === 'image'){
						
						//최대 넓이를 넘길 경우.
						if(_fileinfo.width > options.resize.maxWidth){
							options.resize.width = options.resize.maxWidth;
						}else{
							options.resize.width = _fileinfo.width;
						}
						
						db_file.resizeImageS3( {
							folder: options.folder_url
							, filename: filename
							, resize : options.resize 
							, ogInfo : _fileinfo
							, callback : function(){
								next(_fileinfo);
							}
						});
					}
				}
				//콜백처리.
				, function(next, param){
					if(typeof options.callback === 'function'){
						options.callback({
							file_url : s3.endpoint.href+s3Bucket+'/'+options.folder_url+filename
							, endpoint : s3.endpoint.href
							, bucket : s3Bucket
							, relative_url : options.folder_url+filename
							, file_name : filename
							, fileinfo : param
							, result:true
						});
					}
				}
			]);
			
		});
		
	};
	/*===============================================
	 * s3에 파일 업로드 
	 ===============================================*/
	
	DB_file.prototype.s3_delete_objects= function(objects, callback ){
		s3.deleteObjects({
			Bucket : s3Bucket
			, Delete : {
				Objects : objects
			}
		}, function(err, data){
			if(err){console.log(err, err.statck);return false;}
			
			if(typeof callback === 'function'){callback(data);}
			//console.log(data);
		});
	};
	/*===============================================
	 * s3에서 파일 삭제 
	 ===============================================*/
	
	DB_file.prototype.s3_convert_srcToKeys = function(_data){
		var keys = [], src = ''
			, data = _data
			;
			
		for(var i =0, len =data.length; i < len ; i++){
			src = data[i].src.split('/');
			src.splice(0,4);
			keys[i] = {Key:src.join('/')};
		}
		return keys;
	} 
	/*===============================================
	 * 파일의 주소에서 s3 오브젝트의 키를 추출 
	 ===============================================*/
	
	
	
	return DB_file;
})();


module.exports = DB_file;
