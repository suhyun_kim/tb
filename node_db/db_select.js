
var DB_select = (function(){
	var _db= require('./db')
			, MD5 = require('../TB/lib/js/plugin/md5')
			, TB_func=  require('../TB/lib/js/TB_func')
			, db_config = require('./db_config')
			, when = require('when')
			, passport
			, now
			, fall = require('../node_lib/async_fall')
			;
	
	
	function DB_select(mysql_connection_info){
		db = new _db(mysql_connection_info);
		//console.log('test');
	}
	/*====================================
	 * 셀렉트
	 ====================================*/
	
	var member_login = function(db_user, req, _callback){
		var hasher = require('../node_lib/hasher')
				, result = undefined
				;
		
		hasher(req.body.password, db_user.salt, function(hash, salt, passw){
			var user = undefined
					;
			
			//로그인 
			if(db_user.password === hash){
				user = {};
				user.id = db_user.id;
				user.nickname = db_user.nickname;
				user.member_grade = db_user.member_grade;
				user.member_level = db_user.member_level;
			}else{
				user = false;
			}
			
			if(typeof _callback === 'function'){
				
				//로그인 실패시
				if(!user ){
					_callback(user);
					return false;
				}
				
				//로그인 정보 업데이트
				db.update({
					table : 'TB_member'
					, where : 'WHERE id=binary("'+db_user.id+'") '
					, data : {
						login_time : now.format('yyyy-MM-dd hh:mm:ss')
						, login_ip : req.ip
						, login_count : db_user.login_count+1
						, approach_count : 0
					}
				});
				
				//패스포트 저장.
				req.logIn(user, function(err){
					req.session.save(function(){
						//console.log('로그인');
						_callback(user);
					});
				});
					
			}
		});
	};
	/*==/로그인 ==*/
	
	
	
	/*==/mystory 가져오기 ==*/
	
	/*====================================
	 * 함수 
	 ====================================*/
	
	DB_select.prototype.setQuery = function(req, callback, _passport, _res){
		var result = []
				, query = req.body.reqQuery
				, query_obj = req.body
				;
		
		now = new Date();
		passport = _passport;
		
		switch(query){
			
			case 'achievement_category_list':
			var bbs = require('../node_lib/TB_bbs')(db, TB_func, fall)
					;
				
				bbs.get_bbs({
					table : 'nansolo_achievement_category'
					, page : req.body.page
					, page_num : req.body.page_num||3
					, list_num : req.body.list_num
					, callback : function(data){
						callback(data);
					}
				});
			break;
			
			case 'all_story'://전체 스토리 (관리자)
			case 'mystory'://나의 이야기
				var bbs = require('../node_lib/TB_bbs')(db, TB_func, fall)
						, where = '';
				
				//console.log(req.user.member_grade);
				if(query === 'mystory'){
					where += ' writer_id="'+req.user.id+'" ';
				}
				
				//카테고리
				if(req.body.category &&req.body.category != 'all'){
					where += "AND category='"+req.body.category+"' ";
				}
				
				
				if(req.body.search_field){
					if(req.body.search_field === 'subjectNmemo'){
						where += "AND (subject LIKE '%"+req.body.search_value+"%' OR memo LIKE '%"+req.body.search_value+"%') ";
					}else{
						where += "AND "+req.body.search_field+" LIKE  '%"+req.body.search_value+"%' ";
					}
				}
				
				if(where){ where = "WHERE "+where; }
				//console.log(req.body);
				//console.log(where);
				
				bbs.get_bbs({
					table:'nansolo_story'
					, page : req.body.page
					, list_num : req.body.list_num
					, where : where
					, callback : function(data){
						callback(data);
					}
				});
			break;
			
			case 'get_detail_story':
				fall([
					//스토리 데이터 가져 오기
					function(next, param){
						db.select({
							table : 'nansolo_story'
							, where : "WHERE category='"+req.body.category+"' AND no='"+req.body.no+"' "
							, callback : function(data){
								if(data[0].is_group === 'none'){
									if(typeof callback == 'function'){callback(data);}
									return false;
								}
								next(data.is_group);
							}
						});
					}
					//그룹일 경우 그룹 이미지들 같이 내보내기
					, function(next, param){
						console.log(param);
					}
				]);
			break;
			
			//홈페이지가 만들어져 있나 확인
			case 'isCreate':
				db.select({
					table:'TB_member'
					, where : "WHERE member_grade='supervisor' "
					, field : 'COUNT (no) AS cnt'
					, callback : function(data){
						if(data[0].cnt){//홈페이지가 존재할경우
							result.push({ reqType : "select", reqQuery : "loginAdmin" });
						}else{//홈페이지가 없을 경우.
							result.push({ reqType : "insert", reqQuery : "createAdmin"});
						}
						result[0].isCreate = data[0].cnt;
						if(typeof callback == 'function'){callback(result);}
					}
				});
			break;
			
			case 'get_TB_el'://el 정보를 가져온다.
				db.select({table : 'TB_el'
					, callback : function(data){
						 if(typeof callback == 'function'){callback(data);} 
					 }
				});
			break;
			
			//정보를 가져온다.(메뉴, 사용자정보)
			case 'getConfig':
				//console.log(req.user);
				db.select({table : 'TB_config'
					, callback : function(data){
						var menu
								;
						
						 if(typeof callback == 'function'){
						 	data[0] = TB_func.merge(data[0], db_config.sector);
						 	data[0].user = req.user; 
						 	data[0].menu = data[0].menu&&JSON.parse(data[0].menu);
	 						//console.log(data[0].menu);
						 	//메뉴 객체화
						 	if(data[0].menu){
							 	for(var i = 0, len = data[0].menu.length ; i < len ; i++){
						 			//console.log(data[0].menu[i].sendData);
							 		if(data[0].menu[i].sendData){
							 			//sendData = JSON.parse(data[0].menu[i].sendData);
							 			if(data[0].menu[i].sendData.use_grade){
							 				//req.user 가 없거나 member_grade가 안맞을 경우  메뉴 삭제
							 				if(!req.user
							 					||(req.user &&req.user.member_grade != data[0].menu[i].sendData.use_grade)
						 					){
							 					data[0].menu = data[0].menu.splice(0,i);
							 				}
							 			}
							 		}
							 		
							 	}
						 		
						 	}
						 	
						 	
						 	callback(data[0]);
					 	}
					 }
				});
			break;
			
			//관리자 로그인(session을 사용할 경우.)
			case 'loginAdmin':

				var updateData = {};
						
				var update_approach = function(_user, _callback){
					var update_data = {
								approach_ip : req.ip
								, approach_time : now.format('yyyy-MM-dd hh:mm:ss')
								, approach_count : _user.approach_count+1
							}
							;
					db.update({ table : 'TB_member' , where : "WHERE id=binary('"+_user.id+"')" , data : update_data});
					
					if(typeof _callback === 'function'){ _callback(); }
				};
				/*==/접근 기록 업데이트 ==*/
				
				when
				.promise(function(resolve, reject){
					//console.log('1 :');
					//유저가 있는지 검사
					db.select({table : 'TB_member', where: "WHERE id=binary('"+query_obj.id+"') AND member_grade='supervisor'", field : " * "
						, callback : function(db_user){
							if(db_user.length <=0){//해당하는 사용자가 없을 경우.
								if(typeof callback =='function'){
									reject();
									callback([{"TB_dbMessage":"아이디 및 비밀번호를 잘 못 입력하였습니다."}]);
									return false;
								}
							}else{
								resolve(db_user[0]);
							}
						}
					});
					
				})
				//5분 경과가 안됐을 시 시도
				.then(function(db_user){
					//console.log('2 :');
					var period = now - db_user.approach_time
							;
							
					period = Math.floor(period/1000/60);
					update_approach(db_user);//로그인 접근업데이트
					//로그인 실패 횟수가 5번이상일 경우 5분내에 접속 불가.
					if(period < 5  && db_user.approach_count > 4){
						callback([{"TB_dbMessage":"5분 경과 후 다시 시도해 주십시요.."}]);
						return false;
					}
					
					return db_user;
				})
				.then(function(db_user){//로그인 
					if(!db_user){return false;}
					
					member_login(db_user, req, function(user){
						if(user){
							//console.log('멤버 로그인 석세스');
							callback([{'result':'success', adminInfo:user}]);
						}else{
							callback([{'result':'faield'}]);
						}
					});
					
				});
			break;
			
			//관리자 인지 확인
			case 'isAdmin':
				if(typeof req.user === 'undefined' ||req.user.member_grade != 'supervisor' ){
					callback([{"isAdmin": false}]);
				}else{
					callback([{"isAdmin": true}]);
				}
			break;
			
			//관리자 로그아웃 처리.
			case 'adminLoginOut': 
				db.update({
					 table : 'TB_member'
					, where : "WHERE id=binary('"+req.user.id+"') " 
					, data : {
						logout_time : now
					} 
					, callback : function(data){
						req.logOut();
						req.session.save(function(){
							callback([{'logout':'success'}]);
						});
					}	
				});
			break;
			
			case 'overlap_id'://해당하는 아이디가 존재 하는지 확인
				db.select({ table : 'dongne_client_info', where : "WHERE id=binary('"+query_obj.id+"') ", field : 'COUNT(id) as count'
					, callback : function(data){
						callback([data]);
					}
				});
			break;
			
			case 'joinSMS'://가입 문자 발송.
				var _popbill = require('./Popbill');
				//console.log(_popbill);
				var popbill = new _popbill();
				popbill.message();
				console.log(popbill);
			break;
			
			/*
			case 'visitor'://방문자 정보를 가져온다.
				db.select({table : 'TB_config'
					, callback : function(data){ if(typeof callback == 'function'){callback(data);} }
				});
			break;
			*/
			default://지정된 쿼리가 없을 경우.
				console.log('not defined select query : '+query);
			break;
		}
	};
	/*====================================
	 * 쿼리 셋팅
	 ====================================*/
	
	return DB_select;
})();

module.exports = DB_select;
