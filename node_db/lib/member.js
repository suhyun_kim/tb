module.exports = function(_db){
	var TB_func=  require('../../TB/lib/js/TB_func')
		, when = require('when')
		, db = _db
		; 
	
	var join_user = function(_tableName, _data, callback){
		var tableName = _tableName
			, data = _data
			, hasher = require('../../node_lib/hasher')
			;
				
		data.join_time = new Date().format('yyyy-MM-dd hh:mm:ss');
		data.approach_time = data.join_time;
		if(data.ip){
			data.join_ip = data.ip;
			data.approach_ip = data.join_ip;
			delete data.ip;
		}
		
		if(data.member_grade === 'supervisor'){
			data.nickname = '관리자';
		}
		//console.log(data);
		hasher(data.password, undefined, function(hash, salt){
			data.password = hash;
			data.salt = salt;
			//console.log(data);
			db.insert({
				table : tableName
				, data : data
				, callback : function(result){
					console.log(result);
					callback(result);
				}
			});
		});
		
	};
	/*=============================================
	 * 기본 유저 가입
	 =============================================*/
	
	var approach = function(user, callback){
		var now = new Date().format('yyyy-MM-dd hh:mm:ss')
		db.update({
			table : 'TB_member'
			, where : " WHERE google_id=binary('"+user.google_id+"') AND id=binary('"+user.email+"') "
			, data : {
				login_count : 5
			}
			, callback : function(){
				if(typeof callback === 'function'){callback();}
			}
		});
	};
	/*=============================================
	 * 유저 접근 기록 업데이트
	 =============================================*/
	
	var join_google = function(profile, done){
		//console.log(profile);
		if(!profile){
			throw 'not defined profile';
			return false;
		}
		db.select({
			table : 'TB_member'
			, field: 'id, google_id, nickname, member_grade, member_level, login_count'
			, where : " WHERE google_id=binary('"+profile.id+"') AND id=binary('"+profile.emails[0].value+"')  "
			, callback : function(_user){
	  			//console.log('*-*-*-*-*-*-*-*-*-*-*-*-*');
				//console.log('ttdf');
				var user = {
					id : profile.emails[0].value
					, password : profile.id
					, google_id : profile.id
					, nickname : profile.displayName
					//, gender : profile.gender||'undefined'
					, email : profile.emails[0].value
					, member_grade : 'user'
					, member_level : 5
					, ip : profile.ip
				};
				
				if(profile.gender){
					user.gender = profile.gender;
				}
				//console.log(_user);
				//return false;
				if(_user.length){//해당 유저가 있을 경우 로그인 기록 업로드
					//console.log('접근처리', _user);
					//approach(user.google_id, user.email);
					user.login_count = _user[0].login_count;
					approach(user);
					delete user.password;
					delete user.google_id;
					delete user.login_ip;
					delete user.login_count;
					return done(null, user);
				}else{//회원 가입
					//console.log('회원가입');
					join_user(
						'TB_member'
						, user, function(result){
							delete user.password;
							delete user.google_id;
							delete user.login_ip;
							return done(null, user);
						}
					);
				}
				//done(null, user[0]);
			}
		});
	};
	/*=============================================
	 * 구글 가입
	 =============================================*/
	
	return {
		join_google : join_google
	}
};
