
var DB = (function(){
	var mysql = require('mysql')
			, connection
			, query 
			, pool
			;
	
	function DB(mysql_connection_info){
		pool = mysql.createPool(mysql_connection_info);
	}
	/*=============================================
	 * 생성자 함수
	 =============================================*/
	
	var select_commit = function(query, callback){
		pool.getConnection(function(err, connection){
			if(err){
				if(connection){
					connection.release();
				}
				throw 'Error pool.getConnection \n'+err;
			}
			
			connection.beginTransaction(function(transactionErr){
				
				if(transactionErr){
					if(connection){ connection.release(); }
					throw  "transactionErr : \n"+transactionErr;
				}
				connection.query(query, function(queryErr, result, field){
					
					if(queryErr){
						throw 'query Error : '+query+'\n'+queryErr;
						connection.rollback(function(rollbackError){
							console.log('rollback Error(query) : '+rollbackError);
							//connnection.release();
							//connection.end();
							throw rollbackError; 
						});
					}
					
					connection.commit(function(commitErr){
						if(commitErr){ 
							console.log('commitErr : '+query);
							connection.rollback(function(rollbackError){
								console.log('rollback Error(commit) : '+rollbackError);
								connection.release();
								//connection.end();
								throw rollbackError; 
							});
						}
						
						if(typeof callback === 'function'){
							callback(result);
							connection.release();
							//connection.end();
						}
					});
					
				});
				
			});
		});
		//console.log(query);
	};
	/*=============================================
	 * select query실행
	 =============================================*/
	
	var insert_commit = function(query, data, callback){
		console.log(query);
		console.log(data);
		pool.getConnection(function(err, connection){
			if(err){
				if(connection){
					connection.release();
				}
				throw 'Error pool.getConnection() \n'+err;
			}
			connection.beginTransaction(function(transactionErr){
				if(transactionErr){console.log('transactionErr : '+query);throw transactionErr;}
				connection.query(query, data,function(queryErr, result, field){
					if(queryErr){
						console.log('query Error : '+query+'\n'+queryErr+'');
						connection.rollback(function(rollbackError){
							console.log('rollback Error(query): '+rollbackError);
							//connnection.release();
							//connection.end();
							throw rollbackError; 
						});
					}
					connection.commit(function(commitErr){
						if(commitErr){ 
							console.log('commitErr : '+query);
							connection.rollback(function(rollbackError){
								console.log('rollback Error(commit): '+rollbackError);
								//connection.release();
								//connection.end();
								throw rollbackError; 
							});
						}
						if(typeof callback === 'function'){
							callback(result);
							connection.release();
							//connection.end();
						}
					});
					
				});
				
			});
		});
	};
	/*=============================================
	 * insert query실행
	 =============================================*/
	
	
	var updateString = function(_obj){
		var result = {str : '', arr : new Array()}, k = 0;
		for( var i in _obj){
			if(k ===0){
				result.str += i+"= ? ";
				k++;
			}else{ 
				result.str += ", "+i+"= ? "; 
			}
			result.arr.push(_obj[i]);
		}
		k = null, _obj = null, i = null;
		return result;
	};
	/*=============================================
	 * 객체를 업데이트에 알맞는 형태로 만들어 준다.
	 =============================================*/
	
	DB.prototype.end = function(){
		
	};
	
	DB.prototype.select =function(_obj){
		var obj = { table : _obj.table, field : _obj.field||'*', where : _obj.where||'', callback : _obj.callback};
		if(!obj.table){throw '테이블이 지정되어 있지 않습니다.';return false;}
		query = 'SELECT '+obj.field+' FROM '+obj.table+' '+obj.where;
		select_commit(query, obj.callback);
	};
	/*=========================================
	 * 가장 기본적인 SELECT
	 =========================================*/
	
	DB.prototype.insert = function(_obj){
		query = "INSERT INTO "+_obj.table+" SET ? ";
		insert_commit(query, _obj.data, _obj.callback);
	};
	/*=========================================
	 * 가장 기본적인 INSERT
	 =========================================*/
	
	DB.prototype.update = function(_obj){
		var query = "UPDATE "+_obj.table+" SET "
				, k = 0
				 ;
		if(typeof _obj.data != 'object'){console.log('db update Error 데이터 타입이 오브젝트가 아닙니다.'); return false;}
		for(var i in _obj.data){
			if(k == 0){
				query += i+"='"+_obj.data[i]+"' ";
				k++;
			}else{
				query += ", "+i+"='"+_obj.data[i]+"' ";
			}
		}
		if(_obj.where){ query += _obj.where; }
		select_commit(query, _obj.callback);
		i = null, query = null, k = null;
	};
	/*=========================================
	 * 기본적인 UPDATE문
	 =========================================*/
	
	DB.prototype.delete = function(_obj){
		var query = "DELETE FROM "+_obj.table+" "
				 ;
		if(_obj.where){ query += _obj.where; }
		select_commit(query, _obj.callback);
		i = null, query = null, k = null;
	};
	/*=========================================
	 * 기본 적인 DELETE 문
	 =========================================*/
	
	
	return DB;
})();

module.exports = DB;
