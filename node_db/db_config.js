var TB_config = {};

//가입시 암호화전 패스워드 뒷자리에 붙일 키
TB_config.joinKey = 'Tolol45!@<J';


//기본적으로 지정된곳과 통신하는 것이 맞나 확인하는 키.
TB_config.TB_signalKey = 'TB_dongneand';

//카테 고리.
TB_config.sector = {
	category_list : [
		{
			name : "음식", _class:0 
			, child : [
				{name : '치킨', _class:1}
				,{name : '일식', _class:1} 
				,{name : '피자/양식', _class:1} 
				,{name : '중국집', _class:1} 
				,{name : '한식', _class:1} 
				,{name : '족발/보쌈', _class:1} 
				,{name : '분식', _class:1} 
				,{name : '제과', _class:1} 
				,{name : '기타', _class:1} 
			]
		},{
			name : "식품/마트", _class:0
			,child : [
				{name : '육류', _class:1}
				,{name : '청과', _class:1} 
				,{name : '마트', _class:1} 
				,{name : '기타', _class:1}
			]
		},{
			name : "뷰티", _class:0
			,child : [
				{name : '네일', _class:1}
				,{name : '피부샵', _class:1} 
				,{name : '미용실', _class:1} 
				,{name : '이발소', _class:1}
				,{name : '마사지', _class:1}
			]
		},{
			name : "스포츠/레져/문화", _class:0
			,child : [
				{name : '동호회', _class:1}
				,{name : '탁구', _class:1} 
				,{name : '볼링', _class:1} 
				,{name : '당구', _class:1}
				,{name : '극장', _class:1}
			]
		},{
			name : "의류/유아", _class:0
			,child : [
				{name : '여성용', _class:1}
				,{name : '남성용', _class:1} 
				,{name : '아동복', _class:1} 
				,{name : '유아용품', _class:1} 
				,{name : '신발', _class:1}
			]
		},{
			name : "교육", _class:0
			,child : [
				{name : '종합학원', _class:1}
				,{name : '국비지원학원', _class:1} 
				,{name : '음악학원', _class:1} 
				,{name : '언어학원', _class:1} 
				,{name : '격투기', _class:1}
				,{name : '기타', _class:1}
			]
		},{
			name : "의료/동물", _class:0
			,child : [
				{name : '종합병원', _class:1}
				,{name : '산부인과', _class:1} 
				,{name : '소아과', _class:1} 
				,{name : '이비인후과', _class:1} 
				,{name : '피부과', _class:1} 
				,{name : '치과', _class:1} 
				,{name : '한의원', _class:1}
				,{name : '동물병원', _class:1}
				,{name : '애견용품', _class:1}
			]
		},{
			name : "가구/생활", _class:0
			, child : [
				{name: '가구', _class:1}
				, {name: '주방/생활용품', _class:1}
			]
		},{
			name : "전자/가전/통신", _class:0
			,child : [
				{name : '종합가전', _class:1} 
				, {name : '컴퓨터', _class:1}
				, {name : '핸드폰', _class:1}
				, {name : '인터넷/네트워크', _class:1}
			]
		},{
			name : "자동차", _class:0
			,child : [
				{name : '자동차수리', _class:1}
				,{name : '자동차판매', _class:1}
			]
		},{
			name : "자재/수리/인테리어", _class:0
			,child : [
				{name : '철물점', _class:1}
				, {name : '배관', _class:1} 
				, {name : '전기', _class:1} 
				, {name: '조명', _class:1}
				, {name : '타일/싱크', _class:1} 
				, {name: '인테리어', _class:1}
			]
		},{
			name : "기타", _class:0
			,child : [
				{name : '도서', _class:1}
				,{name : '문구', _class:1} 
				,{name : '여행', _class:1} 
				,{name : '사진', _class:1} 
			]
		},{
			name : "중고장터", _class:0
			,child : [
				{name : '가구', _class:1}
				,{name : '의류', _class:1} 
				,{name : '가전/전자', _class:1} 
				,{name : '기타', _class:1} 
			]
		}
	]
};


module.exports = TB_config;
