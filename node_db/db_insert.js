'use strict';

var DB_insert = (function(){
	var _db= require('./db')
			, db
			, MD5 = require('../TB/lib/js/plugin/md5')
			, TB_func=  require('../TB/lib/js/TB_func')
			, db_config = require('./db_config')
			, when = require('when')
			, fall = require('../node_lib/async_fall')
			, Db_file = require('./db_file')
			, db_file
			;
	
	function DB_insert(mysql_connection_info){
		db = new _db(mysql_connection_info);
		db_file = new Db_file();
		//TB_func.init();
		//console.log(JSON.stringify(db));;
	}
	/*====================================
	 * 셀렉트
	 ====================================*/
	
	var joinUser = function(_tableName, _req, callback){
		var req = _req
			, tableName = _tableName
			, hasher = require('../node_lib/hasher')
			, query_obj = req.body
			;
				
		query_obj.join_time = new Date().format('yyyy-MM-dd hh:mm:ss');
		query_obj.join_ip = req.ip;
		query_obj.approach_time = query_obj.join_time;
		query_obj.approach_ip = query_obj.join_ip;
		
		if(query_obj.member_grade === 'supervisor'){
			query_obj.nickname ='관리자';
		}
		
		when
		.promise(function(resolve, reject){//패스워드 암호화 후 입력.
			hasher(query_obj.password, undefined, function(hash, salt){
				query_obj.password = hash;
				query_obj.salt = salt;
				resolve();
			});
		})
		.then(function(){
			db.insert({table : tableName, data : query_obj
				, callback : function(result){
					callback(result);
				}
			});
		})
		;
	};
	/*====================================
	 * 멤버 가입
	 ====================================*/
	
	var insert_piece = function(data, files, user, now, ip, callback){
		var table_name = ''//db
			, category = data.category
			, insert_data = {//입력 데이터
				subject : data.subject
				, memo : data.memo
				, images_src : ''//대표 이미지
				, images_JSON : data.images_JSON||''
				, status : 'view'
				, stars_cnt : data.stars_cnt
				, writer_id : user.id
				, writer_nick : user.nickname
				, writer_email : user.id
				, writer_ip : ip
				, writetime : now
			};
			
		//테이블 결정
		if(category === 'etc'){table_name = 'nansolo_piece_etc';}
		else if(category === 'travel'){
			table_name = 'nansolo_piece_travel';
			//console.log(data);
			insert_data.price = data.price||0;
			insert_data.itinerary = data.itinerary||0;
			
		}else if(category === 'food'){
			table_name = 'nansolo_piece_food';
			insert_data.price = data.price||0;
		}
		
		//대표 이미지 결정
		if(insert_data.images_JSON){
			insert_data.images_src = JSON.parse(insert_data.images_JSON)[0].src;
		}
		
		fall([
			//조각 입력 데이터
			function(next){
				db.insert({
					table: table_name
					, data : insert_data
					, callback : function(data){
						if(typeof callback === 'function'){ callback();}
						next(data.insertId);
					}
				});
			}
			//스토리 데이터 입력, 
			, function(next, insert_id){
				var images = (insert_data.images_JSON)?JSON.parse(insert_data.images_JSON):'';//이미지파일을 업로드 하기 위해
				insert_data.is_group = 'none';
				insert_data.article_no = insert_id;
				insert_data.category = category;
				
				db.insert({
					table : 'nansolo_story'
					, data : insert_data
					, callback : function(result){
						next({
							images : images
							, insert_data : insert_data
							, user : user
						});
					}
				});
				
			}
			//이미지 테이블 uploaded 수정
			, function(next, param){
				//console.log(param);
				if(!param.images.length){
					next(param);
					return false;
				}
				var where = 'WHERE ';
				for(var i = 0, len = param.images.length ; i < len ;i++){
					where += "no='"+param.images[i].no+"' ";
					where += (i==len-1)?"":' OR ';
				}
				//console.log(where)
				//where = where.slice(0,-3);
				db.update({
					table : 'TB_imageTable'
					, data : {uploaded:'uploaded'}
					, where : where
					, callback : function(res){
						next(param);
					}
				});
			}
			//그동안 안올라간 것들 파일삭제와 db의 이미지 테이블 삭제
			, function(next, param){
				//console.log('phase');
				db.select({
					table : 'TB_imageTable'
					, field : 'no, src'
					, where : " WHERE uploaded='not_yet' AND uploader_id='"+param.user.id+"' "
					, callback : function(data){
						//console.log(data);
						if(!data.length){return false;}
						var keys = [], src = '';
						for(var i =0, len =data.length; i < len ; i++){
							src = data[i].src.split('/');
							src.splice(0,4);
							keys[i] = {Key:src.join('/')};
							delete data[i].src;
						}
						//console.log(keys);
						//return false;
						db_file.s3_delete_objects(keys, function(data){
							//console.log(data);
						});
						
						db.delete({
							table :'TB_imageTable'
							, where :  " WHERE uploaded='not_yet' AND uploader_id='"+param.user.id+"' "
						});
					}
				});
			}
			
		]);
		
	};
	/*====================================
	 * 조각 작성.
	 ====================================*/
	
	DB_insert.prototype.setQuery = function(req, callback){
		var result = []
			, query = req.body.reqQuery
			, query_obj = req.body
			, now = new Date().format('yyyy-MM-dd hh:mm:ss')
			, unix_timestamp = TB_func.unix_timestamp()
			, auth
			;
		
		//insert 시 필요 없는 부분 삭제
		delete query_obj.reqType;		
		delete query_obj.reqQuery;
		
		switch(query){
			
			case 'aheivement_category'://업적 카테고리 입력
				db.insert({table : 'nansolo_achievement_category', data : req.body
					, callback : function(result){
						callback(result);
					}
				});
				return false;
				
				if(req.user.member_grade === 'supervisor'){
					
				}else{
					callback({'TB_dbMessage':'잘못된 접근입니다.'});
				}
			break;
			
			
			case 'insert_async_image'://비동기 이미지 업로드.
				//로그인 안했을 경우.
				if(!req.user){
					callback([{'TB_dbMessage':"로그인 후 작성해 주십시요."}]);
					return false;
				}

				//console.log(req.files[0]);
				db_file.uploadS3(req.files[0], {
					folder_url : 'peice_images'
					, autoCreateDate : true
					, resize : {
						maxWidth : 950
					}
					, callback : function(data){
						var insert_data = {};
						var gps = {};
						insert_data.originalname = req.files[0].originalname;
						insert_data.src = data.file_url;
						insert_data.size = req.files[0].size;
						insert_data.width = data.fileinfo.width;
						insert_data.height = data.fileinfo.height;
						insert_data.uploader_id = req.user.id;
						insert_data.uploadTime = now;
						
						
						if(data.fileinfo.properties){
							//gsp 정보 입력
							if(data.fileinfo.properties['exif:gpslatitude']){
		 						gps = db_file.convertDD(
									data.fileinfo.properties['exif:gpslatitude']
									, data.fileinfo.properties['exif:gpslatituderef']
									, data.fileinfo.properties['exif:gpslongitude']
									, data.fileinfo.properties['exif:gpslongituderef']
								);
								insert_data.lat = gps.lat;
								insert_data.lon = gps.lon;
							}
							//사진찍은 시간.
							if(data.fileinfo.properties['exif:datetime']){
								insert_data.pictured_time = data.fileinfo.properties['exif:datetime'];
							}
						}
						//TB_image_table 입력
						db.insert({
							table : 'TB_imageTable'
							, data : insert_data
							, callback : function(info){
								//console.log(info);
								insert_data.no = info.insertId;
								callback(insert_data);
							}
						});
					}
				});
			break;
			
			
			case 'insert_piece'://조각 업로드
				if(req.user){
					insert_piece(req.body, req.files, req.user, now, req.ip, function(data){
						callback([{'result':'success'}]);
					});
				}else{
					callback([{'TB_dbMessage':"로그인 후 작성해 주십시요."}]);
				}
			break;
			
			case 'directEl_text'://다이렉트 텍스트
			case 'directEl_element'://다이렉트 엘리먼트
				if(session.user_level != 'admin'){
					callback({'TB_dbMessage':"권한 오류 입니다.", "TB_dbAuthError" : true});
					return false;
				}
				if(query_obj.memo){
					if(query_obj.memo.indexOf("\\") >0){
						query_obj.memo = TB_func.removeBackslash(query_obj.memo);
					}
				}
				if(query_obj.style){
					if(query_obj.style.indexOf("\\") >0){
						query_obj.style = TB_func.removeBackslash(query_obj.style);
					}
				}
				if(query_obj.attr){
					if(query_obj.attr.indexOf("\\") >0){
						query_obj.attr = TB_func.removeBackslash(query_obj.attr);
					}
				}
				query_obj.writer = session.user_level;
				query_obj.writetime = now;
				query_obj.type = 'text';
				db.insert({table : 'TB_el', data : query_obj
					, callback : function(){
						callback(query_obj);
					}
				});
			break;
			
			case 'createAdmin'://홈페이지의 관리자 정보 생성
				req.body.member_grade = 'supervisor';
				joinUser('TB_member', req, function(data){
					var result = (data)?[{"result":"createAdmin"}]:[{'result':'failed'}];
					callback(result);
				});
			break;
			
			default://지정된 쿼리가 없을 경우.
				console.log('not defined query : '+query);
			break;
		}
	};
	/*====================================
	 * 쿼리 셋팅
	 ====================================*/
	
	return DB_insert;
})();

module.exports = DB_insert;
