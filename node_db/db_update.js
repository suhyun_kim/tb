var DB_update = (function(){
	var _db= require('./db')
			, MD5 = require('../TB/lib/js/plugin/md5')
			, TB_func=  require('../TB/lib/js/TB_func')
			, db_config = require('./db_config')
			, when = require('when')
			, fall = require('../node_lib/async_fall')
			, Db_file = require('./db_file')
			, db_file

			;
	
	function DB_update(mysql_connection_info){
		db = new _db(mysql_connection_info);
		db_file = new Db_file();
		//TB_func.init();
		//console.log(JSON.stringify(db));;
	}
	/*====================================
	 * 셀렉트
	 ====================================*/
	
	
	var modify_piece = function(data, files, user, now, ip, callback){
		var table_name = ''//db
			, category = data.category
			, modify_data = {//입력 데이터
				subject : data.subject
				, memo : data.memo
				, images_src : ''//대표 이미지
				, images_JSON : data.images_JSON||''
				, status : 'view'
				, stars_cnt : data.stars_cnt
				, modifier_id : user.id
				, modifier_nick : user.nickname
				, modifier_email : user.id
				, modifier_ip : ip
				, modifytime : now
			}
			, og_data = {}//원본이미지
			, images = (modify_data.images_JSON)?JSON.parse(modify_data.images_JSON):'';//이미지파일을 업로드 하기 위해
			;
		
		//테이블 결정
		if(category === 'etc'){table_name = 'nansolo_piece_etc';}
		else if(category === 'travel'){
			table_name = 'nansolo_piece_travel';
			modify_data.price = data.price||0;
			modify_data.itinerary = data.itinerary||0;
		}else if(category === 'food'){
			table_name = 'nansolo_piece_food';
			modify_data.price = data.price||0;
		}
		//대표 이미지 결정
		if(images){
			modify_data.images_src = (images.length)?images[0].src:'';
		}
		
		fall([
			//원본 데이터 가져오기
			function(next, param){
				db.select({
					table : 'nansolo_story'
					, where : "WHERE no='"+data.no+"'  "
					, callback : function(res){
						og_data = res[0];
						if(user.id != res[0].writer_id){
							callback([{"TB_dbMessage":"정상적인 접근이 아닙니다."}]);
							return false;
						}
						next();
					}
				});
			}
			//조각 데이터 업데이트
			, function(next, param){
				db.update({
					table: table_name
					, data : modify_data
					, where : "WHERE no='"+og_data.article_no+"'  "
					, callback : function(res){
						next();
					}
				});
			}
			//스토리 데이터 입력, 
			, function(next, param){
				modify_data.category = data.category;
				db.update({
					table : 'nansolo_story'
					, data : modify_data
					, where : "WHERE no='"+og_data.no+"' "
					, callback : function(result){
						if(typeof callback === 'function'){ callback([{"result":"success"}]);}
						next({
							images : images
							, modify_data : modify_data
							, user : user
						});
					}
				});
			}
			//삭제된 이미지들 이미지테이블에서 not_yet으로 변경하기.
			, function(next, param){
				var og_images = (og_data.images_JSON)?JSON.parse(og_data.images_JSON):''
					, update_query = "WHERE "
					, j= 0, len2 = images.length
					, k = 0
					;
					
				if(og_images){
					for(var i = 0, len = og_images.length ; i < len ; i++){
						for(; j < len2 ; j++){
							if(og_images[i].no == images[j].no){
								og_images[i].use = true;
							}
						}
					}
					console.log(og_images);
					for(var i =0; i < len ; i++){
						if(!og_images[i].use){
							if(k == 0){
								update_query += " no='"+og_images[i].no+"' ";
								k++;
							}else{
								k++;
								update_query += " OR no='"+og_images[i].no+"' ";
							}
						}
					}
					console.log(update_query);
					
					db.update({
						table : 'TB_imageTable'
						, data : {uploaded : 'not_yet'}
						, where : update_query
						, callback : function(res){
							next(param);
						}
					})
				}else{
					next(param);
				}
				
			}
			//업로드된 이미지들 테이블에서 uploaded 수정
			, function(next, param){
				console.log(param);
				if(!param.images.length){
					next(param);
					return false;
				}
				
				var where = 'WHERE ';
				
				for(var i = 0, len = param.images.length ; i < len ;i++){
					where += "no='"+param.images[i].no+"' ";
					where += (i==len-1)?"":' OR ';
				}
				//where = where.slice(0,-3);
				console.log(where);
				db.update({
					table : 'TB_imageTable'
					, data : {uploaded:'uploaded'}
					, where : where
					, callback : function(res){
						next(param);
					}
				});
			}
			//그동안 안올라간 것들 파일삭제와 db의 이미지 테이블 삭제
			, function(next, param){
				//console.log('phase');
				db.select({
					table : 'TB_imageTable'
					, field : 'no, src'
					, where : " WHERE uploaded='not_yet' AND uploader_id='"+param.user.id+"' "
					, callback : function(data){
						//console.log(data);
						if(!data.length){return false;}
						var keys = [], src = '';
						for(var i =0, len =data.length; i < len ; i++){
							src = data[i].src.split('/');
							src.splice(0,4);
							keys[i] = {Key:src.join('/')};
							delete data[i].src;
						}
						//console.log(keys);
						//return false;
						db_file.s3_delete_objects(keys, function(data){
							//console.log(data);
						});
						
						db.delete({
							table :'TB_imageTable'
							, where :  " WHERE uploaded='not_yet' AND uploader_id='"+param.user.id+"' "
						});
					}
				});
			}
			
		]);
		
	};
	/*====================================
	 * 조각 작성.
	 ====================================*/
	
	DB_update.prototype.setQuery = function(req, callback,  _passport, _res){
		var result = []
				, query_obj = req.body
				, query = (typeof query_obj == 'object')?query_obj.reqQuery:query_obj
				, now = new Date().format('yyyy-MM-dd hh:mm:ss')
				, updateData = {}
				, auth
				, res = _res
				;
		//console.log(query_obj);
		//insert 시 필요 없는 부분 삭제
		delete query_obj.reqType;		
		delete query_obj.reqQuery;
		
		switch(query){
			case 'achievement--category_modify'://분류 수정.
				updateData.category = req.body.category
				db.update({table : 'nansolo_achievement_category'
					, data : updateData 
					, where : "WHERE no='"+req.body.no+"' "
					, callback : function(result){
						if(typeof callback == 'function'){ callback(result);}
					}
				});
			break;
			
			case 'modify_piece'://조각 수정.
				if(!req.user){
					callback({'TB_redirect' : '/'});
				}
				modify_piece(req.body, req.files, req.user, now, req.ip, function(data){
					callback(data);
				});

			break;
			
			case 'gender_select':
				if(req.user){
					db.update({
						table : 'TB_member'
						, data : req.body
						, where : "WHERE id='"+req.user.id+"' AND nickname='"+req.user.nickname+"' "
						, callback : function(data){
							if(data.changedRows){
								console.log('asdf');
								callback([{result:'select_success'}]);
							}
						}
					});
				}else{
					callback([{'TB_dbMessage':'성별을 선택해 주세요..'}]);
				}
			break;
			
			case 'saveConfig'://홈페이지의 관리자 정보 생성
				if(req.user.member_grade != 'supervisor'){
					callback([{'TB_dbMessage':'올바른 접근이 아닙니다.'}]);
					return false;
				}
				when
				.promise(function(resolve, reject){
					updateData.menu = query_obj.menu
					db.select({table : 'TB_config', field : "COUNT(no) AS cnt"
						, callback : function(data){
							if(data[0].cnt <=0 ){//설정이 안만들어져 있을 경우 만든다.
								db.insert({table:"TB_config", data:updateData
									,callback: function(){
										callback([{"TB_dbMessage":"저장에 성공 하였습니다."}]);
									}
								});
							}else{
								db.update({table:"TB_config", data:updateData
									,callback: function(){
										callback([{"TB_dbMessage":"저장에 성공 하였습니다."}]);
									}
								});
							}
						}
					});
				})
			break;
			
			case 'directEl_text':
			case 'directEl_element'://동적 엘리먼트 수정
				if(session.user_level != 'admin'){
					callback({'TB_dbMessage':"권한 오류 입니다.", "TB_dbAuthError" : true});
					return false;
				}
				if(query_obj.memo){
					if(query_obj.memo.indexOf("\\") >0){
						query_obj.memo = TB_func.removeBackslash(query_obj.memo);
					}
				}
				if(query_obj.style){
					if(query_obj.style.indexOf("\\") >0){
						query_obj.style = TB_func.removeBackslash(query_obj.style);
					}
				}
				if(query_obj.attr){
					if(query_obj.attr.indexOf("\\") >0){
						query_obj.attr = TB_func.removeBackslash(query_obj.attr);
					}
				}
				query_obj.modifier = session.user_level;
				query_obj.modifytime = now;
				query_obj.type = 'text';
				db.update({table : 'TB_el', data : query_obj , where : "WHERE name='"+query_obj.name+"' "
					, callback : function(){
						if(typeof callback == 'function'){ callback(query_obj);}
					}
				});
			break;
			
			default://지정된 쿼리가 없을 경우.
				console.log('not defined update query : '+query);
			break;
		}
	};
	/*====================================
	 * 쿼리 셋팅
	 ====================================*/
	
	return DB_update;
})();

module.exports = DB_update;
