'use strict';
var Async_fall = function(_arr){
	var arrFunc
			, pointer = 0
			, len = 0
			, last= 0
			, param = undefined
			;
	
	
	var reset = function(){
		pointer = 0;
		len = 0;
		param = undefined;
	};
	
	
	var resolve = function(param){
		
		var last= len -1;
		if(last === pointer){//전체 순회 했을 경우
			reset(async_fall);
			return false;
		}
		param = param||undefined;
		pointer++;
		if(typeof arrFunc[pointer] === 'function'){
			arrFunc[pointer](resolve, param);
		}
	};
	/*==/다음 함수를 실행하기 위한 함수.==*/
	
	var init = function(_arr){
		arrFunc = _arr;
		reset();
		len = arrFunc.length;
		if(typeof arrFunc[pointer] === 'function'){
			arrFunc[pointer](resolve, param);
		}
	}
	/*============================================
	 * 생성자 함수
	 ============================================*/

	
	init(_arr);
	
};


module.exports = Async_fall;

































