'use strict';
var TB_bbs = function(_db, _TB_func, _async){
	var db
		, TB_func
		, async //비동기 함수를 폭포수 형태로 바꿔주는 것.
		;
	
	
	function TB_bbs(_db, _TB_func, _async){
		if(!_db || !_TB_func ||!_async){
			throw 'TB_bbs Error : 인자가 부족합니다.';
			return false;
		}
		//전역 변수 셋팅
		db = _db;
		TB_func = _TB_func;
		async = _async;
	}
	/*============================================
	 * 생성자 함수
	 ============================================*/
	
	TB_bbs.prototype.paging = function(_table, _pgn, _wehre,_callback){
		var pgn= {
					page : _pgn.page||1//볼려고 하는 페이지 수
					, page_num : _pgn.page_num||10//한화면에 출력되는 페이지 수
					, list_num : _pgn.list_num||10 //한 화면에 볼려는 리스트의 수
					, total_article : 0 //전체 글수
					, total_page : 0 //전체 페이지
					, start_num : 0 //현재 페이지 시작번호
					, last_num : 0 //현재페이지 끝번호
					, cur_num : 0 //현재 글 번호
					, total_block : 0 //전체 페이지 블럭
					, cur_block : 0 //현재 블럭
					, first_block : 0 //첫페이지 블럭
					, last_block :0 //마지막 블럭
					, next_block_page :0 //다음 블럭의 첫 페이지
					, prev_block_page : 0 //이전 블럭의 첫 페이지
					, page_link : [] //페이지 링크
					, link:0//페이지 링크 출력시 사용될 변수
				}
				, table = _table//테이블 필 수 
				, where  = _wehre||''
				, i = 0
				;
		//console.log(where);
			
		pgn = TB_func.merge(pgn, _pgn);
			
		if(!table){throw '테이블이 지정되어 있지 않습니다. ';return false;}
		
		async([
			//전체 글수 카운트 
			function(next, param){
				db.select({
					table : table
					, field : "COUNT (no) AS cnt"
					, where : where
					, callback : function(result){
						pgn.total_article = result[0].cnt;
						next();
						//console.log('asdf');
					}
				});
			}
			//나머지값 계산
			, function(next, param){
				pgn.total_page = Math.ceil(pgn.total_article/pgn.list_num);//전체 페이지수
				pgn.start_num =pgn.list_num*(pgn.page-1);
				pgn.last_num =pgn.list_num;
				pgn.cur_num = pgn.total_article - pgn.list_num*(pgn.page-1);//현제 글번호
				
				pgn.total_block = Math.ceil(pgn.total_page/pgn.page_num);
				pgn.cur_block = Math.ceil(pgn.page / pgn.page_num);
				pgn.first_block = (pgn.cur_block-1)*pgn.page_num;
				pgn.last_block = pgn.cur_block*pgn.page_num;
				
				pgn.next_block_page = (pgn.cur_block < pgn.total_block && pgn.total_block > 1)?pgn.last_block+1:0;
				pgn.prev_block_page = (pgn.cur_block > 1)?pgn.first_block:0; 
				
				for(pgn.link = pgn.first_block+1; pgn.link <= pgn.last_block ; pgn.link++ ){
					if(pgn.link <= pgn.total_page){
						pgn.page_link[i] = {
							link : pgn.link
						}
						i++;
					}
				}
				if(typeof _callback === 'function'){_callback(pgn);}
			}
		]);
		
		
	};
	/*==/BBS 타입 보기/==*/
	
	TB_bbs.prototype.get_bbs = function(_options){
		var bbs_data = {
					list : [] //JSON 배열
					, list_info : {} //리스트의 페이지 수등의 정보
				}
				, options = {
					type : _options.type||'normal' //normal, search, category, searchNcategory
					, page : _options.page||1
					, list_num : _options.list_num||10
					, page_num : _options.page_num||10
					, sorting_field : 'no'
					, sort : 'DESC'
				}
				, paging = this.paging
				, where = _options.where||''
				, table = _options.table||''
				, field = _options.field||'*'
				, callback  = _options.callback||undefined
		;
		
		if(!table){throw 'get_bbs Error not defined table';return false;}
		
		//normal
		async([
			//list_info 획득, where 절 완성
			function(next, param){
				//console.log(where);
				paging(
					table, options, where
					,function(pgn){
						//console.log(pgn);
						bbs_data.list_info = pgn;
						if(options.where){
							where += "WHERE "+options.where;
						}
						where += " ORDER BY "+options.sorting_field+" "+options.sort;
						where += " LIMIT "+bbs_data.list_info.start_num+" , "+bbs_data.list_info.last_num;
						next();
					}
				);
			}
			//list 출력
			, function(next, param){
				//console.log(where);
				db.select({
					table : table
					, field : field
					, where : where 
					, callback: function(data){
						bbs_data.list = data;
						if(typeof callback === 'function'){callback(bbs_data);}
					}
				});
			}
		]);
		
		
	};
	/*==/BBS 타입 보기/==*/
	
	/*============================================
	 * 공용 함수
	 ============================================*/
	
	var tb_bbs = new TB_bbs(_db, _TB_func, _async);
	/*============================================
	 * 인스턴스 생성
	 ============================================*/
	
	
	
	return tb_bbs;
};


module.exports = TB_bbs;

































