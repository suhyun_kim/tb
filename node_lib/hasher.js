'use strict';

var Hasher = function(pass, _salt, callback){
		var bkfd2Password= require('pbkdf2-password')
				, hasher = bkfd2Password()
				, opts = {
					password : pass
					, salt : _salt||undefined
				}
				;
		
		hasher(opts, function(err, password, salt, hash){
			if(err){ console.log('hasher Error \n'+err); }
			
			callback(hash, salt, password);
		})
	
};

module.exports = Hasher;